--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases
--





--
-- Drop roles
--

DROP ROLE postgres;


--
-- Roles
--

CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'md53175bce1d3201d16594cebf9d7eb3f9d';






--
-- Database creation
--

REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO postgres;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE django_site OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_site_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_site_id_seq OWNER TO postgres;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: main_reference; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE main_reference (
    id integer NOT NULL,
    generic_drug_code character varying(10) NOT NULL,
    item_iden bigint NOT NULL,
    item_description text,
    item_status character varying(10) NOT NULL,
    item_generic character varying(10) NOT NULL,
    item_scheduled character varying(50),
    items_package_size integer NOT NULL
);


ALTER TABLE main_reference OWNER TO postgres;

--
-- Name: main_reference_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE main_reference_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE main_reference_id_seq OWNER TO postgres;

--
-- Name: main_reference_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE main_reference_id_seq OWNED BY main_reference.id;


--
-- Name: main_refund; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE main_refund (
    id integer NOT NULL,
    "dateFilled" date,
    "Rxnumber" character varying(50) NOT NULL,
    "NDC" bigint NOT NULL,
    dispended_drug_description text,
    "Qty_dispended" double precision NOT NULL,
    "Primary_amt" double precision NOT NULL,
    "Second_amt" double precision NOT NULL,
    "Patient_copay" double precision NOT NULL,
    "Total_sales" double precision NOT NULL,
    "Acquistion_cost" double precision NOT NULL,
    "Profit" double precision NOT NULL,
    "Margin" double precision NOT NULL,
    "Dawcode" character varying(10) NOT NULL,
    "Drug_man" character varying(50) NOT NULL,
    "Acq_unit_cost" double precision NOT NULL
);


ALTER TABLE main_refund OWNER TO postgres;

--
-- Name: main_refund_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE main_refund_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE main_refund_id_seq OWNER TO postgres;

--
-- Name: main_refund_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE main_refund_id_seq OWNED BY main_refund.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: main_reference id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY main_reference ALTER COLUMN id SET DEFAULT nextval('main_reference_id_seq'::regclass);


--
-- Name: main_refund id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY main_refund ALTER COLUMN id SET DEFAULT nextval('main_refund_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can view permission	1	view_permission
5	Can add group	2	add_group
6	Can change group	2	change_group
7	Can delete group	2	delete_group
8	Can view group	2	view_group
9	Can add user	3	add_user
10	Can change user	3	change_user
11	Can delete user	3	delete_user
12	Can view user	3	view_user
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add site	6	add_site
22	Can change site	6	change_site
23	Can delete site	6	delete_site
24	Can view site	6	view_site
25	Can add log entry	7	add_logentry
26	Can change log entry	7	change_logentry
27	Can delete log entry	7	delete_logentry
28	Can view log entry	7	view_logentry
29	Can add refund	8	add_refund
30	Can change refund	8	change_refund
31	Can delete refund	8	delete_refund
32	Can view refund	8	view_refund
33	Can add reference	9	add_reference
34	Can change reference	9	change_reference
35	Can delete reference	9	delete_reference
36	Can view reference	9	view_reference
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	auth	permission
2	auth	group
3	auth	user
4	contenttypes	contenttype
5	sessions	session
6	sites	site
7	admin	logentry
8	main	refund
9	main	reference
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-12-13 07:03:58.933335+00
2	auth	0001_initial	2018-12-13 07:03:59.014036+00
3	admin	0001_initial	2018-12-13 07:03:59.040738+00
4	admin	0002_logentry_remove_auto_add	2018-12-13 07:03:59.05548+00
5	admin	0003_logentry_add_action_flag_choices	2018-12-13 07:03:59.069773+00
6	contenttypes	0002_remove_content_type_name	2018-12-13 07:03:59.095578+00
7	auth	0002_alter_permission_name_max_length	2018-12-13 07:03:59.107221+00
8	auth	0003_alter_user_email_max_length	2018-12-13 07:03:59.119725+00
9	auth	0004_alter_user_username_opts	2018-12-13 07:03:59.130148+00
10	auth	0005_alter_user_last_login_null	2018-12-13 07:03:59.142081+00
11	auth	0006_require_contenttypes_0002	2018-12-13 07:03:59.144883+00
12	auth	0007_alter_validators_add_error_messages	2018-12-13 07:03:59.157653+00
13	auth	0008_alter_user_username_max_length	2018-12-13 07:03:59.175084+00
14	auth	0009_alter_user_last_name_max_length	2018-12-13 07:03:59.187647+00
15	main	0001_initial	2018-12-13 07:03:59.207984+00
16	main	0002_auto_20181213_1003	2018-12-13 07:03:59.223835+00
17	sessions	0001_initial	2018-12-13 07:03:59.241661+00
18	sites	0001_initial	2018-12-13 07:03:59.25095+00
19	sites	0002_alter_domain_unique	2018-12-13 07:03:59.263922+00
20	main	0003_auto_20181213_1007	2018-12-13 07:07:46.925391+00
21	main	0004_auto_20181213_1008	2018-12-13 07:08:27.928707+00
22	main	0005_auto_20181213_1010	2018-12-13 07:10:48.659473+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Data for Name: main_reference; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY main_reference (id, generic_drug_code, item_iden, item_description, item_status, item_generic, item_scheduled, items_package_size) FROM stdin;
1	47524	135020001	Abreva 10% cream tube 2	otc	brand		2
2	36767	54014025	Acarbose 25 mg tablet bottle 100	rx	generic		100
3	6373	65702040710	Accu-chek aviva plus test strp box 50	otc	generic		50
4	70152	50924095120	Accu-chek safe-t-pro 23g lanct box 200	otc	generic		200
5	6373	65702049310	Accu-chek smartview test strip box 100	otc	generic		100
6	5145	378120001	Acebutolol 200 mg capsule bottle 100	rx	generic		100
7	4478	45802073033	Acetaminophen 650 mg suppos box 100	otc	generic		100
8	8165	51672402201	Acetazolamide 125 mg tablet bottle 100	rx	generic		100
9	8166	51672402301	Acetazolamide 250 mg tablet bottle 100	rx	generic		100
10	8101	60432074115	Acetic acid 2% ear solution bottle 15	rx	generic		15
11	592	63323069030	Acetylcysteine 20% vial vial 30	rx	generic		30
12	58644	536718001	Acidophilus caplet bottle 100	otc	generic		100
13	-126862	76394804293	Acidophilus pearls tabs 30 ea 30	otc	generic		30
14	-312917	4116709980	Act dry mouth lozenges 18's 18	otc	brand		18
15	-152710	4116709610	Act total cre fresh mint 18oz 18	otc	brand		18
16	-128631	8216120320	Active seal arm adl shrt 20320 1	otc	generic		1
17	15979	93894701	Acyclovir 800 mg tablet bottle 100	rx	generic		100
18	31789	45802045384	Adapalene 0.1% cream tube 45	rx	generic		45
19	43366	173069500	Advair 100-50 diskus UD blist pack 60	rx	brand		60
20	43367	173069600	Advair 250-50 diskus UD blist pack 60	rx	brand		60
21	43368	173069700	Advair 500-50 diskus UD blist pack 60	rx	brand		60
22	8039	11523115901	Afrin 0.05% nasal spray squeez btl 15	otc	brand		15
23	-409988	4110081123	Afrin nasal spr orig 15ml  1	otc	brand		1
24	40303	597000160	Aggrenox 25 mg-200 mg capsule UU bottle 60	rx	brand		60
25	5039	487950125	Albuterol sul 2.5 mg/3 ml soln UD vial 3	rx	generic		3
26	5039	487950103	Albuterol sul 2.5 mg/3 ml soln UD vial 3	rx	generic		3
27	5033	53489017601	Albuterol sulfate 2 mg tab bottle 100	rx	generic		100
28	-38744	61678411042	Alcohol prep pads med 1.25x2.5 1	otc	brand		1
29	47381	93517244	Alendronate sodium 35 mg tab UD UU blist pack 4	rx	generic		4
30	45052	47335095688	Alfuzosin hcl er 10 mg tablet bottle 100	rx	generic		100
31	63904	37000014343	Align 4 mg capsule blist pack 28	otc	brand		28
32	-326962	1650055426	Alka seltzer fruit chews 1	otc	brand		1
33	59732	16500004019	Alka-seltzer original tab eff bottle 12	otc	brand		12
34	33716	41167041251	Allegra allergy 180 mg tablet bottle 30	otc	brand		30
35	36868	41167431006	Allegra-d 12 hour tablet bottle 30	rx	brand		30
36	-193343	223076436	Allevyn hydrocellular foam dres 2x2 6600 1	rx	brand		1
37	2535	603211521	Allopurinol 100 mg tablet bottle 100	rx	generic		100
38	2535	603211532	Allopurinol 100 mg tablet bottle 1000	rx	generic		1000
39	2536	603211621	Allopurinol 300 mg tablet bottle 100	rx	generic		100
40	2536	603211628	Allopurinol 300 mg tablet bottle 500	rx	generic		500
41	-140485	76845510839	Aloe vesta 2n1 pneal skin liqd 8 oz 240	otc	brand		240
42	59668	23932110	Alphagan p 0.1% drops drop btl 10	rx	brand		10
43	59668	23932105	Alphagan p 0.1% drops drop btl 5	rx	brand		5
44	3773	781106101	Alprazolam 0.25 mg tablet bottle 100	rx	generic	C-4	100
45	3773	781106105	Alprazolam 0.25 mg tablet bottle 500	rx	generic	C-4	500
46	3774	781107705	Alprazolam 0.5 mg tablet bottle 500	rx	generic	C-4	500
47	3775	59762372101	Alprazolam 1 mg tablet bottle 100	rx	generic	C-4	100
48	67226	42806010624	Aluminum acetate astrngnt pwdr packet 12	otc	generic		12
49	4575	832201250	Amantadine 100 mg capsule bottle 500	rx	generic		500
50	27637	832011100	Amantadine 100 mg tablet bottle 100	rx	generic		100
51	266	68382022705	Amiodarone hcl 200 mg tablet bottle 500	rx	generic		500
52	266	68382022714	Amiodarone hcl 200 mg tablet bottle 60	rx	generic		60
53	60341	64764024060	Amitiza 24 mcg capsules bottle 60	rx	brand		60
54	63946	64764008060	Amitiza 8 mcg capsule bottle 60	rx	brand		60
55	46046	603221321	Amitriptyline hcl 25 mg tab bottle 100	rx	generic		100
56	46047	603221421	Amitriptyline hcl 50 mg tab bottle 100	rx	generic		100
57	46048	603221521	Amitriptyline hcl 75 mg tab bottle 100	rx	generic		100
58	5826	245002322	Amlactin 12% lotion bottle 225	otc	brand		225
59	5826	245002340	Amlactin 12% lotion bottle 400	otc	brand		400
60	16927	603211032	Amlodipine besylate 10 mg tab bottle 1000	rx	generic		1000
61	16927	603211002	Amlodipine besylate 10 mg tab bottle 90	rx	generic		90
62	16925	69097012615	Amlodipine besylate 2.5 mg tab bottle 1000	rx	generic		1000
63	16925	69097012605	Amlodipine besylate 2.5 mg tab bottle 90	rx	generic		90
64	16926	69097012715	Amlodipine besylate 5 mg tab bottle 1000	rx	generic		1000
65	16926	69097012705	Amlodipine besylate 5 mg tab bottle 90	rx	generic		90
66	50519	55111034101	Amlodipine-benazepril 10-20 mg bottle 100	rx	generic		100
67	23769	55111033901	Amlodipine-benazepril 5-10 mg bottle 100	rx	generic		100
68	62808	781563931	Amlodipine-valsartan 5-320 mg bottle 30	rx	generic		30
69	28191	45802049383	Ammonium lactate 12% cream tube 140	rx	generic		140
70	5826	45802041954	Ammonium lactate 12% lotion bottle 225	rx	generic		225
71	5826	45802041926	Ammonium lactate 12% lotion bottle 400	rx	generic		400
72	8990	60432006500	Amox-clav 250-62.5 mg/5 ml sus Orange bottle 100	rx	generic		100
73	25898	143998201	Amox-clav 400-57 mg/5 ml susp Fruit bottle 100	rx	generic		100
74	8992	93227434	Amox-clav 500-125 mg tablet bottle 20	rx	generic		20
75	24668	66685100101	Amox-clav 875-125 mg tablet bottle 100	rx	generic		100
302	-7418	3504600431	Co q 10 50mg 30	otc	generic		30
76	24668	781185220	Amox-clav 875-125 mg tablet bottle 20	rx	generic		20
77	8996	93310905	Amoxicillin 500 mg capsule bottle 500	rx	generic		500
78	30949	172524160	Anagrelide hcl 0.5 mg capsule bottle 100	rx	generic		100
79	71883	173086910	Anoro ellipta 62.5-25 mcg inh UD blist pack 60	rx	brand		60
80	6858	713050324	Anucort-hc 25 mg suppository box 24	rx	brand		24
81	-23277	47113060912	Aosept clear care soln 12 oz 360	otc	brand		360
82	9700	42023010401	Aplisol 5t units/0.1 ml vial vial 1	rx	brand		1
83	64701	65649010302	Apriso er 0.375 gram capsule bottle 120	rx	brand		120
84	-13116	7214063386	Aquaphor healing baby oint 14 oz 396	otc	brand		396
85	16307	72140003263	Aquaphor ointment jar 99	otc	brand		99
86	48911	55513002104	Aranesp 40 mcg/0.4 ml syringe syringe 0.4	rx	brand		0
87	48913	55513002304	Aranesp 60 mcg/0.3 ml syringe syringe 0.3	rx	brand		0
88	51333	13668021830	Aripiprazole 10 mg tablet bottle 30	rx	generic		30
89	51334	13668021930	Aripiprazole 15 mg tablet bottle 30	rx	generic		30
90	51335	13668022030	Aripiprazole 20 mg tablet bottle 30	rx	generic		30
91	51336	13668022130	Aripiprazole 30 mg tablet bottle 30	rx	generic		30
92	52898	13668021730	Aripiprazole 5 mg tablet bottle 30	rx	generic		30
93	-132895	33200185819	Arm & ham pure wht tooth whtnr 1	otc	generic		1
94	65421	456045701	Armour thyroid 15 mg tablet bottle 100	rx	brand		100
95	72723	173087610	Arnuity ellipta 200 mcg inh UD blist pack 30	rx	brand		30
96	7936	536108494	Artificial tears 1.4 % drops drop btl 15	otc	generic		15
97	63178	536108691	Artificial tears eye ointment tube 3.5	otc	generic		3
98	59327	85134107	Asmanex twisthaler 220 mcg #30 canister 1	rx	brand		1
99	59328	85134102	Asmanex twisthaler 220 mcg #60 canister 1	rx	brand		1
100	60115	41167005703	Aspercreme 10% cream tube 85	otc	brand		85
101	6139	41167005706	Aspercreme 10% lotion bottle 177.4	otc	brand		177
102	-392908	4116705820	Aspercreme lidocaine 2.7oz 1	otc	brand		1
103	-426325	4116705840	Aspercrm lido 5 pat -- pat --  5 aspercrm lido -- 5 1	otc	brand		1
104	4376	536330510	Aspirin 325 mg tablet bottle 1000	otc	generic		1000
105	4381	536331301	Aspirin ec 325 mg tablet bottle 100	otc	generic		100
106	16995	603002632	Aspirin ec 81 mg tablet bottle 1000	otc	generic		1000
107	40303	93304006	Aspirin-dipyridam er 25-200 mg UU bottle 60	rx	generic		60
108	5138	378075701	Atenolol 100 mg tablet bottle 100	rx	generic		100
109	420	591578201	Atenolol-chlorthalidone 50-25 bottle 100	rx	generic		100
110	29967	59762015501	Atorvastatin 10 mg tablet bottle 90	rx	generic		90
111	29968	60505257908	Atorvastatin 20 mg tablet bottle 1000	rx	generic		1000
112	29969	60505258009	Atorvastatin 40 mg tablet bottle 90	rx	generic		90
113	59081	597008717	Atrovent hfa inhaler aer w/adap 12.9	rx	brand		12
114	65354	8137003600	Aveeno daily moisturizing lot bottle 354	otc	brand		354
115	23869	225052547	Ayr saline nasal gel jar 14.1	otc	brand		14
116	57688	225052848	Ayr saline nasal gel spray bottle 22	otc	brand		22
117	11682	378100501	Azathioprine 50 mg tablet bottle 100	rx	generic		100
118	59121	68546022956	Azilect 1 mg tablet bottle 30	rx	brand		30
119	26721	59762306001	Azithromycin 250 mg tablet UD blist pack 6	rx	generic		6
120	74095	87651076009	Azo cranberry gummies bottle 40	otc	generic		40
121	69977	87651042067	Azo cranberry tablet blist pack 50	otc	generic		50
122	39498	65027510	Azopt 1% eye drops drop btl 10	rx	brand		10
123	63182	65597011330	Azor 10-40 mg tablet bottle 30	rx	brand		30
124	-13616	7431201471	B-12  5000mcg subl tabs 30 30	otc	brand		30
125	16248	74312003860	B-12 2-500 mcg tablet sl Cherry bottle 50	otc	generic		50
126	-257731	2739001162	Bacitracin oint .5oz tube dyna 1162 0.5	otc	brand		0
127	-217449	1678411631	Bacitracin oint 1oz tube 1163 29	otc	brand		29
128	-257732	2739001172	Bacitracin zinc .5oz tube dyna 1172 0.5	otc	brand		0
129	53623	168002135	Bacitracin-polymyxin ointment tube 14.17	otc	generic		14
130	4679	172409660	Baclofen 10 mg tablet bottle 100	rx	generic		100
131	4680	172409760	Baclofen 20 mg tablet bottle 100	rx	generic		100
132	40304	54007928	Balsalazide disodium 750 mg cp bottle 280	rx	generic		280
133	10523	8137005705	Band-aid adh band ex large n/a 10	otc	generic		10
134	23031	8137004431	Band-aid flex fab bandage box 30	otc	generic		30
135	23031	8137004434	Band-aid flex fab bandage n/a 100	otc	generic		100
136	23031	8137004666	Band-aid sheer strip box 40	otc	generic		40
137	-250648	74912320150	Batt duracell gnrc pkg aa 20  20	rx	brand		20
138	-9227	4133321601	Battery dur 9v mn1604 each 2 ea 2	otc	brand		2
139	-9197	4133310310	Battery dur lith 3v 2032bp each 1 ea 1	otc	generic		1
140	7366	11701004514	Baza antifungal 2% cream jar 142	otc	generic		142
141	7366	11701004523	Baza antifungal 2% cream jar 57	otc	generic		57
142	59980	11701004814	Baza clear ointment tube 142	otc	brand		142
143	16167	11701004614	Baza protect cream jar 142	otc	brand		142
144	49383	8290305932	Bd insulin syr 0.5 ml 29gx1/2" box 100	otc	generic		100
145	2846	8290236712	Bd lactinex packet 12	otc	brand		12
146	59051	8290305916	Bd safetyglide needle 25gx1" packet 50	otc	generic		50
147	49400	8290328466	Bd syringe 0.5 ml 12.7mmx30g box 100	otc	generic		100
148	49264	8290309657	Bd syringe 3 ml box 200	otc	generic		200
149	66308	8290320122	Bd ultra-fine pen ndl 4mmx32g box 100	otc	generic		100
150	60904	8290320119	Bd ultra-fine pen ndl 5mmx31g box 100	otc	generic		100
151	43640	41383006300	Beano tablet bottle 100	otc	brand		100
152	72691	6003330	Belsomra 10 mg tablet UD blist pack 30	rx	brand	C-4	30
153	16039	43547033510	Benazepril hcl 5 mg tablet bottle 100	rx	generic		100
154	21725	378474501	Benazepril-hctz 20-12.5 mg tab bottle 100	rx	generic		100
155	61801	74300000530	Bengay greaseless cream jar 57	otc	brand		57
156	59179	74300000535	Bengay ultra strength crm jar 57	otc	brand		57
157	52742	74300008149	Bengay ultra strength patch box 4	otc	brand		4
158	49565	74300000539	Bengay vanishing scent gel tube 57	otc	brand		57
159	4641	65162053650	Benzonatate 100 mg capsule bottle 500	rx	generic		500
160	7568	168005515	Betamethasone dp 0.05% crm tube 15	rx	generic		15
161	7569	168005615	Betamethasone dp 0.05% oint tube 15	rx	generic		15
162	7572	472037045	Betamethasone va 0.1% cream tube 45	rx	generic		45
163	7574	168004160	Betamethasone va 0.1% lotion bottle 60	rx	generic		60
164	24153	93022056	Bicalutamide 50 mg tablet bottle 30	rx	generic		30
165	47624	68180042901	Bimatoprost 0.03% eye drops drop btl 2.5	rx	generic		2
166	72275	48582000330	Biotene dry mouth oral rinse bottle 473	otc	brand		473
167	-128229	4858200155	Biotene moist mouth spr 1.5oz 44.3	otc	generic		44
168	62354	48582051201	Biotene oralbalance gel tube 42	otc	brand		42
169	16901	74312013430	Biotin 5-000 mcg softgel bottle 72	otc	generic		72
170	2944	536135512	Bisacodyl 10 mg suppository box 12	otc	generic		12
171	-18391	19134012704	Bismoline  mdct powd 7.25 oz 218	otc	generic		218
172	17956	29300012601	Bisoprolol fumarate 5 mg tab bottle 100	rx	generic		100
173	21140	29300018801	Bisoprolol-hctz 5-6.25 mg tab bottle 100	rx	generic		100
174	-32501	4167967566	Boost chclt btl liqd 4x6x8oz 4's 1440	otc	brand		1440
175	-32500	4167967466	Boost van btl liqd 4x6x8oz 4's 1440	otc	generic		1440
176	-374838	32878599821	Braun lens filt/ear thermometer lf40us01 1	otc	brand		1
177	26379	57145000242	Breathe right nasal strips box 30	otc	generic		30
178	70972	173085910	Breo ellipta 100-25 mcg inh UD blist pack 60	rx	brand		60
179	71815	173088210	Breo ellipta 200-25 mcg inh UD blist pack 60	rx	brand		60
180	66950	186077760	Brilinta 90 mg tablet bottle 60	rx	brand		60
181	27882	24208041105	Brimonidine 0.2% eye drop drop btl 5	rx	generic		5
182	48333	61314014410	Brimonidine tartrate 0.15% drp drop btl 10	rx	generic		10
183	48333	61314014405	Brimonidine tartrate 0.15% drp drop btl 5	rx	generic		5
184	61579	63402091164	Brovana 15 mcg/2 ml solution vial 2	rx	brand		2
185	46525	591376730	Budesonide 0.25 mg/2 ml susp UD ampul 2	rx	generic		2
186	46526	93681673	Budesonide 0.5 mg/2 ml susp UD vial 2	rx	generic		2
187	75590	60505612902	Budesonide 32 mcg nasal spray aer w/adap 8.43	rx	generic		8
188	25750	49884050101	Budesonide ec 3 mg capsule bottle 100	rx	generic		100
189	8221	185012801	Bumetanide 0.5 mg tablet bottle 100	rx	generic		100
190	8222	185012905	Bumetanide 1 mg tablet bottle 500	rx	generic		500
191	8223	185013001	Bumetanide 2 mg tablet bottle 100	rx	generic		100
192	46237	378043501	Bupropion hcl 100 mg tablet bottle 100	rx	generic		100
193	46239	591354060	Bupropion hcl sr 100 mg tablet bottle 60	rx	generic		60
194	46238	185041505	Bupropion hcl sr 150 mg tablet bottle 500	rx	generic		500
195	50496	591354260	Bupropion hcl sr 200 mg tablet bottle 60	rx	generic		60
196	53006	115681110	Bupropion hcl xl 150 mg tablet bottle 90	rx	generic		90
197	53007	45963014205	Bupropion hcl xl 300 mg tablet bottle 500	rx	generic		500
198	3781	591065801	Buspirone hcl 10 mg tablet bottle 100	rx	generic		100
199	27378	591071818	Buspirone hcl 15 mg tablet bottle 180	rx	generic		180
200	44210	93520006	Buspirone hcl 30 mg tablet bottle 60	rx	generic		60
201	-25928	70942000723	Butler gum orthdntc reg wax 1	otc	brand		1
202	-52192	70942122863	Butler soft  pickdisp        40 1	otc	brand		1
203	59589	59011075004	Butrans 5 mcg/hr patch box 4	rx	brand	C-3	4
204	63511	456141030	Bystolic 10 mg tablet bottle 30	rx	brand		30
205	63510	456140230	Bystolic 2.5 mg tablet bottle 30	rx	brand		30
206	25738	93542088	Cabergoline 0.5 mg tablet bottle 8	rx	generic		8
207	24138	60505082306	Calcitonin-salmon 200 units sp squeez btl 3.7	rx	generic		3
208	2184	64380072306	Calcitriol 0.25 mcg capsule bottle 100	rx	generic		100
209	58018	904323392	Calcium 600-vit d3 400 tablet bottle 150	otc	generic		150
210	69971	74312004233	Calcium 600-vit d3 800 tablet bottle 250	otc	generic		250
211	2689	35515090121	Calcium antacid 500 mg chw tab bottle 150	otc	generic		150
212	-7339	3504600161	Calcium chewable 500mg tablets 60	otc	generic		60
213	-7338	3504600157	Calcium magnesium 60	otc	generic		60
214	-27019	74312004290	Calcium/mag/zinc tabs 100 100	otc	brand		100
215	65841	799000104	Calmoseptine ointment tube 113	otc	brand		113
216	70165	5555619	Caltrate 600+d plus tablet bottle 60	otc	brand		60
217	-250643	30005556919	Caltrate 600+d sftchew van 60 wyc 60	rx	brand		60
218	-358542	55056720	Caltrate 600+d3+min chew 60 ea 60	otc	brand		60
219	-177428	1254607455	Candy gum trident wntrgr ct/12 cad 12	rx	brand		12
220	6017	536252525	Capsaicin 0.025% cream jar 60	otc	generic		60
221	378	143117401	Captopril 100 mg tablet bottle 100	rx	generic		100
222	380	143117201	Captopril 25 mg tablet bottle 100	rx	generic		100
223	58057	41167075142	Capzasin-hp 0.1% cream tube 42.5	otc	brand		42
224	57988	378505201	Carbidopa-levo 25-100 mg odt bottle 100	rx	generic		100
225	57989	62756018888	Carbidopa-levo 25-250 mg odt bottle 100	rx	generic		100
226	19563	62756046188	Carbidopa-levo er 25-100 tab bottle 100	rx	generic		100
227	16043	62756045788	Carbidopa-levo er 50-200 tab bottle 100	rx	generic		100
228	2537	62756051788	Carbidopa-levodopa 10-100 tab bottle 100	rx	generic		100
229	2538	228253910	Carbidopa-levodopa 25-100 tab bottle 100	rx	generic		100
230	52881	64679078404	Carbidopa-levodopa-enta 100 mg bottle 100	rx	generic		100
231	52880	64679078604	Carbidopa-levodopa-enta 150 mg bottle 100	rx	generic		100
232	63191	64679078704	Carbidopa-levodopa-enta 200 mg bottle 100	rx	generic		100
233	22233	68382009405	Carvedilol 12.5 mg tablet bottle 500	rx	generic		500
234	19293	68382009505	Carvedilol 25 mg tablet bottle 500	rx	generic		500
235	28108	68462016205	Carvedilol 3.125 mg tablet bottle 500	rx	generic		500
236	28109	68382009305	Carvedilol 6.25 mg tablet bottle 500	rx	generic		500
237	-201275	8099075000	Cath tray urethral 14fr 20cs 75000 1	otc	brand		1
238	40257	781217660	Cefdinir 300 mg capsule bottle 60	rx	generic		60
239	16932	781543920	Cefpodoxime 200 mg tablet bottle 20	rx	generic		20
240	9162	68180063310	Ceftriaxone 1 gm vial vial 1	rx	generic		1
241	9162	409733201	Ceftriaxone 1 gm vial vial 1	rx	generic		1
242	9137	68180030320	Cefuroxime axetil 500 mg tab bottle 20	rx	generic		20
243	41285	591398301	Celecoxib 100 mg capsule bottle 100	rx	generic		100
244	67887	5446119	Centrum silver chewable tablet Citrus berry bottle 60	otc	brand		60
245	-159271	54075650	Centrum silver women’s 50+ 100	otc	brand		100
246	-159424	54075850	Centrum silvr 100  100	otc	brand		100
247	67735	63824072016	Cepacol sore throat lozenge blist pack 16	otc	brand		16
248	9046	68180012402	Cephalexin 250 mg/5 ml susp Strawberry bottle 200	rx	generic		200
249	60699	187137316	Cerave moisturizing cream jar 453	otc	brand		453
250	60698	187137112	Cerave moisturizing lotion bottle 355	otc	brand		355
251	64732	536344238	Cerovite advanced form tab bottle 130	otc	generic		130
252	2279	536344308	Cerovite jr tablet chew Fruit bottle 60	otc	generic		60
253	58801	904548652	Certavite sr-antioxidant tab bottle 60	otc	generic		60
254	21687	45802097426	Cetirizine hcl 1 mg/ml syrup Grape bottle 118	rx	generic		118
255	17037	45802091987	Cetirizine hcl 10 mg tablet bottle 300	otc	generic		300
256	-250110	2270012705	Cg masc lshblst fsn 865 black  1	rx	brand		1
257	65042	51028000101	Chamosyn ointment tube 113	otc	brand		113
258	-2604	573190524	Chapstick chry blstr pk stck 24 ea 24's 1	otc	generic		1
259	-2608	573192024	Chapstick mdct stck 24 ea 24's 1	otc	generic		1
260	61709	67172110603	Chloraseptic sore throat lozng Cherry blist pack 18	otc	brand		18
261	3734	555003302	Chlordiazepoxide 10 mg capsule bottle 100	rx	generic	C-4	100
262	57959	116200116	Chlorhexidine 0.12% rinse Peppermint bottle 473	rx	generic		473
263	3800	832030200	Chlorpromazine 50 mg tablet bottle 100	rx	generic		100
264	4660	591252001	Chlorzoxazone 500 mg tablet bottle 100	rx	generic		100
265	13675	185094098	Cholestyramine packet Orange packet 60	rx	generic		60
266	-37877	7431200238	Chondroitin gluco cap #238 60	otc	brand		60
267	40971	68462029792	Ciclopirox 0.77% cream tube 90	rx	generic		90
268	37978	60505252201	Cilostazol 100 mg tablet bottle 60	rx	generic		60
269	-82656	74098527069	Cinnamon vgcp 500mg caps 120 ea 120	otc	generic		120
270	15861	61314065625	Ciprofloxacin 0.3% eye drop drop btl 2.5	rx	generic		2
271	9509	16571041110	Ciprofloxacin hcl 250 mg tab bottle 100	rx	generic		100
272	9510	16571041250	Ciprofloxacin hcl 500 mg tab bottle 500	rx	generic		500
273	46206	65862000505	Citalopram hbr 10 mg tablet bottle 500	rx	generic		500
274	46205	54006258	Citalopram hbr 10 mg/5 ml soln Peppermint bottle 240	rx	generic		240
275	46203	65862000605	Citalopram hbr 20 mg tablet bottle 500	rx	generic		500
276	68038	16500054099	Citracal + d er tablet bottle 80	otc	brand		80
277	-390011	1650056015	Citracal +d gummies 70 70	otc	brand		70
278	-203371	1650053505	Citracal caplet + d 120 120	otc	brand		120
279	65023	16500053503	Citracal-vit d 200 mg-250 tab bottle 200	otc	brand		200
280	62322	135008971	Citrucel powder jar 850	otc	brand		850
281	3050	135009070	Citrucel powder s-f jar 479	otc	brand		479
282	9340	591293201	Clindamycin hcl 300 mg capsule bottle 100	rx	generic		100
283	11752	59762374401	Clindamycin phosp 1% lotion bottle 60	rx	generic		60
284	7634	51672125801	Clobetasol 0.05% cream tube 15	rx	generic		15
285	7634	51672125802	Clobetasol 0.05% cream tube 30	rx	generic		30
286	7635	51672125906	Clobetasol 0.05% ointment tube 45	rx	generic		45
287	15349	472040250	Clobetasol 0.05% solution bottle 50	rx	generic		50
288	46121	406880701	Clomipramine 50 mg capsule bottle 100	rx	generic		100
289	4561	93083305	Clonazepam 1 mg tablet bottle 500	rx	generic	C-4	500
290	4562	93083401	Clonazepam 2 mg tablet bottle 100	rx	generic	C-4	100
291	343	591350804	Clonidine 0.1 mg/day patch box 4	rx	generic		4
292	345	591351004	Clonidine 0.3 mg/day patch box 4	rx	generic		4
293	38164	55111019605	Clopidogrel 75 mg tablet bottle 500	rx	generic		500
294	6999	51672200306	Clotrim 1% vaginal cream tube/kit 45	otc	generic		45
295	7361	45802043411	Clotrimazole 1% cream tube 30	otc	generic		30
296	9553	574010770	Clotrimazole 10 mg troche bottle 70	rx	generic		70
297	36534	168025815	Clotrimazole-betamethasone crm tube 15	rx	generic		15
298	36534	168025846	Clotrimazole-betamethasone crm tube 45	rx	generic		45
299	48627	51672130803	Clotrimazole-betamethasone lot bottle 30	rx	generic		30
300	13649	93777205	Clozapine 100 mg tablet bottle 500	rx	generic		500
301	46416	93440501	Clozapine 200 mg tablet bottle 100	rx	generic		100
303	-377063	8300026131	Coconut oil gelcaps 120 1	otc	brand		1
304	3013	67618010928	Colace clear 50 mg softgel bottle 28	otc	brand		28
305	8334	66993016502	Colchicine 0.6 mg tablet bottle 100	rx	generic		100
306	8334	64764011907	Colcrys 0.6 mg tablet bottle 30	rx	brand		30
307	53407	23921110	Combigan 0.2%-0.5% eye drops drop btl 10	rx	brand		10
308	53407	23921105	Combigan 0.2%-0.5% eye drops drop btl 5	rx	brand		5
309	69371	597002402	Combivent respimat inhal spray aer w/adap 4	rx	brand		4
310	-31648	4129805064	Commode spcm    dshp msr each 1 ea 1	otc	generic		1
311	3844	574722612	Compro 25 mg suppository box 12	rx	generic		12
312	6373	193731221	Contour next strips box 100	otc	generic		100
313	6373	193708050	Contour test strips box 50	otc	generic		50
314	24154	11017010140	Corn cushion packet 1	otc	generic		1
315	67160	55970082002	Cosamin asu capsule bottle 90	otc	brand		90
316	61877	17478060430	Cosopt pf eye drops drop btl 60	rx	brand		60
317	38686	6096031	Cozaar 100 mg tablet UU bottle 30	rx	brand		30
318	68829	57896084501	Cranberry 450 mg tablet bottle 100	otc	generic		100
319	-127772	74098522906	Cranberry extr vegcap 21st200 200	otc	generic		200
320	65329	32121207	Creon dr 12-000 units capsule UU bottle 250	rx	brand		250
321	65328	32120601	Creon dr 6-000 units capsule UU bottle 100	rx	brand		100
322	-456588	3700094623	Crest prohealth sens&enamel t-paste 7oz 1	otc	brand		1
323	-37968	3700000322	Crest reg extra large 00322 1	otc	brand		1
324	-8209	3700000391	Crest tart cont pste 6.4 oz 192	otc	brand		192
325	51784	310075190	Crestor 10 mg tablet bottle 90	rx	brand		90
326	51785	310075290	Crestor 20 mg tablet bottle 90	rx	brand		90
327	44694	17478029111	Cromolyn 4% eye drops drop btl 10	rx	generic		10
328	70952	49100036374	Culturelle hlth & wellness cap bottle 30	otc	brand		30
329	-134555	8019630000	Curad ouchls n/stk pad 2x320 20	otc	brand		20
330	-135124	8019630020	Curad telfa 10 10	otc	brand		10
331	-201609	8099007832	Curity iodoform pack strip 1/2"x5yd 7832 1	otc	brand		1
332	-247735	88941141065	Cystex liq cranberry 7.6oz 7.6	otc	brand		7
333	62651	53191036201	D3-50 50-000 units capsule bottle 100	otc	generic		100
334	2532	536354701	Daily-vite tablet bottle 100	otc	generic		100
335	2532	536354710	Daily-vite tablet bottle 1000	otc	generic		1000
336	66612	310009530	Daliresp 500 mcg tablet bottle 30	rx	brand		30
337	4667	115441101	Dantrolene sodium 25 mg cap bottle 100	rx	generic		100
338	58577	591438030	Darifenacin er 15 mg tablet bottle 30	rx	generic		30
339	8084	904386575	Deep sea 0.65% nose spray squeez btl 44	otc	generic		44
340	4630	63824017163	Delsym 30 mg/5 ml suspension Grape bottle 89	otc	brand		89
341	4630	63824017563	Delsym 30 mg/5 ml suspension Orange bottle 89	otc	brand		89
342	76143	23585318	Delzicol dr 400 mg capsule bottle 180	rx	brand		180
343	9213	555070102	Demeclocycline 150 mg tablet bottle 100	rx	generic		100
344	-297122	3600012779	Depend u/w men r/f l/x 12 cs4 12	otc	brand		12
345	-320364	3600038534	Depend u/w men s/ab lgxlg 17 4 17	otc	brand		17
346	-320362	3600038536	Depend u/w wmn lge 19ct cs4 19	otc	brand		19
347	-320368	3600038530	Depend u/w wmn s/ab s/m 19 cs4 19	otc	brand		19
348	-320363	3600038535	Depend u/w wmn sm/med 21 4 21	otc	brand		21
349	-320369	3600038529	Depend u/w wmn x/ab xlg 17 cs4 17	otc	brand		17
350	-320367	3600038531	Depend uwear wmn lge 17 cs4 17	otc	brand		17
351	-320366	3600038532	Depend uwear wmn xlg 15 cs4 15	otc	brand		15
352	-13783	7513785520	Dermoplast 20-0.5 arex 2.75 oz 83	otc	generic		83
353	47061	74300000301	Desitin 13% cream tube 113	otc	brand		113
354	47763	69543010710	Desloratadine 5 mg tablet bottle 100	rx	generic		100
355	19597	591246501	Desmopressin acetate 0.2 mg tb bottle 100	rx	generic		100
356	6780	54879000308	Dexamethasone 0.5 mg/5 ml elx Raspberry bottle 237	rx	generic		237
357	6788	54418325	Dexamethasone 2 mg tablet bottle 100	rx	generic		100
358	6789	54418425	Dexamethasone 4 mg tablet bottle 100	rx	generic		100
359	6778	63323016501	Dexamethasone 4 mg/ml vial vial 1	rx	generic		1
360	64793	64764017130	Dexilant dr 30 mg capsule bottle 30	rx	brand		30
361	64794	64764017530	Dexilant dr 60 mg capsule bottle 30	rx	brand		30
362	1989	409751716	Dextrose 50%-water syringe syringe 50	rx	generic		50
363	3768	378034505	Diazepam 5 mg tablet bottle 500	rx	generic	C-4	500
364	3764	54318863	Diazepam 5 mg/5 ml solution Wintergreen-spice bottle 500	rx	generic	C-4	500
365	18293	65162083366	Diclofenac sodium 1% gel tube 100	rx	generic		100
366	18293	49884093547	Diclofenac sodium 1% gel tube 100	rx	generic		100
367	8984	93312501	Dicloxacillin 500 mg capsule bottle 100	rx	generic		100
368	-28645	81506600116	Digestive advantage ibs cap 32	otc	brand		32
369	19	527132501	Digox 250 mcg tablet bottle 100	rx	generic		100
370	4521	71036924	Dilantin 100 mg capsule bottle 100	rx	brand		100
371	573	378052501	Diltiazem 120 mg tablet bottle 100	rx	generic		100
372	572	378609001	Diltiazem 12hr er 90 mg cap bottle 100	rx	generic		100
373	21282	49884082909	Diltiazem 24hr cd 120 mg cap bottle 90	rx	generic		90
374	16571	49884083109	Diltiazem 24hr cd 240 mg cap bottle 90	rx	generic		90
375	16572	49884083209	Diltiazem 24hr cd 300 mg cap bottle 90	rx	generic		90
376	21282	10370082905	Diltiazem 24hr er 120 mg cap bottle 500	rx	generic		500
377	21282	10370082909	Diltiazem 24hr er 120 mg cap bottle 90	rx	generic		90
378	575	378004501	Diltiazem 60 mg tablet bottle 100	rx	generic		100
379	576	378013501	Diltiazem 90 mg tablet bottle 100	rx	generic		100
380	16675	536077085	Diphenhist 12.5 mg/5 ml soln Cherry bottle 473	otc	generic		473
381	2841	378041501	Diphenoxylate-atrop 2.5-0.025 bottle 100	rx	generic	C-5	100
382	2841	59762106101	Diphenoxylate-atrop 2.5-0.025 bottle 100	rx	generic	C-5	100
383	4538	62756079688	Divalproex sod dr 125 mg tab bottle 100	rx	generic		100
384	4539	29300013905	Divalproex sod dr 250 mg tab bottle 500	rx	generic		500
385	4540	93744105	Divalproex sod dr 500 mg tab bottle 500	rx	generic		500
386	51469	55111053305	Divalproex sod er 250 mg tab bottle 500	rx	generic		500
387	4537	55111053205	Divalproex sodium 125 mg cap bottle 500	rx	generic		500
388	44360	51862012560	Dofetilide 125 mcg capsule bottle 60	rx	generic		60
389	44361	51862002560	Dofetilide 250 mcg capsule bottle 60	rx	generic		60
390	29334	43547027611	Donepezil hcl 10 mg tablet bottle 1000	rx	generic		1000
391	66533	55111030230	Donepezil hcl 23 mg tablet bottle 30	rx	generic		30
392	29335	43547027511	Donepezil hcl 5 mg tablet bottle 1000	rx	generic		1000
393	59039	68382034606	Donepezil hcl odt 5 mg tablet bottle 30	rx	generic		30
394	4773	59212042204	Donnatal elixir Mint bottle 120	rx	brand		120
395	23513	50383023210	Dorzolamide hcl 2% eye drops drop btl 10	rx	generic		10
396	23513	24208048510	Dorzolamide hcl 2% eye drops drop btl 10	rx	generic		10
397	39531	24208048610	Dorzolamide-timolol eye drops drop btl 10	rx	generic		10
398	-3504	1111112212	Dove bodywash snstv liqd 12 oz 360	otc	brand		360
399	-14654	7940050740	Dove inv sol snstv unscnt stck 2.6 oz 78	otc	brand		78
400	-3518	1111161120	Dove soap snstv soap 2pk 2	otc	generic		2
401	-82445	7940056600	Dove ultm clr cl essn stck 2.6 oz 74	otc	brand		74
402	15585	93206901	Doxazosin mesylate 2 mg tab bottle 100	rx	generic		100
403	15586	16729021301	Doxazosin mesylate 4 mg tab bottle 100	rx	generic		100
404	9219	69097022570	Doxycycline hyclate 50 mg cap bottle 50	rx	generic		50
405	-187431	3078020011	Drain bag 2000cc bard 802001 1	rx	brand		1
406	4693	591359360	Dronabinol 10 mg capsule bottle 60	rx	generic	C-3	60
407	66480	85720601	Dulera 100 mcg/5 mcg inhaler aer w/adap 13	rx	brand		13
408	66481	85461001	Dulera 200 mcg/5 mcg inhaler aer w/adap 13	rx	brand		13
409	57891	57237001760	Duloxetine hcl dr 20 mg cap bottle 60	rx	generic		60
410	57892	57237001830	Duloxetine hcl dr 30 mg cap bottle 30	rx	generic		30
411	57893	57237001930	Duloxetine hcl dr 60 mg cap bottle 30	rx	generic		30
412	-37980	4133310210	Duracell lithium 3-volts dl202 1	otc	generic		1
413	64354	65924007	Durezol 0.05% eye drops drop btl 5	rx	brand		5
414	51246	115143810	Dutasteride 0.5 mg capsule bottle 90	rx	generic		90
415	-21484	38056000020	Ear syringe 2oz syrn 1	otc	brand		1
416	-21485	38056000021	Ear syringe 3oz syrn 1	otc	generic		1
417	8120	536101294	Earwax treatment 6.5% drops drop btl 15	otc	generic		15
418	21900	49502020701	Easivent holding chamber box 1	rx	generic		1
419	7371	51672130301	Econazole nitrate 1% cream tube 15	rx	generic		15
420	63045	51801001330	Effer-k 10 meq tablet eff UD blist pack 30	rx	brand		30
421	63046	51801001230	Effer-k 20 meq tablet eff Orange cream UD blist pack 30	rx	brand		30
422	63046	51801001130	Effer-k 20 meq tablet eff UD blist pack 30	rx	brand		30
423	67642	3089321	Eliquis 2.5 mg tablet bottle 60	rx	brand		60
424	70414	3089421	Eliquis 5 mg tablet bottle 60	rx	brand		60
425	6373	94030201003	Embrace test strips box 50	otc	generic		50
426	384	64679092502	Enalapril maleate 10 mg tab bottle 100	rx	generic		100
427	386	64679092602	Enalapril maleate 20 mg tab bottle 100	rx	generic		100
428	387	64679092402	Enalapril maleate 5 mg tablet bottle 100	rx	generic		100
429	19331	955100310	Enoxaparin 30 mg/0.3 ml syr syringe 0.3	rx	generic		0
430	39482	955100410	Enoxaparin 40 mg/0.4 ml syr syringe 0.4	rx	generic		0
431	27993	955100610	Enoxaparin 60 mg/0.6 ml syr syringe 0.6	rx	generic		0
432	27994	955100810	Enoxaparin 80 mg/0.8 ml syr syringe 0.8	rx	generic		0
433	-25785	70074057233	Ensure chclt rtu btl liqd 4x6x8oz 4's 237	otc	brand		237
434	66745	70074040707	Ensure plus liquid bottle 237	otc	brand		237
435	-25789	70074057245	Ensure van rtu btl liqd 4x6x8oz 4's 237	otc	brand		237
436	41199	47335000788	Entacapone 200 mg tablet bottle 100	rx	generic		100
437	74408	78065920	Entresto 24 mg-26 mg tablet bottle 60	rx	brand		60
438	16879	54505010202	Epinephrine 0.3 mg auto-inject syringe 2	rx	generic		2
439	16878	49502050102	Epipen jr 2-pak 0.15 mg injctr syringe 2	rx	brand		2
440	51036	59762171002	Eplerenone 25 mg tablet bottle 30	rx	generic		30
441	51037	59762172002	Eplerenone 50 mg tablet bottle 90	rx	generic		90
442	9263	24338012213	Ery-tab ec 250 mg tablet bottle 100	rx	brand		100
443	7948	17478007035	Erythromycin 0.5% eye ointment tube 3.5	rx	generic		3
444	7948	24208091055	Erythromycin 0.5% eye ointment tube 3.5	rx	generic		3
445	50712	93585101	Escitalopram 10 mg tablet bottle 100	rx	generic		100
446	50760	93585201	Escitalopram 20 mg tablet bottle 100	rx	generic		100
447	51642	93585001	Escitalopram 5 mg tablet bottle 100	rx	generic		100
448	51698	54838055170	Escitalopram oxalate 5 mg/5 ml Peppermint bottle 240	rx	generic		240
449	47525	93645056	Esomeprazole mag dr 20 mg cap bottle 30	rx	generic		30
450	47526	93645156	Esomeprazole mag dr 40 mg cap bottle 30	rx	generic		30
451	47526	93645198	Esomeprazole mag dr 40 mg cap bottle 90	rx	generic		90
452	7011	430375414	Estrace 0.01% cream tube 42.5	rx	brand		42
453	20175	51672401801	Etodolac 400 mg tablet bottle 100	rx	generic		100
454	5321	72140011019	Eucerin original lotion bottle 250	otc	brand		250
455	-80386	7214011016	Eucerin plus intens repair lot 12's 500	otc	brand		500
456	62871	78050215	Exelon 9.5 mg/24hr patch box 30	rx	brand		30
457	44186	47781010830	Exemestane 25 mg tablet bottle 30	rx	generic		30
458	-38016	77602007669	Eyeglass repair kit   op 00766 1	otc	brand		1
459	-189445	8621611841	Falcon air duster 10oz 10	rx	brand		10
460	21995	591327330	Famciclovir 500 mg tablet bottle 30	rx	generic		30
461	11677	172572860	Famotidine 20 mg tablet bottle 100	rx	generic		100
462	11677	172572870	Famotidine 20 mg tablet bottle 500	rx	generic		500
463	44915	115552210	Fenofibrate 160 mg tablet bottle 90	rx	generic		90
464	61199	68382022816	Fenofibrate 48 mg tablet bottle 90	rx	generic		90
465	64310	115551110	Fenofibrate 54 mg tablet bottle 90	rx	generic		90
466	15883	49884076478	Fentanyl 100 mcg/hr patch box 5	rx	generic	C-2	5
467	59102	781724055	Fentanyl 12 mcg/hr patch box 5	rx	generic	C-2	5
468	59102	406901276	Fentanyl 12 mcg/hr patch box 5	rx	generic	C-2	5
469	15880	406902576	Fentanyl 25 mcg/hr patch box 5	rx	generic	C-2	5
470	15881	49884076278	Fentanyl 50 mcg/hr patch box 5	rx	generic	C-2	5
471	15881	406905076	Fentanyl 50 mcg/hr patch box 5	rx	generic	C-2	5
472	15882	406907576	Fentanyl 75 mcg/hr patch box 5	rx	generic	C-2	5
473	1627	51991020301	Ferrex 150 capsule bottle 100	otc	generic		100
474	39734	51991019811	Ferrex 150 forte capsule UD blist pack 100	rx	generic		100
475	1647	536347807	Ferrous sul 160 mg tab sa bottle 30	otc	generic		30
476	63508	54838000180	Ferrous sulf 220 mg/5 ml elix Lemon bottle 473	otc	generic		473
477	1645	603017932	Ferrous sulfate 325 mg tablet bottle 1000	otc	generic		1000
478	31689	45802042578	Fexofenadine hcl 60 mg tablet bottle 100	rx	generic		100
479	3065	536430605	Fiber-lax captabs bottle 500	otc	generic		500
480	41440	43598030390	Finasteride 5 mg tablet bottle 90	rx	generic		90
481	10462	8137004871	First aid cloth tape 1"x10yds box 1	otc	generic		1
482	10462	8137004881	First aid paper tape 1" box 1	otc	generic		1
483	-358700	65628020110	First vancomy 50mg kit 10oz 10	rx	brand		10
484	-358588	65628020005	First-vancomycin 25 25mgml posr 150 ml 150	rx	brand		150
485	-27831	76660074064	Fixodent extra-hold powd 2.7 oz 81	otc	brand		81
486	-27825	76660000866	Fixodent orig crm 2.4 oz 72	otc	brand		72
487	-19665	30768001138	Flax oil organic 1000mg gcap 100 100	otc	brand		100
488	265	54001025	Flecainide acetate 50 mg tab bottle 100	rx	generic		100
489	2996	132030140	Fleet mineral oil enema squeez btl 133	otc	brand		133
490	73300	135057603	Flonase allergy rlf 50 mcg spr aer w/adap 15.8	otc	brand		15
491	34893	66825000201	Florastor 250 mg capsule bottle 50	otc	brand		50
492	19317	173060202	Flovent 100 mcg diskus UD blist pack 60	rx	brand		60
493	21251	173071920	Flovent hfa 110 mcg inhaler aer w/adap 12	rx	brand		12
494	6812	115703301	Fludrocortisone 0.1 mg tablet bottle 100	rx	generic		100
495	7616	51672125302	Fluocinonide 0.05% cream tube 30	rx	generic		30
496	7617	51672126403	Fluocinonide 0.05% ointment jar 60	rx	generic		60
497	7618	168013460	Fluocinonide 0.05% solution bottle 60	rx	generic		60
498	7902	60758088005	Fluorometholone 0.1% drops drop btl 5	rx	generic		5
499	46213	50111064702	Fluoxetine hcl 10 mg capsule bottle 500	rx	generic		500
500	46214	50111064802	Fluoxetine hcl 20 mg capsule bottle 500	rx	generic		500
501	46215	63304063201	Fluoxetine hcl 40 mg capsule bottle 100	rx	generic		100
502	18368	54327099	Fluticasone prop 50 mcg spray aer w/adap 16	rx	generic		16
503	18368	50383070016	Fluticasone prop 50 mcg spray aer w/adap 16	rx	generic		16
504	63767	228284803	Fluvoxamine er 100 mg capsule bottle 30	rx	generic		30
505	46210	60505016601	Fluvoxamine maleate 100 mg tab bottle 100	rx	generic		100
506	46209	378041201	Fluvoxamine maleate 50 mg tab bottle 100	rx	generic		100
507	7901	23031604	Fml s.o.p. 0.1% ointment tube 3.5	rx	brand		3
508	54655	51991038490	Folbic tablet bottle 90	rx	generic		90
509	2365	536384301	Folic acid 0.8 mg tablet bottle 100	otc	generic		100
510	66041	51991081390	Foltanx tablet bottle 90	otc	generic		90
511	24138	245000835	Fortical 200 units nasal spray squeez btl 3.7	rx	brand		3
512	16018	31722020190	Fosinopril sodium 20 mg tab bottle 90	rx	generic		90
513	60096	54092025490	Fosrenol 1-000 mg tablet chew bottle 90	rx	brand		90
514	62136	99073014002	Freestyle control solution vial 1	otc	generic		1
515	6373	99073070827	Freestyle lite test strip box 100	otc	generic		100
516	6373	99073012101	Freestyle test strips box 100	otc	generic		100
517	8208	603373932	Furosemide 20 mg tablet bottle 1000	rx	generic		1000
518	8209	603374021	Furosemide 40 mg tablet bottle 100	rx	generic		100
519	8209	603374032	Furosemide 40 mg tablet bottle 1000	rx	generic		1000
520	8210	378023205	Furosemide 80 mg tablet bottle 500	rx	generic		500
521	-293705	5113120163	Fut hose a-em lg wh kn 71057en 1	otc	brand		1
522	21414	45963055650	Gabapentin 300 mg capsule bottle 500	rx	generic		500
523	58238	10147089103	Galantamine er 8 mg capsule bottle 30	rx	generic		30
524	46925	10147088306	Galantamine hbr 12 mg tablet bottle 60	rx	generic		60
525	46926	10147088106	Galantamine hbr 4 mg tablet bottle 60	rx	generic		60
526	46927	10147088206	Galantamine hbr 8 mg tablet bottle 60	rx	generic		60
527	2819	536102008	Gas relief 125 mg chew tablet Spearmint bottle 60	otc	generic		60
528	-91863	3112	Gauze conforming 2 strl   dyn 12	otc	generic		12
529	-91861	3114	Gauze conforming 4 strl   dyn 12	otc	generic		12
530	22879	8137008524	Gauze pads 4" x 4" box 10	otc	generic		10
531	-188522	44444053501	Gauze sponge 4x4 8ply ns 200bag 441217 1	rx	brand		1
532	41843	43386031214	Gavilax powder jar 510	otc	brand		510
533	6416	69097082112	Gemfibrozil 600 mg tablet bottle 500	rx	generic		500
534	7983	17478028435	Gentak 0.3 % eye ointment tube 3.5	rx	brand		3
535	7724	45802005635	Gentamicin 0.1% cream tube 15	rx	generic		15
536	7725	45802004635	Gentamicin 0.1% ointment tube 15	rx	generic		15
537	7984	60758018805	Gentamicin 3 mg/ml eye drops drop btl 5	rx	generic		5
538	19382	78051824	Genteal mild-moderate eye drop drop btl 15	otc	brand		15
539	-412325	30065042636	Genteal tears moderate 15ml  1	otc	brand		1
540	25179	16729000101	Glimepiride 1 mg tablet bottle 100	rx	generic		100
541	25180	16729000201	Glimepiride 2 mg tablet bottle 100	rx	generic		100
542	25181	16729000301	Glimepiride 4 mg tablet bottle 100	rx	generic		100
543	1776	781145301	Glipizide 10 mg tablet bottle 100	rx	generic		100
544	21839	591084501	Glipizide er 10 mg tablet bottle 100	rx	generic		100
545	21840	591084401	Glipizide er 5 mg tablet bottle 100	rx	generic		100
546	41661	2803101	Glucagon 1 mg emergency kit kit 1	rx	brand		1
547	-236036	7007454328	Glucerna shake inst van 8oz 1	otc	brand		1
548	-26689	74098522301	Gluco/chond max strength 60	otc	generic		60
549	-7459	3504604663	Glucoflex glucosamine & msm 60	otc	generic		60
550	-204723	74098527290	Glucosam chond pl ms m ts tabs 60 ea 60	otc	brand		60
551	22127	904529352	Glucosamine 500 mg capsule bottle 60	otc	generic		60
552	1781	574006945	Glutose 45 gel Lemon tube 112.5	otc	brand		112
553	1774	93834301	Glyburide 2.5 mg tablet bottle 100	rx	generic		100
554	1775	93834401	Glyburide 5 mg tablet bottle 100	rx	generic		100
555	4887	69076047501	Glycopyrrolate 1 mg tablet bottle 100	rx	generic		100
556	4888	49884006601	Glycopyrrolate 2 mg tablet bottle 100	rx	generic		100
557	761	904515460	Guaifenesin 200 mg tablet bottle 100	otc	generic		100
558	364	378116001	Guanfacine 1 mg tablet bottle 100	rx	generic		100
559	-17893	12546062182	Halls c/dr chry lozg 30 30	otc	brand		30
560	-17892	12546062181	Halls c/dr menth lozg 30 30	otc	generic		30
561	-20222	31254662542	Halls c/dr sf bchry lozg 25 25	otc	generic		25
562	-20215	31254662213	Halls c/dr sf honey lemon lozg 25 ea 25	otc	brand		25
563	3972	378035101	Haloperidol 0.5 mg tablet bottle 100	rx	generic		100
564	3973	378025701	Haloperidol 1 mg tablet bottle 100	rx	generic		100
565	3974	68382008001	Haloperidol 10 mg tablet bottle 100	rx	generic		100
566	3976	68382008101	Haloperidol 20 mg tablet bottle 100	rx	generic		100
567	3977	378032701	Haloperidol 5 mg tablet bottle 100	rx	generic		100
568	6549	63323026201	Heparin sod 5-000 unit/ml vial vial 1	rx	generic		1
569	-80575	3400024000	Hershey plain candy bx36 12's 1	otc	generic		1
570	7254	234057504	Hibiclens 4% liquid bottle 118	otc	generic		118
571	7254	234057508	Hibiclens 4% liquid bottle 236	otc	generic		236
572	-200221	61007514604	Hol14604 nw imge flt sk br ctf 1	otc	brand		1
573	-50191	8380507905	Holl 7905 karaya pow  2.5oz 1	otc	generic		1
574	-7630	3566414622	Hose jbst rlf20-30kh cltlrg hose 2 ea 2	otc	brand		2
575	-7629	3566414621	Hose jbst rlf20-30kh cltmed hose 2 ea 2	otc	brand		2
576	34731	2879959	Humalog 100 units/ml kwikpen syringe 3	rx	brand		3
577	27413	2751001	Humalog 100 units/ml vial vial 10	rx	brand		10
578	42076	2879759	Humalog mix 75-25 kwikpen syringe 3	rx	brand		3
579	16311	2871501	Humulin 70-30 vial vial 10	otc	brand		10
580	58952	2880359	Humulin 70/30 kwikpen syringe 3	otc	brand		3
581	51292	2880559	Humulin n 100 units/ml kwikpen syringe 3	otc	brand		3
582	1740	2831501	Humulin n 100 units/ml vial vial 10	otc	brand		10
583	286	50111032703	Hydralazine 25 mg tablet bottle 1000	rx	generic		1000
584	-473317	39277160001	Hydrocerin cr 16oz geritrex 16	otc	brand		16
585	5349	54162060002	Hydrocerin cream jar 113	otc	generic		113
586	18767	54162062016	Hydrocerin lotion bottle 472	otc	generic		472
587	29832	29300013010	Hydrochlorothiazide 12.5 mg cp bottle 1000	rx	generic		1000
588	8183	172208960	Hydrochlorothiazide 50 mg tab bottle 100	rx	generic		100
589	47431	13107002001	Hydrocodon-acetaminoph 7.5-325 bottle 100	rx	generic	C-2	100
590	47431	406012401	Hydrocodon-acetaminoph 7.5-325 bottle 100	rx	generic	C-2	100
591	47430	53746010901	Hydrocodon-acetaminophen 5-325 bottle 100	rx	generic	C-2	100
592	47430	406012301	Hydrocodon-acetaminophen 5-325 bottle 100	rx	generic	C-2	100
593	30623	53746011001	Hydrocodon-acetaminophn 10-325 bottle 100	rx	generic	C-2	100
594	7543	168001431	Hydrocortisone 0.5% cream tube 28.35	otc	generic		28
595	7544	45802043803	Hydrocortisone 1% cream tube 28	rx	generic		28
596	7544	168001531	Hydrocortisone 1% cream tube 28.35	rx	generic		28
597	6703	603390021	Hydrocortisone 10 mg tablet bottle 100	rx	generic		100
598	7545	45802000403	Hydrocortisone 2.5% cream tube 28	rx	generic		28
599	7545	168008031	Hydrocortisone 2.5% cream tube 30	rx	generic		30
600	-189351	8164243805	Hydrocortisone cr 1% 16oz cp 454	rx	brand		454
601	7532	51672129006	Hydrocortisone val 0.2% cream tube 45	rx	generic		45
602	4110	527135301	Hydromorphone 2 mg tablet bottle 100	rx	generic	C-2	100
603	16156	42858030416	Hydromorphone 5 mg/5 ml soln Sweet bottle 473	rx	generic	C-2	473
604	4865	43199001301	Hyoscyamine sulf 0.125 mg tab bottle 100	rx	generic		100
605	61112	536509008	I-vite tablet bottle 60	otc	generic		60
832	-326077	7431271950	N/b b-12 methylc loz 1mmcg 60 60	otc	brand		60
606	58915	55111057503	Ibandronate sodium 150 mg tab UD blist pack 3	rx	generic		3
607	8348	55111068205	Ibuprofen 400 mg tablet bottle 500	rx	generic		500
608	59312	65804053	Icaps tablet bottle 60	otc	brand		60
609	52742	41167000843	Icy hot medicated 5% patch box 5	otc	brand		5
610	-80355	41167008478	Icy hot medicated patch xl 24's 1	otc	brand		1
611	-80356	41167009200	Icy hot vanishing scent gel 12's 1	otc	brand		1
612	70358	65175014	Ilevro 0.3% ophth drops drop btl 3	rx	brand		3
613	72375	173087310	Incruse ellipta 62.5 mcg inh UD blist pack 30	rx	brand		30
614	65452	50458056401	Invega sustenna 234 mg/1.5 ml syringe 1.5	rx	brand		1
615	70791	50458014030	Invokana 100 mg tablet bottle 30	rx	brand		30
616	48604	8213010040	Iodosorb gel jar 40	rx	brand		40
617	48018	487020160	Iprat-albut 0.5-3(2.5) mg/3 ml UD vial 3	rx	generic		3
618	24457	24208039830	Ipratropium 0.03% spray canister 30	rx	generic		30
619	24456	24208039915	Ipratropium 0.06% spray canister 15	rx	generic		15
620	21700	487980125	Ipratropium br 0.02% soln UD vial 2.5	rx	generic		2
621	34468	43547027809	Irbesartan 150 mg tablet bottle 90	rx	generic		90
622	34469	43547027909	Irbesartan 300 mg tablet bottle 90	rx	generic		90
623	34470	43547027703	Irbesartan 75 mg tablet bottle 30	rx	generic		30
624	41897	93823256	Irbesartan-hctz 300-12.5 mg tb bottle 30	rx	generic		30
625	509	49884000901	Isosorbide dn 30 mg tablet bottle 100	rx	generic		100
626	17294	228263111	Isosorbide mn 10 mg tablet bottle 100	rx	generic		100
627	23474	62175012937	Isosorbide mn er 120 mg tab bottle 100	rx	generic		100
628	24488	62175012837	Isosorbide mn er 30 mg tablet bottle 100	rx	generic		100
629	17297	62175011937	Isosorbide mn er 60 mg tablet bottle 100	rx	generic		100
630	14198	832121110	Jantoven 1 mg tablet bottle 1000	rx	generic		1000
631	6561	832121210	Jantoven 2 mg tablet bottle 1000	rx	generic		1000
632	6560	832121310	Jantoven 2.5 mg tablet bottle 1000	rx	generic		1000
633	18080	832121410	Jantoven 3 mg tablet bottle 1000	rx	generic		1000
634	6563	832121800	Jantoven 7.5 mg tablet bottle 100	rx	generic		100
635	61614	6027754	Januvia 100 mg tablet bottle 90	rx	brand		90
636	61612	6022131	Januvia 25 mg tablet bottle 30	rx	brand		30
637	61613	6011254	Januvia 50 mg tablet bottle 90	rx	brand		90
638	68518	597014860	Jentadueto 2.5 mg-1000 mg tab bottle 60	rx	brand		60
639	-21706	38137004708	Jj b/a clear spots ptch 50 ea 50	otc	brand		50
640	-367026	38137116147	Jj bandaid fa mirasorb gauze 4 x 4" 50 1	otc	brand		1
641	-37909	3566421501	Jobst 121501 20-30 md s/bei u- 1	otc	brand		1
642	39564	8137003058	Johnson's baby corn starch jar 450	otc	brand		450
643	6167	8137003718	Johnson's baby shampoo bottle 210	otc	brand		210
644	6167	8137003712	Johnson's baby shampoo bottle 45	otc	brand		45
645	-140489	85707400138	Jointflex pain relv crm 4 oz 120	otc	brand		120
646	65956	59781058012	Juven packet Orange packet 30	otc	brand		30
647	-203055	76845510225	Kaltostat calcium dress 2x2 10 168210 1	otc	brand		1
648	-20981	35167270012	Kerasal mstrz oint 30 gm 30	otc	generic		30
649	-12239	7061044012	Kerr ppsc-1220 rxcp 200 ea 200	otc	brand		200
650	-12208	7061021612	Kerr vial 12dr scp ambr rxvl 300 ea 300	otc	brand		300
651	-12207	7061021608	Kerr vial 8dr scp amber rxvl 450 ea 450	otc	brand		450
652	15568	10147075004	Ketoconazole 2% shampoo bottle 120	rx	generic		120
653	52960	60758077305	Ketorolac 0.4% ophth solution drop btl 5	rx	generic		5
654	43118	17478071710	Ketotifen fum 0.025% eye drops drop btl 5	otc	generic		5
655	22345	66758017010	Klor-con m10 tablet bottle 1000	rx	brand		1000
656	69532	40985027367	Krill oil 300 mg softgel bottle 60	otc	generic		60
657	5098	43199003701	Labetalol hcl 100 mg tablet bottle 100	rx	generic		100
658	5099	43199003801	Labetalol hcl 200 mg tablet bottle 100	rx	generic		100
659	2799	536408706	Lac-dose 3-000 unit captab bottle 50	otc	generic		50
660	64483	10631028605	Lac-hydrin five 5% lotion bottle 226	otc	brand		226
661	28289	45091060	Lactaid fast act 9-000 units box 60	otc	brand		60
662	41655	45093060	Lactaid fast act 9-000 units box 60	otc	brand		60
663	3143	50383079516	Lactulose 10 gm/15 ml solution Vanilla bottle 473	rx	generic		473
664	7336	67400546	Lamisil af defens 1% spray pwd canister 133	otc	brand		133
665	17871	93046305	Lamotrigine 100 mg tablet bottle 500	rx	generic		500
666	17872	51672413001	Lamotrigine 25 mg tablet bottle 100	rx	generic		100
667	30106	55111039890	Lansoprazole dr 15 mg capsule bottle 90	rx	generic		90
668	30107	45963046110	Lansoprazole dr 30 mg capsule bottle 100	rx	generic		100
669	47780	88222033	Lantus 100 units/ml vial vial 10	rx	brand		10
670	62867	88221905	Lantus solostar 100 units/ml syringe 3	rx	brand		3
671	27370	17478062512	Latanoprost 0.005% eye drops drop btl 2.5	rx	generic		2
672	69894	63402031230	Latuda 120 mg tablet bottle 30	rx	brand		30
673	68448	63402030230	Latuda 20 mg tablet bottle 30	rx	brand		30
674	66933	63402030830	Latuda 80 mg tablet bottle 30	rx	brand		30
675	40550	955173730	Leflunomide 20 mg tablet bottle 30	rx	generic		30
676	-78203	8012150103	Leg bag 32oz lgestaps 1	otc	generic		1
677	-201636	8012150507	Leg bag strap fabric 24" pr 150507 1	otc	brand		1
678	29821	16729003410	Letrozole 2.5 mg tablet bottle 30	rx	generic		30
679	41848	66993002227	Levalbuterol 0.63 mg/3 ml sol vial 3	rx	generic		3
680	41849	378968244	Levalbuterol 1.25 mg/3 ml sol UD vial 3	rx	generic		3
681	59586	169368712	Levemir 100 units/ml vial vial 10	rx	brand		10
682	57439	169643810	Levemir flextouch 100 units/ml syringe 3	rx	brand		3
683	47077	68180011507	Levetiracetam 1-000 mg tablet bottle 60	rx	generic		60
684	53031	51991065116	Levetiracetam 100 mg/ml soln Grape bottle 473	rx	generic		473
685	44632	68180011216	Levetiracetam 250 mg tablet bottle 120	rx	generic		120
686	44633	68180011302	Levetiracetam 500 mg tablet bottle 500	rx	generic		500
687	45652	68180011416	Levetiracetam 750 mg tablet bottle 120	rx	generic		120
688	29927	65862053650	Levofloxacin 250 mg tablet bottle 50	rx	generic		50
689	29928	33342002208	Levofloxacin 500 mg tablet bottle 50	rx	generic		50
690	46771	33342002332	Levofloxacin 750 mg tablet bottle 20	rx	generic		20
691	6651	378180910	Levothyroxine 100 mcg tablet bottle 1000	rx	generic		1000
692	6652	378181110	Levothyroxine 112 mcg tablet bottle 1000	rx	generic		1000
693	6653	378181310	Levothyroxine 125 mcg tablet bottle 1000	rx	generic		1000
694	20176	378182377	Levothyroxine 137 mcg tablet bottle 90	rx	generic		90
695	6654	378181510	Levothyroxine 150 mcg tablet bottle 1000	rx	generic		1000
696	6655	378181777	Levothyroxine 175 mcg tablet bottle 90	rx	generic		90
697	6648	378180010	Levothyroxine 25 mcg tablet bottle 1000	rx	generic		1000
698	6648	378180077	Levothyroxine 25 mcg tablet bottle 90	rx	generic		90
699	6649	378180310	Levothyroxine 50 mcg tablet bottle 1000	rx	generic		1000
700	6650	378180510	Levothyroxine 75 mcg tablet bottle 1000	rx	generic		1000
701	15523	378180710	Levothyroxine 88 mcg tablet bottle 1000	rx	generic		1000
702	62058	54092047612	Lialda dr 1.2 gm tablet bottle 120	rx	brand		120
703	40262	43199004030	Lidocaine 4% cream tube 30	otc	generic		30
704	43256	603188016	Lidocaine 5% patch box 30	rx	generic		30
705	3407	591301230	Lidocaine hcl 2% jelly tube 30	rx	generic		30
706	3412	54350547	Lidocaine hcl 4% solution bottle 50	rx	generic		50
707	35495	50383066730	Lidocaine-prilocaine cream tube 30	rx	generic		30
708	69922	456120130	Linzess 145 mcg capsule UU bottle 30	rx	brand		30
709	69923	456120230	Linzess 290 mcg capsule UU bottle 30	rx	brand		30
710	390	68180051403	Lisinopril 10 mg tablet bottle 1000	rx	generic		1000
711	17266	68180051202	Lisinopril 2.5 mg tablet bottle 500	rx	generic		500
712	391	603421221	Lisinopril 20 mg tablet bottle 100	rx	generic		100
713	391	603421232	Lisinopril 20 mg tablet bottle 1000	rx	generic		1000
714	41567	68180051601	Lisinopril 30 mg tablet bottle 100	rx	generic		100
715	392	68180051703	Lisinopril 40 mg tablet bottle 1000	rx	generic		1000
716	393	68180051303	Lisinopril 5 mg tablet bottle 1000	rx	generic		1000
717	21277	68180051801	Lisinopril-hctz 10-12.5 mg tab bottle 100	rx	generic		100
718	388	68180051901	Lisinopril-hctz 20-12.5 mg tab bottle 100	rx	generic		100
719	5693	501100073	Listerine antiseptic bottle 250	otc	brand		250
720	5693	501100172	Listerine antiseptic bottle 500	otc	brand		500
721	-249171	31254742833	Listerine zero clmt mwsh 1000 ml 1000	otc	brand		1000
722	4000	54252625	Lithium carbonate 150 mg cap bottle 100	rx	generic		100
723	4001	54252725	Lithium carbonate 300 mg cap bottle 100	rx	generic		100
724	4003	54452725	Lithium carbonate 300 mg tab bottle 100	rx	generic		100
725	4005	68462022401	Lithium carbonate er 450 mg tb bottle 100	rx	generic		100
726	2843	50383061804	Loperamide 1 mg/5 ml liquid Cherry bottle 118	otc	generic		118
727	18698	45802065087	Loratadine 10 mg tablet bottle 300	otc	generic		300
728	3757	378032101	Lorazepam 0.5 mg tablet bottle 100	rx	generic	C-4	100
729	3757	378032105	Lorazepam 0.5 mg tablet bottle 500	rx	generic	C-4	500
730	3758	378045710	Lorazepam 1 mg tablet bottle 1000	rx	generic	C-4	1000
731	3755	10019010201	Lorazepam 2 mg/ml vial vial 1	rx	generic	C-4	1
732	16363	54353244	Lorazepam intensol 2 mg/ml Unflavored bottle 30	rx	brand	C-4	30
733	38686	93736698	Losartan potassium 100 mg tab bottle 90	rx	generic		90
734	23381	93736498	Losartan potassium 25 mg tab bottle 90	rx	generic		90
735	23382	93736598	Losartan potassium 50 mg tab bottle 90	rx	generic		90
736	59919	13668011790	Losartan-hctz 100-12.5 mg tab bottle 90	rx	generic		90
737	40923	93736856	Losartan-hctz 100-25 mg tab bottle 30	rx	generic		30
738	39106	24208029905	Lotemax 0.5% eye drops drop btl 5	rx	brand		5
739	16310	68180046701	Lovastatin 10 mg tablet bottle 100	rx	generic		100
740	6460	68180046807	Lovastatin 20 mg tablet bottle 60	rx	generic		60
741	6461	68180046901	Lovastatin 40 mg tablet bottle 100	rx	generic		100
742	3981	378701001	Loxapine 10 mg capsule bottle 100	rx	generic		100
743	3984	378705001	Loxapine 50 mg capsule bottle 100	rx	generic		100
744	72191	52800048856	Lubriderm daily moisture lot bottle 473	otc	brand		473
745	65392	23320503	Lumigan 0.01% eye drops drop btl 2.5	rx	brand		2
746	58728	7431204902	Lutein sftgl 20mg gcap 30 ea 30	otc	generic		30
747	57802	71101568	Lyrica 100 mg capsule bottle 90	rx	brand	C-5	90
748	57799	71101268	Lyrica 25 mg capsule bottle 90	rx	brand	C-5	90
749	57800	71101368	Lyrica 50 mg capsule bottle 90	rx	brand	C-5	90
750	57801	71101468	Lyrica 75 mg capsule bottle 90	rx	brand	C-5	90
751	-18400	19200002522	Lysol toilet bowl clnr liqd 720 720	otc	generic		720
752	-270621	76056949412	Mag-ox 400 tab 120 120	otc	brand		120
753	42924	68585000575	Mag64 dr 64 mg tablet bottle 60	otc	generic		60
754	70304	256018407	Magonate 54 mg/5 ml liquid bottle 355	otc	brand		355
755	-237203	78148525001	Max-freeze gel 113.4 gm 113	otc	brand		113
756	4732	59746012110	Meclizine 25 mg tablet bottle 1000	rx	generic		1000
757	-41571	7061021616	Medical supplies         960000 1	otc	generic		1
758	-202698	2739004252	Medicine cup plastic 1oz 100 dyn 4252 1	otc	brand		1
759	-196640	80995803361	Medihoney hydrocld wound paste 44	otc	brand		44
760	21004	54354258	Megestrol acet 40 mg/ml susp Lemon-lime bottle 240	rx	generic		240
761	41568	74312002832	Melatonin 1 mg tablet bottle 180	otc	generic		180
762	61740	30768016836	Melatonin 1 mg/ml liquid bottle 59	otc	generic		59
763	-127147	74098527087	Melatonin ms 5mg tabs 120 ea 120	otc	generic		120
764	71898	35046000392	Melatonin tr 10 mg tablet bottle 60	otc	generic		60
765	26076	54629050260	Melatonin tr 3 mg tablet bottle 60	otc	generic		60
766	29157	68180050201	Meloxicam 15 mg tablet bottle 100	rx	generic		100
767	29156	68180050101	Meloxicam 7.5 mg tablet bottle 100	rx	generic		100
768	32492	591387560	Memantine hcl 10 mg tablet bottle 60	rx	generic		60
769	53324	591387060	Memantine hcl 5 mg tablet bottle 60	rx	generic		60
770	23766	54162055007	Men-phor anti-itch lotion bottle 222	otc	generic		222
771	-192096	733243049766	Mepilex border foam dressing 3x3 5ct 1	rx	brand		1
772	16034	37000002901	Metamucil fiber wafer box 24	otc	brand		24
773	72014	37000002304	Metamucil packet packet 30	otc	brand		30
774	-200944	3700025445	Metamucil st sf orange packets 44 44	otc	brand		44
775	64472	37000074084	Metamucil sugar-free powder jar 425	otc	brand		425
776	51112	65162055310	Metaxalone 800 mg tablet bottle 100	rx	generic		100
777	40974	65862001099	Metformin hcl 1-000 mg tablet bottle 1000	rx	generic		1000
778	13318	57664039753	Metformin hcl 500 mg tablet bottle 500	rx	generic		500
779	16441	57664043551	Metformin hcl 850 mg tablet bottle 100	rx	generic		100
780	46754	93726701	Metformin hcl er 500 mg tablet bottle 100	rx	generic		100
781	9457	64720013910	Methenamine hipp 1 gm tablet bottle 100	rx	generic		100
782	9457	43199002001	Methenamine hipp 1 gm tablet bottle 100	rx	generic		100
783	4654	31722053301	Methocarbamol 500 mg tablet bottle 100	rx	generic		100
784	36872	54455025	Methotrexate 2.5 mg tablet bottle 100	rx	generic		100
785	45311	59746000103	Methylprednisolone 4 mg dosepk UD UU blist pack 21	rx	generic		21
786	6741	603459321	Methylprednisolone 4 mg tablet bottle 100	rx	generic		100
787	5232	591246705	Metoclopramide 5 mg tablet bottle 500	rx	generic		500
788	8217	378617201	Metolazone 2.5 mg tablet bottle 100	rx	generic		100
789	16600	55111046801	Metoprolol succ er 100 mg tab bottle 100	rx	generic		100
790	16601	55111046901	Metoprolol succ er 200 mg tab bottle 100	rx	generic		100
791	47586	62037083010	Metoprolol succ er 25 mg tab bottle 1000	rx	generic		1000
792	16599	62037083110	Metoprolol succ er 50 mg tab bottle 1000	rx	generic		1000
793	5131	378004710	Metoprolol tartrate 100 mg tab bottle 1000	rx	generic		1000
794	50631	378001801	Metoprolol tartrate 25 mg tab bottle 100	rx	generic		100
795	50631	378001805	Metoprolol tartrate 25 mg tab bottle 500	rx	generic		500
796	5132	378003201	Metoprolol tartrate 50 mg tab bottle 100	rx	generic		100
797	5132	378003210	Metoprolol tartrate 50 mg tab bottle 1000	rx	generic		1000
798	41799	168032346	Metronidazole 0.75% cream tube 45	rx	generic		45
799	16939	68682045570	Metronidazole vaginal 0.75% gl tube 70	rx	generic		70
800	7366	51672200101	Miconazole nitrate 2% cream tube 15	otc	generic		15
801	7366	51672200102	Miconazole nitrate 2% cream tube 30	otc	generic		30
802	21900	47360017201	Microspacer for aerosol device box 1	rx	generic		1
803	50491	378190301	Midodrine hcl 10 mg tablet bottle 100	rx	generic		100
804	17117	115422201	Midodrine hcl 5 mg tablet bottle 100	rx	generic		100
805	3026	904078816	Milk of magnesia suspension bottle 473	otc	generic		473
806	9226	13668048450	Minocycline 100 mg capsule bottle 50	rx	generic		50
807	9227	13668048201	Minocycline 50 mg capsule bottle 100	rx	generic		100
808	41843	41100082076	Miralax powder jar 119	otc	brand		119
809	41843	11523723403	Miralax powder jar 238	otc	brand		238
810	47453	65862002106	Mirtazapine 15 mg odt UD blist pack 30	rx	generic		30
811	46450	13107003134	Mirtazapine 15 mg tablet bottle 30	rx	generic		30
812	46450	13107003105	Mirtazapine 15 mg tablet bottle 500	rx	generic		500
813	47454	65862002206	Mirtazapine 30 mg odt UD blist pack 30	rx	generic		30
814	46451	13107000305	Mirtazapine 30 mg tablet bottle 500	rx	generic		500
815	25848	603466116	Modafinil 100 mg tablet bottle 30	rx	generic	C-4	30
816	49383	8881511136	Monoject insulin syr u-100 box 100	rx	generic		100
817	38451	603465532	Montelukast sod 10 mg tablet bottle 1000	rx	generic		1000
818	4090	527142536	Morphine sulf 100 mg/5 ml soln Unflavored bottle 30	rx	generic	C-2	30
819	11887	42858080101	Morphine sulf er 15 mg tablet bottle 100	rx	generic	C-2	100
820	4096	406833001	Morphine sulf er 30 mg tablet bottle 100	rx	generic	C-2	100
821	61457	65649020175	Moviprep powder packet Lemon packet 1	rx	brand		1
822	12073	63824005718	Mucinex d er 600-60 mg tablet blist pack 18	otc	brand		18
823	71885	63824000850	Mucinex er 600 mg tablet bottle 500	otc	brand		500
824	-81577	36382402328	Mucinex ms imr/er 1200mg tabs 28 ea 28	otc	generic		28
825	65367	24414260	Multaq 400 mg tablet bottle 60	rx	brand		60
826	37048	68462056417	Mupirocin 2% cream tube 15	rx	generic		15
827	7732	45802011222	Mupirocin 2% ointment tube 22	rx	generic		22
828	7738	24208027615	Muro-128 2% eye drops drop btl 15	otc	brand		15
829	7739	24208027715	Muro-128 5% eye drops drop btl 15	otc	brand		15
830	69630	469260130	Myrbetriq er 25 mg tablet bottle 30	rx	brand		30
831	69631	469260230	Myrbetriq er 50 mg tablet bottle 30	rx	brand		30
833	-133989	7431206212	N/b red yeast rice 120 ct  15	otc	generic		15
834	-75245	7431215417	N/b tumeric curcumin cap 60 24's 60	otc	generic		60
835	-160101	7431215606	N/b vit d sftgl 1000iu 200 200	otc	brand		200
836	-152261	7431217621	N/b vit d tab 2000iu 100 100	otc	brand		100
837	16574	185014501	Nabumetone 500 mg tablet bottle 100	rx	generic		100
838	16575	591367101	Nabumetone 750 mg tablet bottle 100	rx	generic		100
839	4518	406117001	Naltrexone 50 mg tablet bottle 100	rx	generic		100
840	70868	456341490	Namenda xr 14 mg capsule bottle 90	rx	brand		90
841	70869	456342133	Namenda xr 21 mg capsule bottle 30	rx	brand		30
842	70870	456342890	Namenda xr 28 mg capsule bottle 90	rx	brand		90
843	70867	456340733	Namenda xr 7 mg capsule bottle 30	rx	brand		30
844	23718	65008515	Naphcon-a eye drops drop btl 15	otc	brand		15
845	8362	65162019010	Naproxen 500 mg tablet bottle 100	rx	generic		100
846	-49203	18436900000	Nasalcease str packs pack 5 ea 5	otc	generic		5
847	31186	85128801	Nasonex 50 mcg nasal spray canister 17	rx	brand		17
848	63561	904649335	Natural balance tears eye drop drop btl 15	otc	generic		15
849	-405252	3367410485	Natures way cran rx urinary gummies 60 60	otc	brand		60
850	-39667	7431201719	Nb c-1000mg tabs        1710 100	otc	brand		100
851	-39666	7431201476	Nb c-500mg tabs         1513 250	otc	generic		250
852	-56643	7431213437	Nb calcium 1200mg w d    13437 100's 1	otc	generic		1
853	-219179	7431204363	Nb cranberry w/vit-c trip str softgel250 250	otc	brand		250
854	-39668	7431201798	Nb e-1000iu softgels 1798 60	otc	generic		60
855	-87472	7431213102	Nb fish oil 1200mg 180 1	otc	generic		1
856	-60878	7431203822	Nb fish oil 1351000mg bns 1	otc	generic		1
857	-77314	7431215745	Nb melatonin 5mg 60 1	otc	generic		1
858	-219180	7431219491	Nb melatonin cap 10mg 60 60	otc	brand		60
859	-360828	7431252805	Nb vit b-12 tab 1000mcg 200 200	otc	brand		200
860	-175374	7985407723	Nbl vit d3 400iu liq 7723 1.75	otc	brand		1
861	-177664	7431251135	Nbty q-sorb co q10 100mg sg 90  60	rx	brand		60
862	45997	55513020991	Neupogen 480 mcg/0.8 ml syr syringe 0.8	rx	brand		0
863	60488	50474080503	Neupro 6 mg/24 hr patch box 30	rx	brand		30
864	-12029	7050102080	Neut tmst crm lather shmp shmp 8.45oz 250	otc	brand		250
865	-12062	7050102660	Neut tmst daily deep cond cond 8.45oz 250	otc	brand		250
866	7076	70501005550	Neutrogena light night cream jar 63	otc	generic		63
867	41442	8380018194	New image drainable pouch box 10	otc	generic		10
868	-11170	5113166992	Nexcare gentle paper 3/4in tape 1 ea 1	otc	brand		1
869	-151998	5113199523	Nexcare hdt flx fbrc o/s ptch 30 ea 30	otc	brand		30
870	-152002	5113199525	Nexcare wtrprf clr o/s ptch 20 ea 20	otc	brand		20
871	47525	186502031	Nexium dr 20 mg capsule bottle 30	rx	brand		30
872	47526	186504031	Nexium dr 40 mg capsule bottle 30	rx	brand		30
873	16426	536589588	Nicotine 14 mg/24hr patch box 14	otc	generic		14
874	16427	48985000152	Nicotine 21 mg/24hr patch box 14	otc	generic		14
875	568	43386044024	Nifedipine 10 mg capsule bottle 100	rx	generic		100
876	20616	62175026055	Nifedipine er 30 mg tablet bottle 300	rx	generic		300
877	20617	62175026155	Nifedipine er 60 mg tablet bottle 300	rx	generic		300
878	12061	93205901	Nifedipine er 90 mg tablet bottle 100	rx	generic		100
879	20618	62175026246	Nifedipine er 90 mg tablet bottle 90	rx	generic		90
880	455	49483022110	Nitro-time er 2.5 mg capsule bottle 100	rx	generic		100
881	9428	47781030801	Nitrofurantoin mcr 100 mg cap bottle 100	rx	generic		100
882	9430	93213001	Nitrofurantoin mcr 50 mg cap bottle 100	rx	generic		100
883	16598	47781030301	Nitrofurantoin mono-mcr 100 mg bottle 100	rx	generic		100
884	468	47781029703	Nitroglycerin 0.2 mg/hr patch box 30	rx	generic		30
885	475	59762330403	Nitroglycerin 0.4 mg tablet sl bottle 25	rx	generic		25
886	465	49730011230	Nitroglycerin 0.4 mg/hr patch box 30	rx	generic		30
887	475	71041813	Nitrostat 0.4 mg tablet sl bottle 25	rx	brand		25
888	46059	51672400105	Nortriptyline hcl 10 mg cap bottle 90	rx	generic		90
889	61638	169185250	Novofine 30g x 1/3" needles kit 100	otc	generic		100
890	62772	169185275	Novofine autocover 30g needle box 100	otc	generic		100
891	16311	169183711	Novolin 70-30 100 unit/ml vial vial 10	otc	brand		10
892	1740	169183411	Novolin n 100 units/ml vial vial 10	otc	brand		10
893	1723	169183311	Novolin r 100 units/ml vial vial 10	otc	brand		10
894	44340	169750111	Novolog 100 unit/ml vial vial 10	rx	brand		10
895	44341	169633910	Novolog 100 units/ml flexpen syringe 3	rx	brand		3
896	50134	169369619	Novolog mix 70-30 flexpen syrn syringe 3	rx	brand		3
897	51718	169368512	Novolog mix 70-30 vial vial 10	rx	brand		10
898	-473402	84009310152	Nt magnesium trilpe complex 100cp 1	otc	brand		1
899	65320	50458083004	Nucynta 75 mg tablet bottle 100	rx	brand	C-2	100
900	7282	472016315	Nystatin 100-000 unit/gm cream tube 15	rx	generic		15
901	7282	603781878	Nystatin 100-000 unit/gm cream tube 30	rx	generic		30
902	7284	42543005261	Nystatin 100-000 unit/gm powd bottle 15	rx	generic		15
903	7284	42543005262	Nystatin 100-000 unit/gm powd bottle 30	rx	generic		30
904	7284	68308015215	Nystatin 100-000 unit/gm powd squeez btl 15	rx	generic		15
905	9537	603148158	Nystatin 100-000 unit/ml susp Cherry mint bottle 473	rx	generic		473
906	7283	168000730	Nystatin 100-000 units/gm oint tube 30	rx	generic		30
907	7283	472016630	Nystatin 100-000 units/gm oint tube 30	rx	generic		30
908	48529	51672126302	Nystatin-triamcinolone cream tube 30	rx	generic		30
909	48529	51672126303	Nystatin-triamcinolone cream tube 60	rx	generic		60
910	48530	51672127202	Nystatin-triamcinolone ointm tube 30	rx	generic		30
911	11052	54799030190	Ocusoft pre-moist pads UU packet 30	otc	generic		30
912	-81107	1571810430	Ocusoft premoist pl pads 30 ea 30	otc	generic		30
913	74399	24208073510	Ocuvite eye + multi tablet bottle 60	otc	brand		60
914	70073	24208040319	Ocuvite lutein & zeaxanthin cp bottle 36	otc	brand		36
915	63995	24208046530	Ocuvite softgel bottle 50	otc	brand		50
916	19734	17478071310	Ofloxacin 0.3% eye drops drop btl 5	rx	generic		5
917	27960	60505311308	Olanzapine 10 mg tablet bottle 1000	rx	generic		1000
918	41026	93577101	Olanzapine 15 mg tablet bottle 100	rx	generic		100
919	41027	93510501	Olanzapine 20 mg tablet bottle 100	rx	generic		100
920	45191	55111026381	Olanzapine odt 10 mg tablet UD blist pack 30	rx	generic		30
921	45190	55111026281	Olanzapine odt 5 mg tablet UD blist pack 30	rx	generic		30
922	-17844	12044038970	Old sp classic deod orig stck 3.75 oz 98	otc	generic		98
923	-358212	12044004049	Old sp swagger shmp 12 oz 355	otc	brand		355
924	30796	60505057501	Olopatadine hcl 0.1% eye drops drop btl 5	rx	generic		5
925	58486	49884001908	Omega-3 ethyl esters 1 gm cap bottle 120	rx	generic		120
926	43136	60505014502	Omeprazole dr 10 mg capsule bottle 100	rx	generic		100
927	33530	55111015810	Omeprazole dr 20 mg capsule bottle 1000	rx	generic		1000
928	43137	62175013637	Omeprazole dr 40 mg capsule bottle 100	rx	generic		100
929	43137	60505014601	Omeprazole dr 40 mg capsule bottle 500	rx	generic		500
930	16392	63304045830	Ondansetron hcl 4 mg tablet bottle 30	rx	generic		30
931	16392	45963053830	Ondansetron hcl 4 mg tablet bottle 30	rx	generic		30
932	16393	63304045930	Ondansetron hcl 8 mg tablet bottle 30	rx	generic		30
933	41562	781523864	Ondansetron odt 4 mg tablet UD blist pack 30	rx	generic		30
934	-299924	1650055017	One a day wmn 50+ advantge 120 byr 120	rx	brand		120
935	69033	16500008012	One-a-day men's tablet bottle 100	otc	brand		100
936	70013	53885000850	Onetouch delica 30g lancets box 100	otc	generic		100
937	62136	53885045802	Onetouch ultra control soln bottle 1	otc	generic		1
938	6373	53885024510	Onetouch ultra test strips box 100	otc	generic		100
939	6373	53885099425	Onetouch ultra test strips box 25	otc	generic		25
940	6373	53885024450	Onetouch ultra test strips box 50	otc	generic		50
941	11186	53885039310	Onetouch ultrasoft lancets box 100	otc	generic		100
942	65431	310610530	Onglyza 5 mg tablet bottle 30	rx	brand		30
943	-188106	30065135609	Opti-free replenish mp sol 10oz+2ozbonus 12	rx	brand		12
944	3483	10310003313	Orajel baby jar 10	otc	brand		10
945	-19221	30041080200	Oral-b 40 indicator sft 11 each 1	otc	brand		1
946	-35311	3076803578	Osteo bi-flex trps smcp cplt 120 ea 120	otc	brand		120
947	3769	228206710	Oxazepam 10 mg capsule bottle 100	rx	generic	C-4	100
948	3770	228206910	Oxazepam 15 mg capsule bottle 100	rx	generic	C-4	100
949	27779	51991029301	Oxcarbazepine 300 mg tablet bottle 100	rx	generic		100
950	4929	50111045601	Oxybutynin 5 mg tablet bottle 100	rx	generic		100
951	4929	50111045603	Oxybutynin 5 mg tablet bottle 1000	rx	generic		1000
952	41046	378660501	Oxybutynin cl er 5 mg tablet bottle 100	rx	generic		100
953	48976	68308084201	Oxycodon-acetaminophen 7.5-325 bottle 100	rx	generic	C-2	100
954	13467	10702005601	Oxycodone hcl 10 mg tablet bottle 100	rx	generic	C-2	100
955	46474	10702000801	Oxycodone hcl 15 mg tablet bottle 100	rx	generic	C-2	100
956	45298	10702005701	Oxycodone hcl 20 mg tablet bottle 100	rx	generic	C-2	100
957	4225	10702001801	Oxycodone hcl 5 mg tablet bottle 100	rx	generic	C-2	100
958	72862	115155601	Oxycodone hcl er 10 mg tablet bottle 100	rx	generic	C-2	100
959	72864	115155801	Oxycodone hcl er 20 mg tablet bottle 100	rx	generic	C-2	100
960	72862	59011041010	Oxycontin 10 mg tablet bottle 100	rx	brand	C-2	100
961	72863	59011041510	Oxycontin 15 mg tablet bottle 100	rx	brand	C-2	100
962	72867	59011046010	Oxycontin 60 mg tablet bottle 100	rx	brand	C-2	100
963	1340	536410602	Oysco-500 tablet bottle 250	otc	generic		250
964	59178	904546080	Oyster shell 500-vit d3 200 tb bottle 1000	otc	generic		1000
965	4489	536322210	Pain & fever 325 mg tablet bottle 1000	otc	generic		1000
966	4490	536321810	Pain & fever 500 mg caplet bottle 1000	otc	generic		1000
967	61987	591369530	Paliperidone er 9 mg tablet bottle 30	rx	generic		30
968	-17345	10181040000	Palmer cocoa butter jar crm 3.5 oz 105	otc	brand		105
969	-394092	18368900107	Panda bath tissue 2 ply 4 pak (6) 1	otc	brand		1
970	39545	62175018046	Pantoprazole sod dr 20 mg tab bottle 90	rx	generic		90
971	27462	378668910	Pantoprazole sod dr 40 mg tab bottle 1000	rx	generic		1000
972	-187096	44444053488	Paper souffle cup 3/4oz 250 non024215 1	rx	brand		1
973	46222	68382009716	Paroxetine hcl 10 mg tablet bottle 90	rx	generic		90
974	46224	13107015630	Paroxetine hcl 30 mg tablet bottle 30	rx	generic		30
975	62065	65027225	Pataday 0.2% eye drops drop btl 2.5	rx	brand		2
976	-16797	9243781370	Pedifix tubular foam mix pads 3x3 in 3	otc	brand		3
977	-201212	9243727651	Pedifix visco-gel toe spacer p28dm 1	otc	brand		1
978	47669	16837029150	Pepcid complete tablet chew Berry bottle 50	otc	brand		50
979	3830	603506321	Perphenazine 16 mg tablet bottle 100	rx	generic		100
980	3831	781104601	Perphenazine 2 mg tablet bottle 100	rx	generic		100
981	3832	781104701	Perphenazine 4 mg tablet bottle 100	rx	generic		100
982	9478	42192080201	Phenazopyridine 200 mg tab bottle 100	rx	generic		100
983	46263	43386036021	Phenelzine sulfate 15 mg tab bottle 60	rx	generic		60
984	27610	603516521	Phenobarbital 16.2 mg tablet bottle 100	rx	generic	C-4	100
985	27611	603516621	Phenobarbital 32.4 mg tablet bottle 100	rx	generic	C-4	100
986	27612	603516721	Phenobarbital 64.8 mg tablet bottle 100	rx	generic	C-4	100
987	4529	59762053101	Phenytoin 125 mg/5 ml susp Orange-vanilla bottle 237	rx	generic		237
988	4531	59762521001	Phenytoin 50 mg infatab bottle 100	rx	generic		100
989	-135782	31284353430	Phillips colon health cap 30 30	otc	brand		30
990	-326961	31284355433	Phillips fiber gummies 1	otc	brand		1
991	3026	12843039324	Phillips' milk of magnesia Cherry bottle 355	otc	brand		355
992	3026	12843039325	Phillips' milk of magnesia Cherry bottle 769	otc	brand		769
993	3026	12843036305	Phillips' milk of magnesia Mint bottle 355	otc	brand		355
994	-27677	76394811712	Phy glucosamine sulf 750mg tabs 120 ea 120	otc	generic		120
995	7826	61314020615	Pilocarpine 4% eye drops drop btl 15	rx	generic		15
996	21731	228280111	Pilocarpine hcl 5 mg tablet bottle 100	rx	generic		100
997	19448	49884034701	Pimozide 1 mg tablet bottle 100	rx	generic		100
998	3892	49884034801	Pimozide 2 mg tablet bottle 100	rx	generic		100
999	42943	93204898	Pioglitazone hcl 15 mg tablet bottle 90	rx	generic		90
1000	42944	93727298	Pioglitazone hcl 30 mg tablet bottle 90	rx	generic		90
1001	21185	55150011930	Piperacil-tazobact 2.25 gm vl vial 1	rx	generic		1
1002	21186	55150012030	Piperacil-tazobact 3.375 gm vl vial 1	rx	generic		1
1003	-357469	18368900034	Plenty paper towel 12roll bundle 2906982 1	otc	brand		1
1004	-8095	3600019564	Poise pad xabs pada 6x20ea 6's 20	otc	brand		20
1005	10362	36000033591	Poise pads bag 39	otc	generic		39
1006	10362	36000033558	Poise pads bag 66	otc	generic		66
1007	41843	51991045758	Polyethylene glycol 3350 powd bottle 255	rx	generic		255
1008	41843	51991045757	Polyethylene glycol 3350 powd bottle 527	rx	generic		527
1009	34313	574041207	Polyethylene glycol 3350 powd packet 14	rx	generic		14
1010	48570	24208031510	Polymyxin b-tmp eye drops drop btl 10	rx	generic		10
1011	17000	245007111	Potassium citrate er 10 meq tb UU bottle 100	rx	generic		100
1012	1264	603154258	Potassium cl 10% (20 meq/15 ml Orange bottle 473	rx	generic		473
1013	1266	603154358	Potassium cl 20% (40 meq/15 ml Orange bottle 473	rx	generic		473
1014	1275	781152610	Potassium cl er 10 meq tablet bottle 1000	rx	generic		1000
1015	22346	62037099910	Potassium cl er 20 meq tablet bottle 1000	rx	generic		1000
1016	7225	904110231	Povidone-iodine 10% ointment tube 28.35	otc	generic		28
1017	7233	395232598	Povidone-iodine 10% solution bottle 237	otc	generic		237
1018	66781	597013560	Pradaxa 150 mg capsule UD blist pack 60	rx	brand		60
1019	31781	68462033090	Pramipexole 0.125 mg tablet bottle 90	rx	generic		90
1020	31782	68462033190	Pramipexole 0.25 mg tablet bottle 90	rx	generic		90
1021	39100	68462033290	Pramipexole 0.5 mg tablet bottle 90	rx	generic		90
1022	31779	42543070890	Pramipexole 1 mg tablet bottle 90	rx	generic		90
1023	31780	68462033490	Pramipexole 1.5 mg tablet bottle 90	rx	generic		90
1024	16366	60505016809	Pravastatin sodium 10 mg tab bottle 90	rx	generic		90
1025	16367	60505016907	Pravastatin sodium 20 mg tab bottle 1000	rx	generic		1000
1026	20741	60505017007	Pravastatin sodium 40 mg tab bottle 1000	rx	generic		1000
1027	49758	60505132305	Pravastatin sodium 80 mg tab bottle 500	rx	generic		500
1028	291	378110101	Prazosin 1 mg capsule bottle 100	rx	generic		100
1029	292	93406801	Prazosin 2 mg capsule bottle 100	rx	generic		100
1030	7894	61314063710	Prednisolone ac 1% eye drop drop btl 10	rx	generic		10
1031	7894	60758011910	Prednisolone ac 1% eye drop drop btl 10	rx	generic		10
1032	7894	60758011905	Prednisolone ac 1% eye drop drop btl 5	rx	generic		5
1033	6749	603533832	Prednisone 10 mg tablet bottle 1000	rx	generic		1000
1034	6750	54474225	Prednisone 2.5 mg tablet bottle 100	rx	generic		100
1035	6753	54472831	Prednisone 5 mg tablet bottle 1000	rx	generic		1000
1036	7013	46087221	Premarin vaginal cream-appl tube/kit 30	rx	brand		30
1037	61414	573286820	Preparation h cream tube 51	otc	brand		51
1038	71294	573287110	Preparation h ointment tube 28	otc	brand		28
1039	-270392	32420862504	Preservision areds 2 gcap 120 ea 120	otc	brand		120
1040	62498	24208053230	Preservision areds softgel bottle 120	otc	brand		120
1041	60165	24208063211	Preservision lutein softgel bottle 120	otc	brand		120
1042	51653	64764054311	Prevacid 15 mg solutab Strawberry UD blist pack 100	rx	brand		100
1043	-16687	9089112706	Prevail prem wshclth pada 96 ea 96	otc	generic		96
1044	2615	126003316	Prevident 0.2% rinse Cool mint bottle 473	rx	brand		473
1045	60583	126007492	Prevident 5000 booster plus Spearmint bottle 100	rx	brand		100
1046	4543	527123101	Primidone 250 mg tablet bottle 100	rx	generic		100
1047	4544	527130101	Primidone 50 mg tablet bottle 100	rx	generic		100
1048	28090	59310057922	Proair hfa 90 mcg inhaler canister 8.5	rx	brand		8
1049	3846	378511001	Prochlorperazine 10 mg tab bottle 100	rx	generic		100
1050	11742	59676031001	Procrit 10-000 units/ml vial vial 1	rx	brand		1
1051	58592	59676031204	Procrit 10-000 units/ml vial vial 2	rx	brand		2
1052	25708	59676032004	Procrit 20-000 units/ml vial vial 1	rx	brand		1
1053	41394	59676034001	Procrit 40-000 units/ml vial vial 1	rx	brand		1
1054	23906	69315030230	Procto-med hc 2.5% cream tube 30	rx	brand		30
1055	23906	64980030130	Proctozone-hc 2.5% cream tube 30	rx	generic		30
1056	66396	55513071001	Prolia 60 mg/ml syringe syringe 1	rx	brand		1
1057	3877	10702000201	Promethazine 12.5 mg tablet bottle 100	rx	generic		100
1058	3878	781183001	Promethazine 25 mg tablet bottle 100	rx	generic		100
1059	3873	713052612	Promethegan 25 mg suppository box 12	rx	brand		12
1060	5120	54372763	Propranolol 20 mg/5 ml soln Strawberry-mint bottle 500	rx	generic		500
1061	5114	51991082001	Propranolol er 160 mg capsule bottle 100	rx	generic		100
1062	5115	228277811	Propranolol er 60 mg capsule bottle 100	rx	generic		100
1063	6673	228234810	Propylthiouracil 50 mg tablet bottle 100	rx	generic		100
1064	59632	50484004409	Proshield plus 1% ointment tube 113	otc	generic		113
1065	-25081	64712500001	Prostate formula tabs 270 ea 270	otc	brand		270
1066	4764	115351101	Pyridostigmine br 60 mg tablet bottle 100	rx	generic		100
1067	4484	603083958	Q-pap 160 mg/5 ml solution Cherry bottle 473	otc	generic		473
1068	-142209	63551599597	Qc acet pm caplet xst tyl 50  50	rx	brand		50
1069	-142751	63551599465	Qc alcohol isopropyl 70% 16 oz  16	rx	brand		16
1070	-141829	63551590120	Qc antacid chw reg pmnt tum150  150	rx	brand		150
1071	2701	35515090123	Qc antacid suspension bottle 355	otc	generic		355
1072	2706	35515099921	Qc antacid-antigas max str bottle 355	otc	generic		355
1073	2701	63868069457	Qc antacid-antigas suspension Mint creme bottle 355	otc	generic		355
1074	13672	35515098141	Qc anti-diarrheal 2 mg caplet blist pack 12	otc	generic		12
1075	13672	35515095480	Qc anti-diarrheal 2 mg caplet bottle 60	otc	generic		60
1076	4380	63868046768	Qc child aspirin 81 mg chw tab Orange bottle 36	otc	generic		36
1077	16947	35515098525	Qc child pain rlf 160 mg/5 ml bottle 118	otc	generic		118
1078	11640	35515095357	Qc chlorpheniramine 4 mg tab bottle 100	otc	generic		100
1079	-142674	63551590125	Qc citrate of mag lem 10 oz  10	rx	brand		10
1080	7361	35515095256	Qc clotrimazole 1% cream tube 28	rx	generic		28
1081	11594	35515094576	Qc complete allergy 25 mg cplt blist pack 24	otc	generic		24
1082	-141839	63551598321	Qc cotton balls trpl sz j j100  100	rx	brand		100
1083	-141847	63551598112	Qc cough form tussin dm 4z  118	rx	brand		118
1084	-142036	63551598133	Qc cough form tussin dm rbtsn 8z  8	rx	brand		8
1085	-177583	63551595728	Qc dental flossers mnt 36 plackers  36	rx	brand		36
1086	-142755	63551590883	Qc epsom salt 16 oz  16	rx	brand		16
1087	-142756	63551590885	Qc epsom salt carton 4 lb  64	rx	brand		64
1088	7769	63868037605	Qc eye drops drop btl 15	otc	generic		15
1089	-142216	63551598140	Qc gas relief inf drops mylicon 1 oz 1	rx	brand		1
1090	-141899	63551597846	Qc hearing aid batt #13 rayo4  1	rx	brand		1
1091	-331988	63551596933	Qc lamb's wool padding 3/8oz 1	otc	brand		1
1092	27622	35515095373	Qc loratadine-d 24hr tablet blist pack 10	otc	generic		10
1093	-142727	63551595253	Qc miconazole 2% pwd sp ltrmn 4.6 oz 4	rx	brand		4
1094	11519	35515095681	Qc multi-purpose solution bottle 355	otc	generic		355
1095	64530	35515094676	Qc natural vegetable powder jar 368	otc	generic		368
1096	4490	35515095610	Qc non-aspirin 500 mg gelcap bottle 100	otc	generic		100
1097	4490	35515095771	Qc non-aspirin pain relief tb bottle 100	otc	generic		100
1098	2858	35515090127	Qc pink bismuth 262 mg/15 ml bottle 237	otc	generic		237
1099	3029	63868038045	Qc ready to use enema squeez btl 133	otc	generic		133
1100	5302	35515095495	Qc sweet oil bottle 118	otc	generic		118
1101	-142705	63551595520	Qc tussin cough sf rbtssn 4 oz  4	rx	brand		4
1102	-401758	63551598660	Qc vit fish oil 1200mg enteric softgel90 90	otc	brand		90
1103	-145190	63551595355	Qc vit ocu-health preservision 120  120	rx	brand		120
1104	34188	93816201	Quetiapine fumarate 100 mg tab bottle 100	rx	generic		100
1105	34189	93816301	Quetiapine fumarate 200 mg tab bottle 100	rx	generic		100
1106	34187	93206301	Quetiapine fumarate 25 mg tab bottle 100	rx	generic		100
1107	34187	93206310	Quetiapine fumarate 25 mg tab bottle 1000	rx	generic		1000
1108	47198	93816401	Quetiapine fumarate 300 mg tab bottle 100	rx	generic		100
1109	60293	93816501	Quetiapine fumarate 400 mg tab bottle 100	rx	generic		100
1110	60292	93816610	Quetiapine fumarate 50 mg tab bottle 1000	rx	generic		1000
1111	18774	68180055609	Quinapril 5 mg tablet bottle 90	rx	generic		90
1112	40941	62175030232	Rabeprazole sod dr 20 mg tab bottle 30	rx	generic		30
1113	37022	591236701	Raloxifene hcl 60 mg tablet bottle 100	rx	generic		100
1114	16031	16252057350	Ramipril 10 mg capsule bottle 500	rx	generic		500
1115	15940	16252057101	Ramipril 2.5 mg capsule bottle 100	rx	generic		100
1116	62973	61958100401	Ranexa er 1-000 mg tablet bottle 60	rx	brand		60
1117	60333	61958100301	Ranexa er 500 mg tablet bottle 60	rx	brand		60
1118	11672	54838055080	Ranitidine 15 mg/ml syrup Peppermint bottle 473	rx	generic		473
1119	11673	53746025310	Ranitidine 150 mg tablet bottle 1000	rx	generic		1000
1120	11674	68462024920	Ranitidine 300 mg tablet bottle 250	rx	generic		250
1121	64847	52544015230	Rapaflo 8 mg capsule bottle 30	rx	brand		30
1122	6858	68784010812	Rectacort-hc 25 mg suppository UD blist pack 12	rx	brand		12
1123	30016	23050601	Refresh classic eye drops UD blist pack 30	otc	brand		30
1124	63496	23031204	Refresh lacri-lube ointment tube 3.5	otc	brand		3
1125	63496	23031207	Refresh lacri-lube ointment tube 7	otc	brand		7
1126	8640	23920515	Refresh liquigel 1% eye drops drop btl 15	otc	brand		15
1127	8640	23920530	Refresh liquigel 1% eye drops drop btl 30	otc	brand		30
1128	69132	23430710	Refresh optive advanced drops drop btl 10	otc	brand		10
1129	30011	23548730	Refresh plus 0.5% eye drops blist pack 30	otc	brand		30
1130	16278	23079830	Refresh tears 0.5% eye drops drop btl 30	otc	brand		30
1131	3046	536188179	Reguloid laxative powder Natural jar 284	otc	generic		284
1132	3043	536444454	Reguloid powder Natural jar 369	otc	generic		369
1133	63956	53329016444	Remedy calazime protect paste jar 113	otc	brand		113
1134	-373904	88438911146	Remedy phytoplex hydra cr 4oz msc092534 4	otc	brand		4
1135	75827	327001230	Renacidin irrigation solution bottle 30	rx	brand		30
1136	33515	60258016201	Renal caps softgel bottle 100	rx	generic		100
1137	65494	58468013202	Renvela 0.8 gm powder packet Citrus box 90	rx	brand		90
1138	63473	58468013001	Renvela 800 mg tablet bottle 270	rx	brand		270
1139	66401	2359046010	Replesta 50-000 units wafer Orange packet 4	otc	brand		4
1140	1913	212186662	Resource breeze liquid bottle 237	otc	generic		237
1141	51820	23916330	Restasis 0.05% eye emulsion vial 30	rx	brand		30
1142	63925	60505309702	Risedronate sodium 150 mg tab UD UU blist pack 1	rx	generic		1
1143	50364	65862051904	Risedronate sodium 35 mg tab UD blist pack 4	rx	generic		4
1144	42922	50458059060	Risperidone 0.25 mg tablet bottle 60	rx	generic		60
1145	42923	27241000350	Risperidone 0.5 mg tablet bottle 500	rx	generic		500
1146	26177	51991071641	Risperidone 1 mg/ml solution bottle 30	rx	generic		30
1147	21155	13668003805	Risperidone 2 mg tablet bottle 500	rx	generic		500
1148	21156	27241000506	Risperidone 3 mg tablet bottle 60	rx	generic		60
1149	21157	13668004060	Risperidone 4 mg tablet bottle 60	rx	generic		60
1150	40155	55111035260	Rivastigmine 1.5 mg capsule bottle 60	rx	generic		60
1151	69938	47781040503	Rivastigmine 13.3 mg/24hr ptch box 30	rx	generic		30
1152	40156	51991079406	Rivastigmine 3 mg capsule bottle 60	rx	generic		60
1153	62870	47781030403	Rivastigmine 4.6 mg/24hr patch box 30	rx	generic		30
1154	62871	47781030503	Rivastigmine 9.5 mg/24hr patch box 30	rx	generic		30
1155	34166	43547026950	Ropinirole hcl 0.5 mg tablet bottle 500	rx	generic		500
1156	29160	43547027010	Ropinirole hcl 1 mg tablet bottle 100	rx	generic		100
1157	29161	43547027110	Ropinirole hcl 2 mg tablet bottle 100	rx	generic		100
1158	43202	68462025701	Ropinirole hcl 3 mg tablet bottle 100	rx	generic		100
1159	43203	43547027310	Ropinirole hcl 4 mg tablet bottle 100	rx	generic		100
1160	63859	55111066130	Ropinirole hcl er 4 mg tablet bottle 30	rx	generic		30
1161	51784	16252061630	Rosuvastatin calcium 10 mg tab UU bottle 30	rx	generic		30
1162	51785	16252061730	Rosuvastatin calcium 20 mg tab UU bottle 30	rx	generic		30
1163	52944	16252061530	Rosuvastatin calcium 5 mg tab UU bottle 30	rx	generic		30
1164	59509	64764080510	Rozerem 8 mg tablet bottle 100	rx	brand		100
1165	2701	536194583	Rulox suspension bottle 355	otc	generic		355
1166	-203092	95585000150	Rx prescription tape 1.5" 1	otc	brand		1
1167	73309	64896066243	Rytary er 36.25 mg-145 mg cap bottle 240	rx	brand		240
1168	-401401	46581011060	Salonpas pain rel ptch 60 60	otc	brand		60
1169	-134037	7164132101	Sanford sharpie fine mrk twn bl 236	otc	generic		236
1170	5258	50484001030	Santyl ointment tube 30	rx	brand		30
1171	-6471	3076801014	Saw palmetto  whl 450mg caps 250 250	otc	brand		250
1172	-17589	11017012288	Sch bunion cushion felt pads 6	otc	brand		6
1173	-368317	37000090922	Scope classic mwsh 500 ml 500	otc	brand		500
1174	-39589	3076812608	Sd b-12 tab 500mcgm      576 100	otc	generic		100
1175	-358206	30768029545	Sd krill oil gcap 1000 ea 1000	otc	brand		1000
1176	-59500	3076848678	Sd melatonin 120 3mg bns 1	otc	brand		1
1177	26516	67253070006	Selegiline hcl 5 mg capsule bottle 60	rx	generic		60
1178	6208	41167060616	Selsun blue 1% shampoo bottle 207	otc	brand		207
1179	48044	536408610	Senexon-s tablet bottle 1000	otc	generic		1000
1180	58670	121072208	Senna syrup Natural bottle 237	otc	brand		237
1181	19964	603028232	Senna-lax 8.6 mg tablet bottle 1000	otc	generic		1000
1182	53762	55513007330	Sensipar 30 mg tablet bottle 30	rx	brand		30
1183	53763	55513007430	Sensipar 60 mg tablet bottle 30	rx	brand		30
1184	11540	10119000114	Sensitive eyes drops drop btl 30	otc	generic		30
1185	-19954	31015808404	Sensodyne ex whtg pste 4 oz 120	otc	brand		120
1186	-26765	74098522703	Sentry senior 265	otc	generic		265
1187	31417	173052100	Serevent diskus 50 mcg UD blist pack 60	rx	brand		60
1188	64725	310028160	Seroquel xr 150 mg tablet bottle 60	rx	brand		60
1189	62748	310028260	Seroquel xr 200 mg tablet bottle 60	rx	brand		60
1190	62750	310028460	Seroquel xr 400 mg tablet bottle 60	rx	brand		60
1191	63240	310028060	Seroquel xr 50 mg tablet bottle 60	rx	brand		60
1192	46229	59762491004	Sertraline hcl 100 mg tablet bottle 100	rx	generic		100
1193	46229	59762491005	Sertraline hcl 100 mg tablet bottle 500	rx	generic		500
1194	46227	68180035109	Sertraline hcl 25 mg tablet bottle 90	rx	generic		90
1195	46228	59762490004	Sertraline hcl 50 mg tablet bottle 100	rx	generic		100
1196	46228	59762490005	Sertraline hcl 50 mg tablet bottle 500	rx	generic		500
1197	59211	59762003301	Sildenafil 20 mg tablet bottle 90	rx	generic		90
1198	16414	54838020940	Siltussin dm cough syrup Strawberry bottle 118	otc	generic		118
1199	22210	54838011740	Siltussin sa 100 mg/5 ml syr Strawberry bottle 118	otc	generic		118
1200	22210	54838011780	Siltussin sa 100 mg/5 ml syr Strawberry bottle 473	otc	generic		473
1201	7669	67877012425	Silver sulfadiazine 1% cream tube 25	rx	generic		25
1202	7669	67877012405	Silver sulfadiazine 1% cream tube 50	rx	generic		50
1203	70913	65414727	Simbrinza 1%-0.2% eye drops drop btl 8	rx	brand		8
1204	2821	51645086001	Simethicone 80 mg tab chew bottle 100	otc	generic		100
1205	-279372	22600008552	Simply saline 210ml aer  210	otc	brand		210
1206	16577	55111019805	Simvastatin 10 mg tablet bottle 500	rx	generic		500
1207	16578	55111019905	Simvastatin 20 mg tablet bottle 500	rx	generic		500
1208	16579	55111020005	Simvastatin 40 mg tablet bottle 500	rx	generic		500
1209	16576	55111019790	Simvastatin 5 mg tablet bottle 90	rx	generic		90
1210	40238	55111026890	Simvastatin 80 mg tablet bottle 90	rx	generic		90
1211	10195	8026420200	Skin-prep spray-on dressing squeez btl 118	otc	generic		118
1212	10319	8026420400	Skin-prep wipes box 50	otc	generic		50
1213	43912	121059516	Sod citrate-citric acid soln Grape bottle 473	rx	generic		473
1214	2661	536104710	Sodium bicarb 650 mg tablet bottle 1000	otc	generic		1000
1215	586	378698501	Sodium chloride 0.9% inhal vl UD vial 3	rx	generic		3
1216	586	487930201	Sodium chloride 0.9% inhal vl vial 5	rx	generic		5
1217	9827	409613803	Sodium chloride 0.9% irrig. bottle 500	rx	generic		500
1218	9827	409613822	Sodium chloride 0.9% irrig. flex cont 250	rx	generic		250
1219	7737	17478062235	Sodium chloride 5% eye oint tube 3.5	otc	generic		3
1220	-126661	31011902219	Soothe lg-lst drop 0.6 ml 0.6	otc	brand		0
1221	8612	54162070016	Sorbitol 70% solution bottle 474	rx	generic		474
1222	24097	603577021	Sotalol 120 mg tablet bottle 100	rx	generic		100
1223	13497	60505008100	Sotalol 160 mg tablet bottle 100	rx	generic		100
1224	50714	597007541	Spiriva 18 mcg cp-handihaler UD blist pack 30	rx	brand		30
1225	63164	597010061	Spiriva respimat 2.5 mcg inh aer w/adap 4	rx	brand		4
1226	6816	603576521	Spironolactone 100 mg tablet bottle 100	rx	generic		100
1227	6817	603576328	Spironolactone 25 mg tablet bottle 500	rx	generic		500
1228	-19664	30768001136	St johns wort extrct caps 150 150	otc	brand		150
1229	22850	8137008521	Steri-pad 2" x 2" n/a 25	otc	generic		25
1230	10273	68455010826	Stomahesive protective powd jar 28.3	otc	generic		28
1231	3009	536106210	Stool softener 100 mg softgel bottle 1000	otc	generic		1000
1232	2766	591078001	Sucralfate 1 gm tablet bottle 100	rx	generic		100
1233	9396	53746027205	Sulfamethoxazole-tmp ds tablet bottle 500	rx	generic		500
1234	9395	53746027101	Sulfamethoxazole-tmp ss tablet bottle 100	rx	generic		100
1235	9402	591079605	Sulfasalazine 500 mg tablet bottle 500	rx	generic		500
1236	9403	59762010401	Sulfasalazine dr 500 mg tab bottle 100	rx	generic		100
1237	9394	54879000716	Sulfatrim pediatric suspension Cherry bottle 473	rx	generic		473
1238	17129	55111029309	Sumatriptan succ 100 mg tablet UD blist pack 9	rx	generic		9
1239	-275105	81506600366	Sustenex probiot gummy frt 60 60	otc	brand		60
1240	62726	186037020	Symbicort 160-4.5 mcg inhaler aer w/adap 10.2	rx	brand		10
1241	62725	186037220	Symbicort 80-4.5 mcg inhaler aer w/adap 10.2	rx	brand		10
1242	6649	74455290	Synthroid 50 mcg tablet bottle 90	rx	brand		90
1243	15523	74659490	Synthroid 88 mcg tablet bottle 90	rx	brand		90
1244	52162	65042915	Systane 0.3-0.4% eye drops drop btl 15	otc	brand		15
1245	58288	65043132	Systane 0.3-0.4% eye drops UD box 28	otc	brand		28
1246	66795	65143302	Systane balance 0.6% eye drop drop btl 10	otc	brand		10
1247	67277	65045407	Systane gel eye drops drop btl 10	otc	brand		10
1248	64246	65050935	Systane nighttime eye oint tube 3.5	otc	brand		3
1249	-135704	30065143105	Systane ultra drop 10 ml 10	otc	generic		10
1250	13574	591247330	Tamoxifen 20 mg tablet UU bottle 30	rx	generic		30
1251	27546	57237001405	Tamsulosin hcl 0.4 mg capsule bottle 500	rx	generic		500
1252	7926	65041815	Tears naturale-ii eye drops drop btl 15	otc	brand		15
1253	-189479	44444053490	Ted b/knee w/i-t md/reg 18mm 7115 1	rx	brand		1
1254	-193381	44444053509	Ted b/knee w/i-t sm/long 18mm 7339 1	rx	brand		1
1255	-192112	44444053541	Ted knee-hi beige ct lg/reg 18mm 4289 1	rx	brand		1
1256	-192111	44444053540	Ted knee-hi beige ct md/long 18mm 4323 1	rx	brand		1
1257	-192110	44444053539	Ted knee-hi beige ct md/reg 18mm 4271 1	rx	brand		1
1258	-192113	44444053542	Ted knee-hi beige ct xl/reg 18mm 4296 1	rx	brand		1
1259	-201276	8099341600	Ted thigh hi med reg latex fr 3416lf 1	otc	brand		1
1260	22499	8333162609	Tegaderm 4"x4.75" dressing n/a 4	otc	generic		4
1261	-191565	871142808275	Tegaderm foam dress 4x4" 10 90601 1	rx	brand		1
1262	23002	8225621000	Telfa 2" x 3" sterile pad n/a 10	otc	generic		10
1263	-191836	44444053527	Telfa dressing 3x8 50 sterile 1238 1	rx	brand		1
1264	3689	378401001	Temazepam 15 mg capsule bottle 100	rx	generic	C-4	100
1265	19182	406996001	Temazepam 7.5 mg capsule bottle 100	rx	generic	C-4	100
1266	-13701	7467682001	Tennis elbow supt neobge sprt 1 ea 1	otc	brand		1
1267	22649	781205101	Terazosin 1 mg capsule bottle 100	rx	generic		100
1268	22652	781205401	Terazosin 10 mg capsule bottle 100	rx	generic		100
1269	22650	59746038406	Terazosin 2 mg capsule bottle 100	rx	generic		100
1270	22651	781205301	Terazosin 5 mg capsule bottle 100	rx	generic		100
1271	18273	51672208001	Terbinafine 1% cream tube 15	otc	generic		15
1272	45215	591321630	Testosterone 25 mg/2.5 gm pkt UD packet 2.5	rx	generic	C-3	2
1273	93	62332002531	Theophylline er 300 mg tab bottle 100	rx	generic		100
1274	2523	904053980	Thera tablet bottle 1000	otc	generic		1000
1275	24192	58790000115	Thera tears 0.25% eye drops drop btl 15	otc	brand		15
1276	30012	58790000032	Thera tears 0.25% eye drops UD blist pack 32	otc	brand		32
1277	62795	58790000228	Thera tears liquid gel UD blist pack 28	otc	brand		28
1278	53093	536466711	Therems-h tablet bottle 90	otc	brand		90
1279	39744	536466110	Therems-m tablet bottle 1000	otc	brand		1000
1280	3864	378061401	Thioridazine 25 mg tablet bottle 100	rx	generic		100
1281	3865	378061601	Thioridazine 50 mg tablet bottle 100	rx	generic		100
1282	3996	378501001	Thiothixene 10 mg capsule bottle 100	rx	generic		100
1283	7855	61314022605	Timolol 0.25% eye drops drop btl 5	rx	generic		5
1284	21400	25010081656	Timolol 0.25% gel-solution drop btl 5	rx	generic		5
1285	7856	17478028810	Timolol 0.5% eye drops drop btl 5	rx	generic		5
1286	21401	25010081756	Timolol 0.5% gel-solution drop btl 5	rx	generic		5
1287	30274	29300016915	Tizanidine hcl 4 mg tablet bottle 150	rx	generic		150
1288	7985	65064835	Tobradex eye ointment tube 3.5	rx	brand		3
1289	38588	24208029005	Tobramycin 0.3% eye drops drop btl 5	rx	generic		5
1290	7986	24208029505	Tobramycin-dexameth ophth susp drop btl 5	rx	generic		5
1291	-199370	9243715733	Toe separator asst sizes pedfx 1	otc	brand		1
1292	47328	93205056	Tolterodine tart er 2 mg cap bottle 30	rx	generic		30
1293	47327	93204998	Tolterodine tart er 4 mg cap bottle 90	rx	generic		90
1294	39139	59762080002	Tolterodine tartrate 2 mg tab bottle 60	rx	generic		60
1295	29837	69097012203	Topiramate 25 mg tablet bottle 60	rx	generic		60
1296	26169	68382013914	Topiramate 50 mg tablet bottle 60	rx	generic		60
1297	21407	50111091601	Torsemide 10 mg tablet bottle 100	rx	generic		100
1298	21409	31722053201	Torsemide 100 mg tablet bottle 100	rx	generic		100
1299	21408	50111091701	Torsemide 20 mg tablet bottle 100	rx	generic		100
1300	73567	24586903	Toujeo solostar 300 units/ml syringe 1.5	rx	brand		1
1301	64000	69024230	Toviaz er 4 mg tablet bottle 30	rx	brand		30
1302	64001	69024430	Toviaz er 8 mg tablet bottle 30	rx	brand		30
1303	67353	597014030	Tradjenta 5 mg tablet bottle 30	rx	brand		30
1304	23139	65162062710	Tramadol hcl 50 mg tablet bottle 100	rx	generic	C-4	100
1305	23139	65162062750	Tramadol hcl 50 mg tablet bottle 500	rx	generic	C-4	500
1306	60274	47335085983	Tramadol hcl er 100 mg tablet bottle 30	rx	generic	C-4	30
1307	4704	10019055301	Transderm-scop 1.5 mg/3 day box 10	rx	brand		10
1308	47612	65026025	Travatan z 0.004% eye drop drop btl 2.5	rx	brand		2
1309	46242	50111043402	Trazodone 100 mg tablet bottle 500	rx	generic		500
1310	46243	50111044101	Trazodone 150 mg tablet bottle 100	rx	generic		100
1311	46241	50111043302	Trazodone 50 mg tablet bottle 500	rx	generic		500
1312	7593	45802006336	Triamcinolone 0.025% cream tube 80	rx	generic		80
1313	7594	168000415	Triamcinolone 0.1% cream tube 15	rx	generic		15
1314	7594	67877025145	Triamcinolone 0.1% cream tube 454	rx	generic		454
1315	7594	168000480	Triamcinolone 0.1% cream tube 80	rx	generic		80
1316	7597	168000680	Triamcinolone 0.1% ointment jar 80	rx	generic		80
1317	21718	527163201	Triamterene-hctz 37.5-25 mg cp bottle 100	rx	generic		100
1318	-371568	1254600192	Trident gum mint bliss 12ct 1	otc	brand		1
1319	3852	378241001	Trifluoperazine 10 mg tablet bottle 100	rx	generic		100
1320	3853	378240201	Trifluoperazine 2 mg tablet bottle 100	rx	generic		100
1321	3854	378240501	Trifluoperazine 5 mg tablet bottle 100	rx	generic		100
1322	4582	603624121	Trihexyphenidyl 5 mg tablet bottle 100	rx	generic		100
1323	9497	591557101	Trimethoprim 100 mg tablet bottle 100	rx	generic		100
1324	-217450	1678411851	Triple anti oint 1oz tube 1185 29	otc	brand		29
1325	-257716	2739001184	Triple antibiotic oint .5oz dyna 1184 0.5	otc	brand		0
1326	38085	574014560	Trospium chloride 20 mg tablet bottle 60	rx	generic		60
1327	63466	591363630	Trospium chloride er 60 mg cap bottle 30	rx	generic		30
1328	6373	56151146304	True metrix glucose test strip box 50	otc	generic		50
1329	70793	56151161051	Trueplus glucose 4 gm tab chew bottle 50	otc	brand		50
1330	-140465	31254710504	Tucks pada 40 ea 40	otc	brand		40
1331	2690	766073966	Tums e-x tablet chewable Assorted bottle 96	otc	brand		96
1332	64829	64764091830	Uloric 40 mg tablet bottle 30	rx	brand		30
1333	64830	64764067730	Uloric 80 mg tablet bottle 30	rx	brand		30
1334	2489	536475011	Unicomplex-m tablet bottle 90	otc	generic		90
1335	3707	74300000623	Unisom sleep aid 25 mg tablet bottle 48	otc	brand		48
1336	-190739	61678434542	Unna boot w/zinc 4"x 10yd dyn 3454 1	rx	brand		1
1337	23109	68455010763	Unna-flex elastic unnaboot box 1	otc	generic		1
1338	24333	591236801	Ursodiol 250 mg tablet bottle 100	rx	generic		100
1339	3095	591315901	Ursodiol 300 mg capsule bottle 100	rx	generic		100
1340	65966	169517603	Vagifem 10 mcg vaginal tab UD blist pack 8	rx	brand		8
1341	48809	11509000367	Vagisil cream tube 28	otc	brand		28
1342	23989	63304090430	Valacyclovir hcl 500 mg tablet bottle 30	rx	generic		30
1343	4536	832031011	Valproic acid 250 mg capsule bottle 100	rx	generic		100
1344	48400	31722074790	Valsartan 160 mg tablet bottle 90	rx	generic		90
1345	48399	31722074890	Valsartan 320 mg tablet bottle 90	rx	generic		90
1346	50805	31722074530	Valsartan 40 mg tablet bottle 30	rx	generic		30
1347	38925	591231619	Valsartan-hctz 160-12.5 mg tab bottle 90	rx	generic		90
1348	50256	68180010509	Valsartan-hctz 160-25 mg tab bottle 90	rx	generic		90
1349	7076	45334030001	Vanicream skin cream jar 453	otc	generic		453
1350	5894	521275000	Vaseline lip therapy oint tube 10	otc	brand		10
1351	8482	521232600	Vaseline petroleum jelly tube 112.5	otc	generic		112
1352	8482	521231100	Vaseline petroleum jelly tube 52.5	otc	generic		52
1353	386	187014330	Vasotec 20 mg tablet bottle 30	rx	brand		30
1354	-457545	8103260127	Vee papertowel 150 2ply sheet singl 30cs 1	otc	brand		1
1355	65871	58980078021	Venelex ointment tube 60	rx	brand		60
1356	46399	93738001	Venlafaxine hcl 37.5 mg tablet bottle 100	rx	generic		100
1357	46400	93738101	Venlafaxine hcl 50 mg tablet bottle 100	rx	generic		100
1358	46401	93738201	Venlafaxine hcl 75 mg tablet bottle 100	rx	generic		100
1359	46405	65862069705	Venlafaxine hcl er 150 mg cap bottle 500	rx	generic		500
1360	46403	93738498	Venlafaxine hcl er 37.5 mg cap bottle 90	rx	generic		90
1361	46404	93738598	Venlafaxine hcl er 75 mg cap bottle 90	rx	generic		90
1362	28090	173068220	Ventolin hfa 90 mcg inhaler canister 18	rx	brand		18
1363	26486	591288601	Verapamil 360 mg cap pellet bottle 100	rx	generic		100
1364	57983	51248015103	Vesicare 10 mg tablet bottle 90	rx	brand		90
1365	57982	51248015003	Vesicare 5 mg tablet bottle 90	rx	brand		90
1366	64678	57141000496	Viactiv soft chew Caramel can 60	otc	brand		60
1367	64678	57141000497	Viactiv soft chew Chocolate can 60	otc	brand		60
1368	74655	61874010060	Viberzi 100 mg tablet bottle 60	rx	brand	C-4	60
1369	63554	23900000362	Vicks vaporub ointment jar 100	otc	brand		100
1370	63554	23900000361	Vicks vaporub ointment jar 50	otc	brand		50
1371	65344	169406013	Victoza 3-pak 18 mg/3 ml pen syringe 3	rx	brand		3
1372	52050	65401303	Vigamox 0.5% eye drops drop btl 3	rx	brand		3
1373	67376	456111030	Viibryd 10 mg tablet bottle 30	rx	brand		30
1374	67377	456112030	Viibryd 20 mg tablet bottle 30	rx	brand		30
1375	64435	131248035	Vimpat 200 mg tablet bottle 60	rx	brand	C-5	60
1376	48812	74300000401	Visine allergy relief drop drop btl 15	otc	brand		15
1377	7771	74300000803	Visine original 0.05% eye drop drop btl 15	otc	brand		15
1378	2169	51991060401	Vit d2 1.25 mg (50-000 unit) bottle 100	rx	generic		100
1379	-311029	27917002268	Vitafusion womens chew 70 ea 70	otc	brand		70
1380	64809	168003504	Vitamin a & d ointment jar 113.4	otc	generic		113
1381	64809	168003501	Vitamin a & d ointment tube 56.7	otc	generic		56
1382	-257733	2739001152	Vitamin a&d oint 1oz tube dyna 1152 1	otc	brand		1
1383	2064	536478701	Vitamin b complex capsule bottle 100	otc	generic		100
1384	2451	536468001	Vitamin b-1 100 mg tablet bottle 100	otc	generic		100
1385	2455	536467801	Vitamin b-1 50 mg tablet bottle 100	otc	generic		100
1386	2436	74312000640	Vitamin b-2 100 mg tablet bottle 100	otc	generic		100
1387	2421	536440901	Vitamin b-6 100 mg tablet bottle 100	otc	generic		100
1388	64302	904615760	Vitamin d3 2-000 unit tablet bottle 100	otc	generic		100
1389	19166	74312001140	Vitamin d3 400 unit tablet bottle 100	otc	generic		100
1390	48770	536479901	Vitamin e 400 unit capsule bottle 100	otc	generic		100
1391	18293	63481068447	Voltaren 1% gel tube 100	rx	brand		100
1392	74808	61874013030	Vraylar 3 mg capsule bottle 30	rx	brand		30
1393	57863	66582031231	Vytorin 10-20 mg tablet bottle 30	rx	brand		30
1394	57865	66582031331	Vytorin 10-40 mg tablet bottle 30	rx	brand		30
1395	-315104	4447001212	Walker tray 1212 1ea 1	otc	brand		1
1396	-31652	4129805078	Wash basin dshp each 1 ea 1	otc	generic		1
1397	46172	65597070118	Welchol 625 mg tablet bottle 180	rx	brand		180
1398	-13677	7467630601	Wrist splint 1sz bge sprt 1 ea 1	otc	brand		1
1399	64493	50458058030	Xarelto 10 mg tablet bottle 30	rx	brand		30
1400	68118	50458057830	Xarelto 15 mg tablet bottle 30	rx	brand		30
1401	68119	50458057930	Xarelto 20 mg tablet bottle 30	rx	brand		30
1402	76360	54092060601	Xiidra 5% eye drops vial 60	rx	brand		60
1403	-327244	30065401105	Zaditor 25 eye drp 5ml 5	otc	brand		5
1404	62537	73462015045	Zeasorb powder bottle 71	otc	brand		71
1405	65703	42865030302	Zenpep dr 20-000 units capsule bottle 100	rx	brand		100
1406	51214	66582041454	Zetia 10 mg tablet bottle 90	rx	brand		90
1407	1671	536667101	Zinc chelated 50 mg tablet bottle 100	otc	brand		100
1408	-289111	61678411913	Zinc oxide 2oz tube 1191 1	otc	brand		1
1409	65587	17478060930	Zioptan 0.0015% eye drops UD blist pack 30	rx	brand		30
1410	47568	59762200401	Ziprasidone hcl 80 mg capsule bottle 60	rx	generic		60
1411	47568	378735391	Ziprasidone hcl 80 mg capsule bottle 60	rx	generic		60
1412	19187	781531701	Zolpidem tartrate 5 mg tablet bottle 100	rx	generic	C-4	100
1413	53367	378672501	Zonisamide 25 mg capsule bottle 100	rx	generic		100
1414	48415	50580072824	Zyrtec-d tablet UD blist pack 24	rx	brand		24
1415	6373	65702040810	Accu-chek aviva plus test strp box 100	otc	generic		100
1416	11186	65702028810	Accu-chek fastclix lancets box 102	otc	generic		102
1417	-282019	5113120378	Ace knee elastopreene lg/xlg 1	otc	brand		1
1418	4489	57896010101	Acetaminophen 325 mg tablet bottle 100	otc	generic		100
1419	70603	74312002610	Acidophilus probiotic tablet bottle 100	otc	generic		100
1420	40941	62856024330	Aciphex dr 20 mg tablet bottle 30	rx	brand		30
1421	-272941	41167009680	Act total cr 18oz liq  1	otc	brand		1
1422	-152709	4116709650	Act total cre i/cln/mint 18oz 18	otc	brand		18
1423	-78081	87057900501	Activon arthrtpcl 2oz 1	otc	generic		1
1424	16408	60505530601	Acyclovir 400 mg tablet bottle 100	rx	generic		100
1425	63528	10119002013	Advanced eye relief opth oint tube 3.5	otc	brand		3
1426	13556	573016920	Advil 200 mg liqui-gel capsule bottle 20	otc	brand		20
1427	13556	573016930	Advil 200 mg liqui-gel capsule bottle 40	otc	brand		40
1428	-359826	44224103001	After bite 5% liqd 14 ml 14	otc	brand		14
1429	-3200	869105610	Alcohol ethyl 70% soln 480 ml 480	otc	generic		480
1430	46941	93517144	Alendronate sodium 70 mg tab UD UU blist pack 4	rx	generic		4
1431	21980	25866010505	Aleve 220 mg tablet bottle 100	otc	brand		100
1432	21980	25866010501	Aleve 220 mg tablet bottle 24	otc	brand		24
1433	-159965	1650053759	Alka-seltz+ cold orange 20 20	otc	brand		20
1434	58869	41167043205	Allegra-d 24 hour tablet UD blist pack 10	rx	brand		10
1435	-19792	30997366400	Almay clrgl ff stck 2.25 oz 67	otc	brand		67
1436	-19725	30997130997	Almay snstv ff roll 1.5 oz 45	otc	brand		45
1437	3775	67253090210	Alprazolam 1 mg tablet bottle 100	rx	generic	C-4	100
1438	16925	603210825	Amlodipine besylate 2.5 mg tab bottle 300	rx	generic		300
1439	50519	65862058601	Amlodipine-benazepril 10-20 mg bottle 100	rx	generic		100
1440	-373880	8019652856	Antifungal powd 3oz msc092603 3	otc	brand		3
1441	59985	88250033	Apidra 100 units/ml vial vial 10	rx	brand		10
1442	52898	31722082030	Aripiprazole 5 mg tablet bottle 30	rx	generic		30
1443	-19541	30696051154	Arnica gel 45gm 45	otc	generic		45
1444	-411343	30696203559	Arnicare arnica gel 75 gm 75	otc	brand		75
1445	73198	85433301	Asmanex hfa 100 mcg inhaler canister 13	rx	brand		13
1446	16995	603002622	Aspirin ec 81 mg tablet bottle 120	otc	generic		120
1447	29967	60505257808	Atorvastatin 10 mg tablet bottle 1000	rx	generic		1000
1448	45772	60505267109	Atorvastatin 80 mg tablet bottle 90	rx	generic		90
1449	29893	54029399	Azelastine 0.1% (137 mcg) spry squeez btl 30	rx	generic		30
1450	72029	74312003595	B-12 500 mcg quick dissolve tb Cherry bottle 100	otc	generic		100
1451	-26912	74312000650	B-6  bnus 100mg tabs 2x100 200	otc	brand		200
1452	7731	45802006001	Bacitracin 500 unit/gm ointmnt tube 14	otc	generic		14
1453	-49767	38137005567	Band-aid antibio ex/lg #5567 8	otc	brand		8
1454	23048	8137004444	Band-aid flex fab bandage box 100	otc	generic		100
1455	10523	8137004668	Band-aid sheer strip n/a 60	otc	generic		60
1456	64958	81370004424	Band-aid tough-strips box 10	otc	generic		10
1457	-368625	38137116138	Bandaid fa rlld gze 3inx2.5yds 1	otc	brand		1
1458	-275001	4133300279	Battery dur h/a da312b8zm10 8	otc	brand		8
1459	-275007	4133300433	Battery dur h/a da675b6zm10 6	otc	brand		6
1460	-9199	4133310810	Battery dur lith wch dl1216 each 1 ea 1	otc	generic		1
1461	-277467	3980010964	Battery ener w/c m-fr 377bpz 1	otc	brand		1
1462	-277442	3980011071	Battery ener w/c m-fr 379bpz 1	otc	brand		1
1463	-160123	280210030	Bayer asp regim tab 81mg 300 300	otc	brand		300
1464	-20316	31284350090	Bayer women asa/ca 81-300 cplt 60 ea 60	otc	brand		60
1465	68057	8290329515	Bd autoshield duo ndl 5mmx30g box 100	otc	generic		100
1466	10926	8290323487	Bd home sharps container packet 1	otc	generic		1
1467	9797	8290326895	Bd single use swab box 100	otc	generic		100
1468	49240	8290320109	Bd ultra-fine pen ndl 8mmx31g box 100	otc	generic		100
1469	59179	74300000536	Bengay ultra strength crm jar 113	otc	brand		113
1470	50290	65597010430	Benicar 40 mg tablet bottle 30	rx	brand		30
1471	52835	65597010790	Benicar hct 40-25 mg tablet bottle 90	rx	brand		90
1472	4641	65162053610	Benzonatate 100 mg capsule bottle 100	rx	generic		100
1473	36423	67405083506	Benzoyl peroxide 6% cleanser bottle 170.3	rx	generic		170
1474	-339527	135055701	Biotene 0.15% pste 121.9 gm 121	otc	brand		121
1475	60256	41388021041	Blistex medicated lip ointment tube 10	otc	brand		10
1476	-394499	65197027516	Bonine motion sickness tab16 16	otc	brand		16
1477	58916	50383024971	Bromfenac sodium 0.09% eye drp drop btl 1.7	rx	generic		1
1478	61579	63402091130	Brovana 15 mcg/2 ml solution vial 2	rx	brand		2
1479	46526	591376830	Budesonide 0.5 mg/2 ml susp UD ampul 2	rx	generic		2
1480	46236	781105301	Bupropion hcl 75 mg tablet bottle 100	rx	generic		100
1481	-26945	74312001510	C-500 synth 500mg tabs 100 100	otc	brand		100
1482	-5382	2270005529	C/g contn colr l/s mauve each 1 ea 1	otc	generic		1
1483	13109	536342608	Calcium 600 mg tablet bottle 60	otc	generic		60
1484	-457790	84009310583	Calcium citrate max/d3 tab 100 100	otc	brand		100
1485	-358541	55056711	Caltrate 600+d3+min chew 90 ea 90	otc	brand		90
1486	-39658	70610044030	Caps 30-60dr ppsc3060  kerr 100	otc	generic		100
1487	16133	58914017014	Carafate 1 gm/10 ml susp Cherry bottle 420	rx	brand		420
1488	19293	65862014505	Carvedilol 25 mg tablet bottle 500	rx	generic		500
1489	28108	65862014205	Carvedilol 3.125 mg tablet bottle 500	rx	generic		500
1490	28109	65862014305	Carvedilol 6.25 mg tablet bottle 500	rx	generic		500
1491	16931	65862009520	Cefpodoxime 100 mg tablet bottle 20	rx	generic		20
1492	67736	63824073016	Cepacol sore throat lozenge blist pack 16	otc	brand		16
1493	9042	68180012101	Cephalexin 250 mg capsule bottle 100	rx	generic		100
1494	17037	51660093830	Cetirizine hcl 10 mg tablet bottle 30	otc	generic		30
1495	-250609	2270012232	Cg m/up liq cln clssc bge n 130 p&g 1	rx	brand		1
1496	-2601	573190024	Chapstick reg blstpk stck 24 ea 24's 1	otc	generic		1
1497	-2607	573191524	Chapstick strwb blstpk stck 24 ea 24	otc	generic		24
1498	4484	54838014480	Children's silapap elixir Cherry bottle 473	otc	generic		473
1499	16026	78112001103	Chloraseptic sore throat spray Cherry bottle 177	otc	brand		177
1500	16026	78112001104	Chloraseptic sore throat spray Menthol bottle 177	otc	brand		177
1501	-145159	1650053536	Citracal maximum 180 ct byr 180	rx	brand		180
1502	3025	869068638	Citroma solution bottle 296	otc	generic		296
1503	-59972	4110080602	Claritin redi 10 10mg 1	otc	generic		1
1504	7634	472040030	Clobetasol 0.05% cream tube 30	rx	generic		30
1505	38164	65862035705	Clopidogrel 75 mg tablet bottle 500	rx	generic		500
1506	-16717	9110810013	Cold-eeze  chry box 10mg lozg 18 18	otc	generic		18
1507	-458152	9110830014	Cold-eeze loz tropical orange box 18 18	otc	brand		18
1508	-16718	9110810014	Cold-eeze tropfrt 10mg lozg 18 18	otc	generic		18
1509	-451993	9110830013	Coldeeze loz cherry 18ct 1	otc	brand		1
1510	22344	115521116	Colestipol hcl 1 gm tablet bottle 120	rx	generic		120
1511	-410555	35000051105	Colgate cvt prt 0.76% 70gm 1	otc	brand		1
1512	-7222	3500074102	Colgate total frsh stripe pste 7.8 oz 234	otc	brand		234
1513	65329	32121201	Creon dr 12-000 units capsule UU bottle 100	rx	brand		100
1514	65330	32122407	Creon dr 24-000 units capsule UU bottle 250	rx	brand		250
1515	65328	32120607	Creon dr 6-000 units capsule UU bottle 250	rx	brand		250
1516	-426373	3700094778	Crest 3d white paste artic fresh 4.8oz 1	otc	brand		1
1517	-296583	37000081280	Crest 3d wht gl/w pste 4.1 oz 116	otc	brand		116
1518	-410769	37000094621	Crest pro-health 0.454% 104gm 1	otc	brand		1
1519	-37622	37000000321	Crest reg tube pste 6.4 oz 192	otc	brand		192
1520	51786	310075430	Crestor 40 mg tablet bottle 30	rx	brand		30
1521	-38138	1904510541	Curel ultra healing lotn 13 oz 390	otc	generic		390
1522	9797	8225233300	Curity alcohol swabs box 200	otc	generic		200
1523	-3668	1158847430	Cuticura mdct orig orig soap 3oz 90	otc	generic		90
1524	47478	378077101	Cyclobenzaprine 5 mg tablet bottle 100	rx	generic		100
1525	7875	24208073501	Cyclopentolate 1% eye drops drop btl 2	rx	generic		2
1526	22097	10158009206	Dentu-creme tube 110	otc	generic		110
1527	-275508	3600013792	Depend gds men bns pada 96+8ea 48	otc	brand		48
1528	-394403	3600042660	Depend silhouet act w s/m3 1	otc	brand		1
1529	-406801	3600043616	Depend underwear maximum men s/m 17 cs/2 1	otc	brand		1
1530	-4502	1700002410	Dial bath wht 3pk soap 3pk 3	otc	generic		3
1531	3768	51862006301	Diazepam 5 mg tablet bottle 100	rx	generic	C-4	100
1532	8373	16571020210	Diclofenac sod ec 50 mg tab bottle 100	rx	generic		100
1533	18	527132401	Digox 125 mcg tablet bottle 100	rx	generic		100
1534	16571	68682099798	Diltiazem 24hr er 240 mg cap bottle 90	rx	generic		90
1535	4540	29300014001	Divalproex sod dr 500 mg tab bottle 100	rx	generic		100
1536	-368753	85707400177	Domeboro pwd pkts 2.2gm 12 1	otc	brand		1
1537	29334	43547027609	Donepezil hcl 10 mg tablet bottle 90	rx	generic		90
1538	29335	43547027509	Donepezil hcl 5 mg tablet bottle 90	rx	generic		90
1539	-190266	75475658000	Donut w/cover convoluted 18" n8000 1	rx	brand		1
1540	39531	591248279	Dorzolamide-timolol eye drops drop btl 10	rx	generic		10
1541	-3519	1111161195	Dove soap pink-rosa soap 2x4.75oz 2's 4	otc	generic		4
1542	-33357	1111161133	Dove soap scnt wht soap 2x4.75 286	otc	generic		286
1543	60942	66993081530	Doxycycline ir-dr 40 mg cap bottle 30	rx	generic		30
1544	-405157	89238300257	Dreambone chicken dog treat med 4 1	otc	brand		1
1545	-405156	89238300250	Dreambone chicken mini dog treat 8 1	otc	brand		1
1546	57891	60505299506	Duloxetine hcl dr 20 mg cap bottle 60	rx	generic		60
1547	57893	60505299703	Duloxetine hcl dr 60 mg cap bottle 30	rx	generic		30
1548	-178524	74912333334	Duracell batt in gnrc pkging aaa 4ct 4	rx	brand		4
1549	8120	904322035	Ear drops 6.5% drop btl 15	otc	generic		15
1550	-28258	79450311201	Earplanes adult each 1	otc	brand		1
1551	4381	49692090377	Ecotrin 325 mg tablet ec bottle 125	otc	brand		125
1552	-398706	81483201587	Efferdent org 102 tab 1	otc	brand		1
1553	385	64679092302	Enalapril maleate 2.5 mg tab bottle 100	rx	generic		100
1554	47526	43598051090	Esomeprazole mag dr 40 mg cap bottle 90	rx	generic		90
1555	-95497	2507716971	Ester c 500mg coated tablets 90	otc	generic		90
1556	23471	47781020904	Estradiol 0.1 mg/day patch box 4	rx	generic		4
1557	5349	10356009004	Eucerin creme jar 120	otc	brand		120
1558	11677	61442012110	Famotidine 20 mg tablet bottle 1000	rx	generic		1000
1559	11678	65862086001	Famotidine 40 mg tablet bottle 100	rx	generic		100
1560	61200	68382027016	Fenofibrate 145 mg tablet bottle 90	rx	generic		90
1561	1645	49483006310	Ferrous sulfate 325 mg tablet bottle 1000	otc	generic		1000
1562	-144923	6799050054	Finesse h/s ex hold nonaero 7z lnm 7	rx	brand		7
1563	-26774	74098522731	Fish oil 1000mg enteric coated 90	otc	generic		90
1564	7502	66530024940	Fluorouracil 5% cream tube 40	rx	generic		40
1565	46213	65862019201	Fluoxetine hcl 10 mg capsule bottle 100	rx	generic		100
1566	46757	93744656	Fluvastatin er 80 mg tablet bottle 30	rx	generic		30
1567	60558	15821010115	Freshkote eye drops drop btl 15	otc	brand		15
1568	8209	781196610	Furosemide 40 mg tablet bottle 1000	rx	generic		1000
1569	21413	69097081312	Gabapentin 100 mg capsule bottle 500	rx	generic		500
1570	21415	69097081512	Gabapentin 400 mg capsule bottle 500	rx	generic		500
1571	22136	47046300000	Garlique 400 mg tablet ec bottle 30	otc	generic		30
1572	64246	78047397	Genteal pm ointment tube 3.5	otc	brand		3
1573	-412656	30065041663	Genteal tears pres free 36 1	otc	brand		1
1574	-370586	31170104126	Gentle rain bodywash x-mild 21oz 7233 21	otc	brand		21
1575	-10760	4740024040	Gill foamy shave reg foam 11 oz 330	otc	brand		330
1576	-23294	47400145001	Gill foamy shave reg foam 2 oz 60	otc	brand		60
1577	-10759	4740024030	Gill foamy shave reg foam 6.25 oz 188	otc	brand		188
1578	-10719	4740017970	Gill mach3 blade each 4	otc	brand		4
1579	-10686	4740013070	Gill series shave cl snstv gel 7 oz 210	otc	brand		210
1580	-10703	4740014129	Gill venus blade each 4 ea 4	otc	brand		4
1581	43463	591090030	Glipizide er 2.5 mg tablet bottle 30	rx	generic		30
1582	-408975	653195002603	Glove exam latex p/f ex small 100	otc	brand		100
1583	-25016	64193211111	Glove latex pwd smth sml glov 100 ea 100	otc	generic		100
1584	54503	40985022481	Glucosamine-chondroitin tablet bottle 150	otc	generic		150
1585	-278760	62214001646	Gm1 sponges hvyduty scrng 2pk 1	otc	brand		1
1586	59776	41167001040	Gold bond medicated body powd bottle 113	otc	brand		113
1587	45669	121077516	Guaifenesin-codeine syrup Cherry bottle 473	rx	generic	C-5	473
1588	58112	12546062544	Halls 5.8 mg drops bag 25	otc	brand		25
1589	-38158	3700006194	Head & shldr clscn 1% shmp 420 ml 420	otc	brand		420
1590	-47352	3400021210	Hershey miniatures peg pack 1	otc	generic		1
1591	444	19550541	Hexabrix 39.3/19.6 bottle bottle 75	rx	brand		75
1592	-7928	3566419402	Hose jbst kh 15-20md sbge hose 2	otc	brand		2
1593	-298839	3600031803	Huggies wipes nc ff soft pk 1	otc	brand		1
1594	-20607	32878583502	Humidifier wicking filter each 1 ea 1	otc	brand		1
1595	284	31722051901	Hydralazine 10 mg tablet bottle 100	rx	generic		100
1596	286	68462034201	Hydralazine 25 mg tablet bottle 100	rx	generic		100
1597	7544	603053550	Hydrocortisone 1% cream tube 28.4	rx	generic		28
1598	9580	591304101	Hydroxychloroquine 200 mg tab bottle 100	rx	generic		100
1599	-22861	44033700520	Ice it 6x9in pack 1	otc	brand		1
1600	52742	41167000841	Icy hot medicated patch box 5	otc	brand		5
1601	-32834	30045013404	Imodium a-d      1mg /7.5ml liqd 4 oz 120	otc	brand		120
1602	48990	50580033842	Imodium multi-symptom rel cplt bottle 42	otc	brand		42
1603	8337	378014701	Indomethacin 50 mg capsule bottle 100	rx	generic		100
1604	70792	50458014130	Invokana 300 mg tablet bottle 30	rx	brand		30
1605	48018	487020103	Iprat-albut 0.5-3(2.5) mg/3 ml UD vial 3	rx	generic		3
1606	61614	6027731	Januvia 100 mg tablet bottle 30	rx	brand		30
1607	-73822	1910011006	Jergens lot age defying mv16 8 1	otc	brand		1
1608	-367021	38137116125	Jj bandaid fa gauze pad 3 x 3" 10 1	otc	brand		1
1609	-367024	38137116143	Jj bandaid fa nonstick pad 3 x 4" 10 1	otc	brand		1
1610	5517	8137003021	Johnson's baby powder jar 270	otc	brand		270
1611	-12186	7061014020	Kerr prfpk 20dr  xxx no cap rxvl 200 ea 200	otc	brand		200
1612	7334	168009930	Ketoconazole 2% cream tube 30	rx	generic		30
1613	-249060	3600011974	Kleenex pocket pack each 24x8ea 8	otc	brand		8
1614	22345	66758017001	Klor-con m10 tablet bottle 100	rx	generic		100
1615	22550	29300011305	Lamotrigine 150 mg tablet bottle 500	rx	generic		500
1616	17872	29300011101	Lamotrigine 25 mg tablet bottle 100	rx	generic		100
1617	27370	61314054703	Latanoprost 0.005% eye drops drop btl 2.5	rx	generic		2
1618	-3512	1111132524	Lever 2000 soap reg soap 2 ea 2	otc	generic		2
1619	29927	781579050	Levofloxacin 250 mg tablet bottle 50	rx	generic		50
1620	20176	527163801	Levothyroxine 137 mcg tablet bottle 100	rx	generic		100
1621	6650	527134310	Levothyroxine 75 mcg tablet bottle 1000	rx	generic		1000
1622	43256	591352530	Lidocaine 5% patch box 30	rx	generic		30
1623	43256	63481068706	Lidoderm 5% patch box 30	rx	brand		30
1624	5693	501100071	Listerine antiseptic bottle 1000	otc	brand		1000
1625	5693	501100171	Listerine antiseptic bottle 1000	otc	brand		1000
1626	-20284	31254743455	Listerine esntl care reg gel 4.2 oz 126	otc	brand		126
1627	-249172	31254742834	Listerine zero clmt mwsh 1500 ml 1500	otc	brand		1500
1628	66350	66869020490	Livalo 2 mg tablet bottle 90	rx	brand		90
1629	3757	69315090405	Lorazepam 0.5 mg tablet bottle 500	rx	generic	C-4	500
1630	38686	13668011590	Losartan potassium 100 mg tab bottle 90	rx	generic		90
1631	23381	13668011390	Losartan potassium 25 mg tab bottle 90	rx	generic		90
1632	23382	13668040990	Losartan potassium 50 mg tab bottle 90	rx	generic		90
1633	40923	13668011890	Losartan-hctz 100-25 mg tab bottle 90	rx	generic		90
1634	6460	68180046801	Lovastatin 20 mg tablet bottle 100	rx	generic		100
1635	16995	280210020	Low dose aspirin ec 81 mg tab bottle 200	otc	generic		200
1636	72191	52800048816	Lubriderm daily moisture lot bottle 177	otc	brand		177
1637	-82344	5280048268	Lubriderm inskpr bdy liqd 16 oz 473	otc	brand		473
1638	65392	23320508	Lumigan 0.01% eye drops drop btl 7.5	rx	brand		7
1639	-6980	3373200011	Macks ear seals flange each 2 ea 2	otc	brand		2
1640	-237199	78148525007	Max-freeze roll 89 ml 89	otc	brand		89
1641	4731	59746012206	Meclizine 12.5 mg tablet bottle 100	rx	generic		100
1642	48015	54629061500	Melatonin 5 mg tablet sl bottle 60	otc	generic		60
1643	29157	29300012501	Meloxicam 15 mg tablet bottle 100	rx	generic		100
1644	29156	29300012401	Meloxicam 7.5 mg tablet bottle 100	rx	generic		100
1645	32492	27241007106	Memantine hcl 10 mg tablet bottle 60	rx	generic		60
1646	-51755	22200954402	Menn lady spd stkpwd/fr     2.3oz 1	otc	generic		1
1647	6064	10742000102	Mentholatum ointment jar 90	otc	brand		90
1648	46754	53746017805	Metformin hcl er 500 mg tablet bottle 500	rx	generic		500
1649	45311	591079021	Methylprednisolone 4 mg dosepk UU dose-pack 21	rx	generic		21
1650	5131	378004701	Metoprolol tartrate 100 mg tab bottle 100	rx	generic		100
1651	41798	51672411606	Metronidazole topical 0.75% gl tube 45	rx	generic		45
1652	7003	472073063	Miconazole nitrate 2% cream tube/kit 45	otc	generic		45
1653	31061	19810003404	Mineral ice gel jar 105	otc	brand		105
1654	38451	69452010519	Montelukast sod 10 mg tablet bottle 90	rx	generic		90
1655	71885	63824000820	Mucinex er 600 mg tablet bottle 20	otc	brand		20
1656	32599	67877022501	Mycophenolate 500 mg tablet bottle 100	rx	generic		100
1657	-75243	7431213329	N/b fish oil sftgl 1200mg 100 24's 100	otc	generic		100
1658	-39201	74312017568	N/b vit e dl-a sfg 4ciu 120+80 24's 200	otc	generic		200
1659	16574	68462035801	Nabumetone 500 mg tablet bottle 100	rx	generic		100
1660	58996	456320212	Namenda 2 mg/ml solution bottle 360	rx	brand		360
1661	70870	456342833	Namenda xr 28 mg capsule bottle 30	rx	brand		30
1662	73815	456122830	Namzaric 28 mg-10 mg capsule bottle 30	rx	brand		30
1663	-356523	4116758003	Nasacort otc 0.37oz spy 10.8	otc	brand		10
1664	-155408	7431200139	Nb alpha lipoic acid capl200mg 30	otc	brand		30
1665	-39680	7431207961	Nb biotin tab 1000mcg 100	otc	brand		100
1666	-81490	74312278006	Nb fish oil each 1x100ea 1	otc	generic		1
1667	48559	24208063562	Neomycin-polymyxin-hc ear susp drop btl 10	rx	generic		10
1668	-16172	8680068785	Neut ult sheer s/b spf30 lotn 3 oz 90	otc	brand		90
1669	-11216	5113191382	Nexcare tape flex 1x10yd tape 2 ea 2	otc	brand		2
1670	-11218	5113191386	Nexcare tape ppr carded tape 2 ea 2	otc	brand		2
1671	62245	186402001	Nexium dr 20 mg packet packet 30	rx	brand		30
1672	20617	24979001001	Nifedipine er 60 mg tablet bottle 100	rx	generic		100
1673	9430	47781030701	Nitrofurantoin mcr 50 mg cap bottle 100	rx	generic		100
1674	46060	51672400202	Nortriptyline hcl 25 mg cap bottle 500	rx	generic		500
1675	65423	42192033001	Np thyroid 60 mg tablet bottle 100	rx	generic		100
1676	-414657	40093010523	Nt essential oil peppermint 2oz 1	otc	brand		1
1677	-294129	4390097551	Nutrisour fib pwd can 7.2z cs4 7.2	otc	brand		7
1678	-282494	323900014244	Nyquil liquid cold/flu orig 1	otc	brand		1
1679	7282	45802005935	Nystatin 100-000 unit/gm cream tube 15	rx	generic		15
1680	7282	45802005911	Nystatin 100-000 unit/gm cream tube 30	rx	generic		30
1681	-173454	89866900201	Oasis mst dry/mth spr mint1oz 30	otc	brand		30
1682	-296435	4138806709	Odor-eaters ult durab ins 1pr 1	otc	brand		1
1683	48292	24208041005	Ofloxacin 0.3% ear drops drop btl 5	rx	generic		5
1684	-27463	75609000719	Olay actv hydrt reg crm 2 oz 60	otc	brand		60
1685	-127018	1204401069	Old sp a/s classic liqd 4.25 oz 125	otc	brand		125
1686	-275303	3700026790	Old sp b/w fresh liqd 16 oz 473	otc	brand		473
1687	-8412	3700039316	Old sp bodywash pure sport liqd 18 oz 540	otc	generic		540
1688	70275	16500055110	One-a-day men's 50 plus tablet bottle 120	otc	brand		120
1689	63891	10310031805	Orajel mouth sore gel tube 9.4	otc	brand		9
1690	-33583	6905583834	Oral-b crsact pwrhdl sft each 1 ea 1	otc	brand		1
1691	44598	536410301	Oysco d tablet bottle 100	otc	generic		100
1692	-247713	8087804233	Pant sham col/p volume 12.6oz 12.6	otc	brand		12
1693	8444	536650601	Papaya enzyme tablet Fruit bottle 100	otc	generic		100
1694	46225	43547035003	Paroxetine hcl 40 mg tablet bottle 30	rx	generic		30
1695	4521	62756040201	Phenytoin sod ext 100 mg cap bottle 100	rx	generic		100
1696	63337	60258000601	Phos-nak packet Fruit packet 100	otc	brand		100
1697	68503	50222050247	Picato 0.015% gel tube 3	rx	brand		3
1698	7822	61314020315	Pilocarpine 1% eye drops drop btl 15	rx	generic		15
1699	42943	16729002010	Pioglitazone hcl 15 mg tablet bottle 30	rx	generic		30
1700	10362	36000033594	Poise pads bag 48	otc	generic		48
1701	-298335	10158034438	Polident overnight tab 40	otc	brand		40
1702	-192715	8281503100	Poncho rain adult 1	rx	brand		1
1703	-39054	305210044005	Ponds dry skin crm 6 5oz 04400 1	otc	brand		1
1704	65955	44523041501	Potassium citrate er 15 meq tb UU bottle 100	rx	generic		100
1705	41055	74312001110	Potassium gluconate 595 caplet bottle 100	otc	generic		100
1706	66781	597013554	Pradaxa 150 mg capsule UU bottle 60	rx	brand		60
1707	49758	60505132309	Pravastatin sodium 80 mg tab bottle 90	rx	generic		90
1708	6748	59746017106	Prednisone 1 mg tablet bottle 100	rx	generic		100
1709	6749	59746017306	Prednisone 10 mg tablet bottle 100	rx	generic		100
1710	6753	59746017206	Prednisone 5 mg tablet bottle 100	rx	generic		100
1711	61414	573286810	Preparation h cream tube 26	otc	brand		26
1712	60583	126007292	Prevident 5000 booster plus Fruit bottle 100	rx	brand		100
1713	5123	50111046701	Propranolol 10 mg tablet bottle 100	rx	generic		100
1714	5124	50111046801	Propranolol 20 mg tablet bottle 100	rx	generic		100
1715	-13380	7385209652	Purell hand santz 62% gel 8oz 240	otc	brand		240
1716	10503	521507000	Q-tips cotton swabs flexibl pf appli 170	otc	generic		170
1717	-87078	30521022127	Q-tips purse pck 30 1	otc	brand		1
1718	-142022	63551590843	Qc acet cap xst 500mg tyl 100  100	rx	brand		100
1719	-250613	63551595891	Qc acid control 20mg pep ac 25  25	rx	brand		25
1720	2690	63868012722	Qc antacid xtra str chew tab Assorted bottle 96	otc	generic		96
1721	13672	35515094809	Qc anti-diarrheal 2 mg caplet blist pack 24	otc	generic		24
1722	40944	35515094730	Qc anti-gas 180 mg softgel bottle 60	otc	generic		60
1723	22123	35515094808	Qc arthritis pain er 650 mg bottle 100	otc	generic		100
1724	-142017	63551590431	Qc asp coated tab bayer 100  100	rx	brand		100
1725	-141908	63551598369	Qc bisacodyl tab dulcolax 25  25	rx	brand		25
1726	-142019	63551590180	Qc complt allergy caps bendr 24  24	rx	brand		24
1727	-142660	63551598495	Qc dental flos wx mnt jj 100yd  100	rx	brand		100
1728	-141897	63551590592	Qc foot corn pads scholls 9  9	rx	brand		9
1729	9801	35515099466	Qc hydrogen peroxide 3% soln bottle 473	otc	generic		473
1730	8346	35515098077	Qc ibuprofen 200 mg tablet bottle 100	otc	generic		100
1731	8346	35515090893	Qc ibuprofen 200 mg tablet bottle 50	otc	generic		50
1732	-280718	63551596600	Qc ibuprofen 200mg cap 80ct  1	rx	brand		1
1733	16995	35515095400	Qc lo-dose aspirin ec 81 mg tb bottle 365	otc	generic		365
1734	18698	35515095225	Qc loratadine 10 mg tablet blist pack 30	otc	generic		30
1735	18698	35515095226	Qc loratadine 10 mg tablet UD blist pack 10	otc	generic		10
1736	-142664	63551590875	Qc mouthwash mnt grn scope 33.8 oz  33	rx	brand		33
1737	-299919	63551595435	Qc soap ab gold dial 3x3.5z  3	rx	brand		3
1738	3009	35515090224	Qc stool softener 100 mg cap bottle 100	otc	generic		100
1739	3009	35515094723	Qc stool softener 100 mg sftgl bottle 250	otc	generic		250
1740	5089	35515094583	Qc suphedrine 30 mg tablet blist pack 48	otc	generic		48
1741	-143536	63551595785	Qc suphedrine pse 12hr sudf 10  10	rx	brand		10
1742	-404952	63551599151	Qc toothbrush mega clean soft 2pk 99151 1	otc	brand		1
1743	-142690	63551598039	Qc vit multi esstl 1-aday 100  100	rx	brand		100
1744	-193165	63551598032	Qc vit vit-c chew orange 500mg 60 98032 60	rx	brand		60
1745	5690	35515090884	Qc witch hazel astringent bottle 473	otc	generic		473
1746	34187	67877024201	Quetiapine fumarate 25 mg tab bottle 100	rx	generic		100
1747	18773	68180055809	Quinapril 20 mg tablet bottle 90	rx	generic		90
1748	21909	68180055409	Quinapril 40 mg tablet bottle 90	rx	generic		90
1749	16031	68180059101	Ramipril 10 mg capsule bottle 100	rx	generic		100
1750	11673	68462024805	Ranitidine 150 mg tablet bottle 500	rx	generic		500
1751	62795	23455430	Refresh celluvisc 1% eye drops UD blist pack 30	otc	brand		30
1752	16278	23079815	Refresh tears 0.5% eye drops drop btl 15	otc	brand		15
1753	-96558	3660230033	Ricola c drop nat herb bag 50	otc	brand		50
1754	67405	36602019210	Ricola herb throat drops bag 19	otc	brand		19
1755	-8148	3660207917	Ricola original clpstp lozg 1x12ea 12	otc	brand		12
1756	50364	591207539	Risedronate sodium 35 mg tab UU dose-pack 12	rx	generic		12
1757	42922	27241000206	Risperidone 0.25 mg tablet bottle 60	rx	generic		60
1758	51784	781540192	Rosuvastatin calcium 10 mg tab bottle 90	rx	generic		90
1759	51784	57237016990	Rosuvastatin calcium 10 mg tab bottle 90	rx	generic		90
1760	51786	57237017130	Rosuvastatin calcium 40 mg tab bottle 30	rx	generic		30
1761	52944	57237016890	Rosuvastatin calcium 5 mg tab bottle 90	rx	generic		90
1762	8084	45802035758	Saline mist 0.65% nose spry squeez btl 45	otc	generic		45
1763	-339271	46581090002	Salonpas dprv gl 15- 10-3.1 gel 78 gm 78	otc	brand		78
1764	-4739	2120001023	Scotch magic tp 3/4n x300n tape 12 ea 12	otc	brand		12
1765	-4754	2120003552	Scotch rx label 1.5n bulk tape 1	otc	brand		1
1766	-160680	1150900205	Sea bond dentr adh uppr ptch 30 ea 30	otc	brand		30
1767	19964	536590401	Senexon tablet bottle 100	otc	generic		100
1768	48044	603028121	Sennalax-s tablet bottle 100	otc	generic		100
1769	-26717	74098522390	Sentry senior 100	otc	generic		100
1770	2607	60258015101	Sf 1.1% gel Mint jar 56	rx	generic		56
1771	-17292	10119430025	Sight savers lens spry 12x15ml 12's 15	otc	brand		15
1772	16414	54838020970	Siltussin dm cough syrup Strawberry bottle 237	otc	generic		237
1773	22210	54838011770	Siltussin sa 100 mg/5 ml syr Strawberry bottle 237	otc	generic		237
1774	-93492	4000001502	Snickers miniatures peg pack 4.4	otc	generic		4
1775	-13569	7418226012	Softsoap moist w/aloe liqd 7.5oz 225	otc	brand		225
1776	73235	299382330	Soolantra 1% cream tube 30	rx	brand		30
1777	-293796	2571566950	Specim cont strl 4z apot 66949 1	otc	brand		1
1778	22850	8137008520	Steri-pad 2" x 2" n/a 10	otc	generic		10
1779	16937	81067020	Sudafed 12hr 120 mg caplet bottle 20	otc	brand		20
1780	-236297	31015805475	Super poli-grip+poliseal 2.2oz 2.2	otc	brand		2
1781	-19945	31015806204	Super poligrip free crm 2.4oz 72	otc	brand		72
1782	-297667	38290324911	Syr bd uf ins 0.5ml 31g x 6mm 100	otc	brand		100
1783	52162	65042921	Systane 0.3-0.4% eye drops drop btl 10	otc	brand		10
1784	52162	65042930	Systane 0.3-0.4% eye drops drop btl 30	otc	brand		30
1785	5977	70501009200	T-gel 0.5% therapeutic shampoo bottle 130	otc	brand		130
1786	27546	57237001401	Tamsulosin hcl 0.4 mg capsule bottle 100	rx	generic		100
1787	63562	65041625	Tears naturale free drops UD blist pack 36	otc	brand		36
1788	3689	67877014601	Temazepam 15 mg capsule bottle 100	rx	generic	C-4	100
1789	22651	59746038506	Terazosin 5 mg capsule bottle 100	rx	generic		100
1790	18273	51672208002	Terbinafine 1% cream tube 30	otc	generic		30
1791	7008	591319689	Terconazole 0.4% cream tube/kit 45	rx	generic		45
1792	39744	536466138	Therems-m tablet bottle 130	otc	brand		130
1793	-310358	7205861080	Thick-it #2 10oz 284	otc	brand		284
1794	7855	64980051305	Timolol 0.25% eye drops drop btl 5	rx	generic		5
1795	23139	69543013611	Tramadol hcl 50 mg tablet bottle 1000	rx	generic	C-4	1000
1796	48456	53746061701	Tramadol-acetaminophn 37.5-325 bottle 100	rx	generic	C-4	100
1797	47612	65026005	Travatan z 0.004% eye drop drop btl 5	rx	brand		5
1798	7593	67877031715	Triamcinolone 0.025% cream tube 15	rx	generic		15
1799	7597	45802005536	Triamcinolone 0.1% ointment tube 80	rx	generic		80
1800	7595	45802006535	Triamcinolone 0.5% cream tube 15	rx	generic		15
1801	-80522	1254607509	Trident cinnamon 18 ct12 18	otc	brand		18
1802	-137944	1254715020	Tucks 100  100	otc	brand		100
1803	2690	766725000	Tums smoothies chew tablet Berry bottle 60	otc	brand		60
1804	4490	45044909	Tylenol ex-str 500 mg caplet bottle 100	otc	brand		100
1805	4490	45044905	Tylenol ex-str 500 mg caplet bottle 24	otc	brand		24
1806	-249638	76056949501	Uro-mag cap 140mg 100 100	otc	brand		100
1807	30607	59746032530	Valacyclovir hcl 1 gram tablet bottle 30	rx	generic		30
1808	4535	121067516	Valproic acid 250 mg/5 ml soln Cherry bottle 473	rx	generic		473
1809	48400	43547036909	Valsartan 160 mg tablet bottle 90	rx	generic		90
1810	-89411	30521013441	Vaseline ic lot 10oz cco bttr 1	otc	brand		1
1811	8482	521234500	Vaseline petroleum jelly tube 390	otc	generic		390
1812	46399	68382001901	Venlafaxine hcl 37.5 mg tablet bottle 100	rx	generic		100
1813	28090	173068221	Ventolin hfa 90 mcg inhaler canister 8	rx	brand		8
1814	-135750	32390001051	Vicks vaporub lmn snt oint 1.76 oz 50	otc	brand		50
1815	65344	169406012	Victoza 2-pak 18 mg/3 ml pen syringe 3	rx	brand		3
1816	64434	131247935	Vimpat 150 mg tablet bottle 60	rx	brand	C-5	60
1817	2169	64380073706	Vit d2 1.25 mg (50-000 unit) bottle 100	rx	generic		100
1818	2337	74312001380	Vitamin b-12 1-000 mcg tablet bottle 100	otc	generic		100
1819	-19698	30768090526	Vitamin c  tr 500mg caps 90 90	otc	brand		90
1820	2173	74312015605	Vitamin d3 1-000 units softgel bottle 120	otc	generic		120
1821	64176	536379001	Vitamin d3 2-000 unit softgel bottle 100	otc	generic		100
1822	57865	66582031354	Vytorin 10-40 mg tablet bottle 90	rx	brand		90
1823	-80157	2360101809	Walker ski glde 1 1 8 a80900 2	otc	brand		2
1824	14198	93171201	Warfarin sodium 1 mg tablet bottle 100	rx	generic		100
1825	6561	93171310	Warfarin sodium 2 mg tablet bottle 1000	rx	generic		1000
1826	18080	93171501	Warfarin sodium 3 mg tablet bottle 100	rx	generic		100
1827	19486	93171601	Warfarin sodium 4 mg tablet bottle 100	rx	generic		100
1828	6562	93172110	Warfarin sodium 5 mg tablet bottle 1000	rx	generic		1000
1829	30475	93171801	Warfarin sodium 6 mg tablet bottle 100	rx	generic		100
1830	-27879	76828004670	Wet ones twlt pada 40 40	otc	brand		40
1831	54736	187045302	Zelapar 1.25 mg odt tablet UD blist pack 60	rx	brand		60
1832	5576	168006202	Zinc oxide 20% ointment tube 56.7	otc	generic		56
1833	7670	187099395	Zovirax 5% ointment tube 30	rx	brand		30
1834	-26641	74098521339	Acidophilus 100	otc	generic		100
1835	19141	42794008008	Acitretin 10 mg capsule bottle 30	rx	generic		30
1836	-33009	41167009565	Act restoring cool mint mwsh 18 oz 540	otc	brand		540
1837	61344	173071620	Advair hfa 115-21 mcg inhaler canister 12	rx	brand		12
1838	8346	573016030	Advil 200 mg caplet bottle 50	otc	brand		50
1839	12059	591319301	Afeditab cr 30 mg tablet bottle 100	rx	brand		100
1840	45052	69097084407	Alfuzosin hcl er 10 mg tablet bottle 100	rx	generic		100
1841	2535	591554301	Allopurinol 100 mg tablet bottle 100	rx	generic		100
1842	-4724	2006560038	Alpha betic multivit caps 30 30	otc	brand		30
1843	-151778	3076817965	Alpha lipoic acid 600mg caps 60 ea 60	otc	brand		60
1844	16925	69097083615	Amlodipine besylate 2.5 mg tab bottle 1000	rx	generic		1000
1845	16926	69097083715	Amlodipine besylate 5 mg tab bottle 1000	rx	generic		1000
1846	16926	59762153001	Amlodipine besylate 5 mg tab bottle 90	rx	generic		90
1847	16307	72140045231	Aquaphor w-nat heal oint tube 50	otc	brand		50
1848	65423	456045901	Armour thyroid 60 mg tablet bottle 100	rx	brand		100
1849	4381	12843010349	Aspirin ec 325 mg tablet bottle 100	otc	generic		100
1850	29967	60505257809	Atorvastatin 10 mg tablet bottle 90	rx	generic		90
1851	29968	60505257909	Atorvastatin 20 mg tablet bottle 90	rx	generic		90
1852	-8824	3878552000	Aussie insnt freeze aero 7 oz 210	otc	generic		210
1853	-411911	38137116532	Aveeno daily moist lot 8oz tube 8	otc	brand		8
1854	-377226	31011949311	B&l soothe xp xtra protection lubricant 1	otc	brand		1
1855	10523	8137004452	Band-aid flex fab bandage box 20	otc	generic		20
1856	10523	8137004430	Band-aid flex fab bandage n/a 30	otc	generic		30
1857	23031	8137005635	Band-aid plastic strips box 60	otc	generic		60
1858	-22380	39800015464	Battery evr aa e91bp2 each 2 ea 2	otc	brand		2
1859	49404	8290328468	Bd insulin syr 0.5 ml 8mmx31g box 100	otc	generic		100
1860	2944	713010912	Bisac-evac 10 mg suppository box 12	otc	brand		12
1861	2944	536135501	Bisacodyl 10 mg suppository box 100	otc	generic		100
1862	72230	310653004	Bydureon 2 mg pen inject syringe 4	rx	brand		4
1863	21697	35515091122	Calcium antacid 1-000 mg tab bottle 72	otc	generic		72
1864	4558	51672400501	Carbamazepine 200 mg tablet bottle 100	rx	generic		100
1865	2538	228253950	Carbidopa-levodopa 25-100 tab bottle 500	rx	generic		500
1866	22403	8004012140	Carefree panty shield orig packet 60	otc	generic		60
1867	16489	24208036705	Carteolol hcl 1% eye drops drop btl 5	rx	generic		5
1868	19293	68462016505	Carvedilol 25 mg tablet bottle 500	rx	generic		500
1869	40257	67253001106	Cefdinir 300 mg capsule bottle 60	rx	generic		60
1870	41286	65862090901	Celecoxib 200 mg capsule bottle 100	rx	generic		100
1871	9043	68180012201	Cephalexin 500 mg capsule bottle 100	rx	generic		100
1872	48094	536344508	Cerovite senior tablet bottle 60	otc	generic		60
1873	22534	299391716	Cetaphil moisturizing cream jar 453	otc	brand		453
1874	8213	57664064888	Chlorthalidone 25 mg tablet bottle 100	rx	generic		100
1875	65023	16500053502	Citracal-vit d 200 mg-250 tab bottle 100	otc	brand		100
1876	9340	59762501002	Clindamycin hcl 300 mg capsule bottle 100	rx	generic		100
1877	4560	93083201	Clonazepam 0.5 mg tablet bottle 100	rx	generic	C-4	100
1878	4561	93083301	Clonazepam 1 mg tablet bottle 100	rx	generic	C-4	100
1879	346	53489021501	Clonidine hcl 0.1 mg tablet bottle 100	rx	generic		100
1880	-11979	7006608120	Clubman styptic pncl pencil each 0.25oz 7	otc	generic		7
1881	-16722	9110810125	Cold-eeze hny/lemn box lozg 18 ea 18	otc	generic		18
1882	-410557	35000051406	Colgate cvt prt 0.76% 113gm 1	otc	brand		1
1883	-411995	3500051088	Colgate t-paste family 6oz 6	otc	brand		6
1884	-7217	3500074002	Colgate total pste 4.2 oz 126	otc	brand		126
1885	-7218	3500074003	Colgate total pste 6oz 180	otc	brand		180
1886	-7216	3500074000	Colgate total trsz pste 48x0.75 48's 22.5	otc	brand		22
1887	-410020	4110081138	Coricidin hbp cgh/cld 16 8	otc	brand		8
1888	-190744	61678443052	Cotton tip applicator 2/6" 200 dyn 4305 100	rx	brand		100
1889	43134	904552752	Cranberry fruit 405 mg cap bottle 60	otc	generic		60
1890	70267	74312004349	Cranberry plus vitamin c sftgl bottle 120	otc	generic		120
1891	-8403	3700038592	Crest plus scope pste 36 ea 36	otc	brand		36
1892	-8399	3700038583	Crest plus scope tape 4.4 oz 132	otc	brand		132
1893	7620	45802042235	Desonide 0.05% cream tube 15	rx	generic		15
1894	16008	24208045725	Diclofenac 0.1% eye drops drop btl 2.5	rx	generic		2
1895	4918	378161001	Dicyclomine 10 mg capsule bottle 100	rx	generic		100
1896	16570	68682099498	Diltiazem 24hr er 180 mg cap bottle 90	rx	generic		90
1897	44362	51862000560	Dofetilide 500 mcg capsule bottle 60	rx	generic		60
1898	15586	67253038210	Doxazosin mesylate 4 mg tab bottle 100	rx	generic		100
1899	15587	67253038310	Doxazosin mesylate 8 mg tab bottle 100	rx	generic		100
1900	9218	69097022670	Doxycycline hyclate 100 mg cap bottle 50	rx	generic		50
1901	15943	49884072703	Doxycycline mono 100 mg cap bottle 50	rx	generic		50
1902	-25327	67516010701	Dr dans cortibalm 1% stck 0.14 oz 4	otc	brand		4
1903	57892	60505299603	Duloxetine hcl dr 30 mg cap bottle 30	rx	generic		30
1904	-178522	74912341500	Duracell batt in gnrc pkging aa 4ct  4	rx	brand		4
1905	-272983	41333000277	Duracell h/a 8bat  1	otc	brand		1
1906	40186	19810000023	Excedrin extra strength caplet bottle 100	otc	brand		100
1907	69938	78050315	Exelon 13.3 mg/24hr patch box 30	rx	brand		30
1908	-5932	2571567005	Ezy dose pill rem7dy med each 1	otc	generic		1
1909	16296	603358321	Felodipine er 10 mg tablet bottle 100	rx	generic		100
1910	16295	603358221	Felodipine er 5 mg tablet bottle 100	rx	generic		100
1911	15883	591321472	Fentanyl 100 mcg/hr patch box 5	rx	generic	C-2	5
1912	1645	536100901	Ferrous sulfate 325 mg tablet bottle 100	otc	generic		100
1913	-178626	73942366048	Florajen 3 cap 60 aml 60	rx	brand		60
1914	19318	173060102	Flovent 250 mcg diskus UD blist pack 60	rx	brand		60
1915	21253	173071820	Flovent hfa 44 mcg inhaler aer w/adap 10.6	rx	brand		10
1916	7616	51672125301	Fluocinonide 0.05% cream tube 15	rx	generic		15
1917	2366	603316230	Folic acid 1 mg tablet bottle 2500	rx	generic		2500
1918	6373	99073070822	Freestyle lite test strip box 50	otc	generic		50
1919	-297082	7560210147	Fs butt stick 3.6z cs12 3.6	otc	brand		3
1920	-297081	7560210146	Fs cinn discs 3.6z cs12 3.6	otc	brand		3
1921	-297084	7560210150	Fs spear starlgt 3.6z cs12 3.6	otc	brand		3
1922	2821	536453301	Gas relief 80 mg tablet chew Peppermint bottle 100	otc	generic		100
1923	2819	43011748	Gas-x ex-str 125 mg tab chew Cherry creme blist pack 48	otc	brand		48
1924	41843	43386031208	Gavilax powder jar 238	otc	brand		238
1925	51196	23155011701	Glipizide-metformin 5-500 mg bottle 100	rx	generic		100
1926	-25778	70074055905	Glucerna shake strwb liqd 4x6x8oz 4's 1440	otc	generic		1440
1927	-141414	4116701744	Gold bond foot cream 16% crm 113 gm 113	otc	brand		113
1928	43020	41167001710	Gold bond medicated foot powd bottle 283	otc	brand		283
1929	-310718	41167005510	Gold bond ult int/hl crm 3 oz 85	otc	brand		85
1930	-17894	12546062183	Halls c/dr hny/lmn lozg 30 30	otc	brand		30
1931	-401799	1370021755	Hefty cinch sak tall kitchen 13gal 45 45	otc	brand		45
1932	-196268	61007503804	Hol3804 cntrplck dr pch tr(10) 10	otc	brand		10
1933	-50122	8380503704	Holl 3704 2pc 2.75 w/flange 5	otc	generic		5
1934	5349	54162060001	Hydrocerin cream jar 454	otc	generic		454
1935	8182	603385632	Hydrochlorothiazide 25 mg tab bottle 1000	rx	generic		1000
1936	47431	603389121	Hydrocodon-acetaminoph 7.5-325 bottle 100	rx	generic	C-2	100
1937	47430	603389021	Hydrocodon-acetaminophen 5-325 bottle 100	rx	generic	C-2	100
1938	48554	50383090110	Hydrocortison-acetic acid soln drop btl 10	rx	generic		10
1939	9580	66993005702	Hydroxychloroquine 200 mg tab bottle 100	rx	generic		100
1940	23465	6071731	Hyzaar 50-12.5 tablet UU bottle 30	rx	brand		30
1941	62047	65804083	Icaps mv tablet bottle 100	otc	brand		100
1942	-274952	3076841283	Iron ferr sul tab 65mg sun 120 120	otc	brand		120
1943	22118	8137009217	J & j dental floss waxed packet 1	otc	generic		1
1944	23002	8137004724	J & j medium non-stick pads box 10	otc	generic		10
1945	10462	8137005051	J&j waterproof tape 1"x10yds n/a 1	otc	generic		1
1946	6561	832121200	Jantoven 2 mg tablet bottle 100	rx	generic		100
1947	6562	832121610	Jantoven 5 mg tablet bottle 1000	rx	generic		1000
1948	-21680	38137004408	Jj b/a tough strips ptch 20 ea 20	otc	brand		20
1949	-367023	38137116137	Jj bandaid fa roll gauze 2" x 2.5 yd 1	otc	brand		1
1950	39564	8137003048	Johnson's baby corn starch jar 270	otc	brand		270
1951	15568	45802046564	Ketoconazole 2% shampoo bottle 120	rx	generic		120
1952	1275	66758016005	Klor-con 10 meq tablet bottle 500	rx	brand		500
1953	1248	66758020501	Klor-con sprinkle er 10 meq cp bottle 100	rx	brand		100
1954	60535	51991080833	L-methylfolate 7.5 mg tablet bottle 30	otc	generic		30
1955	29054	603137858	Lactulose 10 gm/15 ml solution Banana bottle 473	rx	generic		473
1956	22550	29300011316	Lamotrigine 150 mg tablet bottle 60	rx	generic		60
1957	30107	51991077233	Lansoprazole dr 30 mg capsule bottle 30	rx	generic		30
1958	44633	68180011316	Levetiracetam 500 mg tablet bottle 120	rx	generic		120
1959	15523	378180777	Levothyroxine 88 mcg tablet bottle 90	rx	generic		90
1960	17266	68180051201	Lisinopril 2.5 mg tablet bottle 100	rx	generic		100
1961	392	68180051701	Lisinopril 40 mg tablet bottle 100	rx	generic		100
1962	-360983	1254744013	Lister ultra clean floss 30yd 1	otc	brand		1
1963	5693	501100373	Listerine antiseptic bottle 250	otc	brand		250
1964	-177068	1254742591	Listerine whitening 16oz rst 473	otc	brand		473
1965	18698	51660052601	Loratadine 10 mg tablet bottle 100	otc	generic		100
1966	3758	378045701	Lorazepam 1 mg tablet bottle 100	rx	generic	C-4	100
1967	23465	13668011690	Losartan-hctz 50-12.5 mg tab bottle 90	rx	generic		90
1968	57803	71101668	Lyrica 150 mg capsule bottle 90	rx	brand	C-5	90
1969	-26766	74098522713	Magnesium 250 mg 110	otc	generic		110
1970	-13638	7431205535	Magnesium oxide 500mg tabs 100 ea 100	otc	brand		100
1971	40974	65862001001	Metformin hcl 1-000 mg tablet bottle 100	rx	generic		100
1972	46754	62756014201	Metformin hcl er 500 mg tablet bottle 100	rx	generic		100
1973	59325	51672529503	Metronidazole topical 1% gel tube 60	rx	generic		60
1974	6079	19810116300	Mineral ice gel jar 240	otc	brand		240
1975	-3231	869420610	Mineral oil usp oil 16 oz 480	otc	generic		480
1976	-19781	30997315500	Mitchum clear unscnt stck 2.25 oz 67	otc	brand		67
1977	4090	54040444	Morphine sulf 100 mg/5 ml soln bottle 30	rx	generic	C-2	30
1978	4096	42858080201	Morphine sulf er 30 mg tablet bottle 100	rx	generic	C-2	100
1979	4091	54023525	Morphine sulfate ir 15 mg tab bottle 100	rx	generic	C-2	100
1980	-394216	20525092729	Move free jnt hlth tabs 80 ea 80	otc	brand		80
1981	71885	63824000840	Mucinex er 600 mg tablet bottle 40	otc	brand		40
1982	-332959	74312512391	Nb krill oil 500mg sftgel 30	otc	brand		30
1983	-27033	74312005800	Niacin  tr 250mg caps 90 90	otc	brand		90
1984	20617	93517301	Nifedical xl 60 mg tablet bottle 100	rx	generic		100
1985	-141449	89764000220	No ad sunblock spf50 lotn 16oz 475	otc	brand		475
1986	48529	51672126301	Nystatin-triamcinolone cream tube 15	rx	generic		15
1987	-27464	75609000744	Olay actv hydrt reg lotn 4 oz 120	otc	brand		120
1988	-3779	1204400027	Old sp he deod pur sprt stck 2.25 oz 68	otc	generic		68
1989	54334	45802088830	Omeprazole dr 20 mg tablet blist pack 28	otc	generic		28
1990	-19210	30041060552	Oral-b satinfls 55yd mint each 1 ea 1	otc	brand		1
1991	41047	93520701	Oxybutynin cl er 10 mg tablet bottle 100	rx	generic		100
1992	27462	378668977	Pantoprazole sod dr 40 mg tab bottle 90	rx	generic		90
1993	46223	13107015530	Paroxetine hcl 20 mg tablet bottle 30	rx	generic		30
1994	5144	378005201	Pindolol 5 mg tablet bottle 100	rx	generic		100
1995	10362	36000033592	Poise pads bag 33	otc	generic		33
1996	-236298	31015805465	Poli-grip ult fresh znc/f 2.4z 2.4	otc	brand		2
1997	22345	62037071001	Potassium cl er 10 meq tablet bottle 100	rx	generic		100
1998	22346	62037099901	Potassium cl er 20 meq tablet bottle 100	rx	generic		100
1999	63997	597014954	Pradaxa 75 mg capsule UU bottle 60	rx	brand		60
2000	16367	60505016909	Pravastatin sodium 20 mg tab bottle 90	rx	generic		90
2001	2605	126001661	Prevident 5000 1.1% dry mouth Mint bottle 100	rx	brand		100
2002	60841	126007061	Prevident 5000 sensitive paste Mint bottle 100	rx	brand		100
2003	-394433	70942306553	Proxabrush gobtwn clnr tight 8	otc	brand		8
2004	62241	186091612	Pulmicort 180 mcg flexhaler canister 1	rx	brand		1
2005	-315671	63551596655	Qc 96655 bk ot 30/40 bei lg 1ea 1	otc	brand		1
2006	-142206	63551590542	Qc acet cap xst 500mg tyl 24  24	rx	brand		24
2007	-141823	63551590842	Qc acet cap xst 500mg tyl 50  50	rx	brand		50
2008	-142021	63551590856	Qc acet tab 325mg tylnl 100  100	rx	brand		100
2009	4381	35515098304	Qc aspirin ec 325 mg tablet bottle 100	otc	generic		100
2010	16995	63868036320	Qc aspirin ec 81 mg tablet bottle 120	otc	generic		120
2011	-456939	63551569622	Qc gel bunion cushion 96922 1	otc	brand		1
2012	-142768	63551598601	Qc hand sanitizer lem purel 8 oz  8	rx	brand		8
2013	-144315	63551599205	Qc iodine tincture mild 2% 1oz  1	rx	brand		1
2014	3026	63868031012	Qc milk of magnesia suspension Cherry bottle 355	otc	generic		355
2015	3026	63868078857	Qc milk of magnesia suspension Mint bottle 355	otc	generic		355
2016	21980	35515097457	Qc naproxen sod 220 mg tablet bottle 50	otc	generic		50
2017	3046	63868034719	Qc natural vegetable powder Orange jar 283	otc	generic		283
2018	-142676	63551590000	Qc senna tab senokot 100  100	rx	brand		100
2019	-458336	63551595239	Qc senna-s lax tab 60 95239 60	otc	brand		60
2020	-142775	63551599541	Qc vit calc 600 w/d min caltr 60  60	rx	brand		60
2021	-142776	63551594629	Qc vit calcium 600 w/d caltr 300  300	rx	brand		300
2022	-178571	63551595927	Qc vit mlti 50+ w/lyco-lut ctrm 300  300	rx	brand		300
2023	-142694	63551595210	Qc vit oystr sh calc 500+d oscal 60  60	rx	brand		60
2024	21909	59762502201	Quinapril 40 mg tablet bottle 90	rx	generic		90
2025	46699	59310020412	Qvar 80 mcg oral inhaler aer w/adap 8.7	rx	brand		8
2026	23441	904634952	Ranitidine 75 mg tablet bottle 60	otc	generic		60
2027	64847	52544015219	Rapaflo 8 mg capsule bottle 90	rx	brand		90
2028	51734	80196075368	Remedy skin repair cream tube 118	otc	brand		118
2029	29161	68462025601	Ropinirole hcl 2 mg tablet bottle 100	rx	generic		100
2030	7549	63824085015	Scalpicin 1% anti-itch liquid bottle 44	otc	brand		44
2031	-3586	1150900163	Sea bond dentr adh lower ptch 15 15	otc	brand		15
2032	6157	74660302	Selsun blue shampoo x-cond bottle 210	otc	brand		210
2033	-19949	31015808350	Sensodyne frsh pste 120 ml 120	otc	brand		120
2034	22115	10158007704	Sensodyne toothpaste tube 113	otc	generic		113
2035	46227	59762496001	Sertraline hcl 25 mg tablet bottle 30	rx	generic		30
2036	24145	60258015001	Sf 5000 plus cream Spearmint jar 51	rx	generic		51
2037	6817	603576321	Spironolactone 25 mg tablet bottle 100	rx	generic		100
2038	-83825	7940018158	Suave h/s aero ext/hld un 11oz 11	otc	brand		11
2039	-236295	31015805455	Super poli-grip crm znc/f 2.4z 2.4	otc	brand		2
2040	6656	74714890	Synthroid 200 mcg tablet bottle 90	rx	brand		90
2041	47347	45802070000	Tacrolimus 0.1% ointment tube 30	rx	generic		30
2042	15871	8333162409	Tegaderm 2.375x2.75" drssng n/a 8	otc	generic		8
2043	62289	78048515	Tekturna 150 mg tablet UU bottle 30	rx	brand		30
2044	7986	24208029525	Tobramycin-dexameth ophth susp drop btl 2.5	rx	generic		2
2045	26170	68382014014	Topiramate 100 mg tablet bottle 60	rx	generic		60
2046	67353	597014090	Tradjenta 5 mg tablet bottle 90	rx	brand		90
2047	46242	50111043401	Trazodone 100 mg tablet bottle 100	rx	generic		100
2048	46243	53489051701	Trazodone 150 mg tablet bottle 100	rx	generic		100
2049	46241	50111043301	Trazodone 50 mg tablet bottle 100	rx	generic		100
2050	7594	67877025115	Triamcinolone 0.1% cream tube 15	rx	generic		15
2051	8177	591034801	Triamterene-hctz 75-50 mg tab bottle 100	rx	generic		100
2052	-414593	94922072917	Triple antibiotic oint fa 1oz 72917 1	otc	brand		1
2053	-392933	2129200618	True metrix test strip 50 50	otc	brand		50
2054	48401	43547036809	Valsartan 80 mg tablet bottle 90	rx	generic		90
2055	-33491	521307700	Vasel ic total moist lotn 295 ml 295	otc	brand		295
2056	46405	93738698	Venlafaxine hcl er 150 mg cap bottle 90	rx	generic		90
2057	46404	59762018102	Venlafaxine hcl er 75 mg cap bottle 90	rx	generic		90
2058	566	378051201	Verapamil 80 mg tablet bottle 100	rx	generic		100
2059	567	68462026001	Verapamil er 240 mg tablet bottle 100	rx	generic		100
2060	-270538	30045010395	Viactiv chewy caramel 100 100	otc	brand		100
2061	43044	74300001067	Visine tears drops drop btl 15	otc	brand		15
2062	28465	904582460	Vitamin d3 1-000 unit tablet bottle 100	otc	generic		100
2063	19188	13668000801	Zolpidem tartrate 10 mg tablet bottle 100	rx	generic	C-4	100
2064	-281292	5113119815	Ace knee wrap neop open univ 1	otc	brand		1
2065	4489	51645070301	Acetaminophen 325 mg tablet bottle 100	otc	generic		100
2066	4490	51645070501	Acetaminophen 500 mg caplet bottle 100	otc	generic		100
2067	625	31874214	Adt robitussin peak cld m-s lq bottle 118	otc	brand		118
2068	-366796	64786589922	Airborne everyday gummies 50 1	otc	brand		1
2069	46941	69543013104	Alendronate sodium 70 mg tab UD UU blist pack 4	rx	generic		4
2070	-207877	3700029417	Align digestive caps 42ct 42	rx	brand		42
2071	11640	904001224	Allergy 4 mg tablet bottle 24	otc	generic		24
2072	3774	67253090110	Alprazolam 0.5 mg tablet bottle 100	rx	generic	C-4	100
2073	39105	24208035305	Alrex 0.2% eye drops drop btl 5	rx	brand		5
2074	16927	69097083805	Amlodipine besylate 10 mg tab bottle 90	rx	generic		90
2075	24668	93227534	Amox-clav 875-125 mg tablet bottle 20	rx	generic		20
2076	59998	573021341	Anbesol liquid Cool mint bottle 12	otc	brand		12
2077	13672	51660012312	Anti-diarrheal 2 mg caplet blist pack 12	otc	generic		12
2078	-144929	6799060011	Aqua net h/s ex sup uns aero 11z lnm 11	rx	brand		11
2079	60225	65862066130	Aripiprazole 2 mg tablet bottle 30	rx	generic		30
2080	-6932	3320018663	Arm/ham adv/wht b/soda gel 4.3 oz 129	otc	generic		129
2081	-235078	48879000807	Artifical tears (boxed) 1	otc	brand		1
2082	29969	55111012390	Atorvastatin 40 mg tablet bottle 90	rx	generic		90
2083	7866	17478021505	Atropine 1% eye drops drop btl 5	rx	generic		5
2084	-21616	38137003623	Aveeno bar dry skin soap 3oz 90	otc	brand		90
2085	-193439	7170917888	Avery marks-a-lot perm marker blk 17888 1	rx	brand		1
2086	-360731	78765176002	Azo bladder control cp 54 24 54	otc	brand		54
2087	23143	472110534	Bacitracin zn 500 unit/gm oint tube 14	otc	generic		14
2088	11594	904555124	Banophen 25 mg tablet blist pack 24	otc	generic		24
2089	-409413	413335354	Battery duracell aaa 24	otc	brand		24
2090	-22371	39800010834	Battery evr h/dty c 1235bp each 2 ea 2	otc	generic		2
2091	4741	50111032501	Bethanechol 25 mg tablet bottle 100	rx	generic		100
2092	23720	76478000210	Betimol 0.5% eye drops drop btl 10	rx	brand		10
2093	13721	65024610	Betoptic s 0.25% eye drops drop btl 10	rx	brand		10
2094	24153	16729002310	Bicalutamide 50 mg tablet bottle 30	rx	generic		30
2095	-369029	7379627424	Bld pressure 1oz kit 1	otc	brand		1
2096	1934	87094643	Boost pudding Chocolate jar 142	otc	brand		142
2097	26379	57145000246	Breathe right nasal strips box 30	otc	generic		30
2098	-405179	3400091216	Brookside crunch clust roast alm/bry12cs 1	otc	brand		1
2099	-319406	6843738908	Brookside dark chocolate pomeg 7	otc	brand		7
2100	-394122	6843791132	Brookside dk choc crunchy clustr pch 5oz 5	otc	brand		5
2101	46525	93681573	Budesonide 0.25 mg/2 ml susp UD vial 2	rx	generic		2
2102	-314091	92850014435	Burts bees throat drop hny pom 20	otc	brand		20
2103	3782	68382018001	Buspirone hcl 5 mg tablet bottle 100	rx	generic		100
2104	64945	456142030	Bystolic 20 mg tablet bottle 30	rx	brand		30
2105	13109	904323292	Calcium 600 mg tablet bottle 150	otc	generic		150
2106	48241	68180013415	Calcium acetate 667 mg capsule bottle 200	rx	generic		200
2107	69971	5550924	Caltrate 600 plus d3 tablet bottle 120	otc	brand		120
2108	4559	13668027101	Carbamazepine 100 mg tab chew Strawberry-vanilla bottle 100	rx	generic		100
2109	-275130	38004100052	Carefree bdy shp reg uns 20 20	otc	brand		20
2110	-160430	2360103709	Carex commode liner 7pck p709 1	otc	brand		1
2111	21282	62037059790	Cartia xt 120 mg capsule bottle 90	rx	brand		90
2112	16571	62037059990	Cartia xt 240 mg capsule bottle 90	rx	brand		90
2113	41285	69097042207	Celecoxib 100 mg capsule bottle 100	rx	generic		100
2114	-187734	79277175016	Cetaklenz skin cleanser liq pump 16oz 16	rx	brand		16
2115	6250	299392304	Cetaphil cleansing bar box 1	otc	generic		1
2116	23161	299392708	Cetaphil daily cleanser bottle 237	otc	generic		237
2117	22533	299391816	Cetaphil moisturizing lotion bottle 473	otc	brand		473
2118	24484	16571040110	Cetirizine hcl 5 mg tablet bottle 100	otc	generic		100
2119	61709	78112001266	Chloraseptic sore throat lozng Citrus blist pack 18	otc	brand		18
2120	13675	49884046565	Cholestyramine packet Orange packet 60	rx	generic		60
2121	46206	65162005210	Citalopram hbr 10 mg tablet bottle 100	rx	generic		100
2122	7634	472040060	Clobetasol 0.05% cream tube 60	rx	generic		60
2123	53749	45802096126	Clobetasol 0.05% shampoo bottle 118	rx	generic		118
2124	38164	31722090190	Clopidogrel 75 mg tablet bottle 90	rx	generic		90
2125	7361	904782236	Clotrimazole 1% cream tube 14.17	rx	generic		14
2126	-378563	7410855327	Conair shower cap 3pk 55327 1	otc	brand		1
2127	-32263	3700044982	Crest pro-hlth clnmnt mwsh 1000 ea 1000	otc	brand		1000
2128	-21213	37000000312	Crest tube gel 6.4 oz 192	otc	brand		192
2129	-155524	8019630002	Curad gauze pd ad2x3 cur47147 20	otc	brand		20
2130	9623	13925050430	Dapsone 25 mg tablet UD blist pack 30	rx	generic		30
2131	-282497	323900014350	Dayquil cold/flu liquid 1	otc	brand		1
2132	24145	64980030550	Denta 5000 plus cream Spearmint tube 51	rx	generic		51
2133	22100	904774439	Denture cleanser tablet bottle 40	otc	generic		40
2134	47763	69543010730	Desloratadine 5 mg tablet bottle 30	rx	generic		30
2135	7620	51672128003	Desonide 0.05% cream jar 60	rx	generic		60
2136	571	378606001	Diltiazem 12hr er 60 mg cap bottle 100	rx	generic		100
2137	574	93031801	Diltiazem 30 mg tablet bottle 100	rx	generic		100
2138	4539	29300013901	Divalproex sod dr 250 mg tab bottle 100	rx	generic		100
2139	5292	96072216	Dml lotion bottle 480	otc	brand		480
2140	3009	904788960	Dok 100 mg capsule bottle 100	otc	generic		100
2141	3009	904645780	Dok 100 mg softgel bottle 1000	otc	generic		1000
2142	23513	591248179	Dorzolamide hcl 2% eye drops drop btl 10	rx	generic		10
2143	-25382	68142102101	Dulcolax 10mg 4 rect supp 4 ea 4	otc	generic		4
2144	-25379	68142102003	Dulcolax 5mg 50 ec 5mg tabs 50 ea 50	otc	generic		50
2145	-20006	31101725220	Duofilm lq wart rmvr liqd 0.33 oz 9	otc	brand		9
2146	11567	41333042401	Duracell battery aaa size packet 4	otc	generic		4
2147	51246	591229230	Dutasteride 0.5 mg capsule bottle 30	rx	generic		30
2148	66843	70074050461	Ensure original liquid Vanilla bottle 237	otc	brand		237
2149	47526	31722057390	Esomeprazole mag dr 40 mg cap bottle 90	rx	generic		90
2150	-134899	7214063605	Eucerin calm bdywsh oil 8.4 oz 250	otc	brand		250
2151	7771	536100294	Eye drops 0.05% drop btl 15	otc	generic		15
2152	-41329	2571567050	Ezydose pill pouch dsp each 1x50ea 50	otc	generic		50
2153	11677	62332000131	Famotidine 20 mg tablet bottle 100	rx	generic		100
2154	-251735	3848100961	Fast-freeze gel tube 119	otc	brand		119
2155	44305	378862977	Fenofibrate 134 mg capsule bottle 90	rx	generic		90
2156	1627	51991020311	Ferrex 150 capsule UD blist pack 100	otc	generic		100
2157	-457501	84009310195	Ferrous sulf iron slow releas 45mg tab60 1	otc	brand		1
2158	-463020	69230020230	Fexofenadine 180mg tabs otc 30	otc	brand		30
2159	58323	65628005004	First-mouthwash blm suspension kit 119	rx	brand		119
2160	2982	132007912	Fleet glycerin adult suppos box 12	otc	brand		12
2161	7608	52565003115	Fluocinolone 0.01% cream tube 15	rx	generic		15
2162	46214	50111064801	Fluoxetine hcl 20 mg capsule bottle 100	rx	generic		100
2163	73300	536109194	Fluticasone prop 50 mcg spray aer w/adap 15.8	rx	generic		15
2164	7272	53076010326	Fungi-nail tincture bottle 30	otc	brand		30
2165	8208	64125011610	Furosemide 20 mg tablet bottle 1000	rx	generic		1000
2166	8209	378021610	Furosemide 40 mg tablet bottle 1000	rx	generic		1000
2167	47927	50383031147	Gabapentin 250 mg/5 ml soln Strawberry anise bottle 470	rx	generic		470
2168	21414	69097081412	Gabapentin 300 mg capsule bottle 500	rx	generic		500
2169	2815	536222075	Gas relief 20 mg/0.3 ml drops Vanilla drop btl 30	otc	generic		30
2170	65583	88117547	Gaviscon 80-14.2 mg tab chew bottle 100	otc	brand		100
2171	6373	8539810003	Ge100 blood glucose test strip box 100	otc	generic		100
2172	-91626	2612	Glove vinyl exam p-f md 100/bx 1000	otc	generic		1000
2173	-141412	4116705060	Gold bond anti-itch lotn 156 gm 155	otc	brand		155
2174	11156	28785020105	Healthmist humidifier box 1	otc	generic		1
2175	21688	904552952	Heartburn relief 10 mg tablet bottle 60	otc	generic		60
2176	6549	25021040201	Heparin sod 5-000 unit/ml vial vial 1	rx	generic		1
2177	47172	2751101	Humalog mix 75-25 vial vial 10	rx	brand		10
2178	284	23155000101	Hydralazine 10 mg tablet bottle 100	rx	generic		100
2179	-257729	2739001139	Hydrocortisone cr 1%.9gr 1728csdyna 1139 1	otc	brand		1
2180	-339521	54973311402	Hyl leg cramps tabs 40 ea 40	otc	brand		40
2181	-250007	5070027631	Hytop plastic forks 24  24	rx	brand		24
2182	8346	904791559	Ibuprofen 200 mg tablet bottle 100	otc	generic		100
2183	61387	41167000891	Icy hot cream tube 85	otc	brand		85
2184	31099	45802036862	Imiquimod 5% cream packet packet 24	rx	generic		24
2185	-219176	5098224000	Incentive spirometer volumetrc dhd224000 1	otc	brand		1
2186	48018	69097017353	Iprat-albut 0.5-3(2.5) mg/3 ml UD vial 3	rx	generic		3
2187	21700	487980101	Ipratropium br 0.02% soln UD vial 2.5	rx	generic		2
2188	21700	591379883	Ipratropium br 0.02% soln UD vial 2.5	rx	generic		2
2189	34469	43547027903	Irbesartan 300 mg tablet bottle 30	rx	generic		30
2190	34470	43547027709	Irbesartan 75 mg tablet bottle 90	rx	generic		90
2191	6560	832121300	Jantoven 2.5 mg tablet bottle 100	rx	generic		100
2192	18080	832121400	Jantoven 3 mg tablet bottle 100	rx	generic		100
2193	19486	832121500	Jantoven 4 mg tablet bottle 100	rx	generic		100
2194	6562	832121600	Jantoven 5 mg tablet bottle 100	rx	generic		100
2195	5517	8137003011	Johnson's baby powder jar 120	otc	brand		120
2196	45836	54629014902	Kelp tablet bottle 200	otc	generic		200
2197	17871	69097014907	Lamotrigine 100 mg tablet bottle 100	rx	generic		100
2198	-375513	3400010350	Lancaster soft creme caramel bag 8oz 8	otc	brand		8
2199	6649	378180377	Levothyroxine 50 mcg tablet bottle 90	rx	generic		90
2200	6650	378180577	Levothyroxine 75 mcg tablet bottle 90	rx	generic		90
2201	-360987	1254744019	Lister acc fb floss head rfll 28	otc	brand		28
2202	5693	12547042825	Listerine antiseptic bottle 500	otc	brand		500
2203	4001	31722054501	Lithium carbonate 300 mg cap bottle 100	rx	generic		100
2204	18698	781507701	Loratadine 10 mg tablet bottle 100	otc	generic		100
2205	18698	51660052631	Loratadine 10 mg tablet bottle 30	otc	generic		30
2206	38686	65862020390	Losartan potassium 100 mg tab bottle 90	rx	generic		90
2207	23382	65862020290	Losartan potassium 50 mg tab bottle 90	rx	generic		90
2208	59919	13668011730	Losartan-hctz 100-12.5 mg tab bottle 30	rx	generic		30
2209	16995	280210012	Low dose aspirin ec 81 mg tab bottle 120	otc	generic		120
2210	69659	52800048234	Lubriderm advanced therapy lot bottle 473	otc	brand		473
2211	21737	52800048316	Lubriderm sensitive skin lot bottle 473	otc	brand		473
2212	-51680	1920074186	Lysol disinf 12oz crsplinen 1	otc	brand		1
2213	4489	904198280	Mapap 325 mg tablet bottle 1000	otc	generic		1000
2214	-4820	2220095104	Mennen spdstk gel sport stck 3 oz 90	otc	brand		90
2215	65542	37000002404	Metamucil fiber singles packet Orange packet 30	otc	brand		30
2216	64472	37000074085	Metamucil sugar-free powder jar 660	otc	brand		660
2217	36872	555057202	Methotrexate 2.5 mg tablet bottle 100	rx	generic		100
2218	50631	904634080	Metoprolol tartrate 25 mg tab bottle 1000	rx	generic		1000
2219	66415	31604014032	Milk thistle 140 mg capsule bottle 50	otc	generic		50
2220	5349	904775127	Minerin creme jar 454	otc	generic		454
2221	18767	904775216	Minerin lotion bottle 473	otc	generic		473
2222	46451	60505024801	Mirtazapine 30 mg tablet bottle 30	rx	generic		30
2223	41478	65862060201	Modafinil 200 mg tablet bottle 100	rx	generic	C-4	100
2224	38451	16729011910	Montelukast sod 10 mg tablet UU bottle 30	rx	generic		30
2225	66559	63824001465	Mucinex fast-max congest-cough bottle 180	otc	brand		180
2226	8362	68462019001	Naproxen 500 mg tablet bottle 100	rx	generic		100
2227	47333	591335401	Nateglinide 60 mg tablet bottle 100	rx	generic		100
2228	48546	574416035	Neomyc-polym-dexamet eye ointm tube 3.5	rx	generic		3
2229	11680	68462042630	Nizatidine 300 mg capsule bottle 30	rx	generic		30
2230	-204641	3160402602	Nm fish oil sfg 1200mg caps 90 ea 90	otc	brand		90
2231	-312150	31604002754	Nm tumeric 60cap  60	otc	brand		60
2232	-13787	7524401000	No rinse bath wipes pada 8 ea 8	otc	generic		8
2233	-27335	75244000100	No rinse shmp 8 oz 240	otc	generic		240
2234	46060	51672400205	Nortriptyline hcl 25 mg cap bottle 90	rx	generic		90
2235	7284	832046530	Nyamyc 100-000 units/gm powder squeez btl 30	rx	generic		30
2236	8084	187526001	Ocean 0.65% nasal spray squeez btl 104	otc	brand		104
2237	-321611	24208465356	Ocuvite softgel eye formula 30	otc	brand		30
2238	58486	60505317007	Omega-3 ethyl esters 1 gm cap bottle 120	rx	generic		120
2239	67540	16500007410	One-a-day women's tablet bottle 100	otc	brand		100
2240	72862	93573101	Oxycodone hcl er 10 mg tablet bottle 100	rx	generic	C-2	100
2241	4222	53746020301	Oxycodone-acetaminophen 5-325 bottle 100	rx	generic	C-2	100
2242	72864	59011042010	Oxycontin 20 mg tablet bottle 100	rx	brand	C-2	100
2243	61092	115123213	Oxymorphone hcl er 10 mg tab bottle 60	rx	generic	C-2	60
2244	61093	115123313	Oxymorphone hcl er 20 mg tab bottle 60	rx	generic	C-2	60
2245	51909	52544092008	Oxytrol 3.9 mg/24hr patch box 8	rx	brand		8
2246	1340	536410608	Oysco-500 tablet bottle 60	otc	generic		60
2247	39545	93001198	Pantoprazole sod dr 20 mg tab bottle 90	rx	generic		90
2248	59182	69387010430	Paricalcitol 2 mcg capsule bottle 30	rx	generic		30
2249	73483	65427325	Pazeo 0.7% eye drops drop btl 2.5	rx	brand		2
2250	21754	67602560	Perdiem overnight relief tb bottle 60	otc	brand		60
2251	-400629	74098527601	Pet ear clean 4	otc	brand		4
2252	-364368	74098527480	Petnc gluc/chon chw 500/400 45  45	rx	brand		45
2253	4521	51672411101	Phenytoin sod ext 100 mg cap bottle 100	rx	generic		100
2254	-90465	7088000001	Ponaris nasal emollient 1	otc	generic		1
2255	-19438	30521001400	Ponds cold cream lrg crm 6.1 oz 183	otc	brand		183
2256	39100	68382019816	Pramipexole 0.5 mg tablet bottle 90	rx	generic		90
2257	65762	55111061130	Pramipexole er 0.375 mg tablet UU bottle 30	rx	generic		30
2258	16367	16252052790	Pravastatin sodium 20 mg tab bottle 90	rx	generic		90
2259	20741	16252052890	Pravastatin sodium 40 mg tab bottle 90	rx	generic		90
2260	66068	5197102	Prevnar 13 syringe syringe 0.5	rx	brand		0
2261	62135	8484990310	Prodigy control solution low bottle 1	otc	generic		1
2262	-132100	38484053350	Prodigy ctrl hgh 4ml 1	otc	generic		1
2263	6373	8484072500	Prodigy no coding test strips box 50	otc	generic		50
2264	-13379	7385209650	Purell hndsan orig 62% gel 2oz 60	otc	generic		60
2265	-189969	63551595551	Qc calcium 600mg w/d+mineral chew 60 60	rx	brand		60
2266	-141841	63551598322	Qc cosmetic round j j 80  80	rx	brand		80
2267	-364118	63551595869	Qc cotton balls trpl size 200  200	rx	brand		200
2268	-141917	63551591152	Qc nail pol removr reg cutx 6z  6	rx	brand		6
2269	-250621	63551596510	Qc razor disp 3blade men gil 4  4	rx	brand		4
2270	-142770	63551599220	Qc thermometer digital b-d  1	rx	brand		1
2271	-400785	63551599149	Qc toothbrush complete clean soft 99149 1	otc	brand		1
2272	18772	65862061890	Quinapril 10 mg tablet bottle 90	rx	generic		90
2273	11673	53746025360	Ranitidine 150 mg tablet bottle 60	rx	generic		60
2274	11673	68462024860	Ranitidine 150 mg tablet bottle 60	rx	generic		60
2275	62806	23324015	Refresh optive eye drops drop btl 15	otc	brand		15
2276	64505	23341630	Refresh optive sensitive drops drop btl 30	otc	brand		30
2277	62977	80196075370	Remedy nutrashield protectant bottle 118	otc	brand		118
2278	51820	23916360	Restasis 0.05% eye emulsion vial 60	rx	brand		60
2279	-454527	61007505361	Restore fm drg sil bdr 3.5x3.5 10 520003 10	otc	brand		10
2280	-277280	3660230155	Ricola thr/drp dual/c chrry 24 24	otc	brand		24
2281	67579	8137008809	Rolled gauze roll 4"x2.5yd box 1	otc	generic		1
2282	-40648	44444000840	Rx vial snp cp 60dr   5st60060 140's 1	otc	generic		1
2283	-57309	15127000039	S b oyster calcium 500mg 00039 60's 1	otc	generic		1
2284	49432	8290309594	Safety-lok 3 ml syringe box 100	otc	generic		100
2285	7423	145063005	Sarna sensitive 1% lotion bottle 222	otc	brand		222
2286	-17592	11017014800	Sch moleskin plus 3pk4x3 pads 1	otc	brand		1
2287	46229	68180035306	Sertraline hcl 100 mg tablet bottle 30	rx	generic		30
2288	5517	8137000713	Shower to shower body powdr jar 390	otc	brand		390
2289	-24346	59262034611	Similasan eye #2 alg drop 10 ml 10	otc	brand		10
2290	16577	16729000415	Simvastatin 10 mg tablet bottle 90	rx	generic		90
2291	16578	16729000515	Simvastatin 20 mg tablet bottle 90	rx	generic		90
2292	16579	16729000615	Simvastatin 40 mg tablet bottle 90	rx	generic		90
2293	8612	46287050001	Sorbitol 70% solution bottle 473	rx	generic		473
2294	17196	60505008000	Sotalol 80 mg tablet bottle 100	rx	generic		100
2295	22863	8137008523	Steri-pad 3" x 3" n/a 25	otc	generic		25
2296	9396	53746027201	Sulfamethoxazole-tmp ds tablet bottle 100	rx	generic		100
2297	-81828	4160887039	Summers eve fem wash d/blsm liqd 9 oz 266	otc	brand		266
2298	-83048	4160887049	Summers eve fem wash ssn liqd 9 oz 266	otc	brand		266
2299	-191083	3400015902	Symphony milk w/almond & toffee 36ct 1	rx	brand		1
2300	-458220	30065043187	Systane day ultra/night gel drop valu pk 1	otc	brand		1
2301	13574	93078256	Tamoxifen 20 mg tablet bottle 30	rx	generic		30
2302	-13248	7301032110	Tampax supr tmpn 40 40	otc	brand		40
2303	24478	62037070090	Taztia xt 360 mg capsule bottle 90	rx	brand		90
2304	40910	54054318	Telmisartan 40 mg tablet UD blist pack 30	rx	generic		30
2305	-192098	980007102	Tic tac chill exotic cherry 9ct 1	rx	brand		1
2306	-92214	980000001	Tic tac fresh mint 24	otc	generic		24
2307	-275973	980005777	Tic tac wintergreen kentucky 24	otc	brand		24
2308	7855	60758080205	Timolol 0.25% eye drops drop btl 5	rx	generic		5
2309	23139	69543013610	Tramadol hcl 50 mg tablet bottle 100	rx	generic	C-4	100
2310	46241	603616021	Trazodone 50 mg tablet bottle 100	rx	generic		100
2311	-4866	2240000521	Tresemme two h/s xhld aero 11 oz 330	otc	brand		330
2312	7594	713022515	Triamcinolone 0.1% cream tube 15	rx	generic		15
2313	7594	713022580	Triamcinolone 0.1% cream tube 80	rx	generic		80
2314	7594	67877025180	Triamcinolone 0.1% cream tube 80	rx	generic		80
2315	7597	51672128408	Triamcinolone 0.1% ointment jar 80	rx	generic		80
2316	7597	51672128401	Triamcinolone 0.1% ointment tube 15	rx	generic		15
2317	21718	781207401	Triamterene-hctz 37.5-25 mg cp bottle 100	rx	generic		100
2318	66540	65597011630	Tribenzor 40-5-25 mg tablet bottle 30	rx	brand		30
2319	-25976	71603015000	Trim nail clipper deluxe each 6	otc	generic		6
2320	21697	766074610	Tums ultra tablet chewable Assorted bottle 160	otc	brand		160
2321	4489	45049660	Tylenol 325 mg tablet bottle 100	otc	brand		100
2322	66391	58657045601	Uro-mp capsule bottle 100	rx	generic		100
2323	7066	11509000392	Vagisil feminine powder bottle 227	otc	brand		227
2324	8482	521238400	Vaseline petroleum jelly tube 75	otc	generic		75
2325	2337	536355601	Vitamin b-12 1-000 mcg tablet bottle 100	otc	generic		100
2326	28465	536333401	Vitamin d3 1-000 unit tablet bottle 100	otc	generic		100
2327	-257967	1536006002	Walker tray inva 6002 1	otc	brand		1
2328	-188099	4129801084	Walker tray universal 510-1084-0300 1	rx	brand		1
2329	-192878	2360145390	Walker wheels 5" swivel carex a83300 1	rx	brand		1
2330	-4202	1616706271	Walker wheels 5in fixed eqip 2 ea 2	otc	generic		2
2331	-272366	3504600391	Wm melatonin 1mg tab 100	otc	brand		100
2332	-26650	74098521393	Zinc 50 mg 110	otc	generic		110
2333	-60235	5207700346	Zooth h kitty 4.2oz 1	otc	generic		1
2334	-176815	4098527113	21st vit b 12 5000mcg sublingl 110	otc	brand		110
2335	-83122	30766080155	Abreva pump 10% crm 2 gm 2	otc	brand		2
2336	62247	74312005721	Acidophilus w-bifidus wafer Strawberry bottle 100	otc	generic		100
2337	-393801	4116709200	Act tpst dry mth 4.6z sthn mnt 1	otc	brand		1
2338	61345	173071720	Advair hfa 230-21 mcg inhaler canister 12	rx	brand		12
2339	39494	10119000252	Advanced eye relief eye wash bottle 118	otc	brand		118
2340	8346	573016151	Advil 200 mg caplet bottle 200	otc	brand		200
2341	8039	85411201	Afrin 0.05% nasal spray squeez btl 15	otc	generic		15
2342	-80213	31011902227	Alaway eye itch relief 10ml 1	otc	generic		1
2343	21980	25866010506	Aleve 220 mg caplet bottle 100	otc	brand		100
2344	21980	25866010504	Aleve 220 mg caplet bottle 50	otc	brand		50
2345	-359789	1650055531	Alka seltzer + day/nite cold/flu l/gel 20 1	otc	brand		1
2346	36868	41167431004	Allegra-d 12 hour tablet UD blist pack 20	rx	brand		20
2347	58869	41167432007	Allegra-d 24 hour tablet bottle 15	rx	brand		15
2348	3773	67253090010	Alprazolam 0.25 mg tablet bottle 100	rx	generic	C-4	100
2349	-220257	3700027333	Always pantiliners long dri linr+odor 34 34	otc	brand		34
2350	46043	603221221	Amitriptyline hcl 10 mg tab bottle 100	rx	generic		100
2351	46044	603221621	Amitriptyline hcl 100 mg tab bottle 100	rx	generic		100
2352	60722	65862058501	Amlodipine-benazepril 5-40 mg bottle 100	rx	generic		100
2353	62180	781561531	Amlodipine-valsartan 5-160 mg bottle 30	rx	generic		30
2354	22123	35515095498	Arthritis pain er 650 mg caplt bottle 50	otc	generic		50
2355	22072	41167005740	Aspercreme heat 10% gel tube 70.8	otc	brand		70
2356	4381	603016921	Aspirin ec 325 mg tablet bottle 100	otc	generic		100
2357	15975	8137003690	Aveeno anti-itch lotion bottle 118	otc	brand		118
2358	-21621	38137003646	Aveeno rlf bodywash liqd 12 oz 360	otc	brand		360
2359	8084	225038080	Ayr saline 0.65% nose spray squeez btl 50	otc	brand		50
2360	66339	46017007703	Balneol cleansing lotion squeez btl 89	otc	brand		89
2361	-4588	1904500114	Ban classic reg roll 3.5 oz 105	otc	generic		105
2362	23031	8137004670	Band-aid clear adh bandage n/a 30	otc	generic		30
2363	-13181	7214085750	Basis facial cln cloth pada 20 ea 20	otc	brand		20
2364	-274996	4133300275	Battery dura h/a da10b8zm10 8	otc	brand		8
2365	4376	31284353635	Bayer tab reg 100	otc	brand		100
2366	49405	8290328418	Bd insulin syr 1 ml 8mmx31g box 100	otc	generic		100
2367	24972	12547017167	Benadryl itch stopping crm tube 28.3	otc	brand		28
2368	16041	43547033710	Benazepril hcl 20 mg tablet bottle 100	rx	generic		100
2369	2289	536665901	Biotin 300 mcg tablet bottle 100	otc	generic		100
2370	16901	40985027116	Biotin 5-000 mcg capsule bottle 110	otc	generic		110
2371	-399479	45611000931	Blue-emu 1% crm 28 gm 28	otc	brand		28
2372	46239	185041060	Bupropion hcl sr 100 mg tablet bottle 60	rx	generic		60
2373	4120	527131201	Butalbital comp-codeine #3 cap bottle 100	rx	generic	C-3	100
2374	59072	310651201	Byetta 5 mcg dose pen inj cartridge 1.2	rx	brand		1
2375	-26915	74312000690	C-1000 w/rh 1000mg tabs 100 100	otc	brand		100
2376	58018	536342408	Calcium 600-vit d3 400 tablet bottle 60	otc	generic		60
2377	64628	40985027493	Calcium citrate-vit d3 caplet bottle 120	otc	generic		120
2378	24154	11017010390	Callus cushion packet 1	otc	generic		1
2379	69971	5550919	Caltrate 600 plus d3 tablet bottle 60	otc	brand		60
2380	73116	5551750	Caltrate gummy bites Assorted fruit bottle 50	otc	brand		50
2381	37015	49884065809	Candesartan cilexetil 4 mg tab bottle 90	rx	generic		90
2382	-144577	7726000876	Candy russ lemon wedges 24x12z r s 12	rx	brand		12
2383	-141947	7726006912	Candy russ s/f chcmnt min 7 oz r s 1	rx	brand		1
2384	4558	13668026801	Carbamazepine 200 mg tablet bottle 100	rx	generic		100
2385	-159916	2900007037	Cashew halves&pieces 9.25oz 12 9.25	otc	brand		9
2386	48094	5417758	Centrum silver tablet bottle 150	otc	brand		150
2387	9043	69543010210	Cephalexin 500 mg capsule bottle 100	rx	generic		100
2388	-135760	67811201748	Chloraseptic mx wbry spin 30 ml 30	otc	generic		30
2389	9510	16571041210	Ciprofloxacin hcl 500 mg tab bottle 100	rx	generic		100
2390	46204	65862000701	Citalopram hbr 40 mg tablet bottle 100	rx	generic		100
2391	-277087	30766041976	Citrucel fiber capl 180 180	otc	brand		180
2392	16368	781196260	Clarithromycin 500 mg tablet bottle 60	rx	generic		60
2393	9339	591570801	Clindamycin hcl 150 mg capsule bottle 100	rx	generic		100
2394	7634	472040045	Clobetasol 0.05% cream tube 45	rx	generic		45
2395	344	591350904	Clonidine 0.2 mg/day patch box 4	rx	generic		4
2396	-26642	74098521341	Co q 10 30mg 30	otc	generic		30
2397	-313273	74098527434	Co q-10 cap 100mg 21st 150 150	otc	brand		150
2398	-313274	74098527435	Co q-10 cap 200mg 21st 120 120	otc	brand		120
2399	17134	54629067390	Coenzyme q10 50 mg capsule bottle 90	otc	generic		90
2400	-7174	3500055300	Colgate plus sft adult each 1	otc	brand		1
2401	-410583	35000051090	Colgate t p 0.24% 170gm whtg 1	otc	brand		1
2402	-7223	3500074118	Colgate total plus whtg gel 4.2 oz 126	otc	brand		126
2403	-130487	3500076232	Colgate tpst total ad wht5 8oz 1	otc	brand		1
2404	15723	75137058507	Compound w 17% gel tube 7	otc	generic		7
2405	27957	41100080743	Coricidin hbp softgel blist pack 20	otc	generic		20
2406	7544	74300000391	Cortizone-10 1% creme tube 28	otc	brand		28
2407	6562	56017270	Coumadin 5 mg tablet bottle 100	rx	brand		100
2408	-141632	3700020198	Crest gld fls pck mnt each 30 ea 30	otc	brand		30
2409	-356474	3700087182	Crest notice 10stp 1	otc	brand		1
2410	-32262	3700044981	Crest pro-hlth clnmnt mwsh 500  ml 500	otc	brand		500
2411	-152509	8019630023	Curad band sens skn spots 50 50	otc	brand		50
2412	47478	603307821	Cyclobenzaprine 5 mg tablet bottle 100	rx	generic		100
2413	-280013	2390001436	Dayquil liquid 10 oz 1	otc	brand		1
2414	-297121	3600012776	Depend u/w silh s/m 10 cs4 10	otc	brand		10
2415	-4513	1700002725	Dial complte foaming pump liqd 7.5 oz 225	otc	generic		225
2416	-17431	10331000016	Dickinsons witch hazel liqd 16 oz 480	otc	brand		480
2417	4918	378161005	Dicyclomine 10 mg capsule bottle 500	rx	generic		500
2418	5292	96072208	Dml lotion bottle 240	otc	brand		240
2419	46090	378425001	Doxepin 50 mg capsule bottle 100	rx	generic		100
2420	57891	51991074690	Duloxetine hcl dr 20 mg cap bottle 90	rx	generic		90
2421	57892	51991074790	Duloxetine hcl dr 30 mg cap bottle 90	rx	generic		90
2422	57893	51991074890	Duloxetine hcl dr 60 mg cap bottle 90	rx	generic		90
2423	6373	94030000202	Embrace test strips box 50	otc	generic		50
2424	384	51672403903	Enalapril maleate 10 mg tab bottle 1000	rx	generic		1000
2425	387	51672403801	Enalapril maleate 5 mg tablet UU bottle 100	rx	generic		100
2426	41199	65862065401	Entacapone 200 mg tablet bottle 100	rx	generic		100
2427	16879	49502050002	Epipen 2-pak 0.3 mg auto-injct syringe 2	otc	brand		2
2428	50760	16729017001	Escitalopram 20 mg tablet bottle 100	rx	generic		100
2429	47525	93645098	Esomeprazole mag dr 20 mg cap bottle 90	rx	generic		90
2430	47526	13668015590	Esomeprazole mag dr 40 mg cap bottle 90	rx	generic		90
2431	3202	378464226	Estradiol 0.05 mg patch box 8	rx	generic		8
2432	-13112	7214063378	Eucerin calming creme crm 8 oz 226	otc	brand		226
2433	15881	781724255	Fentanyl 50 mcg/hr patch box 5	rx	generic	C-2	5
2434	61349	51991070390	Ferrex 150 plus capsule bottle 90	otc	brand		90
2435	263	62559038101	Flecainide acetate 100 mg tab bottle 100	rx	generic		100
2436	265	62559038001	Flecainide acetate 50 mg tab bottle 100	rx	generic		100
2437	2947	132070402	Fleet bisacodyl ec 5 mg tab bottle 25	otc	brand		25
2438	3029	132020142	Fleet enema squeez btl 266	otc	brand		266
2439	2982	132007924	Fleet glycerin 2 gm adult supp box 24	otc	brand		24
2440	-392630	35310020010	Flonase (otc) nasal spr 0.05% 8	otc	brand		8
2441	13723	67405060203	Fluconazole 100 mg tablet bottle 30	rx	generic		30
2442	8079	24208034425	Flunisolide 0.025% spray squeez btl 25	rx	generic		25
2443	7612	51672136504	Fluocinolone 0.01% solution bottle 60	rx	generic		60
2444	7616	51672125303	Fluocinonide 0.05% cream jar 60	rx	generic		60
2445	7618	43199003160	Fluocinonide 0.05% solution bottle 60	rx	generic		60
2446	2365	30768000676	Folic acid 800 mcg tablet bottle 100	otc	generic		100
2447	22400	90891020018	Fq prevail underpad large packet 18	otc	generic		18
2448	-88624	45334024007	Free & clear 7oz hair style 210	otc	brand		210
2449	-160179	33436200004	Fresh n brite dentur t/p 3.8oz 3.8	otc	brand		3
2450	8208	781181810	Furosemide 20 mg tablet bottle 1000	rx	generic		1000
2451	21414	43547026650	Gabapentin 300 mg capsule bottle 500	rx	generic		500
2452	41805	68462012601	Gabapentin 600 mg tablet bottle 100	rx	generic		100
2453	66373	50383018902	Gatifloxacin 0.5% eye drops drop btl 2.5	rx	generic		2
2454	-151765	7431205145	Ginger root 550mg caps 100 ea 100	otc	brand		100
2455	-316625	4650073339	Glade aerosol powder fresh 8z jhn 8	rx	brand		8
2456	-26356	73313200230	Glide floss 50metr unflv each 1 ea 1	otc	generic		1
2457	-37577	3700003866	Glide floss dp cln mnt each 1 ea 1	otc	brand		1
2458	1777	781145210	Glipizide 5 mg tablet bottle 1000	rx	generic		1000
2459	21840	59762054101	Glipizide xl 5 mg tablet bottle 100	rx	generic		100
2460	43402	70074054545	Glucerna shake bottle 237	otc	brand		237
2461	-80300	74098523023	Gluco/chond original strength 120	otc	generic		120
2462	9813	88101002	Gly-oxide liquid bottle 60	otc	brand		60
2463	62533	52268010001	Golytely solution bottle 4000	rx	brand		4000
2464	-83155	31254663329	Halls c/dr sf hnybry lozg 25 ea 25	otc	brand		25
2465	-38164	3700006203	Head&shldr 2n1 clscn 1% shmp 420 ml 420	otc	brand		420
2466	-46991	3400013100	Hershey kisses reg 1	otc	generic		1
2467	-41305	3400004911	Hershey s.f. york patties 1	otc	generic		1
2468	-394721	3400079001	Hershey's dk choc caramel bag 7.2oz 7.2	otc	brand		7
2469	-394716	3400079000	Hershey's milk choc caramel bag 7.2oz 7.2	otc	brand		7
2470	-7953	3566421466	Hose jbst kh 30-40md sbge hose 2 ea 2	otc	brand		2
2471	8182	29300012810	Hydrochlorothiazide 25 mg tab bottle 1000	rx	generic		1000
2472	60338	64376064801	Hydrocodon-acetaminophen 5-300 bottle 100	rx	generic	C-2	100
2473	31773	574709312	Hydrocortisone 30 mg supp box 12	rx	generic		12
2474	16266	51672201302	Hydrocortisone-aloe 1% cream tube 28.4	otc	generic		28
2475	-3234	869470410	Hydrogen peroxide 3% soln 240 ml 240	otc	generic		240
2476	4112	527135401	Hydromorphone 4 mg tablet bottle 100	rx	generic	C-2	100
2477	-24041	54973295602	Hyl leg cramps quinin tabs 100 100	otc	brand		100
2478	8350	55111068401	Ibuprofen 800 mg tablet bottle 100	rx	generic		100
2479	71924	65804010	Icaps areds formula dr tablet bottle 120	otc	brand		120
2480	-198439	34000720606	Ice breakers s f cool mints 1	otc	brand		1
2481	31099	168043224	Imiquimod 5% cream packet packet 24	rx	generic		24
2482	24456	54004641	Ipratropium 0.06% spray canister 15	rx	generic		15
2483	41234	43547033009	Irbesartan-hctz 150-12.5 mg tb bottle 90	rx	generic		90
2484	9416	185435030	Isoniazid 300 mg tablet bottle 30	rx	generic		30
2485	22118	8137009234	J & j dental floss waxed packet 1	otc	generic		1
2486	62532	6057761	Janumet 50-1-000 mg tablet bottle 60	rx	brand		60
2487	61613	6011231	Januvia 50 mg tablet bottle 30	rx	brand		30
2488	-4644	1910000975	Jergens mild lt enr hand liqd 225	otc	brand		225
2489	-33010	81370095668	Jj act x2 icy spmnt mwsh 18 oz 540	otc	brand		540
2490	-185902	38137002667	Jj baby powd lavender/chamomil 22oz 3043 22	rx	brand		22
2491	-74512	3566412014	Jobst body adh it st 2z 112014 12's 2	otc	generic		2
2492	5517	8137003001	Johnson's baby powder jar 45	otc	brand		45
2493	8252	486111101	K-phos original tablet bottle 100	rx	brand		100
2494	-12209	7061021630	Kerr vial 30dr scp ambr rxvl 130 ea 130	otc	brand		130
2495	7334	168009960	Ketoconazole 2% cream tube 60	rx	generic		60
2496	22403	36000001066	Kotex lightdays pantiliners packet 40	otc	generic		40
2497	22403	36000003900	Kotex ultra thin long maxi packet 20	otc	generic		20
2498	1880	536673101	L-lysine 500 mg tablet bottle 100	otc	generic		100
2499	-127129	31184514935	L-tryptophan s/frml 500mg caps 60 ea 60	otc	generic		60
2500	17871	51672413101	Lamotrigine 100 mg tablet bottle 100	rx	generic		100
2501	6653	527134701	Levothyroxine 125 mcg tablet bottle 100	rx	generic		100
2502	6654	378181577	Levothyroxine 150 mcg tablet bottle 90	rx	generic		90
2503	6656	527135101	Levothyroxine 200 mcg tablet bottle 100	rx	generic		100
2504	-48114	1900008503	Lifesaver candy bag peppermint 6.25	otc	generic		6
2505	-193234	3746602077	Lindt lindor truffles dark choc 60ct3513 1	rx	brand		1
2506	-193233	3746602078	Lindt lindor truffles white choc 60ct 1	rx	brand		1
2507	6659	42794001802	Liothyronine sod 5 mcg tab bottle 100	rx	generic		100
2508	-407038	88947651100	Lipo-flavonoid plus capl 100 100	otc	brand		100
2509	-360984	1254744015	Lister gntle g/c floss 50yd 1	otc	brand		1
2510	-249018	31254730622	Listerine ttl/c pl/w 0.0221 mwsh 473 ml 473	otc	brand		473
2511	60904	32671000532	Lite touch pen needle 31g box 100	otc	generic		100
2512	40262	496088215	Lmx 4 4% cream tube 15	otc	brand		15
2513	2842	93031101	Loperamide 2 mg capsule bottle 100	rx	generic		100
2514	18698	45802065078	Loratadine 10 mg tablet bottle 100	otc	generic		100
2515	23381	13668011310	Losartan potassium 25 mg tab bottle 1000	rx	generic		1000
2516	23465	13668011630	Losartan-hctz 50-12.5 mg tab bottle 30	rx	generic		30
2517	6460	68180046803	Lovastatin 20 mg tablet bottle 1000	rx	generic		1000
2518	-249588	81483201042	Luden thr/d hny/licr bag30 30	otc	brand		30
2519	65392	23320505	Lumigan 0.01% eye drops drop btl 5	rx	brand		5
2520	58728	74098527073	Lutein softgel 20mg gcap 60 ea 60	otc	generic		60
2521	-96872	4000001244	M/m peanut butter 24	otc	generic		24
2522	-24719	61480120040	Magna therm thermome mf eqip 1	otc	generic		1
2523	1408	60258017101	Magnesium oxide 400 mg tablet bottle 120	otc	generic		120
2524	-191459	31184505531	Mas stool softener 100mg 100 553 100	rx	brand		100
2525	4731	536101701	Meclizine 12.5 mg caplet bottle 100	rx	generic		100
2526	71356	74312007903	Melatonin 3 mg odt bottle 240	otc	generic		240
2527	13318	23155010210	Metformin hcl 500 mg tablet bottle 1000	rx	generic		1000
2528	46754	67877015905	Metformin hcl er 500 mg tablet bottle 500	rx	generic		500
2529	4240	67877011601	Methadone hcl 10 mg tablet bottle 100	rx	generic	C-2	100
2530	36872	54455015	Methotrexate 2.5 mg tablet bottle 36	rx	generic		36
2531	68879	51672529509	Metronidazole topical 1% gel bottle 55	rx	generic		55
2532	-313406	4000042206	Milky way original 36	otc	brand		36
2533	-83711	4110082071	Miralax pwd 17.9oz 17.09	otc	generic		17
2534	46452	60505024901	Mirtazapine 45 mg tablet bottle 30	rx	generic		30
2535	31186	60505083001	Mometasone furoate 50 mcg spry canister 17	rx	generic		17
2536	4097	42858080301	Morphine sulf er 60 mg tablet bottle 100	rx	generic	C-2	100
2537	12073	63824005736	Mucinex d er 600-60 mg tablet blist pack 36	otc	brand		36
2538	12074	63824005640	Mucinex dm er 600-30 mg tablet box 40	otc	brand		40
2539	71885	63824000815	Mucinex er 600 mg tablet blist pack 100	otc	brand		100
2540	41845	67877023022	Mycophenolate 200 mg/ml susp Mixed fruit bottle 160	rx	generic		160
2541	-39319	7431213794	N/b cranberry sftgl trp/str 60 24's 60	otc	brand		60
2542	-75241	7431213168	N/b super b+vit c capl 100 24's 100	otc	generic		100
2543	-45278	4400000210	Nab ritz peanut butter crack s 1	otc	generic		1
2544	-193358	4400088211	Nabisco chees crackr w/chedar chees 8ct 1	rx	brand		1
2545	5137	378113201	Nadolol 80 mg tablet bottle 100	rx	generic		100
2546	-356524	4116758005	Nasacort otc 0.57oz spy 16.9	otc	brand		16
2547	-89430	7431204886	Nb co q10 100mg softgel bonus 45	otc	generic		45
2548	-204812	7431219939	Nb d-2000iu ss 2000iu gcap 200 ea 200	otc	brand		200
2549	-174061	7431203925	Nb fish oil 1000 mg odorless s 200	otc	brand		200
2550	-38977	74312413834	Nb iron tab 65mg         41383 100's 1	otc	brand		1
2551	-49828	7431201683	Nb l-carnitin 60 500mg twn 60	otc	brand		60
2552	-414197	3076864825	Nb osteo total joint/mscle tab 80	otc	brand		80
2553	-177092	7431219377	Nb vit d3 5000iu sftgel 100	otc	brand		100
2554	-360507	7431229176	Nb vit d3 softgel 5000iu 200 200	otc	brand		200
2555	-257721	1221551297	Necco wafers chocolate 24ct 1297 1	otc	brand		1
2556	-316470	241035301	Neo-syneph spray 1/2 n-515 15ml 1	otc	brand		1
2557	7544	12547023852	Neosporin 1% anti-itch cream tube 28	otc	brand		28
2558	-152927	31254723779	Neosporin+ pain crm 1oz 1	otc	brand		1
2559	-175373	8680087310	Neutro ultra snblk spf100 8731 3	otc	brand		3
2560	73231	573245014	Nexium 24hr 22.3 mg capsule bottle 14	otc	brand		14
2561	73231	573245042	Nexium 24hr 22.3 mg capsule bottle 42	otc	brand		42
2562	2386	54629007101	Niacin 100 mg tablet bottle 100	otc	generic		100
2563	20616	93081955	Nifedical xl 30 mg tablet bottle 300	rx	generic		300
2564	-151651	72140119515	Nivea tch of smthnss b/w liqd 16.9 oz 500	otc	brand		500
2565	6933	55741040202	Nupercainal 1% ointment tube 56.7	otc	brand		56
2566	-282492	323900014268	Nyquil liquid cold/flu orig 1	otc	brand		1
2567	-313222	30187526003	Ocean saline nasal spr 1.5oz 1.5	otc	brand		1
2568	61112	24208038762	Ocuvite with lutein tablet bottle 120	otc	brand		120
2569	43136	62175011432	Omeprazole dr 10 mg capsule bottle 30	rx	generic		30
2570	41563	378773497	Ondansetron odt 8 mg tablet bottle 10	rx	generic		10
2571	63964	10310032280	Orajel dry mouth gel tube 42	otc	brand		42
2572	4929	603497528	Oxybutynin 5 mg tablet bottle 500	rx	generic		500
2573	42606	93520801	Oxybutynin cl er 15 mg tablet bottle 100	rx	generic		100
2574	41046	93520601	Oxybutynin cl er 5 mg tablet bottle 100	rx	generic		100
2575	72868	59011048010	Oxycontin 80 mg tablet bottle 100	rx	brand	C-2	100
2576	-81106	8087800406	Pantene pro v 2n1 clscn shmp 12.6 oz 378	otc	generic		378
2577	46223	43547034809	Paroxetine hcl 20 mg tablet bottle 90	rx	generic		90
2578	62659	62175044601	Peg 3350 electrolyte soln Unflavored bottle 4000	rx	generic		4000
2579	-412646	3320000092	Pepsodent t/p reg 5.5oz 1	otc	brand		1
2580	-426263	1320000508	Phazyme 180mg gcap 48 ea 48	otc	brand		48
2581	-297046	31284354120	Phillips colon health cap 60 60	otc	brand		60
2582	3009	12843003520	Phillips' lax liqui-gels bottle 30	otc	brand		30
2583	3026	12843035302	Phillips' milk of magnesia bottle 355	otc	brand		355
2584	10959	79573001115	Pill splitter box 1	otc	generic		1
2585	-296505	854065002109	Pine bros softish drp nat hny 32	otc	brand		32
2586	-296506	854065002116	Pine bros softish drp wld chry 32	otc	brand		32
2587	42944	16729002110	Pioglitazone hcl 30 mg tablet bottle 30	rx	generic		30
2588	-93068	2900007370	Planters mixed nuts with peanu 12	otc	generic		12
2589	22542	86414000116	Plax advanced formula rinse bottle 473	otc	brand		473
2590	10362	36000033593	Poise pads bag 27	otc	generic		27
2591	48570	60758090810	Polymyxin b-tmp eye drops drop btl 10	rx	generic		10
2592	-271227	5210008979	Ponds even 30twl  1	otc	brand		1
2593	-271226	5210008978	Ponds morn 30twl  1	otc	brand		1
2594	1248	574018105	Potassium cl er 10 meq capsule bottle 500	rx	generic		500
2595	31782	13668009290	Pramipexole 0.25 mg tablet bottle 90	rx	generic		90
2596	16367	16252052750	Pravastatin sodium 20 mg tab bottle 500	rx	generic		500
2597	20741	16252052850	Pravastatin sodium 40 mg tab bottle 500	rx	generic		500
2598	24145	126028766	Prevident 5000 plus cream Spearmint jar 51	rx	brand		51
2599	25703	37000045502	Prilosec otc 20.6 mg tablet blist pack 14	otc	brand		14
2600	25703	37000035906	Prilosec otc 20.6 mg tablet blist pack 28	otc	brand		28
2601	25703	37000045504	Prilosec otc 20.6 mg tablet blist pack 42	otc	brand		42
2602	19751	603544921	Propafenone hcl 225 mg tab bottle 100	rx	generic		100
2603	5125	23155011201	Propranolol 40 mg tablet bottle 100	rx	generic		100
2604	-178535	63551595866	Qc adhes super strips 1x3 jj 20  20	rx	brand		20
2605	10463	63868089715	Qc adhesive tape 1" n/a 1	otc	generic		1
2606	-142752	63551598296	Qc alcohol isopropyl 91% 16 oz  16	rx	brand		16
2607	17037	35515095898	Qc all day allergy 10 mg tab bottle 90	otc	generic		90
2608	51850	35515099223	Qc artificial tears drops drop btl 15	otc	brand		15
2609	4376	35515090432	Qc aspirin 325 mg tablet bottle 300	otc	generic		300
2610	-142718	63551590817	Qc bandage ahsv shr stp 2x4 10  1	rx	brand		1
2611	-218257	63551595993	Qc cold cong sev daytime tyl 24  24	rx	brand		24
2612	11582	35515094579	Qc complete allergy 25 mg cap bottle 100	otc	generic		100
2613	-144312	63551595727	Qc cosmetic foam rounds 12ct  12	rx	brand		12
2614	-133629	63551590023	Qc fiberlax caplet fibercon 90  90	rx	generic		90
2615	-142032	63551598076	Qc ibuprofen 200mg capl adv100  100	rx	brand		100
2616	-405519	63551599164	Qc mouth guard max protection 99164 1	otc	brand		1
2617	21980	35515097459	Qc naproxen sod 220 mg tablet bottle 100	otc	generic		100
2618	4490	35515095609	Qc non-aspirin 500 mg gelcap bottle 50	otc	generic		50
2619	3699	35515095105	Qc non-aspirin pm geltab bottle 50	otc	generic		50
2620	47690	35515094594	Qc triple antibiotic oint tube 28.4	otc	generic		28
2621	-142689	63551590167	Qc vit calcium 600 w/d caltr 60  60	rx	brand		60
2622	-193181	63551599553	Qc vit chromium picolinate 100 99553 100	rx	brand		100
2623	-401759	63551598663	Qc vit lutein tab 10mg 30 98663 30	otc	brand		30
2624	34188	67877025001	Quetiapine fumarate 100 mg tab bottle 100	rx	generic		100
2625	18773	59762502101	Quinapril 20 mg tablet bottle 90	rx	generic		90
2626	40941	65162072409	Rabeprazole sod dr 20 mg tab bottle 90	rx	generic		90
2627	11674	68462024901	Ranitidine 300 mg tablet bottle 100	rx	generic		100
2628	-58453	51660060660	Ranitidine tab  75mg ohm 60660 60	otc	generic		60
2629	-47353	3400044600	Reese miniature peg pack 1	otc	generic		1
2630	1913	212186462	Resource breeze liquid bottle 237	otc	generic		237
2631	-192424	3660230245	Ricola sf cough/throat drop lem/mint 45 45	rx	brand		45
2632	-8170	3660230207	Ricola sf ech grn tea lozg 19 ea 19	otc	brand		19
2633	-10779	4740026279	Right gd sport a/p unscnt aero 6 oz 180	otc	brand		180
2634	16414	904005309	Robafen-dm syrup bottle 237	otc	generic		237
2635	-134453	42002010502	Rogaine wmn hair reg/s kit 2oz 2	otc	brand		2
2636	51785	57237017090	Rosuvastatin calcium 20 mg tab bottle 90	rx	generic		90
2637	-84227	7726009632	Russel stov s/f coconut 3oz 10 3	otc	generic		3
2638	-370110	34658175001	Salonpas pain patch hot lg 75001 1	otc	brand		1
2639	23766	145062805	Sarna anti-itch lotion bottle 222	otc	brand		222
2640	-17606	11017026200	Sch corn/callus rmvr 12.6% liqd 10ml 10	otc	brand		10
2641	-203800	2052510434	Schiff megared omega-3 sftg 60 60	otc	brand		60
2642	-3585	1150900162	Sea bond dentr adh upper ptch 15 15	otc	brand		15
2643	-27085	74312032011	Selenium 200mcg tabs 100 ea 100	otc	brand		100
2644	11514	10119000238	Sensitive eyes plus saline bottle 355	otc	generic		355
2645	-19955	31015808504	Sensodyne tart cntrl pste 4oz 120	otc	brand		120
2646	46228	68180035202	Sertraline hcl 50 mg tablet bottle 500	rx	generic		500
2647	-177372	7417024836	Sh hard nails adv strngth tc  1	rx	brand		1
2648	-278092	22600002930	Simply saline 0.9% spin 89 ml 89	otc	brand		89
2649	-25866	70592800200	Sinus rinse rfl pckt 100 ea 100	otc	brand		100
2650	3009	536106229	Stool softener 100 mg softgel bottle 100	otc	generic		100
2651	2766	93221005	Sucralfate 1 gm tablet bottle 500	rx	generic		500
2652	-10505	4561100204	Super strength blue- emu crm 4 oz 120	otc	generic		120
2653	6651	74662490	Synthroid 100 mcg tablet bottle 90	rx	brand		90
2654	6648	74434190	Synthroid 25 mcg tablet bottle 90	rx	brand		90
2655	6650	74518290	Synthroid 75 mcg tablet bottle 90	rx	brand		90
2656	5977	70501009220	T-gel 0.5% therapeutic shampoo bottle 250	otc	brand		250
2657	27546	228299650	Tamsulosin hcl 0.4 mg capsule bottle 500	rx	generic		500
2658	3690	67877014701	Temazepam 30 mg capsule bottle 100	rx	generic	C-4	100
2659	10362	80040046500	Tena serenity pads bag 30	otc	generic		30
2660	22652	59746038606	Terazosin 10 mg capsule bottle 100	rx	generic		100
2661	64561	58790000360	Thera tears nutrition capsule bottle 90	otc	brand		90
2662	-458257	9800057186	Tic tac mixer cherry cola 12ct 1	otc	brand		1
2663	7856	17478028811	Timolol 0.5% eye drops drop btl 10	rx	generic		10
2664	66128	24090049684	Tirosint 112 mcg capsule UD blist pack 28	rx	brand		28
2665	37042	65162091446	Tobramycin 300 mg/5 ml ampule ampul 5	rx	generic		5
2666	-91047	60986304704	Topricin jar 1	otc	generic		1
2667	21408	31722053101	Torsemide 20 mg tablet bottle 100	rx	generic		100
2668	4736	536101801	Travel sickness 25 mg tab chew bottle 100	otc	generic		100
2669	5799	472011720	Tretinoin 0.025% cream tube 20	rx	generic		20
2670	7597	45802005505	Triamcinolone 0.1% ointment jar 454	rx	generic		454
2671	7597	168000615	Triamcinolone 0.1% ointment tube 15	rx	generic		15
2672	2599	64980032005	Triamcinolone 0.1% paste Vanilla tube 5	rx	generic		5
2673	8176	781112301	Triamterene-hctz 37.5-25 mg tb bottle 100	rx	generic		100
2674	-80521	1254607506	Trident original 18ct ct12 18	otc	brand		18
2675	7942	59762004001	Trifluridine 1% eye drops drop btl 7.5	rx	generic		7
2676	2689	766074052	Tums tablet chewable Peppermint bottle 150	otc	brand		150
2677	22123	50580011221	Tylenol arthritis er 650 mg tb bottle 100	otc	brand		100
2678	-368704	3600001084	U by kotex maxi pad24 cs8 192	otc	brand		192
2679	50805	591216719	Valsartan 40 mg tablet bottle 90	rx	generic		90
2680	-23065	45334032039	Vanicream cleansing bar soap 3.9oz 117	otc	brand		117
2681	-26686	74098522294	Vit c-250 mg 110	otc	generic		110
2682	-321034	81655901172	Vo5 hr spr 8.5z supr hth can 1	otc	brand		1
2683	6563	93171901	Warfarin sodium 7.5 mg tablet bottle 100	rx	generic		100
2684	-127832	7674007012	Whitmans sampler gft/bx 12oz 6 12	otc	generic		12
2685	40663	4025752	Xenical 120 mg capsule bottle 90	rx	brand		90
2686	-181371	54756091623	Xxlker 4wheel 1  1	otc	brand		1
2687	-173269	85593300051	Xylimelts for dry mouth 40 40	otc	brand		40
2688	-373405	3400004752	York peppermint patties bag 12oz 12	otc	brand		12
2689	11673	81421003202	Zantac 150 mg tablet blist pack 24	otc	brand		24
2690	-24903	62750000411	Zicam allergy spin 0.5oz 15	otc	brand		15
2691	-247739	73221630069	Zicam cold rem+ oral mist 1oz 1	otc	brand		1
2692	5576	168006231	Zinc oxide 20% ointment tube 28.35	otc	generic		28
2693	-190939	44444053516	Abdominal pad sterile 5x9 36 7196d 1	rx	brand		1
2694	19413	65702048310	Accu-chek nano smartview meter box 1	otc	generic		1
2695	-282006	5113119808	Ace ankle brace knit med 1	otc	brand		1
2696	-282005	5113119807	Ace ankle brace knit sm 1	otc	brand		1
2697	-281995	5113120369	Ace band self adh beige 3" 1	otc	brand		1
2698	5145	65162066910	Acebutolol 200 mg capsule bottle 100	rx	generic		100
2699	4475	45802073230	Acetaminophen 120 mg suppos box 12	otc	generic		12
2700	4165	406048401	Acetaminophen-cod #3 tablet bottle 100	rx	generic	C-3	100
2701	591	409330703	Acetylcysteine 10% vial vial 30	rx	generic		30
2702	592	409330803	Acetylcysteine 20% vial vial 30	rx	generic		30
2703	23441	51660035230	Acid reducer 75 mg tablet blist pack 30	otc	generic		30
2704	48702	54092038701	Adderall xr 20 mg capsule bottle 100	rx	brand	C-2	100
2705	625	31874220	Adt robitussin peak cld m-s lq bottle 237	otc	brand		237
2706	13556	573016940	Advil 200 mg liqui-gel capsule bottle 80	otc	brand		80
2707	-77478	30573015475	Advil 200mg 200 1	otc	generic		1
2708	12060	591319401	Afeditab cr 60 mg tablet bottle 100	rx	brand		100
2709	-10982	4786510004	Airborne eff lemon/ lime tabs 10 ea 10	otc	brand		10
2710	5040	24208034720	Albuterol 5 mg/ml solution bottle 20	rx	generic		20
2711	5039	487950101	Albuterol sul 2.5 mg/3 ml soln UD vial 3	rx	generic		3
2712	5039	487950160	Albuterol sul 2.5 mg/3 ml soln UD vial 3	rx	generic		3
2713	-3203	869115810	Alcohol isopropyl 70% soln 128oz 3840	otc	generic		3840
2714	-247138	3367460194	Alive mns ene tabs 50 ea 50	otc	brand		50
2715	-294839	3367460243	Alive womens tabs 50 ea 50	otc	brand		50
2716	-412780	16500056664	Alka-sel hrtbrn rlf chew 36 ea 36	otc	brand		36
2717	-173216	1650053760	Alka-seltz+eff cold cough 20 20	otc	brand		20
2718	2535	591554310	Allopurinol 100 mg tablet bottle 1000	rx	generic		1000
2719	3774	67253090111	Alprazolam 0.5 mg tablet bottle 1000	rx	generic	C-4	1000
2720	3775	67253090250	Alprazolam 1 mg tablet bottle 500	rx	generic	C-4	500
2721	4575	69452014220	Amantadine 100 mg capsule bottle 100	rx	generic		100
2722	17241	51672405506	Amiodarone hcl 100 mg tablet bottle 30	rx	generic		30
2723	8990	60432006547	Amox-clav 250-62.5 mg/5 ml sus Orange bottle 150	rx	generic		150
2724	48449	93867578	Amox-clav 600-42.9 mg/5 ml sus bottle 75	rx	generic		75
2725	48449	143985316	Amox-clav 600-42.9 mg/5 ml sus Orange bottle 125	rx	generic		125
2726	8997	93415073	Amoxicillin 125 mg/5 ml susp Mixed berry bottle 100	rx	generic		100
2727	42683	143988701	Amoxicillin 400 mg/5 ml susp Fruit bottle 100	rx	generic		100
2728	42683	143988750	Amoxicillin 400 mg/5 ml susp Fruit bottle 50	rx	generic		50
2729	42683	143988775	Amoxicillin 400 mg/5 ml susp Fruit bottle 75	rx	generic		75
2730	67366	51846233	Androgel 1.62% gel pump bottle 75	rx	brand	C-3	75
2731	7340	536515026	Anti-fungal 1% powder jar 45	otc	generic		45
2732	6858	713050312	Anucort-hc 25 mg suppository box 12	rx	brand		12
2733	61991	58914021460	Aquadeks pediatric liquid Black cherry drop btl 60	otc	brand		60
2734	51333	13668021890	Aripiprazole 10 mg tablet bottle 90	rx	generic		90
2735	51336	13668022190	Aripiprazole 30 mg tablet bottle 90	rx	generic		90
2736	72722	173087410	Arnuity ellipta 100 mcg inh UD blist pack 30	rx	brand		30
2737	22123	24385062981	Arthritis pain rlf er 650 mg bottle 200	otc	generic		200
2738	63178	17478006235	Artificial tears eye ointment tube 3.5	otc	brand		3
2739	73197	85433401	Asmanex hfa 200 mcg inhaler canister 13	rx	brand		13
2740	5138	781150701	Atenolol 100 mg tablet bottle 100	rx	generic		100
2741	5139	93075210	Atenolol 50 mg tablet bottle 1000	rx	generic		1000
2742	61173	15584010101	Atripla tablet UU bottle 30	rx	brand		30
2743	7866	17478021502	Atropine 1% eye drops drop btl 2	rx	generic		2
2744	-28592	81370003955	Aveeno bdy cln strs relief liqd 12 oz 360	otc	brand		360
2745	-21532	38137001310	Aveeno postv radiant cln pada 28 ea 28	otc	brand		28
2746	29893	60505083305	Azelastine 0.1% (137 mcg) spry squeez btl 30	rx	generic		30
2747	65577	45802002683	Azelastine 0.15% nasal spray squeez btl 30	rx	generic		30
2748	60915	68546014256	Azilect 0.5 mg tablet bottle 30	rx	brand		30
2749	26721	781149668	Azithromycin 250 mg tablet UD blist pack 6	rx	generic		6
2750	4679	527133010	Baclofen 10 mg tablet bottle 1000	rx	generic		1000
2751	8528	74196607	Bacteriostatic saline vial vial 30	rx	generic		30
2752	-136588	44444039406	Bag loctop 1000  1	otc	brand		1
2753	10523	8137005685	Band-aid flexible fab bandages box 10	otc	generic		10
2754	10523	8137004667	Band-aid sheer strip n/a 40	otc	generic		40
2755	16167	11701004623	Baza protect cream jar 57	otc	brand		57
2756	49338	8290309581	Bd 3 ml syringe 25gx1" box 100	otc	generic		100
2757	49403	8290328438	Bd insulin syr 0.3 ml 8mmx31g box 100	otc	generic		100
2758	49401	8290328411	Bd insulin syr 1 ml 12.7mmx30g box 100	otc	generic		100
2759	-188314	1803095753	Bd luerlok syr 3cc 21g 1" 100 309575 1	rx	brand		1
2760	-91598	5212305195	Bd needle 18gx1        #305195 100	otc	brand		100
2761	58590	8290305901	Bd safetyglide needle packet 50	otc	generic		50
2762	49235	8290328203	Bd ultra-fine ndl 12.7mmx29g box 100	otc	generic		100
2763	-198	20004	Bed check valve dshp eqip 1 ea 1	otc	generic		1
2764	71057	62856052960	Belviq 10 mg tablet bottle 60	rx	brand	C-4	60
2765	11582	12547017021	Benadryl 25 mg liqui-gels blist pack 24	otc	brand		24
2766	16042	43547033810	Benazepril hcl 40 mg tablet bottle 100	rx	generic		100
2767	-356594	31254717157	Bendaryl es itch stop gel3.5oz 3.5	otc	brand		3
2768	50289	65597010390	Benicar 20 mg tablet bottle 90	rx	brand		90
2769	7568	472038045	Betamethasone dp 0.05% crm tube 45	rx	generic		45
2770	7561	45802037632	Betamethasone dp aug 0.05% crm tube 50	rx	generic		50
2771	4742	65162057110	Bethanechol 5 mg tablet bottle 100	rx	generic		100
2772	41889	60793060010	Bicillin c-r 1.2 million unit syringe 2	rx	brand		2
2773	-329076	31124010018	Biofreeze arex 4 oz 118	otc	brand		118
2774	2944	713010905	Bisac-evac 10 mg suppository box 500	otc	brand		500
2775	2947	536338101	Bisacodyl ec 5 mg tablet bottle 100	otc	generic		100
2776	17955	29300012701	Bisoprolol fumarate 10 mg tab bottle 100	rx	generic		100
2777	-24857	62103333002	Boudreauxs butt pst 16% pste 2oz 60	otc	brand		60
2778	3359	409116301	Bupivacaine 0.5% vial vial 50	rx	generic		50
2779	29313	54017713	Buprenorphine 8 mg tablet sl bottle 30	rx	generic	C-3	30
2780	46236	60505015801	Bupropion hcl 75 mg tablet bottle 100	rx	generic		100
2781	50496	185111160	Bupropion hcl sr 200 mg tablet bottle 60	rx	generic		60
2782	-213699	79285000112	Burts bees b/wsh ppmt/rsmy 12z  12	rx	brand		12
2783	3781	68382018105	Buspirone hcl 10 mg tablet bottle 500	rx	generic		500
2784	27378	378116580	Buspirone hcl 15 mg tablet bottle 180	rx	generic		180
2785	27378	591071860	Buspirone hcl 15 mg tablet bottle 60	rx	generic		60
2786	3782	68382018005	Buspirone hcl 5 mg tablet bottle 500	rx	generic		500
2787	47644	378114501	Buspirone hcl 7.5 mg tablet bottle 100	rx	generic		100
2788	4149	603255321	Butalb-caff-acetaminoph-codein bottle 100	rx	generic	C-3	100
2789	-411998	4698501621	Buzz away spry 2 oz 60	otc	brand		60
2790	36654	456140530	Bystolic 5 mg tablet bottle 30	rx	brand		30
2791	64628	904527260	Calcitrate + vit d caplet bottle 100	otc	generic		100
2792	39187	904506260	Calcitrate 200 mg (950 mg) tab bottle 100	otc	generic		100
2793	-457188	84009310067	Calcium 1200mg+vit-d3 5000iu 100+20bonus 120	otc	brand		120
2794	43733	904585692	Calcium 600-vit d3 200 tablet bottle 150	otc	generic		150
2795	66083	536342201	Calcium lactate 648 mg tablet bottle 100	otc	generic		100
2796	4559	51672404101	Carbamazepine 100 mg tab chew bottle 100	rx	generic		100
2797	4557	60432012916	Carbamazepine 100 mg/5 ml susp Citrus-vanilla bottle 450	rx	generic		450
2798	4558	13668026810	Carbamazepine 200 mg tablet bottle 1000	rx	generic		1000
2799	4663	591551310	Carisoprodol 350 mg tablet bottle 1000	rx	generic	C-4	1000
2800	22233	68001015103	Carvedilol 12.5 mg tablet bottle 500	rx	generic		500
2801	-134487	8019631234	Cast prot adult arm cur200aa 1	otc	generic		1
2802	-134493	8019631249	Cast prot adult leg cur200al 1	otc	generic		1
2803	58005	65862021901	Cefdinir 250 mg/5 ml susp Strawberry cream bottle 100	rx	generic		100
2804	58005	65862021960	Cefdinir 250 mg/5 ml susp Strawberry cream bottle 60	rx	generic		60
2805	16932	781543901	Cefpodoxime 200 mg tablet bottle 100	rx	generic		100
2806	9162	781320895	Ceftriaxone 1 gm vial vial 1	rx	generic		1
2807	9162	60505075204	Ceftriaxone 1 gm vial vial 1	rx	generic		1
2808	9136	65862069920	Cefuroxime axetil 250 mg tab bottle 20	rx	generic		20
2809	46203	456402001	Celexa 20 mg tablet bottle 100	rx	brand		100
2810	65992	5475756	Centrum men's tablet bottle 120	otc	brand		120
2811	9046	93417773	Cephalexin 250 mg/5 ml susp Mixed berry bottle 100	rx	generic		100
2812	9043	68180012202	Cephalexin 500 mg capsule bottle 500	rx	generic		500
2813	-294651	2993093108	Cetaphil drm 8oz liq  1	otc	brand		1
2814	17037	16571040210	Cetirizine hcl 10 mg tablet bottle 100	otc	generic		100
2815	24484	378363501	Cetirizine hcl 5 mg tablet bottle 100	otc	generic		100
2816	60897	69046956	Chantix 1 mg tablet bottle 56	rx	brand		56
2817	4902	76439030210	Chlordiazepoxide-clidinium cap bottle 100	rx	generic		100
2818	8174	378016201	Chlorothiazide 500 mg tablet bottle 100	rx	generic		100
2819	3788	641139835	Chlorpromazine 25 mg/ml amp ampul 2	rx	generic		2
2820	4422	54838052270	Choline mag trisal liquid Cherry bottle 240	rx	generic		240
2821	51657	2446430	Cialis 20 mg tablet bottle 30	rx	brand		30
2822	53296	2446230	Cialis 5 mg tablet bottle 30	rx	brand		30
2823	37020	51672530200	Ciclopirox 8% solution bottle 6.6	rx	generic		6
2824	39552	50419077301	Cipro 10% suspension Strawberry bottle 100	rx	brand		100
2825	46204	65862000705	Citalopram hbr 40 mg tablet bottle 500	rx	generic		500
2826	9346	574012901	Clindamycin 75 mg/5 ml soln Cherry bottle 100	rx	generic		100
2827	7727	51672408103	Clindamycin ph 1% solution bottle 30	rx	generic		30
2828	7727	472098792	Clindamycin ph 1% solution bottle 60	rx	generic		60
2829	6575	49884070155	Clomiphene citrate 50 mg tab UD blist pack 30	rx	generic		30
2830	4560	93083205	Clonazepam 0.5 mg tablet bottle 500	rx	generic	C-4	500
2831	7361	51672200201	Clotrimazole 1% cream tube 15	rx	generic		15
2832	7361	68462018135	Clotrimazole 1% cream tube 30	rx	generic		30
2833	7361	51672200202	Clotrimazole 1% cream tube 30	rx	generic		30
2834	2629	395194988	Clove oil bottle 3.5	otc	generic		3
2835	27037	93440401	Clozapine 50 mg tablet bottle 100	rx	generic		100
2836	4186	54024425	Codeine sulfate 30 mg tablet bottle 100	rx	generic	C-2	100
2837	6373	193709021	Contour test strips box 100	otc	generic		100
2838	6705	9001201	Cortef 5 mg tablet bottle 50	rx	brand		50
2839	-414490	3600045376	Cotonelle tp clean care 6pk 8cs 45376 1	otc	brand		1
2840	10523	10356013007	Coverlet adh dressing digit n/a 50	otc	generic		50
2841	64883	40985021324	Cranberry 200 mg capsule bottle 60	otc	generic		60
2842	65330	32122401	Creon dr 24-000 units capsule UU bottle 100	rx	brand		100
2843	-160983	3700022879	Crest xwht +scp olst llmt pste 5.8 oz 164	otc	brand		164
2844	53190	67919001101	Cubicin 500 mg vial vial 1	rx	brand		1
2845	-158620	4910040008	Culturelle f/kds lc/f pckt 30 ea 30	otc	brand		30
2846	-202856	8099003033	Curity gauze spge ster 4x4 12ply 50 3033 1	otc	brand		1
2847	4681	378075110	Cyclobenzaprine 10 mg tablet bottle 1000	rx	generic		1000
2848	4010	64980050448	Cyproheptadine 2 mg/5 ml syrup Mint bottle 473	rx	generic		473
2849	9622	13925050530	Dapsone 100 mg tablet UD blist pack 30	rx	generic		30
2850	58576	591437519	Darifenacin er 7.5 mg tablet bottle 90	rx	generic		90
2851	-14620	7940026540	Degree inv sol cool rush stck 2.7 oz 81	otc	brand		81
2852	-391807	79400034361	Degree motionsense 23.3% aero 107 gm 107	otc	brand		107
2853	4630	63824017165	Delsym 30 mg/5 ml suspension Grape bottle 148	otc	brand		148
2854	4630	63824017565	Delsym 30 mg/5 ml suspension Orange bottle 148	otc	brand		148
2855	59805	409118069	Demerol 100 mg/ml syringe syringe 1	rx	brand	C-2	1
2856	22393	36000038531	Depend underwear for women lrg bag 17	otc	generic		17
2857	6725	9347501	Depo-medrol 80 mg/ml vial vial 1	rx	brand		1
2858	7584	51672126203	Desoximetasone 0.25% ointment jar 60	rx	generic		60
2859	6776	641036725	Dexamethasone 10 mg/ml vial vial 1	rx	generic		1
2860	6778	63323016530	Dexamethasone 120 mg/30 ml vl vial 30	rx	generic		30
2861	6790	54418625	Dexamethasone 6 mg tablet bottle 100	rx	generic		100
2862	6782	54317644	Dexamethasone intensol 1mg/1ml Unflavored drop btl 30	rx	brand		30
2863	5001	13107007301	Dextroamp-amphetamin 20 mg tab bottle 100	rx	generic	C-2	100
2864	34359	13107007401	Dextroamp-amphetamin 30 mg tab bottle 100	rx	generic	C-2	100
2865	6183	96072508	Dhs clear shampoo bottle 240	otc	brand		240
2866	59781	93613832	Diazepam 10 mg rectal gel syst kit 1	rx	generic	C-4	1
2867	3766	51862006405	Diazepam 10 mg tablet bottle 500	rx	generic	C-4	500
2868	3768	51862006310	Diazepam 5 mg tablet bottle 1000	rx	generic	C-4	1000
2869	3765	54318544	Diazepam 5 mg/ml oral conc Unflavored bottle 30	rx	generic	C-4	30
2870	3763	409321312	Diazepam 5 mg/ml vial vial 10	rx	generic	C-4	10
2871	17	54005746	Digoxin 0.05 mg/ml solution Lime drop btl 60	rx	generic		60
2872	16570	49884083009	Diltiazem 24hr cd 180 mg cap bottle 90	rx	generic		90
2873	21282	68682099398	Diltiazem 24hr er 120 mg cap bottle 90	rx	generic		90
2874	574	378002301	Diltiazem 30 mg tablet bottle 100	rx	generic		100
2875	11582	536101001	Diphenhist 25 mg capsule bottle 100	otc	generic		100
2876	11582	185064810	Diphenhydramine 25 mg capsule bottle 1000	otc	generic		1000
2877	11583	66424002101	Diphenhydramine 50 mg capsule bottle 100	otc	generic		100
2878	11590	641037625	Diphenhydramine 50 mg/ml vial vial 1	rx	generic		1
2879	46315	55111053405	Divalproex sod er 500 mg tab bottle 500	rx	generic		500
2880	46087	378641001	Doxepin 100 mg capsule bottle 100	rx	generic		100
2881	9218	591544005	Doxycycline hyclate 100 mg cap bottle 500	rx	generic		500
2882	15943	68382070718	Doxycycline mono 100 mg cap bottle 50	rx	generic		50
2883	57893	51991074810	Duloxetine hcl dr 60 mg cap bottle 1000	rx	generic		1000
2884	-259813	74912310150	Duracell repackaged blister aa 10pk 1	otc	brand		1
2885	-378417	61678412502	Dynalube sterile lube 2.7gr packet 144ct 1	otc	brand		1
2886	70013	94030000204	Embrace 30g lancets box 100	otc	generic		100
2887	-378140	89403000204	Embrace lancets 100 02gm0123 100	otc	brand		100
2888	-378141	89403000124	Embrace lancing device 02km0124 1	otc	brand		1
2889	58576	430017015	Enablex 7.5 mg tablet bottle 30	rx	brand		30
2890	27995	955101010	Enoxaparin 100 mg/ml syringe syringe 1	rx	generic		1
2891	38895	955101601	Enoxaparin 300 mg/3 ml vial vial 3	rx	generic		3
2892	27994	703868023	Enoxaparin 80 mg/0.8 ml syr syringe 0.8	rx	generic		0
2893	27994	781342868	Enoxaparin 80 mg/0.8 ml syr syringe 0.8	rx	generic		0
2894	66702	70074056643	Ensure clear liquid bottle 200	otc	brand		200
2895	-188207	76394805229	Enzym iron complex softgel 90 90	rx	brand		90
2896	57896	49702020613	Epzicom tablet bottle 30	rx	brand		30
2897	7948	17478007031	Erythromycin 0.5% eye ointment UD tube 1	rx	generic		1
2898	7678	68682090023	Erythromycin-benzoyl gel jar 23.3	rx	generic		23
2899	7678	68682090146	Erythromycin-benzoyl gel jar 46.6	rx	generic		46
2900	23472	47781020604	Estradiol 0.05 mg/day patch box 4	rx	generic		4
2901	52830	47781020704	Estradiol 0.06 mg/day patch box 4	rx	generic		4
2902	3205	51862033401	Estradiol 2 mg tablet bottle 100	rx	generic		100
2903	58484	54029013	Eszopiclone 1 mg tablet bottle 30	rx	generic	C-4	30
2904	58482	54029225	Eszopiclone 3 mg tablet bottle 100	rx	generic	C-4	100
2905	5349	10356009001	Eucerin creme jar 454	otc	brand		454
2906	-53317	319810302038	Excedrin migr x/s 24 24	otc	generic		24
2907	40186	19810000866	Excedrin migraine caplet bottle 100	otc	brand		100
2908	70755	310621030	Farxiga 10 mg tablet bottle 30	rx	brand		30
2909	64677	68382007816	Fenofibric acid dr 135 mg cap bottle 90	rx	generic		90
2910	15883	406900076	Fentanyl 100 mcg/hr patch box 5	rx	generic	C-2	5
2911	1645	57896070301	Ferrous sulfate 325 mg tablet bottle 100	otc	generic		100
2912	1645	64376080910	Ferrous sulfate 325 mg tablet bottle 1000	otc	generic		1000
2913	33716	45802057178	Fexofenadine hcl 180 mg tablet bottle 100	rx	generic		100
2914	33716	904621446	Fexofenadine hcl 180 mg tablet bottle 30	rx	generic		30
2915	3053	24385046678	Fiber therapy 500 mg caplet bottle 100	otc	generic		100
2916	-160448	6799050143	Finesse 2in1 shm cnd enhan13z 13	otc	brand		13
2917	-78021	738384950054	Finger cot mdlatex 100 1	otc	generic		1
2918	68365	65628008005	First-lansoprazole 3 mg/ml kit 150	rx	brand		150
2919	68329	65628007005	First-omeprazole 2 mg/ml susp Strawberry kit 150	rx	brand		150
2920	-358591	65628020105	First-vancomycin 50 50mgml posr 150 ml 150	rx	brand		150
2921	23908	132070336	Fleet bisacodyl 10 mg enema squeez btl 37	otc	brand		37
2922	22141	59762501701	Fluconazole 150 mg tablet UD blist pack 1	rx	generic		1
2923	13724	59762501801	Fluconazole 200 mg tablet bottle 30	rx	generic		30
2924	18636	59762503001	Fluconazole 40 mg/ml susp bottle 35	rx	generic		35
2925	7611	52565001315	Fluocinolone 0.025% ointment tube 15	rx	generic		15
2926	7617	51672126402	Fluocinonide 0.05% ointment tube 30	rx	generic		30
2927	7617	43199002960	Fluocinonide 0.05% ointment tube 60	rx	generic		60
2928	-12949	7166100120	Frt of e aloe vera gel 12oz 360	otc	brand		360
2929	8206	54329450	Furosemide 10 mg/ml solution Orange bottle 120	rx	generic		120
2930	8208	603373934	Furosemide 20 mg tablet bottle 5000	rx	generic		5000
2931	8209	603374034	Furosemide 40 mg tablet bottle 5000	rx	generic		5000
2932	8205	409610204	Furosemide 40 mg/4 ml vial vial 4	rx	generic		4
2933	8205	36000028325	Furosemide 40 mg/4 ml vial vial 4	rx	generic		4
2934	70450	52747050230	Fusion plus capsule bottle 30	rx	brand		30
2935	21413	45963055511	Gabapentin 100 mg capsule bottle 100	rx	generic		100
2936	21413	45963055550	Gabapentin 100 mg capsule bottle 500	rx	generic		500
2937	21415	45963055750	Gabapentin 400 mg capsule bottle 500	rx	generic		500
2938	46927	60505254306	Galantamine hbr 8 mg tablet bottle 60	rx	generic		60
2939	65081	52544008430	Gelnique 10% gel sachets UD packet 1	rx	brand		1
2940	9299	409120703	Gentamicin 80 mg/2 ml vial vial 2	rx	generic		2
2941	1777	781145201	Glipizide 5 mg tablet bottle 100	rx	generic		100
2942	43463	378034093	Glipizide er 2.5 mg tablet bottle 30	rx	generic		30
2943	-18927	23601875013	Glove cttn lrg glov 2 ea 2	otc	brand		2
2944	-91853	2513	Glove nitrile lg pf        dyn 100	otc	generic		100
2945	-91855	2511	Glove nitrile sm pf        dyn 100	otc	generic		100
2946	34069	904548118	Glucosamine-chondroitin cap bottle 120	otc	generic		120
2947	4886	517462025	Glycopyrrolate 0.2 mg/ml vial vial 20	rx	generic		20
2948	-209419	8770140391	Gnp lambs wool 1	otc	brand		1
2949	8084	87701055205	Gnp saline 0.65% nose spray squeez btl 44	otc	generic		44
2950	62533	52268010101	Golytely solution Pineapple bottle 4000	rx	brand		4000
2951	6283	395104994	Green soap tincture bottle 120	otc	generic		120
3249	-255500	7046201234	Sour patch kids 2oz ct/24 1	otc	brand		1
2952	45669	121077504	Guaifen-codeine 100-10 mg/5 ml Cherry bottle 118	rx	generic	C-5	118
2953	65570	228285011	Guanfacine hcl er 1 mg tablet bottle 100	rx	generic		100
2954	61425	39328006412	H-chlor 12 0.125% solution bottle 473	otc	brand		473
2955	7625	10631009420	Halog 0.1% cream tube 30	rx	brand		30
2956	3972	781139101	Haloperidol 0.5 mg tablet bottle 100	rx	generic		100
2957	3967	63323046905	Haloperidol dec 50 mg/ml vial vial 5	rx	generic		5
2958	3971	121058104	Haloperidol lac 2 mg/ml conc Unflavored bottle 120	rx	generic		120
2959	3971	54838050115	Haloperidol lac 2 mg/ml conc Unflavored bottle 15	rx	generic		15
2960	3970	67457042612	Haloperidol lac 5 mg/ml vial vial 1	rx	generic		1
2961	3970	25021080601	Haloperidol lac 5 mg/ml vial vial 1	rx	generic		1
2962	6549	63739095325	Heparin sod 5-000 unit/ml vial vial 1	rx	generic		1
2963	27414	2751659	Humalog 100 units/ml cartridge cartridge 3	rx	brand		3
2964	1740	2831517	Humulin n 100 units/ml vial vial 3	otc	brand		3
2965	284	50111039803	Hydralazine 10 mg tablet bottle 1000	rx	generic		1000
2966	285	31722052201	Hydralazine 100 mg tablet bottle 100	rx	generic		100
2967	8182	16729018317	Hydrochlorothiazide 25 mg tab bottle 1000	rx	generic		1000
2968	846	43386035001	Hydrocod-homatrop 5-1.5 mg tab bottle 100	rx	generic	C-2	100
2969	53582	64376064016	Hydrocodon-acetamin 7.5-325/15 Tropical fruit punch bottle 473	rx	generic	C-2	473
2970	57726	64376064301	Hydrocodon-acetaminophn 10-300 bottle 100	rx	generic	C-2	100
2971	48492	62175049064	Hydrocodone-chlorphen er susp bottle 115	rx	generic	C-2	115
2972	6703	59762007401	Hydrocortisone 10 mg tablet bottle 100	rx	generic		100
2973	7554	45802093726	Hydrocortisone 2.5% lotion bottle 118	rx	generic		118
2974	7554	45802093716	Hydrocortisone 2.5% lotion bottle 59	rx	generic		59
2975	7548	45802001405	Hydrocortisone 2.5% ointment jar 454	rx	generic		454
2976	7548	168014630	Hydrocortisone 2.5% ointment tube 28.35	rx	generic		28
2977	6704	54505033310	Hydrocortisone 20 mg tablet bottle 100	rx	generic		100
2978	6705	59762007301	Hydrocortisone 5 mg tablet bottle 50	rx	generic		50
2979	6858	574709012	Hydrocortisone ac 25 mg supp box 12	rx	generic		12
2980	7138	45802093064	Hydrocortisone-iodoquinol crm tube 28.4	rx	generic		28
2981	4106	641012125	Hydromorphone 2 mg/ml vial vial 1	rx	generic	C-2	1
2982	15190	527135501	Hydromorphone 8 mg tablet bottle 100	rx	generic	C-2	100
2983	8775	49884072401	Hydroxyurea 500 mg capsule bottle 100	rx	generic		100
2984	3725	54838050280	Hydroxyzine 10 mg/5 ml syrup Peppermint bottle 473	rx	generic		473
2985	3728	10702001110	Hydroxyzine hcl 25 mg tablet bottle 1000	rx	generic		1000
2986	3731	185067401	Hydroxyzine pam 25 mg cap bottle 100	rx	generic		100
2987	-250017	5070004410	Hytop plate paper white 100  100	rx	brand		100
2988	12080	45802095243	Ibuprofen 100 mg/5 ml susp Berry bottle 473	rx	generic		473
2989	8346	904791580	Ibuprofen 200 mg tablet bottle 1000	otc	generic		1000
2990	8349	55111068305	Ibuprofen 600 mg tablet bottle 500	rx	generic		500
2991	8350	55111068405	Ibuprofen 800 mg tablet bottle 500	rx	generic		500
2992	46068	49884005401	Imipramine hcl 10 mg tablet bottle 100	rx	generic		100
2993	70911	187074633	Insta-glucose gel Cherry tube 31	otc	brand		31
2994	63231	6022761	Isentress 400 mg tablet bottle 60	rx	brand		60
2995	16949	10147170003	Itraconazole 100 mg capsule bottle 30	rx	generic		30
2996	6561	832121201	Jantoven 2 mg tablet UD blist pack 100	rx	generic		100
2997	6560	832121301	Jantoven 2.5 mg tablet UD blist pack 100	rx	generic		100
2998	18080	832121401	Jantoven 3 mg tablet UD blist pack 100	rx	generic		100
2999	-279999	38137103004	Jj baby shampoo 3 oz otg 1	otc	brand		1
3000	5362	8137003314	Johnson's baby oil bottle 420	otc	brand		420
3001	60298	84984000001	Just d 400 units/ml drop drop btl 50	otc	generic		50
3002	53031	50474000148	Keppra 100 mg/ml oral soln Grape bottle 473	rx	brand		473
3003	-238985	50683006795	Kerlix gze roll 3 4in 6p 6725 1	otc	brand		1
3004	-12184	7061014012	Kerr prfpk 12dr no cap rxvl 325 ea 325	otc	brand		325
3005	-12189	7061014060	Kerr prfpk 60dr ambr no cap rxvl 70 ea 70	otc	brand		70
3006	-12183	7061014008	Kerr prfpk 8dr ambr no cap rxvl 500 ea 500	otc	brand		500
3007	-37481	97187034012	Kerr prfpk crc nnn 12-20 rxcp 200 ea 200's 1	otc	brand		1
3008	-12223	7061034030	Kerr prfpk crc-3060 rxcp 100 ea 100	otc	brand		100
3009	-12222	7061034008	Kerr prfpk crc-8 xxx rxcp 300 ea 300	otc	brand		300
3010	-12210	7061021640	Kerr vial 40dr scp ambr rxvl 95 ea 95	otc	brand		95
3011	7334	168009915	Ketoconazole 2% cream tube 15	rx	generic		15
3012	7334	51672129802	Ketoconazole 2% cream tube 30	rx	generic		30
3013	10438	8137104143	Kling rolls bandage 3" box 5	otc	generic		5
3014	22551	173064460	Lamictal 200 mg tablet bottle 60	rx	brand		60
3015	58518	173059402	Lamictal tb start kit (orange) UU dose-pack 49	rx	brand		49
3016	24417	68180060207	Lamivudine 150 mg tablet bottle 60	rx	generic		60
3017	49905	68180060306	Lamivudine 300 mg tablet bottle 30	rx	generic		30
3018	22551	68382001005	Lamotrigine 200 mg tablet bottle 500	rx	generic		500
3019	17872	93003905	Lamotrigine 25 mg tablet bottle 500	rx	generic		500
3020	65252	49884056311	Lamotrigine er 100 mg tablet bottle 30	rx	generic		30
3021	65166	115152708	Lamotrigine odt 50 mg tablet UD blist pack 30	rx	generic		30
3022	64746	23361670	Latisse 0.03% eyelash solution drop btl 3	rx	brand		3
3023	66932	63402030430	Latuda 40 mg tablet bottle 30	rx	brand		30
3024	71415	63402030630	Latuda 60 mg tablet bottle 30	rx	brand		30
3025	41848	591273723	Levalbuterol 0.63 mg/3 ml sol UD vial 3	rx	generic		3
3026	58890	591292754	Levalbuterol tar hfa 45mcg inh aer w/adap 15	rx	generic		15
3027	44632	68180011202	Levetiracetam 250 mg tablet bottle 500	rx	generic		500
3028	62733	50383017104	Levocarnitine 100 mg/ml soln Cherry bottle 118	rx	generic		118
3029	48920	45802059465	Levocetirizine 5 mg tablet bottle 30	rx	generic		30
3030	6653	378181377	Levothyroxine 125 mcg tablet bottle 90	rx	generic		90
3031	6657	527135201	Levothyroxine 300 mcg tablet bottle 100	rx	generic		100
3032	50760	456202001	Lexapro 20 mg tablet bottle 100	rx	brand		100
3033	3411	54350049	Lidocaine 2% viscous soln Light cherry spearmint bottle 100	rx	generic		100
3034	35495	168035755	Lidocaine-prilocaine cream tube 5	rx	generic		5
3035	17266	43547035150	Lisinopril 2.5 mg tablet bottle 500	rx	generic		500
3036	391	68180051503	Lisinopril 20 mg tablet bottle 1000	rx	generic		1000
3037	21277	68180051802	Lisinopril-hctz 10-12.5 mg tab bottle 500	rx	generic		500
3038	389	378202501	Lisinopril-hctz 20-25 mg tab bottle 100	rx	generic		100
3039	-254713	42002040273	Listerine cool mint 250ml 1	otc	brand		1
3040	66349	66869010490	Livalo 1 mg tablet bottle 90	rx	brand		90
3041	11495	34672010188	Lobob hard contct wet soln bottle 60	otc	generic		60
3042	2842	93031105	Loperamide 2 mg capsule bottle 500	rx	generic		500
3043	16363	121077001	Lorazepam 2 mg/ml oral concent bottle 30	rx	generic	C-4	30
3044	3982	591037101	Loxapine 25 mg capsule bottle 100	rx	generic		100
3045	-249587	81483201041	Luden thr/d honey lemon bag30 30	otc	brand		30
3046	-248848	81483201052	Ludens wild honey cls lozg 1x12ea 12	otc	brand		12
3047	1422	35046000323	Magnesium 200 mg tablet bottle 100	otc	generic		100
3048	40224	6380118	Maxalt mlt 10 mg tablet Peppermint packet 3	rx	brand		3
3049	4732	59746012106	Meclizine 25 mg tablet bottle 100	rx	generic		100
3050	26098	59762453802	Medroxyprogesterone 150 mg/ml syringe 1	rx	generic		1
3051	17584	59762453701	Medroxyprogesterone 150 mg/ml vial 1	rx	generic		1
3052	26076	47469000458	Melatonin sr 3 mg tablet bottle 100	otc	generic		100
3053	5884	10742000401	Mentholatum lipbalm stick pf appli 1	otc	brand		1
3054	-271549	8318295300	Mepilex bordr 5drs  1	otc	brand		1
3055	64472	37000002401	Metamucil sugar-free powder Orange jar 283	otc	brand		283
3056	54018	591272060	Metformin er 1-000 mg osm-tab bottle 60	rx	generic		60
3057	40974	68382076010	Metformin hcl 1-000 mg tablet bottle 1000	rx	generic		1000
3058	4235	67457021720	Methadone hcl 10 mg/ml vial vial 20	rx	generic	C-2	20
3059	4242	406575501	Methadone hcl 5 mg tablet bottle 100	rx	generic	C-2	100
3060	4239	54355344	Methadone intensol 10 mg/ml Unflavored bottle 30	rx	brand	C-2	30
3061	6674	49884064101	Methimazole 10 mg tablet bottle 100	rx	generic		100
3062	6674	23155007101	Methimazole 10 mg tablet bottle 100	rx	generic		100
3063	6675	49884064001	Methimazole 5 mg tablet bottle 100	rx	generic		100
3064	8798	61703035038	Methotrexate 50 mg/2 ml vial vial 2	rx	generic		2
3065	362	378042101	Methyldopa 500 mg tablet bottle 100	rx	generic		100
3066	4026	65580053001	Methylphenidate 10 mg tablet bottle 100	rx	generic	C-2	100
3067	4027	65580053201	Methylphenidate 20 mg tablet bottle 100	rx	generic	C-2	100
3068	4028	65580053101	Methylphenidate 5 mg tablet bottle 100	rx	generic	C-2	100
3069	53057	62175015237	Methylphenidate cd 20 mg cap bottle 100	rx	generic	C-2	100
3070	45982	591271701	Methylphenidate er 36 mg tab bottle 100	rx	generic	C-2	100
3071	47318	591271801	Methylphenidate er 54 mg tab bottle 100	rx	generic	C-2	100
3072	5230	121057616	Metoclopramide 5 mg/5 ml soln Berry-citrus bottle 473	rx	generic		473
3073	8218	378617301	Metolazone 5 mg tablet bottle 100	rx	generic		100
3074	41799	713063337	Metronidazole 0.75% cream tube 45	rx	generic		45
3075	7367	46122003927	Miconazorb af 2% powder bottle 71	otc	generic		71
3076	21900	47360017202	Microchamber box 1	rx	generic		1
3077	3780	17478052410	Midazolam hcl 5 mg/ml vial vial 10	rx	generic	C-4	10
3078	3026	121043130	Milk of magnesia suspension Spearmint UD blist pack 30	otc	generic		30
3079	9226	591569550	Minocycline 100 mg capsule bottle 50	rx	generic		50
3080	7638	45802025742	Mometasone furoate 0.1% cream tube 45	rx	generic		45
3081	35527	713070185	Mometasone furoate 0.1% soln bottle 30	rx	generic		30
3082	44803	69452010613	Montelukast sod 4 mg tab chew Cherry bottle 30	rx	generic		30
3083	37003	781555592	Montelukast sod 5 mg tab chew bottle 90	rx	generic		90
3084	21871	456430001	Monurol 3 gm sachet Orange packet 1	rx	brand		1
3085	4089	54023863	Morphine sulf 20 mg/5 ml soln bottle 500	rx	generic	C-2	500
3086	11886	42858080401	Morphine sulf er 100 mg tablet bottle 100	rx	generic	C-2	100
3087	16522	228434711	Morphine sulf er 200 mg tablet bottle 100	rx	generic	C-2	100
3088	23373	641612725	Morphine sulfate 10 mg/ml vial vial 1	rx	generic	C-2	1
3089	60356	228350206	Morphine sulfate er 20 mg cap bottle 60	rx	generic	C-2	60
3090	61722	228350606	Morphine sulfate er 80 mg cap bottle 60	rx	generic	C-2	60
3091	4092	54023625	Morphine sulfate ir 30 mg tab bottle 100	rx	generic	C-2	100
3092	-96943	3400024300	Mr goodbar 36	otc	generic		36
3093	61686	469321110	Mycamine 100 mg vial vial 1	rx	brand		1
3094	5136	378117101	Nadolol 40 mg tablet bottle 100	rx	generic		100
3095	4516	409121501	Naloxone 0.4 mg/ml vial vial 1	rx	generic		1
3096	8360	65162018810	Naproxen 250 mg tablet bottle 100	rx	generic		100
3097	8362	93014901	Naproxen 500 mg tablet bottle 100	rx	generic		100
3098	-363895	4116758002	Nasacort otc 2x120 sprays 1.14 cht 1	rx	brand		1
3099	51827	46122010006	Natural fiber laxative capsule bottle 160	otc	generic		160
3100	63495	64727329801	Nature-throid 16.25 mg tablet bottle 100	rx	brand		100
3101	-362884	7431252802	Nb melatonin 10mg quick diss 1	otc	brand		1
3102	72606	5928000100	Neilmed sinus rinse kit kit 50	otc	brand		50
3103	-377340	4850303032	Neo wrist/thumb support sm 0303-s 1	otc	brand		1
3104	9284	93117701	Neomycin 500 mg tablet bottle 100	rx	generic		100
3105	52110	501371205	Neosporin plus cream tube 15	otc	brand		15
3106	-33412	7050105065	Neut eye mkup rmvr oil fr liqd 5.5 oz 162	otc	brand		162
3107	-12085	7050105100	Neut mkup rmvr twlt vntypk pada 25 ea 25	otc	brand		25
3108	63668	186401001	Nexium dr 10 mg packet packet 30	rx	brand		30
3109	4930	536340401	Nicotine 2 mg chewing gum Cinnamon blist pack 100	otc	generic		100
3110	16427	536589688	Nicotine 21 mg/24hr patch box 14	otc	generic		14
3111	13486	536340501	Nicotine 4 mg chewing gum blist pack 100	otc	generic		100
3112	20618	24979000901	Nifedipine er 90 mg tablet bottle 100	rx	generic		100
3113	9434	65162068988	Nitrofurantoin 25 mg/5 ml susp Tutti-frutti bottle 230	rx	generic		230
3114	466	47781029903	Nitroglycerin 0.6 mg/hr patch box 30	rx	generic		30
3115	44359	45802021001	Nitroglycerin lingual 0.4 mg bottle 4.9	rx	generic		4
3116	1218	8290306546	Normal saline flush syringe syringe 10	rx	generic		10
3117	46060	51862001601	Nortriptyline hcl 25 mg cap bottle 100	rx	generic		100
3118	62868	169185189	Novofine 32g needles box 100	otc	generic		100
3119	66936	169185389	Novotwist needle 32g 5mm box 100	otc	generic		100
3120	65321	50458084004	Nucynta 100 mg tablet bottle 100	rx	brand	C-2	100
3121	48035	43900097550	Nutrisource fiber powder canister 205	otc	brand		205
3122	7284	42543005263	Nystatin 100-000 unit/gm powd bottle 60	rx	generic		60
3123	7284	68308015260	Nystatin 100-000 unit/gm powd squeez btl 60	rx	generic		60
3124	9538	53489040001	Nystatin 500-000 unit oral tab bottle 100	rx	generic		100
3125	53978	703331104	Octreotide acet 100 mcg/ml vl vial 1	rx	generic		1
3126	75634	61958210101	Odefsey tablet UU bottle 30	rx	brand		30
3127	27960	65862056430	Olanzapine 10 mg tablet bottle 30	rx	generic		30
3128	54334	45802088855	Omeprazole dr 20 mg tablet blist pack 42	otc	generic		42
3129	43137	55111064505	Omeprazole dr 40 mg capsule bottle 500	rx	generic		500
3130	41563	781523964	Ondansetron odt 8 mg tablet UD blist pack 30	rx	generic		30
3131	-80192	1650052586	One a day all day enrgy tab 50	otc	brand		50
3132	6373	53885000775	One touch verio test strip box 100	otc	generic		100
3133	6373	53885000774	One touch verio test strip box 50	otc	generic		50
3134	17026	67386031401	Onfi 10 mg tablet bottle 100	rx	brand	C-4	100
3135	4261	42799021701	Opium tincture 10 mg/ml bottle 118	rx	generic	C-2	118
3136	-19219	30041080140	Oral-b 40 indicator med 14 each 1	otc	brand		1
3137	53123	44087115001	Ovidrel 250 mcg/0.5 ml syrg syringe 0.5	rx	brand		0
3138	33724	65162064978	Oxcarbazepine 300 mg/5 ml susp bottle 250	rx	generic		250
3139	4929	603497521	Oxybutynin 5 mg tablet bottle 100	rx	generic		100
3140	4928	603149158	Oxybutynin 5 mg/5 ml syrup Apple-watermelon bottle 473	rx	generic		473
3141	15065	68308002003	Oxycodone hcl 100 mg/5 ml soln Berry bottle 30	rx	generic	C-2	30
3142	46475	10702000950	Oxycodone hcl 30 mg tablet bottle 500	rx	generic	C-2	500
3143	24507	68462020401	Oxycodone hcl 5 mg capsule bottle 100	rx	generic	C-2	100
3144	4224	60432070605	Oxycodone hcl 5 mg/5 ml soln Raspberry bottle 500	rx	generic	C-2	500
3145	72868	115156201	Oxycodone hcl er 80 mg tablet bottle 100	rx	generic	C-2	100
3146	48977	53746020401	Oxycodone-acetaminophen 10-325 bottle 100	rx	generic	C-2	100
3147	72866	59011044010	Oxycontin 40 mg tablet bottle 100	rx	brand	C-2	100
3148	61087	10702007101	Oxymorphone hcl 10 mg tablet bottle 100	rx	generic	C-2	100
3149	61094	115123413	Oxymorphone hcl er 40 mg tab bottle 60	rx	generic	C-2	60
3150	59178	536781708	Oysco 500-vit d3 200 tablet bottle 60	otc	generic		60
3151	44598	904188260	Oyster shell 250 mg + vit d tb bottle 100	otc	generic		100
3152	-409668	686864026440	Pad non-adherent 3x8" sterile 50	otc	brand		50
3153	-391563	7695569124	Paper plates 40 ct cs/24 1	otc	brand		1
3154	50136	62175047132	Paroxetine er 25 mg tablet bottle 30	rx	generic		30
3155	-248685	14832010881	Pediacare allergy 4oz 118	otc	brand		118
3156	-247811	81483201083	Pediacare child l-a cgh grp 4z 4	otc	brand		4
3157	8877	93412773	Penicillin vk 250 mg/5 ml soln Cherry bottle 100	rx	generic		100
3158	2860	1490003977	Pepto-bismol tablet chew bottle 30	otc	brand		30
3159	13631	472024260	Permethrin 5% cream tube 60	rx	generic		60
3160	5159	527144501	Phentermine 37.5 mg tablet bottle 100	rx	generic	C-4	100
3161	-14866	7957309162	Pill crusher each 1	otc	generic		1
3162	42943	781542031	Pioglitazone hcl 15 mg tablet bottle 30	rx	generic		30
3163	42943	591320519	Pioglitazone hcl 15 mg tablet bottle 90	rx	generic		90
3164	59686	13668028160	Pioglitazone-metformin 15-850 bottle 60	rx	generic		60
3165	21185	67457052122	Piperacil-tazobact 2.25 gm vl vial 1	rx	generic		1
3166	-93078	2900007537	Planters tube hot & spicy pp 2 18	otc	generic		18
3167	-28122	78300008480	Playtex gntl supr deod tmpn 8	otc	brand		8
3168	-49106	7830009838	Playtex gntl supr unscnt tmpn 20 ea 18	otc	brand		18
3169	-394056	18368918271	Plenty paper towel 112 ct flex bundle (12) 1	otc	brand		1
3170	66420	44946104501	Pnv prenatal plus multivit tab bottle 100	rx	generic		100
3171	72616	87040203	Poly-vi-sol drops drop btl 50	otc	brand		50
3172	72601	87040501	Poly-vi-sol with iron drops drop btl 50	otc	brand		50
3173	53623	81079888	Polysporin ointment tube 14.2	otc	brand		14
3174	1262	76439034310	Potassium cl 20 meq packet packet 100	rx	generic		100
3175	1262	76439034330	Potassium cl 20 meq packet packet 30	rx	generic		30
3176	1275	574027505	Potassium cl er 10 meq tablet bottle 500	rx	generic		500
3177	293	378320501	Prazosin 5 mg capsule bottle 100	rx	generic		100
3178	47282	121075908	Prednisolone 15 mg/5 ml soln Grape bottle 237	rx	generic		237
3179	7894	61314063715	Prednisolone ac 1% eye drop drop btl 15	rx	generic		15
3180	6751	59746017506	Prednisone 20 mg tablet bottle 100	rx	generic		100
3181	3211	46110081	Premarin 0.3 mg tablet bottle 100	rx	brand		100
3182	3212	46110281	Premarin 0.625 mg tablet bottle 100	rx	brand		100
3183	52179	46110611	Prempro 0.45-1.5 mg tablet UD UU blist pack 28	rx	brand		28
3184	30106	67628614	Prevacid 24hr dr 15 mg capsule bottle 14	otc	brand		14
3185	51654	64764054411	Prevacid 30 mg solutab Strawberry UD blist pack 100	rx	brand		100
3186	73052	59676057530	Prezcobix 800 mg-150 mg tablet bottle 30	rx	brand		30
3187	64774	186062501	Prilosec dr 2.5 mg suspension packet 30	rx	brand		30
3188	63736	8121130	Pristiq er 50 mg tablet bottle 30	rx	brand		30
3189	3848	378510501	Prochlorperazine 5 mg tablet bottle 100	rx	generic		100
3190	-332124	38484073200	Prodigy medicare test strips 50ea 1	otc	brand		1
3191	43801	69387010101	Progesterone 100 mg capsule bottle 100	rx	generic		100
3192	43802	69387010201	Progesterone 200 mg capsule bottle 100	rx	generic		100
3193	3262	591312879	Progesterone oil 50 mg/ml vl vial 10	rx	generic		10
3194	21797	469065773	Prograf 5 mg capsule bottle 100	rx	brand		100
3195	3879	10702000401	Promethazine 50 mg tablet bottle 100	rx	generic		100
3196	3876	603158458	Promethazine 6.25 mg/5 ml syrp Apple-watermelon bottle 473	rx	generic		473
3197	48489	50383080416	Promethazine-codeine syrup bottle 473	rx	generic	C-5	473
3198	48493	603158658	Promethazine-dm syrup Orange-pineapple bottle 473	rx	generic		473
3199	13646	591058201	Propafenone hcl 150 mg tablet bottle 100	rx	generic		100
3200	4877	54472125	Propantheline 15 mg tablet bottle 100	rx	generic		100
3201	5125	50111046901	Propranolol 40 mg tablet bottle 100	rx	generic		100
3202	5126	603548521	Propranolol 60 mg tablet bottle 100	rx	generic		100
3203	-38221	30521516328	Q-tips cotton swabs bnus swab 375 ea 375	otc	generic		375
3204	-394528	63551599132	Qc acetaminophen cherry dye free 4 oz 1	otc	brand		1
3205	-400306	63551599257	Qc athl foot spray tinactin 5.3  5	rx	brand		5
3206	-410019	63551599247	Qc cetirizine child allergy dyef grp 4oz 4	otc	brand		4
3207	-403317	63551599105	Qc hand santizer orig liq 2oz 99105 2	otc	brand		2
3208	11673	35515095890	Qc heartburn 150 mg tablet blist pack 24	otc	generic		24
3209	-405116	63551599260	Qc hydrocortisone 1% max str 1oz 99260 1	otc	brand		1
3210	59707	35515095615	Qc sinus pain relief caplet blist pack 24	otc	generic		24
3211	625	35515095707	Qc tussin cf liquid bottle 237	otc	generic		237
3212	47198	47335090686	Quetiapine fumarate 300 mg tab bottle 60	rx	generic		60
3213	60292	67877024910	Quetiapine fumarate 50 mg tab bottle 1000	rx	generic		1000
3214	70374	24478020525	Quillivant xr 25 mg/5 ml susp bottle 150	rx	brand	C-2	150
3215	11673	904635024	Ranitidine 150 mg tablet UD blist pack 24	rx	generic		24
3216	-46894	3400000440	Reese cup 1	otc	generic		1
3217	68482	65649055107	Relistor 12 mg/0.6 ml syringe syringe 0.6	rx	brand		0
3218	49606	49234005	Relpax 40 mg tablet UD blist pack 12	rx	brand		12
3219	38907	574024101	Repaglinide 1 mg tablet bottle 100	rx	generic		100
3220	23887	8380009974	Restore hydrogel jar 90	otc	generic		90
3221	69115	54799091730	Retaine mgd eye drops blist pack 30	otc	brand		30
3222	74446	59148003813	Rexulti 2 mg tablet bottle 30	rx	brand		30
3223	63925	591204454	Risedronate sodium 150 mg tab UD UU blist pack 1	rx	generic		1
3224	34166	68462025401	Ropinirole hcl 0.5 mg tablet bottle 100	rx	generic		100
3225	29162	68462025901	Ropinirole hcl 5 mg tablet bottle 100	rx	generic		100
3226	-40661	44444000853	Rx btl oval pl  3oz   5st20003 200's 1	otc	generic		1
3227	-40663	44444000855	Rx btl oval pl  8oz   5st20008 100's 1	otc	generic		1
3228	-11138	5113159116	Scotch magic tp 3/4n x500n tape 1 rl 1	otc	generic		1
3229	-367624	3600036924	Scott towel mega roll white 24cs 36924 1	otc	brand		1
3230	48044	51645085006	Senna plus tablet bottle 60	otc	generic		60
3231	-151922	31015885000	Sensodyne pronamel whtg each 113 gm 113	otc	brand		113
3232	60293	310027910	Seroquel 400 mg tablet bottle 100	rx	brand		100
3233	46230	59762494001	Sertraline 20 mg/ml oral conc Menthol bottle 60	rx	generic		60
3234	46229	68180035302	Sertraline hcl 100 mg tablet bottle 500	rx	generic		500
3235	46227	65862001105	Sertraline hcl 25 mg tablet bottle 500	rx	generic		500
3236	59211	591378019	Sildenafil 20 mg tablet bottle 90	rx	generic		90
3237	16414	54838020980	Siltussin dm cough syrup Strawberry bottle 473	otc	generic		473
3238	7669	67877012450	Silver sulfadiazine 1% cream jar 50	rx	generic		50
3239	38451	6011754	Singulair 10 mg tablet bottle 90	rx	brand		90
3240	-401859	2200013547	Skittles orchards 24ct 1	otc	brand		1
3241	586	487930103	Sodium chloride 0.9% inhal vl UD vial 3	rx	generic		3
3242	9827	409713809	Sodium chloride 0.9% irrig. bottle 1000	rx	generic		1000
3243	1210	409798437	Sodium chloride 0.9% solution bag 100	rx	generic		100
3244	1226	223176001	Sodium chloride 1 gm tablet bottle 100	otc	generic		100
3245	588	487900360	Sodium chloride 3% vial vial 4	rx	generic		4
3246	-311361	74182028172	Softsoap liqd 15 oz 443	otc	brand		443
3247	66110	9001104	Solu-cortef 100 mg vial vial 1	rx	brand		1
3248	17196	603576921	Sotalol 80 mg tablet bottle 100	rx	generic		100
3250	13662	555901658	Sprintec 28 day tablet UD blist pack 28	rx	generic		28
3251	9825	338000404	Sterile water for irrigation flex cont 1000	rx	generic		1000
3252	51489	2322730	Strattera 10 mg capsule bottle 30	rx	brand		30
3253	60391	2325130	Strattera 100 mg capsule bottle 30	rx	brand		30
3254	51491	2322830	Strattera 25 mg capsule bottle 30	rx	brand		30
3255	60390	2325030	Strattera 80 mg capsule bottle 30	rx	brand		30
3256	67478	904026352	Stress formula with iron tab bottle 60	otc	generic		60
3257	-9262	4138809701	Stri-dex  ms 2% pada 55 55	otc	brand		55
3258	70262	12496121203	Suboxone 12 mg-3 mg sl film packet 30	rx	brand	C-3	30
3259	70259	12496120403	Suboxone 4 mg-1 mg sl film packet 30	rx	brand	C-3	30
3260	66636	12496120803	Suboxone 8 mg-2 mg sl film packet 30	rx	brand	C-3	30
3261	49941	44523060301	Sulfacetamide-sulfur 10-5% crm bottle 28	rx	generic		28
3262	23799	55111029109	Sumatriptan succ 25 mg tablet UD blist pack 9	rx	generic		9
3263	22479	55111029209	Sumatriptan succ 50 mg tablet UD blist pack 9	rx	generic		9
3264	64243	31604002727	Super b-complex folic-vit c tb bottle 140	otc	generic		140
3265	49384	86227090105	Sure comfort 1 ml syringe box 100	otc	generic		100
3266	6653	74706890	Synthroid 125 mcg tablet bottle 90	rx	brand		90
3267	43706	4080085	Tamiflu 75 mg capsule UD blist pack 10	rx	brand		10
3268	-13251	7301037853	Tampax pearl sup frsh tmpn 18 ea 18	otc	brand		18
3269	72404	173086735	Tanzeum 50 mg pen inject syringe 1	rx	brand		1
3270	4558	78050905	Tegretol 200 mg tablet bottle 100	rx	brand		100
3271	63589	78052115	Tekturna hct 150-12.5 mg tab UU bottle 30	rx	brand		30
3272	3689	67877014605	Temazepam 15 mg capsule bottle 500	rx	generic	C-4	500
3273	10375	68702063201	Tena knit pants plus packet 10	otc	generic		10
3274	22650	378226401	Terazosin 2 mg capsule bottle 100	rx	generic		100
3275	18638	67405050003	Terbinafine hcl 250 mg tablet bottle 30	rx	generic		30
3276	5025	527131801	Terbutaline sulfate 2.5 mg tab bottle 100	rx	generic		100
3277	3148	591412879	Testosteron cyp 2-000 mg/10 ml vial 10	rx	generic	C-3	10
3278	7861	24208092064	Tetracaine 0.5% eye drops drop btl 15	rx	generic		15
3279	20990	52244040010	Theo-24 er 400 mg capsule bottle 100	rx	brand		100
3280	2523	536466038	Therems tablet bottle 130	otc	generic		130
3281	-152899	9376460224	Therm ear inst-read ut-302 1	otc	brand		1
3282	50565	573301002	Thermacare heatwrap box 2	otc	generic		2
3283	-326938	30573301301	Thermacare muscl heat wrap 8 1	otc	brand		1
3284	-90930	3700001870	Tide to go 1	otc	generic		1
3285	71322	49702022813	Tivicay 50 mg tablet bottle 30	rx	brand		30
3286	26171	50458064265	Topamax 200 mg tablet bottle 60	rx	brand		60
3287	29837	69097012212	Topiramate 25 mg tablet bottle 500	rx	generic		500
3288	21409	50111091801	Torsemide 100 mg tablet bottle 100	rx	generic		100
3289	4704	67434504	Transderm-scop 1.5 mg/3 day box 4	rx	brand		4
3290	46241	603616032	Trazodone 50 mg tablet bottle 1000	rx	generic		1000
3291	5798	43478024415	Tretinoin 0.025% gel tube 15	rx	generic		15
3292	7594	51672128202	Triamcinolone 0.1% cream tube 30	rx	generic		30
3293	7594	67877025130	Triamcinolone 0.1% cream tube 30	rx	generic		30
3294	7597	713022815	Triamcinolone 0.1% ointment tube 15	rx	generic		15
3295	72054	45802044301	Triamcinolone 55 mcg nasal spr bottle 16.9	rx	generic		16
3296	9497	93215801	Trimethoprim 100 mg tablet bottle 100	rx	generic		100
3297	72718	49702023113	Triumeq tablet UU bottle 30	rx	brand		30
3298	71346	17772010315	Trokendi xr 100 mg capsule UD blist pack 30	rx	brand		30
3299	72872	2143380	Trulicity 0.75 mg/0.5 ml pen syringe 0.5	rx	brand		0
3300	16308	54295030815	Urea 40% cream bottle 28.35	rx	generic		28
3301	3095	527132601	Ursodiol 300 mg capsule bottle 100	rx	generic		100
3302	23989	59746032430	Valacyclovir hcl 500 mg tablet bottle 30	rx	generic		30
3303	4535	50383079216	Valproic acid 250 mg/5 ml soln Raspberry bottle 473	rx	generic		473
3304	-20571	32878530019	Vaporizer vicks scnt pads pads 6 ea 6	otc	brand		6
3305	46402	93738301	Venlafaxine hcl 100 mg tablet bottle 100	rx	generic		100
3306	46401	68382002101	Venlafaxine hcl 75 mg tablet bottle 100	rx	generic		100
3307	64447	131326832	Venlafaxine hcl er 225 mg tab bottle 30	rx	generic		30
3308	-80364	4740007106	Venus breeze cartridge 48's 1	otc	generic		1
3309	16605	378638001	Verapamil er 180 mg capsule bottle 100	rx	generic		100
3310	48843	61958040101	Viread 300 mg tablet UU bottle 30	rx	brand		30
3311	2336	536354201	Vitamin b-12 100 mcg tablet bottle 100	otc	generic		100
3312	2148	904052260	Vitamin c 250 mg tablet bottle 100	otc	generic		100
3313	19166	904582360	Vitamin d3 400 unit tablet bottle 100	otc	generic		100
3314	-426374	84009310532	Vitamin d3 5000iu 2oz 2	otc	brand		2
3315	2302	409915701	Vitamin k-1 1 mg/0.5 ml ampul ampul 0.5	rx	generic		0
3316	62284	59417010510	Vyvanse 50 mg capsule bottle 100	rx	brand	C-2	100
3317	6561	93171301	Warfarin sodium 2 mg tablet bottle 100	rx	generic		100
3318	1167	409488710	Water for injection vial vial 10	rx	generic		10
3319	1167	409488750	Water for injection vial vial 50	rx	generic		50
3320	-377273	80921940001	White rain cream bw conut/hibiscus 12oz 12	otc	brand		12
3321	-38821	35046007164	Wm saw palmetto 160mg cap 716 60's 1	otc	generic		1
3322	23130	8884433500	Xeroform petrolatum dress n/a 150	rx	generic		150
3323	66295	65649030302	Xifaxan 550 mg tablet bottle 60	rx	brand		60
3324	49828	378334053	Xulane patch box 3	rx	brand		3
3325	47787	50419040203	Yasmin 28 tablet UD blist pack 28	rx	brand		28
3326	-190219	3400006642	York changemaker patties 200ct 6642 1	rx	brand		1
3327	-158725	7346215013	Zeasorb-af cs j/itch 2% powd 2.5 oz 70	otc	brand		70
3328	65701	42865030602	Zenpep dr 10-000 units capsule bottle 100	rx	brand		100
3329	37036	64980020413	Zolmitriptan 5 mg tablet UD blist pack 3	rx	generic		3
3330	59696	228348111	Zolpidem tart er 6.25 mg tab bottle 100	rx	generic	C-4	100
3331	45100	378672701	Zonisamide 100 mg capsule bottle 100	rx	generic		100
3332	3308	51862026006	Zovia 1-35e tablet UD blist pack 28	rx	generic		28
3333	-155496	74098527112	21st vit b 12 2500mcg sublngl 110	otc	brand		110
3334	14182	50383081016	Acyclovir 200 mg/5 ml susp Banana bottle 473	rx	generic		473
3335	16408	93894301	Acyclovir 400 mg tablet bottle 100	rx	generic		100
3336	62811	66993088445	Adapalene 0.3% gel tube 45	rx	generic		45
3337	-1703	85314201	Afrin no drip nasal spin 15ml 15	otc	brand		15
3338	8227	64980015101	Amiloride hcl 5 mg tablet bottle 100	rx	generic		100
3339	266	185014405	Amiodarone hcl 200 mg tablet bottle 500	rx	generic		500
3340	16925	67877019705	Amlodipine besylate 2.5 mg tab bottle 500	rx	generic		500
3341	24668	66685100100	Amox-clav 875-125 mg tablet bottle 20	rx	generic		20
3342	8996	57237003105	Amoxicillin 500 mg capsule bottle 500	rx	generic		500
3343	40292	143995101	Amoxicillin 875 mg tablet bottle 100	rx	generic		100
3344	-49097	7685570069	Apex 7 day mediplanr each 1 ea 1	otc	brand		1
3345	64856	96072416	Aquanil cleanser bottle 480	otc	brand		480
3346	10959	8470600001	Autodrop kit 1	otc	generic		1
3347	65577	60505084805	Azelastine 0.15% nasal spray squeez btl 30	rx	generic		30
3348	26721	60505258103	Azithromycin 250 mg tablet bottle 30	rx	generic		30
3349	22624	60505258203	Azithromycin 500 mg tablet bottle 30	rx	generic		30
3350	-3363	998001530	Azo standard 95mg tabs 30 30	otc	brand		30
3351	16042	43547033850	Benazepril hcl 40 mg tablet bottle 500	rx	generic		500
3352	50289	65597010330	Benicar 20 mg tablet bottle 30	rx	brand		30
3353	4589	69315013601	Benztropine mes 0.5 mg tab bottle 100	rx	generic		100
3354	4590	603243421	Benztropine mes 1 mg tablet bottle 100	rx	generic		100
3355	-46074	7033020122	Bic pen round med blue gsmp101 1	otc	generic		1
3356	2947	904792717	Bisacodyl ec 5 mg tablet bottle 25	otc	generic		25
3357	46237	60505015701	Bupropion hcl 100 mg tablet bottle 100	rx	generic		100
3358	27378	64380074318	Buspirone hcl 15 mg tablet bottle 180	rx	generic		180
3359	59073	310652401	Byetta 10 mcg dose pen inj cartridge 2.4	rx	brand		2
3360	58018	904323352	Calcium 600-vit d3 400 tablet bottle 60	otc	generic		60
3361	17876	51672412501	Carbamazepine er 400 mg tablet bottle 100	rx	generic		100
3362	2538	228253996	Carbidopa-levodopa 25-100 tab bottle 1000	rx	generic		1000
3363	4663	64980017405	Carisoprodol 350 mg tablet bottle 500	rx	generic	C-4	500
3364	22233	65862014405	Carvedilol 12.5 mg tablet bottle 500	rx	generic		500
3365	28108	43547025450	Carvedilol 3.125 mg tablet bottle 500	rx	generic		500
3366	9137	65862070020	Cefuroxime axetil 500 mg tab bottle 20	rx	generic		20
3367	9046	93417774	Cephalexin 250 mg/5 ml susp Mixed berry bottle 200	rx	generic		200
3368	37020	713031788	Ciclopirox 8% solution bottle 6.6	rx	generic		6
3369	37979	93206506	Cilostazol 50 mg tablet bottle 60	rx	generic		60
3370	57815	74312014020	Cinnamon 1000 mg capsule bottle 100	otc	generic		100
3371	52911	65853302	Ciprodex otic suspension drop btl 7.5	rx	brand		7
3372	3025	869069338	Citroma solution Cherry bottle 296	otc	generic		296
3373	7726	59762374301	Clindamycin ph 1% gel jar 30	rx	generic		30
3374	37628	378868854	Clindamycin-benzoyl perox 1-5% jar 50	rx	generic		50
3375	7634	51672125806	Clobetasol 0.05% cream tube 45	rx	generic		45
3376	38164	47335089481	Clopidogrel 75 mg tablet bottle 90	rx	generic		90
3377	7361	51672127501	Clotrimazole 1% cream tube 15	rx	generic		15
3378	-6501	3076803553	Coenzyme q10 100mg caps 30 ea 30	otc	brand		30
3379	-7227	3500074126	Colgate total plus whtg pste 6 oz 180	otc	brand		180
3380	60186	55513080060	Corlanor 5 mg tablet bottle 60	rx	brand		60
3381	-191715	3786788005	Cuties baby diaper size 6 35+lb 23 1	rx	brand		1
3382	-332950	90891125296	Cuties wipe sft pk snstv 12 c 72	otc	brand		72
3383	2508	536354601	Daily-vites with iron tablet bottle 100	otc	generic		100
3384	48983	781568901	Dexmethylphenidate 5 mg tab bottle 100	rx	generic	C-2	100
3385	59190	93555001	Dexmethylphenidate er 5 mg cap bottle 100	rx	generic	C-2	100
3386	652	61787006204	Diabetic tussin dm liquid bottle 118	otc	generic		118
3387	11582	69618002401	Diphenhydramine 25 mg capsule bottle 100	otc	generic		100
3388	4540	29300014005	Divalproex sod dr 500 mg tab bottle 500	rx	generic		500
3389	29335	31722073790	Donepezil hcl 5 mg tablet bottle 90	rx	generic		90
3390	9223	69097022770	Doxycycline hyclate 100 mg tab bottle 50	rx	generic		50
3391	-381473	74312530982	Dual spectrum mltnin tb 5mg nb 60	otc	brand		60
3392	51246	60505387709	Dutasteride 0.5 mg capsule bottle 90	rx	generic		90
3393	69144	37024523	Dymista nasal spray squeez btl 23	rx	brand		23
3394	27993	703856023	Enoxaparin 60 mg/0.6 ml syr syringe 0.6	rx	generic		0
3395	-256162	76394805623	Enzym b12 infusion chew tab 30 30	otc	brand		30
3396	65912	54505010102	Epinephrine 0.15 mg auto-injct syringe 2	rx	generic		2
3397	7678	781709459	Erythromycin-benzoyl gel jar 46.6	rx	generic		46
3398	58483	54029125	Eszopiclone 2 mg tablet bottle 100	rx	generic	C-4	100
3399	8201	42799040501	Ethacrynic acid 25 mg tablet bottle 100	rx	generic		100
3400	21743	603358121	Felodipine er 2.5 mg tablet bottle 100	rx	generic		100
3401	44915	68180036309	Fenofibrate 160 mg tablet bottle 90	rx	generic		90
3402	1645	49483006410	Ferrous sulfate 325 mg tablet bottle 1000	otc	generic		1000
3403	61762	904560413	Fish oil 500 mg softgel bottle 130	otc	generic		130
3404	-140510	39423440482	Florajen 3 caps 30 ea 30	otc	brand		30
3405	-178625	73942322048	Florajen acidophilus cap 60 aml 60	rx	brand		60
3406	46216	93718856	Fluoxetine hcl 10 mg tablet bottle 30	rx	generic		30
3407	8206	54329446	Furosemide 10 mg/ml solution Orange bottle 60	rx	generic		60
3408	8208	378020810	Furosemide 20 mg tablet bottle 1000	rx	generic		1000
3409	58239	47335083683	Galantamine er 16 mg capsule bottle 30	rx	generic		30
3410	-26754	74098522663	Gelatin caps 100	otc	generic		100
3411	285	23155000401	Hydralazine 100 mg tablet bottle 100	rx	generic		100
3412	4868	43199001101	Hyoscyamine 0.125 mg tab sl Mint bottle 100	rx	generic		100
3413	70358	65175007	Ilevro 0.3% ophth drops drop btl 1.7	rx	brand		1
3414	46070	69315013501	Imipramine hcl 50 mg tablet bottle 100	rx	generic		100
3415	507	781155601	Isosorbide dn 10 mg tablet bottle 100	rx	generic		100
3416	24488	603411032	Isosorbide mn er 30 mg tablet bottle 1000	rx	generic		1000
3417	61612	6022154	Januvia 25 mg tablet bottle 90	rx	brand		90
3418	72489	597015330	Jardiance 25 mg tablet bottle 30	rx	brand		30
3419	3300	555902858	Junel fe 1.5 mg-30 mcg tablet UD blist pack 28	rx	generic		28
3420	7334	51672129803	Ketoconazole 2% cream tube 60	rx	generic		60
3421	31613	409379601	Ketorolac 60 mg/2 ml vial vial 2	rx	generic		2
3422	-202708	7074040161	Label look different green 40161 1	otc	brand		1
3423	22551	51672413304	Lamotrigine 200 mg tablet bottle 60	rx	generic		60
3424	17872	29300011105	Lamotrigine 25 mg tablet bottle 500	rx	generic		500
3425	65171	49884088099	Lamotrigine odt kit (blue) UU dose-pack 28	rx	generic		28
3426	47077	43547022406	Levetiracetam 1-000 mg tablet bottle 60	rx	generic		60
3427	44632	43547022115	Levetiracetam 250 mg tablet bottle 120	rx	generic		120
3428	64416	43547034506	Levetiracetam er 500 mg tablet bottle 60	rx	generic		60
3429	390	43547035311	Lisinopril 10 mg tablet bottle 1000	rx	generic		1000
3430	391	43547035411	Lisinopril 20 mg tablet bottle 1000	rx	generic		1000
3431	40888	69238125106	Lopreeza 1 mg-0.5 mg tablet UD UU blist pack 28	rx	generic		28
3432	3757	69315090410	Lorazepam 0.5 mg tablet bottle 1000	rx	generic	C-4	1000
3433	3758	69315090510	Lorazepam 1 mg tablet bottle 1000	rx	generic	C-4	1000
3434	59401	71101968	Lyrica 225 mg capsule bottle 90	rx	brand	C-5	90
3435	1408	536352141	Magnesium oxide 400 mg tablet bottle 120	otc	generic		120
3436	3359	409161050	Marcaine 0.5% vial vial 50	rx	brand		50
3437	61738	30768019484	Melatonin 10 mg capsule bottle 90	otc	generic		90
3438	24665	536641208	Melatonin 3 mg tablet bottle 60	otc	generic		60
3439	32492	13668022360	Memantine hcl 10 mg tablet bottle 60	rx	generic		60
3440	53324	13668022260	Memantine hcl 5 mg tablet bottle 60	rx	generic		60
3441	13318	23155010205	Metformin hcl 500 mg tablet bottle 500	rx	generic		500
3442	46754	51224000760	Metformin hcl er 500 mg tablet bottle 500	rx	generic		500
3443	8168	48102010001	Methazolamide 25 mg tablet bottle 100	rx	generic		100
3444	9460	42799010601	Methenamine md 1 gm tablet bottle 100	rx	generic		100
3445	45311	59762444002	Methylprednisolone 4 mg dosepk UU dose-pack 21	rx	generic		21
3446	3026	536247085	Milk of magnesia suspension Mint bottle 473	otc	generic		473
3447	37003	69452010713	Montelukast sod 5 mg tab chew Cherry bottle 30	rx	generic		30
3448	43879	65862060330	Moxifloxacin hcl 400 mg tablet bottle 30	rx	generic		30
3449	12074	63824005632	Mucinex dm er 600-30 mg tablet blist pack 20	otc	brand		20
3450	-391638	74312552731	Nb turmeric 500mg cap 45 45	otc	brand		45
3451	-34244	30081073088	Neosporin pk-on disply unit 24 pc 1	otc	generic		1
3452	33364	47335053981	Niacin er 500 mg tablet bottle 90	rx	generic		90
3453	71709	85432402	Noxafil dr 100 mg tablet bottle 60	rx	brand		60
3454	33530	60505006501	Omeprazole dr 20 mg capsule bottle 1000	rx	generic		1000
3455	43137	62175013643	Omeprazole dr 40 mg capsule bottle 1000	rx	generic		1000
3456	46475	10702000901	Oxycodone hcl 30 mg tablet bottle 100	rx	generic	C-2	100
3457	4222	53746020305	Oxycodone-acetaminophen 5-325 bottle 500	rx	generic	C-2	500
3458	27462	93001298	Pantoprazole sod dr 40 mg tab bottle 90	rx	generic		90
3459	11677	16837085525	Pepcid ac 20 mg tablet blist pack 25	otc	brand		25
3460	-401008	7685570093	Pill organizer 7day am/am push open70093 1	otc	brand		1
3461	-401020	7685570110	Pill organizer 7day am/pm w/sleeve 70110 1	otc	brand		1
3462	-39563	2571567375	Pill remind 7x2day am/pm7375 1	otc	generic		1
3463	-406337	7685570094	Pill reminder week 7 comp am/pm tray 1	otc	brand		1
3464	6749	59746017310	Prednisone 10 mg tablet bottle 1000	rx	generic		1000
3465	13662	603764217	Previfem tablet UD blist pack 28	rx	generic		28
3466	3871	641092925	Promethazine 50 mg/ml vial vial 1	rx	generic		1
3467	5124	591555501	Propranolol 20 mg tablet bottle 100	rx	generic		100
3468	5124	23155011101	Propranolol 20 mg tablet bottle 100	rx	generic		100
3469	5113	51991081901	Propranolol er 120 mg capsule bottle 100	rx	generic		100
3470	5116	51991081801	Propranolol er 80 mg capsule bottle 100	rx	generic		100
3471	28090	85113201	Proventil hfa 90 mcg inhaler canister 6.7	rx	brand		6
3472	21688	35515095526	Qc acid controller 10 mg tab blist pack 30	otc	generic		30
3473	9797	35515099989	Qc alcohol 70% swabs box 100	otc	generic		100
3474	12080	35515095677	Qc child ibuprofen 100 mg/5 ml Berry bottle 118	otc	generic		118
3475	16947	35515098529	Qc child pain rlf 160 mg/5 ml bottle 118	otc	generic		118
3476	-142675	63551590895	Qc mineral oil heavy 16 oz  16	rx	brand		16
3477	4490	35515099721	Qc non-aspirin 500 mg caplet bottle 500	otc	generic		500
3478	-142687	63551590133	Qc suphedrine nsl dec tb sdfd 24  24	rx	brand		24
3479	18773	69097084205	Quinapril 20 mg tablet bottle 90	rx	generic		90
3480	11672	57664014134	Ranitidine 15 mg/ml syrup Peppermint bottle 473	rx	generic		473
3481	11673	904635051	Ranitidine 150 mg tablet bottle 50	rx	generic		50
3482	49606	49234045	Relpax 40 mg tablet UD blist pack 6	rx	brand		6
3483	21155	68382011514	Risperidone 2 mg tablet bottle 60	rx	generic		60
3484	40224	378370259	Rizatriptan 10 mg odt bottle 9	rx	generic		9
3485	40222	65862060012	Rizatriptan 10 mg tablet UD blist pack 12	rx	generic		12
3486	-38803	21200035517	Scotch tape 1 00x72 bulk 800 1	otc	brand		1
3487	-332962	3076852957	Sd calcium w d3 gummies 50	otc	brand		50
3488	-281733	2260002915	Simply saline nas spr 44ml 1	otc	brand		1
3489	40238	16729000715	Simvastatin 80 mg tablet bottle 90	rx	generic		90
3490	588	378699789	Sodium chloride 3% vial UD vial 15	rx	generic		15
3491	24097	603577025	Sotalol 120 mg tablet bottle 300	rx	generic		300
3492	76420	46287000601	Sps 15 gm/60 ml suspension Cherry bottle 473	rx	brand		473
3493	51492	2322930	Strattera 40 mg capsule bottle 30	rx	brand		30
3494	66635	12496120203	Suboxone 2 mg-0.5 mg sl film packet 30	rx	brand	C-3	30
3495	5089	81086524	Sudafed 30 mg tablet bottle 24	otc	brand		24
3496	27546	62756016088	Tamsulosin hcl 0.4 mg capsule bottle 100	rx	generic		100
3497	23139	65162062711	Tramadol hcl 50 mg tablet bottle 1000	rx	generic	C-4	1000
3498	21718	781207410	Triamterene-hctz 37.5-25 mg cp bottle 1000	rx	generic		1000
3499	-373354	1254600469	Trident gum peppermint 12ct 1	otc	brand		1
3500	63466	574011830	Trospium chloride er 60 mg cap UU bottle 30	rx	generic		30
3501	37354	43547031109	Valsartan-hctz 80-12.5 mg tab bottle 90	rx	generic		90
3502	46399	65162030209	Venlafaxine hcl 37.5 mg tablet bottle 90	rx	generic		90
3503	565	591040401	Verapamil 40 mg tablet bottle 100	rx	generic		100
3504	57982	51248015001	Vesicare 5 mg tablet bottle 30	rx	brand		30
3505	67378	456114030	Viibryd 40 mg tablet bottle 30	rx	brand		30
3506	-274864	2791701950	Vitafus prenat asst gummy 90 90	otc	brand		90
3507	-394144	27917000025	Vitafusion b-12 1500 chew 90 ea 90	otc	brand		90
3508	-151799	2791701468	Vitafusion power c gummy chew 70 ea 70	otc	brand		70
3509	2341	536355101	Vitamin b12 500 mcg tablet bottle 100	otc	generic		100
3510	-458263	7985409330	Vitamin d3 softgel 1000iu 300 9330 300	otc	brand		300
3511	-96960	2200011488	Wrig orbit sf gum wintermint 12	otc	generic		12
3512	41880	65649030103	Xifaxan 200 mg tablet bottle 30	rx	brand		30
3513	46229	49491030	Zoloft 100 mg tablet UU bottle 30	rx	brand		30
\.


--
-- Data for Name: main_refund; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY main_refund (id, "dateFilled", "Rxnumber", "NDC", dispended_drug_description, "Qty_dispended", "Primary_amt", "Second_amt", "Patient_copay", "Total_sales", "Acquistion_cost", "Profit", "Margin", "Dawcode", "Drug_man", "Acq_unit_cost") FROM stdin;
1	2018-08-09	48012018	38779003100	 SR (T3) 15MCG CAP	60	0	0	45.4500000000000028	45.4500000000000028	12.6500000000000004	32.7999999999999972	0.721672169999999946	N0		0
2	2018-08-22	44032488	38779092701	ABHR 1MG12.5MG2MG20MG/ML	6	31.5	0	0	31.5	0.92000000000000004	30.5799999999999983	0.970793649999999952	N0		0
3	2018-08-27	46256872	59148001113	ABILIFY 30MG TABLET	30	1347.31999999999994	0	0	1347.31999999999994	1248.74000000000001	98.5799999999999983	0.0731674699999999983	Y1	OTSUKA AMERICA	41.6246000000000009
4	2018-08-07	46255293	65702040810	ACCU-CHEK AVIVA PLUS TEST S	100	154.639999999999986	0	0	154.639999999999986	142.650000000000006	11.9900000000000002	0.0775349199999999933	N0	ROCHE DIAGNOSTI	1.4265000000000001
5	2018-08-20	46248301	65702040710	ACCU-CHEK AVIVA PLUS TEST S	50	77.8199999999999932	0	0	77.8199999999999932	71.3299999999999983	6.49000000000000021	0.0833975799999999989	N0	ROCHE DIAGNOSTI	1.42660000000000009
6	2018-08-04	46255058	65702040710	ACCU-CHEK AVIVA PLUS TEST S	50	77.8199999999999932	0	0	77.8199999999999932	71.3299999999999983	6.49000000000000021	0.0833975799999999989	N0	ROCHE DIAGNOSTI	1.42660000000000009
7	2018-08-02	46244899	65702040710	ACCU-CHEK AVIVA PLUS TEST S	50	77.8199999999999932	0	0	77.8199999999999932	71.3299999999999983	6.49000000000000021	0.0833975799999999989	N0	ROCHE DIAGNOSTI	1.42660000000000009
8	2018-08-03	46230048	65702028810	ACCU-CHEK FASTCLIX LANCETS	306	38.9399999999999977	0	0	38.9399999999999977	30.75	8.1899999999999995	0.210323570000000015	N0	ROCHE DIAGNOSTI	0.100500000000000006
9	2018-08-21	46249132	65702028810	ACCU-CHEK FASTCLIX LANCETS	102	13.7799999999999994	0	0	13.7799999999999994	10.25	3.5299999999999998	0.256168359999999984	N0	ROCHE DIAGNOSTI	0.100500000000000006
10	2018-08-21	46254029	65702071110	ACCU-CHEK GUIDE TEST STRIP	50	20.6799999999999997	0	0	20.6799999999999997	19.1000000000000014	1.58000000000000007	0.0764023199999999958	N0	ROCHE DIAGNOSTI	0.381900000000000017
11	2018-08-28	46252179	65702071110	ACCU-CHEK GUIDE TEST STRIP	100	40.3599999999999994	0	0	40.3599999999999994	38.1899999999999977	2.16999999999999993	0.0537661099999999989	N0	ROCHE DIAGNOSTI	0.381900000000000017
12	2018-08-20	48012228	38779040908	ACETAMIN(CF) 160MG/5ML SU	100	12	0	0	12	5.25	6.75	0.5625	N0		0
13	2018-08-31	48011946	38779040908	ACETAMIN(GT)160MG/5ML SUS	360	39.740000000000002	0	0	39.740000000000002	3.37999999999999989	36.3599999999999994	0.914947159999999982	N0		0
14	2018-08-14	46256192	45802073230	ACETAMINOPHEN 120 MG SUPPOS	12	3.89999999999999991	0	0	3.89999999999999991	3.7200000000000002	0.179999999999999993	0.0461538499999999963	N0	PERRIGO CO.	0.30980000000000002
15	2018-08-04	46255095	57896010110	ACETAMINOPHEN 325 MG TABLET	6	2.04000000000000004	0	0	2.04000000000000004	0.0299999999999999989	2.00999999999999979	0.98529411999999994	N0	GERI-CARE	0.0057999999999999996
16	2018-08-27	46257860	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
17	2018-08-05	46255153	57896010110	ACETAMINOPHEN 325 MG TABLET	8	2.04999999999999982	0	0	2.04999999999999982	0.0500000000000000028	2	0.975609759999999993	N0	GERI-CARE	0.0057999999999999996
18	2018-08-14	46256246	57896010110	ACETAMINOPHEN 325 MG TABLET	60	2.39999999999999991	0	0	2.39999999999999991	0.349999999999999978	2.04999999999999982	0.854166670000000017	N0	GERI-CARE	0.0057999999999999996
19	2018-08-01	46254677	57896010110	ACETAMINOPHEN 325 MG TABLET	120	1.69999999999999996	0	0	1.69999999999999996	0.699999999999999956	1	0.588235289999999966	N0	GERI-CARE	0.0057999999999999996
20	2018-08-29	46242605	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
21	2018-08-05	46254329	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
22	2018-08-17	46256904	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
23	2018-08-06	46255274	57896010110	ACETAMINOPHEN 325 MG TABLET	8	2.04999999999999982	0	0	2.04999999999999982	0.0500000000000000028	2	0.975609759999999993	N0	GERI-CARE	0.0057999999999999996
24	2018-08-03	46254915	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
25	2018-08-12	46255921	57896010110	ACETAMINOPHEN 325 MG TABLET	9	2.06000000000000005	0	0	2.06000000000000005	0.0500000000000000028	2.00999999999999979	0.975728160000000067	N0	GERI-CARE	0.0057999999999999996
26	2018-08-17	46256618	57896010110	ACETAMINOPHEN 325 MG TABLET	20	2.14000000000000012	0	0	2.14000000000000012	0.119999999999999996	2.02000000000000002	0.943925229999999948	N0	GERI-CARE	0.0057999999999999996
27	2018-08-16	46255137	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
28	2018-08-27	46255137	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
29	2018-08-09	46255781	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
30	2018-08-24	46255137	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
31	2018-08-12	46255891	57896010110	ACETAMINOPHEN 325 MG TABLET	42	2.2799999999999998	0	0	2.2799999999999998	0.239999999999999991	2.04000000000000004	0.894736840000000089	N0	GERI-CARE	0.0057999999999999996
32	2018-08-21	46257112	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
33	2018-08-15	46255699	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
34	2018-08-09	46255699	57896010110	ACETAMINOPHEN 325 MG TABLET	20	2.14000000000000012	0	0	2.14000000000000012	0.119999999999999996	2.02000000000000002	0.943925229999999948	N0	GERI-CARE	0.0057999999999999996
35	2018-08-08	46255581	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
36	2018-08-09	46241003	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
37	2018-08-19	46256915	57896010110	ACETAMINOPHEN 325 MG TABLET	3	2.02000000000000002	0	0	2.02000000000000002	0.0200000000000000004	2	0.990099010000000002	N0	GERI-CARE	0.0057999999999999996
38	2018-08-15	46256459	57896010110	ACETAMINOPHEN 325 MG TABLET	27	2.18000000000000016	0	0	2.18000000000000016	0.160000000000000003	2.02000000000000002	0.926605499999999971	N0	GERI-CARE	0.0057999999999999996
39	2018-08-19	46255137	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
40	2018-08-24	46257589	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
41	2018-08-21	46254012	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
42	2018-08-07	46255366	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
43	2018-08-29	46251419	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
44	2018-08-06	46252088	57896010110	ACETAMINOPHEN 325 MG TABLET	135	2.89000000000000012	0	0	2.89000000000000012	0.780000000000000027	2.10999999999999988	0.730103809999999909	N0	GERI-CARE	0.0057999999999999996
45	2018-08-16	46256667	57896010110	ACETAMINOPHEN 325 MG TABLET	4	2.0299999999999998	0	0	2.0299999999999998	0.0200000000000000004	2.00999999999999979	0.990147780000000033	N0	GERI-CARE	0.0057999999999999996
46	2018-08-06	46255269	57896010110	ACETAMINOPHEN 325 MG TABLET	4	2.0299999999999998	0	0	2.0299999999999998	0.0200000000000000004	2.00999999999999979	0.990147780000000033	N0	GERI-CARE	0.0057999999999999996
47	2018-08-17	46256604	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
48	2018-08-16	46256502	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
49	2018-08-31	46258267	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
50	2018-08-06	46255137	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
51	2018-08-09	46255778	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
52	2018-08-12	46255902	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
53	2018-08-30	46258306	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
54	2018-08-13	46255137	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
55	2018-08-15	46256437	57896010110	ACETAMINOPHEN 325 MG TABLET	15	2.10000000000000009	0	0	2.10000000000000009	0.0899999999999999967	2.00999999999999979	0.95714286000000004	N0	GERI-CARE	0.0057999999999999996
56	2018-08-12	46256034	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
57	2018-08-12	46256052	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
58	2018-08-10	46256007	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
59	2018-08-06	46254493	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
60	2018-08-19	46256459	57896010110	ACETAMINOPHEN 325 MG TABLET	27	2.18000000000000016	0	0	2.18000000000000016	0.160000000000000003	2.02000000000000002	0.926605499999999971	N0	GERI-CARE	0.0057999999999999996
61	2018-08-08	46254329	57896010110	ACETAMINOPHEN 325 MG TABLET	12	2.08000000000000007	0	0	2.08000000000000007	0.0700000000000000067	2.00999999999999979	0.966346150000000015	N0	GERI-CARE	0.0057999999999999996
62	2018-08-09	46255137	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
63	2018-08-22	46257205	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
64	2018-08-02	46254329	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
65	2018-08-12	46256051	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
66	2018-08-22	46257194	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
67	2018-08-08	46255524	57896010110	ACETAMINOPHEN 325 MG TABLET	12	2.08000000000000007	0	0	2.08000000000000007	0.0700000000000000067	2.00999999999999979	0.966346150000000015	N0	GERI-CARE	0.0057999999999999996
68	2018-08-07	46255254	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
69	2018-08-28	46258021	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
70	2018-08-21	46256459	57896010110	ACETAMINOPHEN 325 MG TABLET	45	2.29999999999999982	0	0	2.29999999999999982	0.260000000000000009	2.04000000000000004	0.886956519999999915	N0	GERI-CARE	0.0057999999999999996
71	2018-08-21	46257073	57896010110	ACETAMINOPHEN 325 MG TABLET	120	2.22999999999999998	0	0	2.22999999999999998	0.699999999999999956	1.53000000000000003	0.686098650000000032	N0	GERI-CARE	0.0057999999999999996
72	2018-08-10	46254493	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
73	2018-08-16	46256472	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
74	2018-08-28	46246204	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
75	2018-08-22	46257131	57896010110	ACETAMINOPHEN 325 MG TABLET	90	1.53000000000000003	0	0	1.53000000000000003	0.520000000000000018	1.01000000000000001	0.660130719999999949	N0	GERI-CARE	0.0057999999999999996
76	2018-08-20	46257065	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
77	2018-08-13	46255958	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
78	2018-08-24	46241003	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
79	2018-08-30	46258271	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
80	2018-08-07	46255308	57896010110	ACETAMINOPHEN 325 MG TABLET	18	2.12000000000000011	0	0	2.12000000000000011	0.100000000000000006	2.02000000000000002	0.95283018999999991	N0	GERI-CARE	0.0057999999999999996
81	2018-08-28	46257942	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
82	2018-08-30	46258210	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
83	2018-08-12	46256061	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
84	2018-08-09	46251599	57896010110	ACETAMINOPHEN 325 MG TABLET	60	0	0	2.4700000000000002	2.4700000000000002	0.349999999999999978	2.12000000000000011	0.858299600000000051	N0	GERI-CARE	0.0057999999999999996
85	2018-08-20	46256852	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
86	2018-08-14	46256137	57896010110	ACETAMINOPHEN 325 MG TABLET	12	2.08000000000000007	0	0	2.08000000000000007	0.0700000000000000067	2.00999999999999979	0.966346150000000015	N0	GERI-CARE	0.0057999999999999996
87	2018-08-17	46256894	57896010110	ACETAMINOPHEN 325 MG TABLET	2	2.02000000000000002	0	0	2.02000000000000002	0.0100000000000000002	2.00999999999999979	0.99504949999999992	N0	GERI-CARE	0.0057999999999999996
88	2018-08-03	46255042	57896010110	ACETAMINOPHEN 325 MG TABLET	6	2.04000000000000004	0	0	2.04000000000000004	0.0299999999999999989	2.00999999999999979	0.98529411999999994	N0	GERI-CARE	0.0057999999999999996
89	2018-08-18	46256789	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
90	2018-08-04	46255027	57896010110	ACETAMINOPHEN 325 MG TABLET	20	2.14000000000000012	0	0	2.14000000000000012	0.119999999999999996	2.02000000000000002	0.943925229999999948	N0	GERI-CARE	0.0057999999999999996
91	2018-08-28	46257910	57896010110	ACETAMINOPHEN 325 MG TABLET	12	2.08000000000000007	0	0	2.08000000000000007	0.0700000000000000067	2.00999999999999979	0.966346150000000015	N0	GERI-CARE	0.0057999999999999996
92	2018-08-30	46255366	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
93	2018-08-08	46255513	57896010110	ACETAMINOPHEN 325 MG TABLET	90	2.58999999999999986	0	0	2.58999999999999986	0.520000000000000018	2.06999999999999984	0.799227800000000044	N0	GERI-CARE	0.0057999999999999996
94	2018-08-09	46252827	57896010110	ACETAMINOPHEN 325 MG TABLET	135	2.89000000000000012	0	0	2.89000000000000012	0.780000000000000027	2.10999999999999988	0.730103809999999909	N0	GERI-CARE	0.0057999999999999996
95	2018-08-01	46242605	57896010110	ACETAMINOPHEN 325 MG TABLET	30	2.18999999999999995	0	0	2.18999999999999995	0.170000000000000012	2.02000000000000002	0.922374429999999967	N0	GERI-CARE	0.0057999999999999996
96	2018-08-22	46257200	57896010110	ACETAMINOPHEN 325 MG TABLET	135	2.89000000000000012	0	0	2.89000000000000012	0.780000000000000027	2.10999999999999988	0.730103809999999909	N0	GERI-CARE	0.0057999999999999996
97	2018-08-06	46253446	536322210	ACETAMINOPHEN 325MG TABLE	30	0	1.6100000000000001	1	2.60999999999999988	0.429999999999999993	2.18000000000000016	0.835249040000000109	N0	RUGBY	0.0143000000000000002
98	2018-08-25	46247632	536322210	ACETAMINOPHEN 325MG TABLE	120	0	0	1	1	1.71999999999999997	-0.719999999999999973	-0.719999999999999973	N0	RUGBY	0.0143000000000000002
99	2018-08-20	46253446	536322210	ACETAMINOPHEN 325MG TABLE	30	0	1.6100000000000001	1	2.60999999999999988	0.429999999999999993	2.18000000000000016	0.835249040000000109	N0	RUGBY	0.0143000000000000002
100	2018-08-27	46253446	536322210	ACETAMINOPHEN 325MG TABLE	30	0	1.6100000000000001	1	2.60999999999999988	0.429999999999999993	2.18000000000000016	0.835249040000000109	N0	RUGBY	0.0143000000000000002
101	2018-08-20	46256924	536321810	ACETAMINOPHEN 500MG CPL	18	2.35999999999999988	0	0	2.35999999999999988	0.190000000000000002	2.16999999999999993	0.919491529999999946	N0	RUGBY	0.0106999999999999994
102	2018-08-12	46255908	536321810	ACETAMINOPHEN 500MG CPL	9	2.22999999999999998	0	0	2.22999999999999998	0.100000000000000006	2.12999999999999989	0.955156949999999894	N0	RUGBY	0.0106999999999999994
103	2018-08-15	46237182	536321810	ACETAMINOPHEN 500MG CPL	90	3.79000000000000004	0	0	3.79000000000000004	0.959999999999999964	2.83000000000000007	0.74670185	N0	RUGBY	0.0106999999999999994
104	2018-08-05	46254208	536321810	ACETAMINOPHEN 500MG CPL	4	2.08000000000000007	0	0	2.08000000000000007	0.0400000000000000008	2.04000000000000004	0.980769229999999936	N0	RUGBY	0.0106999999999999994
105	2018-08-29	46258115	536321810	ACETAMINOPHEN 500MG CPL	6	2.12000000000000011	0	0	2.12000000000000011	0.0599999999999999978	2.06000000000000005	0.971698109999999948	N0	RUGBY	0.0106999999999999994
106	2018-08-23	46256924	536321810	ACETAMINOPHEN 500MG CPL	18	2.35999999999999988	0	0	2.35999999999999988	0.190000000000000002	2.16999999999999993	0.919491529999999946	N0	RUGBY	0.0106999999999999994
107	2018-08-16	46256479	536321810	ACETAMINOPHEN 500MG CPL	30	1.66999999999999993	0	0	1.66999999999999993	0.320000000000000007	1.35000000000000009	0.808383230000000008	N0	RUGBY	0.0106999999999999994
108	2018-08-02	46247666	536321810	ACETAMINOPHEN 500MG CPL	90	4.29000000000000004	0	0	4.29000000000000004	0.959999999999999964	3.33000000000000007	0.77622378000000003	N0	RUGBY	0.0106999999999999994
109	2018-08-28	46257880	536321810	ACETAMINOPHEN 500MG CPL	30	0	1.60000000000000009	1	2.60000000000000009	0.320000000000000007	2.2799999999999998	0.876923079999999966	N0	RUGBY	0.0106999999999999994
110	2018-08-26	46257637	536321810	ACETAMINOPHEN 500MG CPL	30	2.77000000000000002	0	0	2.77000000000000002	0.320000000000000007	2.45000000000000018	0.884476530000000039	N0	RUGBY	0.0106999999999999994
111	2018-08-21	46257035	536321810	ACETAMINOPHEN 500MG CPL	100	0	5.57000000000000028	1	6.57000000000000028	1.07000000000000006	5.5	0.837138510000000058	N0	RUGBY	0.0106999999999999994
112	2018-08-17	46256730	536321810	ACETAMINOPHEN 500MG CPL	30	2.77000000000000002	0	0	2.77000000000000002	0.320000000000000007	2.45000000000000018	0.884476530000000039	N0	RUGBY	0.0106999999999999994
113	2018-08-05	46254208	536321810	ACETAMINOPHEN 500MG CPL	12	2.24000000000000021	0	0	2.24000000000000021	0.130000000000000004	2.10999999999999988	0.941964289999999926	N0	RUGBY	0.0106999999999999994
114	2018-08-10	46247726	536321810	ACETAMINOPHEN 500MG CPL	60	3.18999999999999995	0	0	3.18999999999999995	0.640000000000000013	2.54999999999999982	0.799373039999999979	N0	RUGBY	0.0106999999999999994
115	2018-08-24	46256118	536321810	ACETAMINOPHEN 500MG CPL	60	3.18999999999999995	0	0	3.18999999999999995	0.640000000000000013	2.54999999999999982	0.799373039999999979	N0	RUGBY	0.0106999999999999994
116	2018-08-07	46252802	536321810	ACETAMINOPHEN 500MG CPL	60	3.18999999999999995	0	0	3.18999999999999995	0.640000000000000013	2.54999999999999982	0.799373039999999979	N0	RUGBY	0.0106999999999999994
117	2018-08-11	46255541	536321810	ACETAMINOPHEN 500MG CPL	9	2.22999999999999998	0	0	2.22999999999999998	0.100000000000000006	2.12999999999999989	0.955156949999999894	N0	RUGBY	0.0106999999999999994
118	2018-08-09	46251300	536321810	ACETAMINOPHEN 500MG CPL	60	2.35000000000000009	0	0	2.35000000000000009	0.640000000000000013	1.70999999999999996	0.727659570000000033	N0	RUGBY	0.0106999999999999994
119	2018-08-27	46247666	536321810	ACETAMINOPHEN 500MG CPL	90	3.79000000000000004	0	0	3.79000000000000004	0.959999999999999964	2.83000000000000007	0.74670185	N0	RUGBY	0.0106999999999999994
120	2018-08-15	46255676	536321810	ACETAMINOPHEN 500MG CPL	3	2.06000000000000005	0	0	2.06000000000000005	0.0299999999999999989	2.0299999999999998	0.98543689000000001	N0	RUGBY	0.0106999999999999994
121	2018-08-13	46256118	536321810	ACETAMINOPHEN 500MG CPL	60	3.5299999999999998	0	0	3.5299999999999998	0.640000000000000013	2.89000000000000012	0.81869687999999996	N0	RUGBY	0.0106999999999999994
122	2018-08-01	46254682	536321810	ACETAMINOPHEN 500MG CPL	12	2.31000000000000005	0	0	2.31000000000000005	0.130000000000000004	2.18000000000000016	0.94372294000000001	N0	RUGBY	0.0106999999999999994
123	2018-08-16	46238058	536321810	ACETAMINOPHEN 500MG CPL	30	2.60000000000000009	0	0	2.60000000000000009	0.320000000000000007	2.2799999999999998	0.876923079999999966	N0	RUGBY	0.0106999999999999994
124	2018-08-24	46257544	536321810	ACETAMINOPHEN 500MG CPL	20	2.50999999999999979	0	0	2.50999999999999979	0.209999999999999992	2.29999999999999982	0.916334659999999968	N0	RUGBY	0.0106999999999999994
125	2018-08-08	46255541	536321810	ACETAMINOPHEN 500MG CPL	9	2.22999999999999998	0	0	2.22999999999999998	0.100000000000000006	2.12999999999999989	0.955156949999999894	N0	RUGBY	0.0106999999999999994
126	2018-08-06	46253055	536321810	ACETAMINOPHEN 500MG CPL	45	3.14999999999999991	0	0	3.14999999999999991	0.479999999999999982	2.66999999999999993	0.847619050000000041	N0	RUGBY	0.0106999999999999994
127	2018-08-22	46257269	536321810	ACETAMINOPHEN 500MG CPL	20	2.50999999999999979	0	0	2.50999999999999979	0.209999999999999992	2.29999999999999982	0.916334659999999968	N0	RUGBY	0.0106999999999999994
128	2018-08-09	46255676	536321810	ACETAMINOPHEN 500MG CPL	3	2.08000000000000007	0	0	2.08000000000000007	0.0299999999999999989	2.04999999999999982	0.985576920000000078	N0	RUGBY	0.0106999999999999994
129	2018-08-21	46257019	536321810	ACETAMINOPHEN 500MG CPL	90	3.79000000000000004	0	0	3.79000000000000004	0.959999999999999964	2.83000000000000007	0.74670185	N0	RUGBY	0.0106999999999999994
130	2018-08-05	46254682	536321810	ACETAMINOPHEN 500MG CPL	12	2.31000000000000005	0	0	2.31000000000000005	0.130000000000000004	2.18000000000000016	0.94372294000000001	N0	RUGBY	0.0106999999999999994
131	2018-08-28	46257894	536321810	ACETAMINOPHEN 500MG CPL	60	3.18999999999999995	0	0	3.18999999999999995	0.640000000000000013	2.54999999999999982	0.799373039999999979	N0	RUGBY	0.0106999999999999994
132	2018-08-30	46238058	536321810	ACETAMINOPHEN 500MG CPL	30	2.60000000000000009	0	0	2.60000000000000009	0.320000000000000007	2.2799999999999998	0.876923079999999966	N0	RUGBY	0.0106999999999999994
133	2018-08-13	46230125	536321810	ACETAMINOPHEN 500MG CPL	90	0	0	1	1	0.959999999999999964	0.0400000000000000008	0.0400000000000000008	N0	RUGBY	0.0106999999999999994
134	2018-08-02	46254208	536321810	ACETAMINOPHEN 500MG CPL	12	2.24000000000000021	0	0	2.24000000000000021	0.130000000000000004	2.10999999999999988	0.941964289999999926	N0	RUGBY	0.0106999999999999994
135	2018-08-12	46255676	536321810	ACETAMINOPHEN 500MG CPL	3	2.08000000000000007	0	0	2.08000000000000007	0.0299999999999999989	2.04999999999999982	0.985576920000000078	N0	RUGBY	0.0106999999999999994
136	2018-08-29	46237182	536321810	ACETAMINOPHEN 500MG CPL	90	3.79000000000000004	0	0	3.79000000000000004	0.959999999999999964	2.83000000000000007	0.74670185	N0	RUGBY	0.0106999999999999994
753	2018-08-06	48011145	38779038805	BACLOFEN 5MG/ML SUSP	270	34.4500000000000028	0	0	34.4500000000000028	8.77999999999999936	25.6700000000000017	0.74513788000000003	N0		0
137	2018-08-12	46256057	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
138	2018-08-01	46241833	45802073033	ACETAMINOPHEN 650 MG SUPPOS	30	0	0	14.9000000000000004	14.9000000000000004	6.16999999999999993	8.73000000000000043	0.585906040000000017	N	PERRIGO CO.	0.205499999999999988
139	2018-08-31	46258498	45802073033	ACETAMINOPHEN 650 MG SUPPOS	2	2.72999999999999998	0	0	2.72999999999999998	0.409999999999999976	2.31999999999999984	0.849816849999999957	N0	PERRIGO CO.	0.205499999999999988
140	2018-08-23	46257302	45802073033	ACETAMINOPHEN 650 MG SUPPOS	12	6.38999999999999968	0	0	6.38999999999999968	2.4700000000000002	3.91999999999999993	0.613458529999999946	N0	PERRIGO CO.	0.205499999999999988
141	2018-08-30	46258239	45802073033	ACETAMINOPHEN 650 MG SUPPOS	8	4.91999999999999993	0	0	4.91999999999999993	1.6399999999999999	3.2799999999999998	0.666666670000000017	N0	PERRIGO CO.	0.205499999999999988
142	2018-08-02	46254914	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
143	2018-08-31	46258269	45802073033	ACETAMINOPHEN 650 MG SUPPOS	6	4.19000000000000039	0	0	4.19000000000000039	1.22999999999999998	2.95999999999999996	0.706443909999999953	N0	PERRIGO CO.	0.205499999999999988
144	2018-08-18	46256876	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
145	2018-08-09	46255779	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
146	2018-08-21	46257164	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
147	2018-08-01	46254808	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
148	2018-08-04	46255104	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
149	2018-08-23	46257511	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
150	2018-08-21	46257173	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
151	2018-08-14	46256177	45802073033	ACETAMINOPHEN 650 MG SUPPOS	12	6.38999999999999968	0	0	6.38999999999999968	2.4700000000000002	3.91999999999999993	0.613458529999999946	N0	PERRIGO CO.	0.205499999999999988
152	2018-08-24	46257597	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
153	2018-08-31	46257713	45802073033	ACETAMINOPHEN 650 MG SUPPOS	9	5.29000000000000004	0	0	5.29000000000000004	1.85000000000000009	3.43999999999999995	0.650283550000000043	N0	PERRIGO CO.	0.205499999999999988
154	2018-08-09	46255658	45802073033	ACETAMINOPHEN 650 MG SUPPOS	12	0	0	7.16000000000000014	7.16000000000000014	2.4700000000000002	4.69000000000000039	0.655027929999999925	N0	PERRIGO CO.	0.205499999999999988
155	2018-08-16	46256664	45802073033	ACETAMINOPHEN 650 MG SUPPOS	2	2.72999999999999998	0	0	2.72999999999999998	0.409999999999999976	2.31999999999999984	0.849816849999999957	N0	PERRIGO CO.	0.205499999999999988
156	2018-08-06	46255245	45802073033	ACETAMINOPHEN 650 MG SUPPOS	12	6.38999999999999968	0	0	6.38999999999999968	2.4700000000000002	3.91999999999999993	0.613458529999999946	N0	PERRIGO CO.	0.205499999999999988
157	2018-08-07	46255499	45802073033	ACETAMINOPHEN 650 MG SUPPOS	2	2.72999999999999998	0	0	2.72999999999999998	0.409999999999999976	2.31999999999999984	0.849816849999999957	N0	PERRIGO CO.	0.205499999999999988
158	2018-08-07	46255488	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
159	2018-08-27	46257713	45802073033	ACETAMINOPHEN 650 MG SUPPOS	9	5.29000000000000004	0	0	5.29000000000000004	1.85000000000000009	3.43999999999999995	0.650283550000000043	N0	PERRIGO CO.	0.205499999999999988
160	2018-08-10	46241833	45802073033	ACETAMINOPHEN 650 MG SUPPOS	30	0	0	14.9000000000000004	14.9000000000000004	6.16999999999999993	8.73000000000000043	0.585906040000000017	N	PERRIGO CO.	0.205499999999999988
161	2018-08-08	46255587	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
162	2018-08-27	46257863	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
163	2018-08-20	46241833	45802073033	ACETAMINOPHEN 650 MG SUPPOS	30	0	0	14.9000000000000004	14.9000000000000004	6.16999999999999993	8.73000000000000043	0.585906040000000017	N	PERRIGO CO.	0.205499999999999988
164	2018-08-06	46255277	45802073033	ACETAMINOPHEN 650 MG SUPPOS	1	2.37000000000000011	0	0	2.37000000000000011	0.209999999999999992	2.16000000000000014	0.911392409999999931	N0	PERRIGO CO.	0.205499999999999988
165	2018-08-26	46257622	45802073033	ACETAMINOPHEN 650 MG SUPPOS	15	7.48000000000000043	0	0	7.48000000000000043	3.08000000000000007	4.40000000000000036	0.588235289999999966	N0	PERRIGO CO.	0.205499999999999988
166	2018-08-29	46241833	45802073033	ACETAMINOPHEN 650 MG SUPPOS	30	0	0	14.9000000000000004	14.9000000000000004	6.16999999999999993	8.73000000000000043	0.585906040000000017	N	PERRIGO CO.	0.205499999999999988
167	2018-08-19	46256814	45802073033	ACETAMINOPHEN 650 MG SUPPOS	12	6.38999999999999968	0	0	6.38999999999999968	2.4700000000000002	3.91999999999999993	0.613458529999999946	N0	PERRIGO CO.	0.205499999999999988
168	2018-08-08	46255584	45802073033	ACETAMINOPHEN 650 MG SUPPOS	2	2.72999999999999998	0	0	2.72999999999999998	0.409999999999999976	2.31999999999999984	0.849816849999999957	N0	PERRIGO CO.	0.205499999999999988
169	2018-08-07	44032270	406048401	ACETAMINOPHEN-COD #3 TABLET	16	5.26999999999999957	0	0	5.26999999999999957	1.19999999999999996	4.07000000000000028	0.772296019999999972	N0	MALLINCKRODT PH	0.0976000000000000062
170	2018-08-06	44031043	406048401	ACETAMINOPHEN-COD #3 TABLET	30	0	0	21.7399999999999984	21.7399999999999984	2.24000000000000021	19.5	0.89696411999999992	N0	MALLINCKRODT PH	0.0976000000000000062
171	2018-08-02	44032195	93015001	ACETAMINOPHEN/COD #3 TAB	8	2.85000000000000009	0	0	2.85000000000000009	0.92000000000000004	1.92999999999999994	0.677192979999999944	N0	TEVA USA	0.114900000000000002
172	2018-08-22	44032465	406048410	ACETAMINOPHEN/COD #3 TABL	16	1.37999999999999989	0	1.94999999999999996	3.33000000000000007	0.900000000000000022	2.43000000000000016	0.729729729999999965	N0	MALLINCKRODT PH	0.0563999999999999987
173	2018-08-13	44032350	406048410	ACETAMINOPHEN/COD #3 TABL	85	38.9200000000000017	0	0	38.9200000000000017	4.79000000000000004	34.1300000000000026	0.876927029999999941	N0	MALLINCKRODT PH	0.0563999999999999987
174	2018-08-28	44032547	406048410	ACETAMINOPHEN/COD #3 TABL	30	2.14999999999999991	0	1.14999999999999991	3.29999999999999982	1.68999999999999995	1.6100000000000001	0.487878789999999951	N0	MALLINCKRODT PH	0.0563999999999999987
175	2018-08-28	46248526	51672402301	ACETAZOLAMIDE 250MG TABLE	15	38.6099999999999994	0	0	38.6099999999999994	14.4399999999999995	24.1700000000000017	0.626003630000000033	N0	TARO PHARM USA	0.962400000000000033
176	2018-08-14	46248526	51672402301	ACETAZOLAMIDE 250MG TABLE	15	38.6099999999999994	0	0	38.6099999999999994	19.6999999999999993	18.9100000000000001	0.489769489999999974	N0	TARO PHARM USA	0.962400000000000033
177	2018-08-17	46256675	51672402301	ACETAZOLAMIDE 250MG TABLE	30	34	0	0	34	28.870000000000001	5.12999999999999989	0.150882349999999998	N0	TARO PHARM USA	0.962400000000000033
178	2018-08-06	46255206	409330803	ACETYLCYSTEINE 20%	240	68.5999999999999943	0	1	69.5999999999999943	76.4200000000000017	-6.82000000000000028	-0.0979885100000000148	N0	HOSPIRA/PFIZER	0.318400000000000016
179	2018-08-13	46251345	409330803	ACETYLCYSTEINE 20% VIAL	240	104.969999999999999	0	0	104.969999999999999	51.5499999999999972	53.4200000000000017	0.508907310000000002	N0	HOSPIRA/PFIZER	0.214799999999999991
180	2018-08-12	46255944	23350730	ACUVAIL 0.45% OPHTH SOLUTIO	30	271.089999999999975	0	29.6700000000000017	300.759999999999991	274.339999999999975	26.4200000000000017	0.0878441300000000064	N0	ALLERGAN INC.	9.14460000000000051
754	2018-08-02	48012333	38779038805	BACLOFEN 5MG/ML SUSP	90	20.3900000000000006	0	0	20.3900000000000006	3.77000000000000002	16.620000000000001	0.815105439999999959	N0		0
181	2018-08-03	46234554	60505004206	ACYCLOVIR 200 MG CAPSULE	25	0	0	2.97999999999999998	2.97999999999999998	2.12000000000000011	0.859999999999999987	0.288590599999999975	N0	APOTEX CORP	0.0616999999999999979
182	2018-08-28	46244832	93894001	ACYCLOVIR 200MG CAPSULE	120	5.48000000000000043	0	1	6.48000000000000043	13.6899999999999995	-7.20999999999999996	-1.11265432000000009	N0	TEVA USA	0.114099999999999993
183	2018-08-23	46257368	60505530601	ACYCLOVIR 400 MG TABLET	70	3.12000000000000011	0	1	4.12000000000000011	4.78000000000000025	-0.660000000000000031	-0.160194169999999997	N0	APOTEX CORP	0.0682999999999999996
184	2018-08-03	46255017	60505530601	ACYCLOVIR 400 MG TABLET	28	3.68000000000000016	0	0	3.68000000000000016	2.18999999999999995	1.48999999999999999	0.40489130000000001	N0	APOTEX CORP	0.0682999999999999996
185	2018-08-02	46254783	60505530601	ACYCLOVIR 400 MG TABLET	60	5.41000000000000014	0	2.4700000000000002	7.87999999999999989	4.69000000000000039	3.18999999999999995	0.404822340000000003	N0	APOTEX CORP	0.0682999999999999996
186	2018-08-13	46246078	60505530601	ACYCLOVIR 400 MG TABLET	15	1.96999999999999997	0	1	2.9700000000000002	1.02000000000000002	1.94999999999999996	0.65656565999999994	N0	APOTEX CORP	0.0682999999999999996
187	2018-08-01	46254707	60505530601	ACYCLOVIR 400 MG TABLET	30	59.3100000000000023	0	0	59.3100000000000023	2.35000000000000009	56.9600000000000009	0.960377680000000011	N0	APOTEX CORP	0.0682999999999999996
188	2018-08-23	46254707	60505530601	ACYCLOVIR 400 MG TABLET	30	59.3100000000000023	0	0	59.3100000000000023	2.04999999999999982	57.259999999999998	0.965435849999999984	N0	APOTEX CORP	0.0682999999999999996
189	2018-08-15	46256382	49281040010	ADACEL VIAL	0.5	25.1799999999999997	20	0	45.1799999999999997	34.1400000000000006	11.0399999999999991	0.244355909999999982	N0	SANOFI-PASTEUR	68.2740000000000009
190	2018-08-16	42055807	54092038501	ADDERALL XR 15MG CAPSULE	30	214.969999999999999	0	0	214.969999999999999	211.550000000000011	3.41999999999999993	0.0159091999999999983	N9	SHIRE US INC.	7.0517000000000003
191	2018-08-13	42055738	54092038501	ADDERALL XR 15MG CAPSULE	30	208.900000000000006	0	3	211.900000000000006	211.550000000000011	0.349999999999999978	0.00165172000000000018	Y9	SHIRE US INC.	7.0517000000000003
192	2018-08-03	42055518	54092038701	ADDERALL XR 20MG CAPSULE	60	423.449999999999989	0	0	423.449999999999989	401.949999999999989	21.5	0.050773409999999998	N9	SHIRE US INC.	6.69909999999999961
193	2018-08-31	42056174	54092038701	ADDERALL XR 20MG CAPSULE	60	423.449999999999989	0	0	423.449999999999989	401.949999999999989	21.5	0.050773409999999998	Y9	SHIRE US INC.	6.69909999999999961
194	2018-08-06	42055340	54092038901	ADDERALL XR 25MG CAPSULE	30	0	0	0	0	211.550000000000011	-211.550000000000011	0	N9	SHIRE US INC.	7.0517000000000003
195	2018-08-29	42053595	54092039101	ADDERALL XR 30MG  CAPSULE	30	190.879999999999995	21.0199999999999996	0	211.900000000000006	211.550000000000011	0.349999999999999978	0.00165172000000000018	Y9	SHIRE US INC.	7.0517000000000003
196	2018-08-04	46234598	173069600	ADVAIR 250-50 DISKUS	60	375.759999999999991	0	3.70000000000000018	379.45999999999998	347.050000000000011	32.4099999999999966	0.0854108500000000104	N0	GLAXOSMITHKLINE	5.78409999999999958
197	2018-08-24	46242129	173069700	ADVAIR 500-50 DISKUS	60	548.730000000000018	0	0	548.730000000000018	456.449999999999989	92.2800000000000011	0.168170139999999996	N0	GLAXOSMITHKLINE	7.60749999999999993
198	2018-08-03	46250426	173069700	ADVAIR 500-50 DISKUS	60	549.830000000000041	0	3.70000000000000018	553.529999999999973	456.449999999999989	97.0799999999999983	0.175383449999999996	N0	GLAXOSMITHKLINE	7.60749999999999993
199	2018-08-10	46245221	173069500	ADVAIR DISKUS 100/50	180	850.600000000000023	0	62.5	913.100000000000023	837.950000000000045	75.1500000000000057	0.0823020500000000016	N0	GLAXOSMITHKLINE	4.65530000000000044
200	2018-08-13	46251383	173069600	ADVAIR DISKUS 250/50	60	394.45999999999998	0	8.34999999999999964	402.810000000000002	347.050000000000011	55.759999999999998	0.13842755000000001	N0	GLAXOSMITHKLINE	5.78409999999999958
201	2018-08-24	46240161	173069600	ADVAIR DISKUS 250/50	60	388.129999999999995	0	0	388.129999999999995	347.050000000000011	41.0799999999999983	0.105840829999999997	N0	GLAXOSMITHKLINE	5.78409999999999958
202	2018-08-11	46229391	173069600	ADVAIR DISKUS 250/50	60	189.72999999999999	0	189.72999999999999	379.45999999999998	347.050000000000011	32.4099999999999966	0.0854108500000000104	N0	GLAXOSMITHKLINE	5.78409999999999958
203	2018-08-30	46221696	173069600	ADVAIR DISKUS 250/50	60	345.339999999999975	0	45	390.339999999999975	347.050000000000011	43.2899999999999991	0.11090332	N0	GLAXOSMITHKLINE	5.78409999999999958
204	2018-08-31	46258355	173071720	ADVAIR HFA 230/21 MCG INHAL	12	532.460000000000036	0	0	532.460000000000036	456.449999999999989	76.0100000000000051	0.142752509999999999	N0	GLAXOSMITHKLINE	38.0375000000000014
205	2018-08-10	46219832	173071720	ADVAIR HFA 230/21 MCG INHAL	12	384.509999999999991	0	128.169999999999987	512.67999999999995	456.449999999999989	56.2299999999999969	0.10967855	N0	GLAXOSMITHKLINE	38.0375000000000014
206	2018-08-22	46249371	173071720	ADVAIR HFA 230/21 MCG INHAL	12	450.050000000000011	70.4099999999999966	9	529.460000000000036	456.449999999999989	73.0100000000000051	0.137895210000000018	N0	GLAXOSMITHKLINE	38.0375000000000014
207	2018-08-21	46257040	52054055022	ALBENZA 200 MG TABLET	2	468.410000000000025	0	0	468.410000000000025	361.980000000000018	106.430000000000007	0.227215470000000003	N0	AMEDRA PHARMACE	180.990000000000009
208	2018-08-29	46258162	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
209	2018-08-29	46258171	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
210	2018-08-07	46255495	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
211	2018-08-14	46256331	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
212	2018-08-31	46258495	378699093	ALBUTEROL 0.083% INHAL SOLN	9	8	0	0	8	2.74000000000000021	5.25999999999999979	0.657499999999999973	N0	MYLAN	0.304599999999999982
213	2018-08-09	46255776	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
214	2018-08-27	46257877	378699093	ALBUTEROL 0.083% INHAL SOLN	6	6.66000000000000014	0	0	6.66000000000000014	1.83000000000000007	4.83000000000000007	0.725225230000000054	N0	MYLAN	0.304599999999999982
215	2018-08-03	46255044	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
216	2018-08-25	46257714	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
217	2018-08-05	46255157	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
218	2018-08-04	46255097	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
219	2018-08-05	46255155	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
220	2018-08-13	46256174	378699093	ALBUTEROL 0.083% INHAL SOLN	6	6.66000000000000014	0	0	6.66000000000000014	1.83000000000000007	4.83000000000000007	0.725225230000000054	N0	MYLAN	0.304599999999999982
221	2018-08-08	46255598	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
222	2018-08-30	46258303	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
223	2018-08-06	46255276	378699093	ALBUTEROL 0.083% INHAL SOLN	3	5.33999999999999986	0	0	5.33999999999999986	0.910000000000000031	4.42999999999999972	0.829588009999999931	N0	MYLAN	0.304599999999999982
224	2018-08-02	46241753	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	150	3.83999999999999986	0	7.41000000000000014	11.25	5.45999999999999996	5.79000000000000004	0.514666669999999993	N0	NEPHRON CORP	0.028500000000000001
225	2018-08-07	46255289	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	45	14.1999999999999993	0	0	14.1999999999999993	1.6399999999999999	12.5600000000000005	0.884507040000000022	N0	NEPHRON CORP	0.028500000000000001
226	2018-08-13	46255479	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	60	17.6000000000000014	0	0	17.6000000000000014	2.18000000000000016	15.4199999999999999	0.876136359999999947	N0	NEPHRON CORP	0.028500000000000001
227	2018-08-27	46257765	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	30	10.8000000000000007	0	0	10.8000000000000007	0.859999999999999987	9.9399999999999995	0.920370369999999993	N0	NEPHRON CORP	0.028500000000000001
228	2018-08-14	46255479	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	90	24.3999999999999986	0	0	24.3999999999999986	3.2799999999999998	21.120000000000001	0.86557377000000002	N0	NEPHRON CORP	0.028500000000000001
229	2018-08-15	46256348	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	45	14.1999999999999993	0	0	14.1999999999999993	1.6399999999999999	12.5600000000000005	0.884507040000000022	N0	NEPHRON CORP	0.028500000000000001
230	2018-08-02	46254768	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	44.7999999999999972	0	0	44.7999999999999972	6.54999999999999982	38.25	0.853794639999999938	N0	NEPHRON CORP	0.028500000000000001
231	2018-08-23	46226149	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	360	36.6199999999999974	0	9.33999999999999986	45.9600000000000009	13.0999999999999996	32.8599999999999994	0.714969540000000015	N0	NEPHRON CORP	0.028500000000000001
232	2018-08-03	46254935	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	270	16.0799999999999983	0	0	16.0799999999999983	9.83000000000000007	6.25	0.388681589999999966	N0	NEPHRON CORP	0.028500000000000001
233	2018-08-20	46233040	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	4.51999999999999957	0	1	5.51999999999999957	6.54999999999999982	-1.03000000000000003	-0.186594200000000016	N0	NEPHRON CORP	0.028500000000000001
234	2018-08-15	46256440	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	45	14.1999999999999993	0	0	14.1999999999999993	1.6399999999999999	12.5600000000000005	0.884507040000000022	N0	NEPHRON CORP	0.028500000000000001
235	2018-08-21	46257095	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	44.7999999999999972	0	0	44.7999999999999972	6.54999999999999982	38.25	0.853794639999999938	N0	NEPHRON CORP	0.028500000000000001
236	2018-08-07	46255335	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	90	3.83000000000000007	0	0.959999999999999964	4.79000000000000004	3.2799999999999998	1.51000000000000001	0.315240079999999978	N0	NEPHRON CORP	0.028500000000000001
237	2018-08-10	46255766	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	270	16.0799999999999983	0	0	16.0799999999999983	9.83000000000000007	6.25	0.388681589999999966	N0	NEPHRON CORP	0.028500000000000001
238	2018-08-07	46251998	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	7.49000000000000021	0	0	7.49000000000000021	6.54999999999999982	0.939999999999999947	0.125500670000000009	N0	NEPHRON CORP	0.028500000000000001
239	2018-08-14	46256240	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	90	1.92999999999999994	0	1	2.93000000000000016	2.56999999999999984	0.359999999999999987	0.122866890000000006	N0	NEPHRON CORP	0.028500000000000001
240	2018-08-09	46255479	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	60	17.6000000000000014	0	0	17.6000000000000014	2.18000000000000016	15.4199999999999999	0.876136359999999947	N0	NEPHRON CORP	0.028500000000000001
241	2018-08-10	46255755	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	15	7.40000000000000036	0	0	7.40000000000000036	0.550000000000000044	6.84999999999999964	0.92567567999999989	N0	NEPHRON CORP	0.028500000000000001
242	2018-08-22	46251998	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	7.49000000000000021	0	0	7.49000000000000021	6.54999999999999982	0.939999999999999947	0.125500670000000009	N0	NEPHRON CORP	0.028500000000000001
243	2018-08-28	46219584	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	300	8.96000000000000085	0	0	8.96000000000000085	8.55000000000000071	0.409999999999999976	0.0457589299999999963	N0	NEPHRON CORP	0.028500000000000001
244	2018-08-27	46257772	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	44.7999999999999972	0	0	44.7999999999999972	5.12999999999999989	39.6700000000000017	0.885491070000000047	N0	NEPHRON CORP	0.028500000000000001
245	2018-08-09	46247645	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	7.25999999999999979	0	0	7.25999999999999979	6.54999999999999982	0.709999999999999964	0.0977961400000000036	N0	NEPHRON CORP	0.028500000000000001
246	2018-08-02	46242160	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	90	24.3999999999999986	0	0	24.3999999999999986	3.2799999999999998	21.120000000000001	0.86557377000000002	N0	NEPHRON CORP	0.028500000000000001
247	2018-08-22	46237757	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	44.7999999999999972	0	0	44.7999999999999972	6.54999999999999982	38.25	0.853794639999999938	N0	NEPHRON CORP	0.028500000000000001
248	2018-08-30	46254377	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	44.7999999999999972	0	0	44.7999999999999972	5.12999999999999989	39.6700000000000017	0.885491070000000047	N0	NEPHRON CORP	0.028500000000000001
249	2018-08-27	46257654	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	75	5.19000000000000039	0	0	5.19000000000000039	2.14000000000000012	3.04999999999999982	0.587668589999999935	N0	NEPHRON CORP	0.028500000000000001
250	2018-08-23	46241362	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	360	9.67999999999999972	0	1	10.6799999999999997	13.0999999999999996	-2.41999999999999993	-0.226591759999999975	N0	NEPHRON CORP	0.028500000000000001
251	2018-08-27	46257695	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	360	15.6300000000000008	0	3.91000000000000014	19.5399999999999991	10.2599999999999998	9.27999999999999936	0.474923229999999974	N0	NEPHRON CORP	0.028500000000000001
252	2018-08-28	46257765	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	45	14.1999999999999993	0	0	14.1999999999999993	1.28000000000000003	12.9199999999999999	0.909859150000000061	N0	NEPHRON CORP	0.028500000000000001
253	2018-08-17	46256714	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	90	24.3999999999999986	0	0	24.3999999999999986	3.2799999999999998	21.120000000000001	0.86557377000000002	N0	NEPHRON CORP	0.028500000000000001
254	2018-08-13	46242160	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	90	24.3999999999999986	0	0	24.3999999999999986	3.2799999999999998	21.120000000000001	0.86557377000000002	N0	NEPHRON CORP	0.028500000000000001
255	2018-08-16	46253316	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	360	10.6799999999999997	0	0	10.6799999999999997	10.2599999999999998	0.419999999999999984	0.0393258400000000008	N0	NEPHRON CORP	0.028500000000000001
256	2018-08-20	46256985	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	15	7.40000000000000036	0	0	7.40000000000000036	0.550000000000000044	6.84999999999999964	0.92567567999999989	N0	NEPHRON CORP	0.028500000000000001
257	2018-08-16	46256576	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	180	44.7999999999999972	0	0	44.7999999999999972	6.54999999999999982	38.25	0.853794639999999938	N0	NEPHRON CORP	0.028500000000000001
258	2018-08-29	46242160	487950125	ALBUTEROL 0.83 MG/ML SOLUTI	90	24.3999999999999986	0	0	24.3999999999999986	2.56999999999999984	21.8299999999999983	0.89467213000000001	N0	NEPHRON CORP	0.028500000000000001
259	2018-08-19	46256816	487950103	ALBUTEROL SOL.083% 3ML AMP	30	10.8000000000000007	0	0	10.8000000000000007	1.09000000000000008	9.71000000000000085	0.899074070000000058	N0	NEPHRON CORP	0.036200000000000003
260	2018-08-22	46254182	487950103	ALBUTEROL SOL.083% 3ML AMP	540	3.5299999999999998	0	0	3.5299999999999998	19.5500000000000007	-16.0199999999999996	-4.53824363000000019	N0	NEPHRON CORP	0.036200000000000003
261	2018-08-19	46256825	487950103	ALBUTEROL SOL.083% 3ML AMP	30	10.8000000000000007	0	0	10.8000000000000007	1.09000000000000008	9.71000000000000085	0.899074070000000058	N0	NEPHRON CORP	0.036200000000000003
262	2018-08-03	46254983	67777012013	ALCOHOL SWABS	100	0	0	2.89999999999999991	2.89999999999999991	0.900000000000000022	2	0.689655170000000095	N0		0.00899999999999999932
263	2018-08-21	46249133	67777012013	ALCOHOL SWABS	100	0	0	2.89999999999999991	2.89999999999999991	0.900000000000000022	2	0.689655170000000095	N0		0.00899999999999999932
264	2018-08-30	46226124	8225233300	ALCOHOL SWABS 200s CURITY	300	3.06999999999999984	0	0	3.06999999999999984	2.60999999999999988	0.46000000000000002	0.149837129999999985	N0	BEIERSDORF-FUTU	0.0086999999999999994
265	2018-08-13	46255198	115167934	ALENDRONATE 35MG	12	5.88999999999999968	0	1.25	7.13999999999999968	4.23000000000000043	2.91000000000000014	0.407563030000000048	N0	IMPAX GENERICS	0.35249999999999998
266	2018-08-14	46256217	115167808	ALENDRONATE SODIUM 10 MG TA	30	0	7.48000000000000043	0	7.48000000000000043	2.62999999999999989	4.84999999999999964	0.648395720000000009	N0	IMPAX GENERICS	0.0875999999999999973
267	2018-08-06	46255149	115167934	ALENDRONATE SODIUM 35 MG TA	4	0	0	5	5	1.40999999999999992	3.58999999999999986	0.717999999999999972	N0	IMPAX GENERICS	0.35249999999999998
268	2018-08-30	46255149	115167934	ALENDRONATE SODIUM 35 MG TA	4	0	0	5	5	1.40999999999999992	3.58999999999999986	0.717999999999999972	N0	IMPAX GENERICS	0.35249999999999998
269	2018-08-08	46216993	115168134	ALENDRONATE SODIUM 70 MG TA	4	4.12000000000000011	0	0	4.12000000000000011	0.979999999999999982	3.14000000000000012	0.762135920000000078	N0	IMPAX GENERICS	0.244999999999999996
270	2018-08-03	46245179	115168134	ALENDRONATE SODIUM 70 MG TA	4	1.70999999999999996	0	3	4.70999999999999996	0.979999999999999982	3.72999999999999998	0.791932060000000049	N0	IMPAX GENERICS	0.244999999999999996
271	2018-08-06	46255122	115168134	ALENDRONATE SODIUM 70 MG TA	12	0	0	10.3399999999999999	10.3399999999999999	2.93999999999999995	7.40000000000000036	0.715667310000000056	N0	IMPAX GENERICS	0.244999999999999996
272	2018-08-13	46218187	115168134	ALENDRONATE SODIUM 70 MG TA	4	0	0	19.0100000000000016	19.0100000000000016	0.979999999999999982	18.0300000000000011	0.948448190000000024	N0	IMPAX GENERICS	0.244999999999999996
273	2018-08-27	46257739	115168134	ALENDRONATE SODIUM 70 MG TA	12	26.1900000000000013	0	3.35000000000000009	29.5399999999999991	2.93999999999999995	26.6000000000000014	0.900473929999999978	N0	IMPAX GENERICS	0.244999999999999996
274	2018-08-28	46238877	115168134	ALENDRONATE SODIUM 70 MG TA	4	1.85000000000000009	0	1.25	3.10000000000000009	0.979999999999999982	2.12000000000000011	0.683870969999999967	N0	IMPAX GENERICS	0.244999999999999996
275	2018-08-15	46256319	115168134	ALENDRONATE SODIUM 70 MG TA	4	3.10000000000000009	0	0	3.10000000000000009	0.979999999999999982	2.12000000000000011	0.683870969999999967	N0	IMPAX GENERICS	0.244999999999999996
276	2018-08-04	46244154	115168134	ALENDRONATE SODIUM 70 MG TA	12	12	0	1.25	13.25	2.93999999999999995	10.3100000000000005	0.778113210000000111	N0	IMPAX GENERICS	0.244999999999999996
277	2018-08-27	46257737	29300015501	ALFUZOSIN HCL ER 10 MG TABL	30	0	0	7.44000000000000039	7.44000000000000039	2.10999999999999988	5.33000000000000007	0.716397850000000003	N0	UNICHEM PHARMAC	0.0704000000000000042
278	2018-08-17	46256741	47335095688	ALFUZOSIN HCL ER 10 MG TABL	5	10.8000000000000007	0	0	10.8000000000000007	0.5	10.3000000000000007	0.953703699999999932	N0	SUN PHARMA GLOB	0.100400000000000003
279	2018-08-20	46225982	47335095688	ALFUZOSIN HCL ER 10 MG TABL	30	0	0	20.870000000000001	20.870000000000001	3.00999999999999979	17.8599999999999994	0.855773839999999897	N0	SUN PHARMA GLOB	0.100400000000000003
280	2018-08-16	46256505	41167412003	ALLEGRA OTC 180MG TABLET	90	0	0	58.1599999999999966	58.1599999999999966	47.0300000000000011	11.1300000000000008	0.191368639999999979	N0	CHATTEM CONS PR	0.522599999999999953
281	2018-08-08	46255430	41167412003	ALLEGRA OTC 180MG TABLET	30	15.7899999999999991	0	0	15.7899999999999991	15.6799999999999997	0.110000000000000001	0.00696643000000000002	N0	CHATTEM CONS PR	0.522599999999999953
282	2018-08-07	46248054	41167432005	ALLEGRA-D 24 HOUR TABLET	30	0	0	53.0700000000000003	53.0700000000000003	33.8699999999999974	19.1999999999999993	0.361786319999999995	N0	CHATTEM CONS PR	1.129
283	2018-08-15	46256351	591554310	ALLOPURINOL 100 MG TABLET	3	4.90000000000000036	0	0	4.90000000000000036	0.209999999999999992	4.69000000000000039	0.95714286000000004	N0	ACTAVIS PHARMA/	0.0686999999999999972
284	2018-08-13	46256076	603211521	ALLOPURINOL 100 MG TABLET	180	24.7800000000000011	0	30	54.7800000000000011	23.9899999999999984	30.7899999999999991	0.562066449999999995	N0	QUALITEST	0.133300000000000002
285	2018-08-27	46257664	591554310	ALLOPURINOL 100 MG TABLET	15	8.5	0	0	8.5	1.03000000000000003	7.46999999999999975	0.878823529999999908	N0	ACTAVIS PHARMA/	0.0686999999999999972
286	2018-08-08	46255483	591554310	ALLOPURINOL 100 MG TABLET	90	0.930000000000000049	0	18	18.9299999999999997	6.17999999999999972	12.75	0.673534070000000096	N0	ACTAVIS PHARMA/	0.0686999999999999972
287	2018-08-17	46256742	591554310	ALLOPURINOL 100 MG TABLET	3	4.90000000000000036	0	0	4.90000000000000036	0.209999999999999992	4.69000000000000039	0.95714286000000004	N0	ACTAVIS PHARMA/	0.0686999999999999972
288	2018-08-24	46257490	591554310	ALLOPURINOL 100 MG TABLET	15	0	0	3.99000000000000021	3.99000000000000021	1.03000000000000003	2.95999999999999996	0.74185464000000001	N0	ACTAVIS PHARMA/	0.0686999999999999972
289	2018-08-29	46258120	591554310	ALLOPURINOL 100 MG TABLET	3	4.90000000000000036	0	0	4.90000000000000036	0.209999999999999992	4.69000000000000039	0.95714286000000004	N0	ACTAVIS PHARMA/	0.0686999999999999972
290	2018-08-06	46242063	591554310	ALLOPURINOL 100 MG TABLET	30	5.08000000000000007	0	1.25	6.33000000000000007	2.06000000000000005	4.26999999999999957	0.674565560000000009	N0	ACTAVIS PHARMA/	0.0686999999999999972
291	2018-08-09	46245164	591554405	ALLOPURINOL 300 MG TABLET	30	13.4399999999999995	0	1.25	14.6899999999999995	4.29000000000000004	10.4000000000000004	0.707964599999999944	N0	ACTAVIS PHARMA/	0.143100000000000005
292	2018-08-19	46256820	603211621	ALLOPURINOL 300 MG TABLET	3	6.34999999999999964	0	0	6.34999999999999964	0.780000000000000027	5.57000000000000028	0.877165349999999955	N0	QUALITEST/PAR P	0.259799999999999975
293	2018-08-13	46252050	591554401	ALLOPURINOL 300 MG TABLET	30	2.2200000000000002	0	4.67999999999999972	6.90000000000000036	5.25	1.64999999999999991	0.239130429999999977	N0	ACTAVIS PHARMA/	0.147499999999999992
294	2018-08-22	46257139	30768017965	ALPHA LIPOIC ACID 600 MG CA	180	0	0	48.9399999999999977	48.9399999999999977	27.2899999999999991	21.6499999999999986	0.442378419999999994	N0	SUNDOWN	0.151600000000000013
295	2018-08-20	46252211	23932115	ALPHAGAN P 0.1% DROPS	15	278.480000000000018	0	149.949999999999989	428.430000000000007	395.910000000000025	32.5200000000000031	0.0759050500000000017	N0	ALLERGAN INC.	26.3939999999999984
296	2018-08-16	44030850	67253090050	ALPRAZOLAM 0.25 MG TABLET	50	0	0	5.09999999999999964	5.09999999999999964	0.489999999999999991	4.61000000000000032	0.903921569999999952	N0	DAVA PHARMACEUT	0.00979999999999999968
297	2018-08-07	44030562	67253090050	ALPRAZOLAM 0.25 MG TABLET	30	1.22999999999999998	0	0	1.22999999999999998	0.28999999999999998	0.939999999999999947	0.764227639999999986	N0	DAVA PHARMACEUT	0.00979999999999999968
298	2018-08-11	44031676	67253090011	ALPRAZOLAM 0.25 MG TABLET	30	4.51999999999999957	0	0.790000000000000036	5.30999999999999961	0.380000000000000004	4.92999999999999972	0.92843691000000006	N0	DAVA PHARMACEUT	0.0126000000000000001
299	2018-08-09	44032068	67253090011	ALPRAZOLAM 0.25 MG TABLET	30	7.95000000000000018	0	0	7.95000000000000018	0.380000000000000004	7.57000000000000028	0.952201259999999938	N0	DAVA PHARMACEUT	0.0126000000000000001
300	2018-08-01	44031526	67253090011	ALPRAZOLAM 0.25 MG TABLET	30	0	0	10	10	0.380000000000000004	9.61999999999999922	0.962000000000000077	N0	DAVA PHARMACEUT	0.0126000000000000001
301	2018-08-28	44032103	67253090011	ALPRAZOLAM 0.25 MG TABLET	30	0	0	1	1	0.380000000000000004	0.619999999999999996	0.619999999999999996	N0	DAVA PHARMACEUT	0.0126000000000000001
302	2018-08-04	44031446	67253090050	ALPRAZOLAM 0.25 MG TABLET	120	0	0	6.05999999999999961	6.05999999999999961	1.17999999999999994	4.87999999999999989	0.805280529999999994	N0	DAVA PHARMACEUT	0.00979999999999999968
303	2018-08-09	44031545	67253090050	ALPRAZOLAM 0.25 MG TABLET	30	0	0	0.569999999999999951	0.569999999999999951	0.28999999999999998	0.280000000000000027	0.491228070000000017	N0	DAVA PHARMACEUT	0.00979999999999999968
304	2018-08-02	44032198	67253090011	ALPRAZOLAM 0.25 MG TABLET	15	0.510000000000000009	0	0.0899999999999999967	0.599999999999999978	0.190000000000000002	0.409999999999999976	0.683333329999999961	N0	DAVA PHARMACEUT	0.0126000000000000001
305	2018-08-15	44032383	67253090011	ALPRAZOLAM 0.25 MG TABLET	10	0	0	6	6	0.130000000000000004	5.87000000000000011	0.978333330000000001	N	DAVA PHARMACEUT	0.0126000000000000001
306	2018-08-09	44032300	67253090050	ALPRAZOLAM 0.25 MG TABLET	30	10.1799999999999997	0	0	10.1799999999999997	0.28999999999999998	9.89000000000000057	0.971512769999999914	N0	DAVA PHARMACEUT	0.00979999999999999968
307	2018-08-24	44032494	67253090011	ALPRAZOLAM 0.25 MG TABLET	90	0	0	5.16000000000000014	5.16000000000000014	1.12999999999999989	4.03000000000000025	0.781007750000000001	N0	DAVA PHARMACEUT	0.0126000000000000001
308	2018-08-30	44032310	67253090011	ALPRAZOLAM 0.25 MG TABLET	30	7.95000000000000018	0	0	7.95000000000000018	0.380000000000000004	7.57000000000000028	0.952201259999999938	N0	DAVA PHARMACEUT	0.0126000000000000001
309	2018-08-09	44032078	67253090011	ALPRAZOLAM 0.25 MG TABLET	30	10.1799999999999997	0	0	10.1799999999999997	0.380000000000000004	9.80000000000000071	0.962671909999999964	N0	DAVA PHARMACEUT	0.0126000000000000001
310	2018-08-27	44032078	67253090011	ALPRAZOLAM 0.25 MG TABLET	30	10.1799999999999997	0	0	10.1799999999999997	0.380000000000000004	9.80000000000000071	0.962671909999999964	N0	DAVA PHARMACEUT	0.0126000000000000001
311	2018-08-08	44031352	781106105	ALPRAZOLAM 0.25MG TABLET	90	13.8900000000000006	0	1.25	15.1400000000000006	1.6100000000000001	13.5299999999999994	0.893659179999999886	N0	SANDOZ	0.0178999999999999992
312	2018-08-17	44032404	67253090111	ALPRAZOLAM 0.5 MG TABLET	45	17.4100000000000001	0	0	17.4100000000000001	0.369999999999999996	17.0399999999999991	0.978747849999999975	N0	DAVA PHARMACEUT	0.00830000000000000009
313	2018-08-20	44032004	67253090150	ALPRAZOLAM 0.5 MG TABLET	120	16.75	0	1.25	18	1.18999999999999995	16.8099999999999987	0.933888890000000083	N0	DAVA PHARMACEUT	0.00990000000000000081
314	2018-08-24	44031320	67253090111	ALPRAZOLAM 0.5 MG TABLET	30	0	0	5	5	0.25	4.75	0.949999999999999956	N0	DAVA PHARMACEUT	0.00830000000000000009
315	2018-08-14	44032358	67253090111	ALPRAZOLAM 0.5 MG TABLET	30	12.9399999999999995	0	0	12.9399999999999995	0.25	12.6899999999999995	0.980680059999999965	N0	DAVA PHARMACEUT	0.00830000000000000009
316	2018-08-18	44031624	67253090111	ALPRAZOLAM 0.5 MG TABLET	90	0	0	12.8300000000000001	12.8300000000000001	0.75	12.0800000000000001	0.941543259999999993	N0	DAVA PHARMACEUT	0.00830000000000000009
317	2018-08-02	44031891	67253090111	ALPRAZOLAM 0.5 MG TABLET	30	0	0	1.18999999999999995	1.18999999999999995	0.25	0.939999999999999947	0.789915970000000023	N0	DAVA PHARMACEUT	0.00830000000000000009
318	2018-08-23	44032485	67253090111	ALPRAZOLAM 0.5 MG TABLET	90	0	0	9.11999999999999922	9.11999999999999922	0.75	8.36999999999999922	0.917763159999999911	N0	DAVA PHARMACEUT	0.00830000000000000009
319	2018-08-27	44032515	67253090111	ALPRAZOLAM 0.5 MG TABLET	60	0.760000000000000009	0	2	2.75999999999999979	0.5	2.25999999999999979	0.818840579999999929	N0	DAVA PHARMACEUT	0.00830000000000000009
320	2018-08-08	44032284	67253090211	ALPRAZOLAM 1 MG TABLET	60	3.89000000000000012	0	1.25	5.13999999999999968	0.930000000000000049	4.20999999999999996	0.819066150000000048	N0	DAVA PHARMACEUT	0.0154999999999999999
321	2018-08-20	44032416	67253090250	ALPRAZOLAM 1 MG TABLET	2	0	0	0.23000000000000001	0.23000000000000001	0.0299999999999999989	0.200000000000000011	0.869565220000000028	N0	DAVA PHARMACEUT	0.0165000000000000008
322	2018-08-27	44032516	67253090211	ALPRAZOLAM 1 MG TABLET	2	0	0	0.23000000000000001	0.23000000000000001	0.0299999999999999989	0.200000000000000011	0.869565220000000028	N0	DAVA PHARMACEUT	0.0154999999999999999
323	2018-08-15	44032371	67253090211	ALPRAZOLAM 1 MG TABLET	30	0	0	1.8600000000000001	1.8600000000000001	0.469999999999999973	1.3899999999999999	0.74731183000000001	N0	DAVA PHARMACEUT	0.0154999999999999999
324	2018-08-08	44032289	67253090250	ALPRAZOLAM 1 MG TABLET	60	3.89000000000000012	0	1.25	5.13999999999999968	0.989999999999999991	4.15000000000000036	0.807393000000000027	N0	DAVA PHARMACEUT	0.0165000000000000008
325	2018-08-24	44031807	67253090250	ALPRAZOLAM 1 MG TABLET	90	0	0	12.7899999999999991	12.7899999999999991	1.48999999999999999	11.3000000000000007	0.883502739999999953	N0	DAVA PHARMACEUT	0.0165000000000000008
326	2018-08-03	44032214	67253090211	ALPRAZOLAM 1 MG TABLET	90	14.8599999999999994	0	1.25	16.1099999999999994	1.39999999999999991	14.7100000000000009	0.913097450000000088	N0	DAVA PHARMACEUT	0.0154999999999999999
327	2018-08-20	44032426	67253090250	ALPRAZOLAM 1 MG TABLET	90	6.42999999999999972	0	1.25	7.67999999999999972	1.48999999999999999	6.19000000000000039	0.805989579999999983	N0	DAVA PHARMACEUT	0.0165000000000000008
328	2018-08-29	44032568	59762372101	ALPRAZOLAM 1MG TABLET	45	30.1000000000000014	0	0	30.1000000000000014	1.29000000000000004	28.8099999999999987	0.95714286000000004	N0	GREENSTONE LLC.	0.0286999999999999998
329	2018-08-20	44032059	69452011330	ALPRAZOLAM 2 MG TABLET	120	47.9600000000000009	0	0	47.9600000000000009	4.62999999999999989	43.3299999999999983	0.903461219999999954	N0	BIONPHARMA INC.	0.0386000000000000024
330	2018-08-06	44032253	69452011330	ALPRAZOLAM 2 MG TABLET	60	3.12999999999999989	0	1	4.12999999999999989	2.31999999999999984	1.81000000000000005	0.438256659999999965	N0	BIONPHARMA INC.	0.0386000000000000024
331	2018-08-06	44032252	69452011330	ALPRAZOLAM 2 MG TABLET	60	7.55999999999999961	0	0	7.55999999999999961	2.31999999999999984	5.24000000000000021	0.693121690000000013	N0	BIONPHARMA INC.	0.0386000000000000024
332	2018-08-27	46256396	69452014230	AMANTADINE 100 MG CAPSULE	60	86.3700000000000045	0	1.25	87.6200000000000045	43.5300000000000011	44.0900000000000034	0.503195619999999955	N0	BIONPHARMA INC.	0.725500000000000034
333	2018-08-02	46250553	69452014220	AMANTADINE 100 MG CAPSULE	60	54.7800000000000011	0	15	69.7800000000000011	43.7800000000000011	26	0.372599599999999975	N0	BIONPHARMA INC.	0.729600000000000026
334	2018-08-23	46255219	51862024030	AMIODARONE HCL 100 MG TABLE	15	98.730000000000004	0	0	98.730000000000004	29.25	69.480000000000004	0.703737469999999976	N0	MAYNE PHARMA IN	1.95029999999999992
335	2018-08-15	46256350	51672405506	AMIODARONE HCL 100 MG TABLE	3	22.9499999999999993	0	0	22.9499999999999993	7.21999999999999975	15.7300000000000004	0.685403050000000014	N0	TARO PHARM USA	2.40799999999999992
336	2018-08-12	46255631	51672405506	AMIODARONE HCL 100 MG TABLE	3	22.9499999999999993	0	0	22.9499999999999993	7.21999999999999975	15.7300000000000004	0.685403050000000014	N0	TARO PHARM USA	2.40799999999999992
337	2018-08-29	46258089	51672405506	AMIODARONE HCL 100 MG TABLE	15	98.730000000000004	0	0	98.730000000000004	36.1199999999999974	62.6099999999999994	0.634153749999999961	N0	TARO PHARM USA	2.40799999999999992
338	2018-08-09	46255631	51672405506	AMIODARONE HCL 100 MG TABLE	3	22.9499999999999993	0	0	22.9499999999999993	7.21999999999999975	15.7300000000000004	0.685403050000000014	N0	TARO PHARM USA	2.40799999999999992
339	2018-08-06	46249728	51862024030	AMIODARONE HCL 100 MG TABLE	15	98.730000000000004	0	0	98.730000000000004	29.25	69.480000000000004	0.703737469999999976	N0	MAYNE PHARMA IN	1.95029999999999992
340	2018-08-06	46255219	51862024030	AMIODARONE HCL 100 MG TABLE	15	98.730000000000004	0	0	98.730000000000004	29.25	69.480000000000004	0.703737469999999976	N0	MAYNE PHARMA IN	1.95029999999999992
341	2018-08-27	46255219	51862024030	AMIODARONE HCL 100 MG TABLE	15	98.730000000000004	0	0	98.730000000000004	29.25	69.480000000000004	0.703737469999999976	N0	MAYNE PHARMA IN	1.95029999999999992
342	2018-08-21	46249728	51862024030	AMIODARONE HCL 100 MG TABLE	15	98.730000000000004	0	0	98.730000000000004	29.25	69.480000000000004	0.703737469999999976	N0	MAYNE PHARMA IN	1.95029999999999992
343	2018-08-06	46255191	68382022714	AMIODARONE HCL 200 MG TABLE	3	7.07000000000000028	0	0	7.07000000000000028	0.330000000000000016	6.74000000000000021	0.953323900000000002	N0	ZYDUS PHARMACEU	0.0686999999999999972
344	2018-08-07	46254052	68382022714	AMIODARONE HCL 200 MG TABLE	6	10.1300000000000008	0	0	10.1300000000000008	0.67000000000000004	9.46000000000000085	0.933859820000000007	N0	ZYDUS PHARMACEU	0.0686999999999999972
345	2018-08-28	46257928	68382022714	AMIODARONE HCL 200 MG TABLE	3	5.58000000000000007	0	0	5.58000000000000007	0.209999999999999992	5.37000000000000011	0.962365589999999993	N0	ZYDUS PHARMACEU	0.0686999999999999972
346	2018-08-29	46217978	68382022714	AMIODARONE HCL 200 MG TABLE	30	0	0	4.88999999999999968	4.88999999999999968	2.06000000000000005	2.83000000000000007	0.578732110000000022	N0	ZYDUS PHARMACEU	0.0686999999999999972
347	2018-08-20	46238475	68382022714	AMIODARONE HCL 200 MG TABLE	12	0	0	3.08000000000000007	3.08000000000000007	1.34000000000000008	1.73999999999999999	0.564935059999999933	N0	ZYDUS PHARMACEU	0.0686999999999999972
348	2018-08-14	46256204	68382022714	AMIODARONE HCL 200 MG TABLE	15	19.3299999999999983	0	0	19.3299999999999983	1.66999999999999993	17.6600000000000001	0.913605789999999973	N0	ZYDUS PHARMACEU	0.0686999999999999972
349	2018-08-17	46256680	68382022714	AMIODARONE HCL 200 MG TABLE	60	6.45000000000000018	0	0	6.45000000000000018	4.12000000000000011	2.33000000000000007	0.361240310000000009	N0	ZYDUS PHARMACEU	0.0686999999999999972
350	2018-08-08	46255191	68382022714	AMIODARONE HCL 200 MG TABLE	3	7.07000000000000028	0	0	7.07000000000000028	0.330000000000000016	6.74000000000000021	0.953323900000000002	N0	ZYDUS PHARMACEU	0.0686999999999999972
351	2018-08-31	46258282	68382022714	AMIODARONE HCL 200 MG TABLE	5	9.10999999999999943	0	0	9.10999999999999943	0.340000000000000024	8.76999999999999957	0.962678379999999945	N0	ZYDUS PHARMACEU	0.0686999999999999972
352	2018-08-04	46254052	68382022714	AMIODARONE HCL 200 MG TABLE	6	10.1300000000000008	0	0	10.1300000000000008	0.67000000000000004	9.46000000000000085	0.933859820000000007	N0	ZYDUS PHARMACEU	0.0686999999999999972
353	2018-08-02	46254793	68382022714	AMIODARONE HCL 200 MG TABLE	90	0	0	12.5600000000000005	12.5600000000000005	10.0399999999999991	2.52000000000000002	0.200636940000000014	N0	ZYDUS PHARMACEU	0.0686999999999999972
354	2018-08-31	46236401	68382022714	AMIODARONE HCL 200 MG TABLE	90	0	0	23.5300000000000011	23.5300000000000011	6.17999999999999972	17.3500000000000014	0.737356569999999989	N0	ZYDUS PHARMACEU	0.0686999999999999972
355	2018-08-08	46244458	68382022714	AMIODARONE HCL 200 MG TABLE	30	7.16000000000000014	0	0	7.16000000000000014	3.35000000000000009	3.81000000000000005	0.532122910000000005	N0	ZYDUS PHARMACEU	0.0686999999999999972
356	2018-08-01	46254052	68382022714	AMIODARONE HCL 200 MG TABLE	6	10.1300000000000008	0	0	10.1300000000000008	0.67000000000000004	9.46000000000000085	0.933859820000000007	N0	ZYDUS PHARMACEU	0.0686999999999999972
755	2018-08-13	48012255	38779038805	BACLOFEN GT 5MG/ML SUSP	45	0	0	0	0	0.800000000000000044	-0.800000000000000044	0	N0		0
357	2018-08-10	46232940	68382022714	AMIODARONE HCL 200 MG TABLE	45	0	0	11.7799999999999994	11.7799999999999994	5.01999999999999957	6.75999999999999979	0.573853990000000036	N0	ZYDUS PHARMACEU	0.0686999999999999972
358	2018-08-14	48012130	51927180000	AMIODARONE(F) 5MG/ML SUSP	300	36.7700000000000031	0	0	36.7700000000000031	9.51999999999999957	27.25	0.741093280000000076	N0		0
359	2018-08-29	46241024	64764024060	AMITIZA 24 MCG CAPSULES	60	392.829999999999984	0	3.70000000000000018	396.529999999999973	326.970000000000027	69.5600000000000023	0.175421779999999999	N0	TAKEDA PHARMACE	5.44949999999999957
360	2018-08-11	46255852	64764024060	AMITIZA 24 MCG CAPSULES	6	41.8500000000000014	0	0	41.8500000000000014	32.7000000000000028	9.15000000000000036	0.218637990000000004	N0	TAKEDA PHARMACE	5.44949999999999957
361	2018-08-14	46256235	64764008060	AMITIZA 8 MCG CAPSULE	5	35.5399999999999991	0	0	35.5399999999999991	27.25	8.28999999999999915	0.233258300000000002	N0	TAKEDA PHARMACE	5.44949999999999957
362	2018-08-11	46248198	603221432	AMITRIPTYLINE 50MG	30	0	0	12	12	8.32000000000000028	3.68000000000000016	0.30666667000000003	N0	QUALITEST/PAR P	0.277200000000000002
363	2018-08-05	46255085	603221432	AMITRIPTYLINE 50MG	3	6.69000000000000039	0	0	6.69000000000000039	0.82999999999999996	5.86000000000000032	0.875934230000000036	N0	QUALITEST/PAR P	0.277200000000000002
364	2018-08-10	46245828	603221432	AMITRIPTYLINE 50MG	30	12.4800000000000004	0	3.29999999999999982	15.7799999999999994	8.32000000000000028	7.45999999999999996	0.472750320000000057	N0	QUALITEST/PAR P	0.277200000000000002
365	2018-08-09	46250190	603221432	AMITRIPTYLINE 50MG	30	12.1099999999999994	0	0	12.1099999999999994	8.32000000000000028	3.79000000000000004	0.312964489999999984	N0	QUALITEST/PAR P	0.277200000000000002
366	2018-08-31	46256210	16729017101	AMITRIPTYLINE HCL 10 MG TAB	30	0	0	4.24000000000000021	4.24000000000000021	1.32000000000000006	2.91999999999999993	0.688679249999999965	N0	ACCORD HEALTHCA	0.0441000000000000003
367	2018-08-21	46257013	16729017101	AMITRIPTYLINE HCL 10 MG TAB	30	1.73999999999999999	0	1	2.74000000000000021	1.32000000000000006	1.41999999999999993	0.518248180000000058	N0	ACCORD HEALTHCA	0.0441000000000000003
368	2018-08-14	46256210	16729017101	AMITRIPTYLINE HCL 10 MG TAB	30	0	0	4.19000000000000039	4.19000000000000039	1.32000000000000006	2.87000000000000011	0.684964200000000023	N0	ACCORD HEALTHCA	0.0441000000000000003
369	2018-08-08	46255464	16729017101	AMITRIPTYLINE HCL 10 MG TAB	30	5.87999999999999989	0	0	5.87999999999999989	1.32000000000000006	4.55999999999999961	0.775510199999999927	N0	ACCORD HEALTHCA	0.0441000000000000003
370	2018-08-29	46253512	16729017101	AMITRIPTYLINE HCL 10 MG TAB	30	7.37999999999999989	0	0	7.37999999999999989	1.32000000000000006	6.05999999999999961	0.82113820999999998	N0	ACCORD HEALTHCA	0.0441000000000000003
371	2018-08-01	46251392	603221621	AMITRIPTYLINE HCL 100MG T	30	52.2899999999999991	0	1.25	53.5399999999999991	19.6499999999999986	33.8900000000000006	0.632984679999999966	N0	QUALITEST/PAR P	0.655100000000000016
372	2018-08-09	46250189	378268501	AMITRIPTYLINE HCL 100MG T	30	22.5799999999999983	0	0	22.5799999999999983	20.5399999999999991	2.04000000000000004	0.0903454399999999991	N0	MYLAN	0.684499999999999997
373	2018-08-22	46257137	603221621	AMITRIPTYLINE HCL 100MG T	30	22.5799999999999983	0	0	22.5799999999999983	19.6499999999999986	2.93000000000000016	0.129760849999999983	N0	QUALITEST/PAR P	0.655100000000000016
374	2018-08-29	46254941	603221621	AMITRIPTYLINE HCL 100MG T	15	36.3800000000000026	0	0	36.3800000000000026	9.83000000000000007	26.5500000000000007	0.729796589999999967	N0	QUALITEST/PAR P	0.655100000000000016
375	2018-08-03	46254941	603221621	AMITRIPTYLINE HCL 100MG T	15	36.3800000000000026	0	0	36.3800000000000026	9.83000000000000007	26.5500000000000007	0.729796589999999967	N0	QUALITEST/PAR P	0.655100000000000016
376	2018-08-15	46242793	603221221	AMITRIPTYLINE HCL 10MG TAB	30	3.20999999999999996	0	1	4.20999999999999996	2.60999999999999988	1.60000000000000009	0.380047509999999977	N0	QUALITEST/PAR P	0.0870999999999999969
377	2018-08-23	46257321	603221221	AMITRIPTYLINE HCL 10MG TAB	30	3.20999999999999996	0	1	4.20999999999999996	2.60999999999999988	1.60000000000000009	0.380047509999999977	N0	QUALITEST/PAR P	0.0870999999999999969
378	2018-08-22	46234773	603221221	AMITRIPTYLINE HCL 10MG TAB	60	2.08999999999999986	0	3.04000000000000004	5.12999999999999989	5.23000000000000043	-0.100000000000000006	-0.0194931800000000023	N0	QUALITEST/PAR P	0.0870999999999999969
379	2018-08-08	46251938	603221321	AMITRIPTYLINE HCL 25 MG TAB	60	26.9100000000000001	0	0	26.9100000000000001	10.25	16.6600000000000001	0.619100709999999999	N0	QUALITEST/PAR P	0.139899999999999997
380	2018-08-06	46254724	603221321	AMITRIPTYLINE HCL 25 MG TAB	3	5.62000000000000011	0	0	5.62000000000000011	0.510000000000000009	5.11000000000000032	0.909252670000000096	N0	QUALITEST/PAR P	0.139899999999999997
381	2018-08-02	46252434	603221321	AMITRIPTYLINE HCL 25 MG TAB	15	12.1099999999999994	0	0	12.1099999999999994	2.56000000000000005	9.55000000000000071	0.788604459999999952	N0	QUALITEST/PAR P	0.139899999999999997
382	2018-08-15	46244277	603221321	AMITRIPTYLINE HCL 25 MG TAB	30	0	0	8.02999999999999936	8.02999999999999936	5.12999999999999989	2.89999999999999991	0.361145700000000014	N0	QUALITEST/PAR P	0.139899999999999997
383	2018-08-28	46252434	603221321	AMITRIPTYLINE HCL 25 MG TAB	15	12.1099999999999994	0	0	12.1099999999999994	2.56000000000000005	9.55000000000000071	0.788604459999999952	N0	QUALITEST/PAR P	0.139899999999999997
384	2018-08-01	46251430	603221321	AMITRIPTYLINE HCL 25 MG TAB	60	0	0	11.6099999999999994	11.6099999999999994	10.25	1.3600000000000001	0.117140400000000006	N0	QUALITEST/PAR P	0.139899999999999997
385	2018-08-04	46254724	603221321	AMITRIPTYLINE HCL 25 MG TAB	3	5.62000000000000011	0	0	5.62000000000000011	0.510000000000000009	5.11000000000000032	0.909252670000000096	N0	QUALITEST/PAR P	0.139899999999999997
386	2018-08-01	46254724	603221321	AMITRIPTYLINE HCL 25 MG TAB	3	5.62000000000000011	0	0	5.62000000000000011	0.510000000000000009	5.11000000000000032	0.909252670000000096	N0	QUALITEST/PAR P	0.139899999999999997
387	2018-08-15	46252434	603221321	AMITRIPTYLINE HCL 25 MG TAB	15	12.1099999999999994	0	0	12.1099999999999994	2.56000000000000005	9.55000000000000071	0.788604459999999952	N0	QUALITEST/PAR P	0.139899999999999997
388	2018-08-31	46258350	603221321	AMITRIPTYLINE HCL 25 MG TAB	30	3.83999999999999986	0	1	4.83999999999999986	5.12999999999999989	-0.28999999999999998	-0.0599173600000000028	N0	QUALITEST/PAR P	0.139899999999999997
389	2018-08-07	46240037	245002340	AMLACTIN LOT 12%	400	0	0	17.7899999999999991	17.7899999999999991	10.5600000000000005	7.23000000000000043	0.40640809	N0	UPSHER SMITH	0.0263999999999999999
390	2018-08-10	46224681	65862058701	AMLODIP-BENAZEPR 10-40 MG	30	0	0	25	25	4.30999999999999961	20.6900000000000013	0.827600000000000002	N0	AUROBINDO PHARM	0.143499999999999989
391	2018-08-13	46240258	603211002	AMLODIPINE BESYLATE 10 MG T	90	22.5	0	3.35000000000000009	25.8500000000000014	1.94999999999999996	23.8999999999999986	0.924564799999999964	N0	QUALITEST/PAR P	0.0217000000000000005
392	2018-08-29	46256807	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
393	2018-08-19	46256807	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
394	2018-08-08	46254904	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
395	2018-08-14	46254904	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
396	2018-08-23	46256807	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
397	2018-08-01	46254614	67877019910	AMLODIPINE BESYLATE 10 MG T	90	5.84999999999999964	0	4.58999999999999986	10.4399999999999995	2.12000000000000011	8.32000000000000028	0.796934870000000073	N0	ASCEND LABORATO	0.0235999999999999995
398	2018-08-16	46254904	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
399	2018-08-27	46256807	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
1110	2018-08-13	48012402	409613922	BORIC4%ETHANOL20%ST H2O	60	0	0	18.1999999999999993	18.1999999999999993	2.49000000000000021	15.7100000000000009	0.863186809999999971	N0		0
400	2018-08-03	46243461	67877019910	AMLODIPINE BESYLATE 10 MG T	30	4.28000000000000025	0	1	5.28000000000000025	0.709999999999999964	4.57000000000000028	0.865530300000000086	N0	ASCEND LABORATO	0.0235999999999999995
401	2018-08-25	46245874	603211002	AMLODIPINE BESYLATE 10 MG T	30	1.71999999999999997	0	1.25	2.9700000000000002	0.650000000000000022	2.31999999999999984	0.781144780000000094	N0	QUALITEST/PAR P	0.0217000000000000005
402	2018-08-17	46256731	67877019910	AMLODIPINE BESYLATE 10 MG T	5	11.2699999999999996	0	0	11.2699999999999996	0.119999999999999996	11.1500000000000004	0.989352259999999983	N0	ASCEND LABORATO	0.0235999999999999995
403	2018-08-15	46256366	67877019910	AMLODIPINE BESYLATE 10 MG T	30	0.550000000000000044	0	10	10.5500000000000007	0.709999999999999964	9.83999999999999986	0.932701420000000114	N0	ASCEND LABORATO	0.0235999999999999995
404	2018-08-28	46257892	67877019910	AMLODIPINE BESYLATE 10 MG T	86	0	0	10.0199999999999996	10.0199999999999996	2.0299999999999998	7.99000000000000021	0.797405190000000097	N0	ASCEND LABORATO	0.0235999999999999995
405	2018-08-29	46243461	67877019910	AMLODIPINE BESYLATE 10 MG T	30	4.28000000000000025	0	1	5.28000000000000025	0.709999999999999964	4.57000000000000028	0.865530300000000086	N0	ASCEND LABORATO	0.0235999999999999995
406	2018-08-05	46254904	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
407	2018-08-21	46251783	603211002	AMLODIPINE BESYLATE 10 MG T	30	1.32000000000000006	0	0.440000000000000002	1.76000000000000001	0.650000000000000022	1.1100000000000001	0.630681819999999949	N0	QUALITEST/PAR P	0.0217000000000000005
408	2018-08-09	46236703	603211002	AMLODIPINE BESYLATE 10 MG T	30	4.78000000000000025	0	0	4.78000000000000025	0.650000000000000022	4.12999999999999989	0.86401673999999995	N0	QUALITEST/PAR P	0.0217000000000000005
409	2018-08-29	46248712	603211002	AMLODIPINE BESYLATE 10 MG T	30	4.78000000000000025	0	0	4.78000000000000025	0.650000000000000022	4.12999999999999989	0.86401673999999995	N0	QUALITEST/PAR P	0.0217000000000000005
410	2018-08-03	46254904	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
411	2018-08-11	46254904	67877019910	AMLODIPINE BESYLATE 10 MG T	3	8.35999999999999943	0	0	8.35999999999999943	0.0700000000000000067	8.28999999999999915	0.99162678999999998	N0	ASCEND LABORATO	0.0235999999999999995
412	2018-08-06	46247347	603211002	AMLODIPINE BESYLATE 10 MG T	30	0	0	3.5	3.5	0.650000000000000022	2.85000000000000009	0.814285710000000051	N0	QUALITEST/PAR P	0.0217000000000000005
413	2018-08-23	46249626	67877019710	AMLODIPINE BESYLATE 2.5 MG	30	4.53000000000000025	0	0	4.53000000000000025	52.3200000000000003	-47.7899999999999991	-10.5496688699999996	N0	ASCEND LABORATO	0.00910000000000000045
414	2018-08-20	46249055	69097012615	AMLODIPINE BESYLATE 2.5 MG	30	0	0	2.12999999999999989	2.12999999999999989	0.429999999999999993	1.69999999999999996	0.798122070000000017	N0	CIPLA USA, INC.	0.0143999999999999996
415	2018-08-22	46252326	69097012615	AMLODIPINE BESYLATE 2.5 MG	15	14.9499999999999993	0	0	14.9499999999999993	0.220000000000000001	14.7300000000000004	0.985284280000000012	N0	CIPLA USA, INC.	0.0143999999999999996
416	2018-08-06	46239718	69097012615	AMLODIPINE BESYLATE 2.5 MG	30	0	0	1.72999999999999998	1.72999999999999998	0.429999999999999993	1.30000000000000004	0.751445090000000038	N0	CIPLA USA, INC.	0.0143999999999999996
417	2018-08-09	46247788	69097012615	AMLODIPINE BESYLATE 2.5 MG	15	14.9499999999999993	0	0	14.9499999999999993	0.220000000000000001	14.7300000000000004	0.985284280000000012	N0	CIPLA USA, INC.	0.0143999999999999996
418	2018-08-27	46237383	603210825	AMLODIPINE BESYLATE 2.5 MG	30	2.41000000000000014	0	1.25	3.66000000000000014	0.540000000000000036	3.12000000000000011	0.852459019999999956	N0	QUALITEST/PAR P	0.0178999999999999992
419	2018-08-06	46234638	69097012615	AMLODIPINE BESYLATE 2.5 MG	90	0	0	10.5500000000000007	10.5500000000000007	1.30000000000000004	9.25	0.876777249999999952	N0	CIPLA USA, INC.	0.0143999999999999996
420	2018-08-14	46256253	69097012615	AMLODIPINE BESYLATE 2.5 MG	30	25.8999999999999986	0	0	25.8999999999999986	0.429999999999999993	25.4699999999999989	0.983397680000000052	N0	CIPLA USA, INC.	0.0143999999999999996
421	2018-08-15	46256374	69097012615	AMLODIPINE BESYLATE 2.5 MG	30	4.01999999999999957	0	1.25	5.26999999999999957	0.429999999999999993	4.83999999999999986	0.918406070000000074	N0	CIPLA USA, INC.	0.0143999999999999996
422	2018-08-15	46242692	69097012615	AMLODIPINE BESYLATE 2.5 MG	30	0	0	3.5	3.5	0.429999999999999993	3.06999999999999984	0.877142859999999969	N0	CIPLA USA, INC.	0.0143999999999999996
423	2018-08-06	46252326	69097012615	AMLODIPINE BESYLATE 2.5 MG	15	14.9499999999999993	0	0	14.9499999999999993	0.220000000000000001	14.7300000000000004	0.985284280000000012	N0	CIPLA USA, INC.	0.0143999999999999996
424	2018-08-02	46251208	69097012615	AMLODIPINE BESYLATE 2.5 MG	30	0	0	2.20999999999999996	2.20999999999999996	0.429999999999999993	1.78000000000000003	0.805429859999999942	N0	CIPLA USA, INC.	0.0143999999999999996
425	2018-08-09	46252231	69097012615	AMLODIPINE BESYLATE 2.5 MG	30	0	0	2.20999999999999996	2.20999999999999996	0.429999999999999993	1.78000000000000003	0.805429859999999942	N0	CIPLA USA, INC.	0.0143999999999999996
426	2018-08-03	46247788	69097012615	AMLODIPINE BESYLATE 2.5 MG	15	14.9499999999999993	0	0	14.9499999999999993	0.220000000000000001	14.7300000000000004	0.985284280000000012	N0	CIPLA USA, INC.	0.0143999999999999996
427	2018-08-22	46216359	69097012615	AMLODIPINE BESYLATE 2.5 MG	90	0	0	27.8399999999999999	27.8399999999999999	1.30000000000000004	26.5399999999999991	0.953304600000000057	N0	CIPLA USA, INC.	0.0143999999999999996
428	2018-08-28	46247622	69097012615	AMLODIPINE BESYLATE 2.5 MG	30	0.609999999999999987	0	0	0.609999999999999987	0.429999999999999993	0.179999999999999993	0.295081969999999971	N0	CIPLA USA, INC.	0.0143999999999999996
429	2018-08-17	46256614	69097012615	AMLODIPINE BESYLATE 2.5 MG	3	6.19000000000000039	0	0	6.19000000000000039	0.0400000000000000008	6.15000000000000036	0.993537960000000053	N0	CIPLA USA, INC.	0.0143999999999999996
430	2018-08-04	46244151	69097012615	AMLODIPINE BESYLATE 2.5 MG	90	13.5500000000000007	0	1.25	14.8000000000000007	1.30000000000000004	13.5	0.912162160000000055	N0	CIPLA USA, INC.	0.0143999999999999996
431	2018-08-20	46254159	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0.560000000000000053	0	3.43999999999999995	4	0.67000000000000004	3.33000000000000007	0.832500000000000018	N0	CIPLA USA, INC.	0.022200000000000001
432	2018-08-14	46246340	69097012705	AMLODIPINE BESYLATE 5 MG TA	90	0	0	28.3099999999999987	28.3099999999999987	2	26.3099999999999987	0.929353590000000063	N0	CIPLA USA, INC.	0.022200000000000001
433	2018-08-20	46239592	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0	0	1.59000000000000008	1.59000000000000008	0.67000000000000004	0.92000000000000004	0.578616350000000002	N0	CIPLA USA, INC.	0.022200000000000001
434	2018-08-02	46215545	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	1.54000000000000004	0	2.66000000000000014	4.20000000000000018	0.67000000000000004	3.5299999999999998	0.840476189999999956	N0	CIPLA USA, INC.	0.022200000000000001
435	2018-08-16	46241612	69097012705	AMLODIPINE BESYLATE 5 MG TA	90	0	0	5.07000000000000028	5.07000000000000028	2	3.06999999999999984	0.60552267999999998	N0	CIPLA USA, INC.	0.022200000000000001
436	2018-08-05	46254210	69097012705	AMLODIPINE BESYLATE 5 MG TA	1	4.73000000000000043	0	0	4.73000000000000043	0.0200000000000000004	4.70999999999999996	0.995771669999999998	N0	CIPLA USA, INC.	0.022200000000000001
437	2018-08-08	46255398	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0.100000000000000006	0	10	10.0999999999999996	0.67000000000000004	9.42999999999999972	0.933663369999999992	N0	CIPLA USA, INC.	0.022200000000000001
438	2018-08-11	46255843	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0.979999999999999982	0	0.320000000000000007	1.30000000000000004	0.67000000000000004	0.630000000000000004	0.484615379999999984	N0	CIPLA USA, INC.	0.022200000000000001
439	2018-08-17	46251071	69097012705	AMLODIPINE BESYLATE 5 MG TA	15	14.9499999999999993	0	0	14.9499999999999993	0.330000000000000016	14.6199999999999992	0.977926419999999963	N0	CIPLA USA, INC.	0.022200000000000001
440	2018-08-09	46255374	69097012705	AMLODIPINE BESYLATE 5 MG TA	90	8.9399999999999995	0	3.06000000000000005	12	2	10	0.833333329999999983	N0	CIPLA USA, INC.	0.022200000000000001
441	2018-08-23	46257377	69097012705	AMLODIPINE BESYLATE 5 MG TA	3	6.19000000000000039	0	0	6.19000000000000039	0.0700000000000000067	6.12000000000000011	0.988691440000000088	N0	CIPLA USA, INC.	0.022200000000000001
442	2018-08-21	46232513	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0.0500000000000000028	0	1.25	1.30000000000000004	0.67000000000000004	0.630000000000000004	0.484615379999999984	N0	CIPLA USA, INC.	0.022200000000000001
443	2018-08-03	46245178	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0	0	2.18999999999999995	2.18999999999999995	0.67000000000000004	1.52000000000000002	0.694063929999999996	N0	CIPLA USA, INC.	0.022200000000000001
444	2018-08-29	46258086	69097012705	AMLODIPINE BESYLATE 5 MG TA	15	14.9499999999999993	0	0	14.9499999999999993	0.330000000000000016	14.6199999999999992	0.977926419999999963	N0	CIPLA USA, INC.	0.022200000000000001
445	2018-08-27	46257657	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	1.43999999999999995	0	2.66000000000000014	4.09999999999999964	0.67000000000000004	3.43000000000000016	0.836585369999999995	N0	CIPLA USA, INC.	0.022200000000000001
446	2018-08-28	46246503	69097012705	AMLODIPINE BESYLATE 5 MG TA	90	6.95000000000000018	0	0	6.95000000000000018	2	4.95000000000000018	0.712230220000000025	N0	CIPLA USA, INC.	0.022200000000000001
447	2018-08-27	46251422	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0	0	3.24000000000000021	3.24000000000000021	0.67000000000000004	2.56999999999999984	0.793209880000000034	N0	CIPLA USA, INC.	0.022200000000000001
448	2018-08-08	46244588	69097012705	AMLODIPINE BESYLATE 5 MG TA	15	14.9499999999999993	0	0	14.9499999999999993	0.330000000000000016	14.6199999999999992	0.977926419999999963	N0	CIPLA USA, INC.	0.022200000000000001
449	2018-08-27	46223053	69097012715	AMLODIPINE BESYLATE 5 MG TA	30	0.67000000000000004	0	0	0.67000000000000004	0.270000000000000018	0.400000000000000022	0.597014929999999944	N0	CIPLA USA, INC.	0.00889999999999999993
450	2018-08-30	46258219	69097012705	AMLODIPINE BESYLATE 5 MG TA	90	15.3699999999999992	0	1.25	16.620000000000001	2	14.6199999999999992	0.879663060000000052	N0	CIPLA USA, INC.	0.022200000000000001
451	2018-08-20	46241646	69097012705	AMLODIPINE BESYLATE 5 MG TA	31	0	0	4.59999999999999964	4.59999999999999964	0.689999999999999947	3.91000000000000014	0.849999999999999978	N0	CIPLA USA, INC.	0.022200000000000001
452	2018-08-14	46244588	69097012705	AMLODIPINE BESYLATE 5 MG TA	15	14.9499999999999993	0	0	14.9499999999999993	0.330000000000000016	14.6199999999999992	0.977926419999999963	N0	CIPLA USA, INC.	0.022200000000000001
453	2018-08-02	46250155	69097012705	AMLODIPINE BESYLATE 5 MG TA	90	0	0	5.83000000000000007	5.83000000000000007	2	3.83000000000000007	0.656946829999999982	N0	CIPLA USA, INC.	0.022200000000000001
454	2018-08-14	46250569	69097012705	AMLODIPINE BESYLATE 5 MG TA	15	14.9499999999999993	0	0	14.9499999999999993	0.330000000000000016	14.6199999999999992	0.977926419999999963	N0	CIPLA USA, INC.	0.022200000000000001
455	2018-08-05	46254210	69097012705	AMLODIPINE BESYLATE 5 MG TA	3	6.19000000000000039	0	0	6.19000000000000039	0.0700000000000000067	6.12000000000000011	0.988691440000000088	N0	CIPLA USA, INC.	0.022200000000000001
456	2018-08-10	46255797	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0.100000000000000006	2	8	10.0999999999999996	0.67000000000000004	9.42999999999999972	0.933663369999999992	N0	CIPLA USA, INC.	0.022200000000000001
457	2018-08-08	46255520	69097012705	AMLODIPINE BESYLATE 5 MG TA	3	6.19000000000000039	0	0	6.19000000000000039	0.0700000000000000067	6.12000000000000011	0.988691440000000088	N0	CIPLA USA, INC.	0.022200000000000001
458	2018-08-29	46240237	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0	0	1.56000000000000005	1.56000000000000005	0.67000000000000004	0.890000000000000013	0.570512820000000032	N0	CIPLA USA, INC.	0.022200000000000001
459	2018-08-02	46254210	69097012705	AMLODIPINE BESYLATE 5 MG TA	3	6.19000000000000039	0	0	6.19000000000000039	0.0700000000000000067	6.12000000000000011	0.988691440000000088	N0	CIPLA USA, INC.	0.022200000000000001
460	2018-08-29	46250797	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0	0	4.76999999999999957	4.76999999999999957	0.67000000000000004	4.09999999999999964	0.859538780000000058	N0	CIPLA USA, INC.	0.022200000000000001
461	2018-08-15	46245851	67877019805	AMLODIPINE BESYLATE 5 MG TA	90	0	0	16.620000000000001	16.620000000000001	0.869999999999999996	15.75	0.947653430000000019	N0	ASCEND LABORATO	0.00970000000000000029
462	2018-08-29	46258035	69097012705	AMLODIPINE BESYLATE 5 MG TA	30	0.100000000000000006	0	10	10.0999999999999996	0.67000000000000004	9.42999999999999972	0.933663369999999992	N0	CIPLA USA, INC.	0.022200000000000001
463	2018-08-10	48012122	51927180000	AMLODIPINE(GT) 1mg/ml SUS	180	0	37.8200000000000003	0	37.8200000000000003	1.82000000000000006	36	0.951877309999999976	N0		0
464	2018-08-09	46244079	65862058401	AMLODIPINE-BENAZEPRIL 5-20	90	0	0	36.1499999999999986	36.1499999999999986	10.1300000000000008	26.0199999999999996	0.719778699999999994	N0	AUROBINDO PHARM	0.112500000000000003
465	2018-08-10	46248869	33342019307	AMLODIPINE-OLMESARTAN 10-40	30	19.379999999999999	0	30	49.3800000000000026	23.3099999999999987	26.0700000000000003	0.527946539999999964	N0	MACLEODS PHARMA	0.777000000000000024
466	2018-08-22	46257209	45802049326	AMMONIUM LACTATE 12% CREA	385	33.3400000000000034	0	0	33.3400000000000034	31.8399999999999999	1.5	0.0449910000000000032	N0	PERRIGO CO.	0.0826999999999999957
467	2018-08-06	46251180	45802041926	AMMONIUM LACTATE 12% LOTION	400	0	0	31.9800000000000004	31.9800000000000004	27.8399999999999999	4.13999999999999968	0.129455910000000007	N0	PERRIGO CO.	0.0695999999999999952
468	2018-08-02	46248487	45802041954	AMMONIUM LACTATE 12% LOTION	225	15.5600000000000005	0	0	15.5600000000000005	15.2100000000000009	0.349999999999999978	0.0224935699999999975	N0	PERRIGO CO.	0.0675999999999999934
469	2018-08-09	46255682	143998201	AMOCLAN 400-57/5 SUSPENSION	200	19.7100000000000009	0	0	19.7100000000000009	13.0999999999999996	6.61000000000000032	0.335362759999999982	N0	WEST-WARD,INC.	0.0655000000000000027
470	2018-08-30	46258203	66685100200	AMOX TR-K CLV 500-125 MG TA	4	0	0	1.30000000000000004	1.30000000000000004	0.890000000000000013	0.409999999999999976	0.315384620000000004	N0	SANDOZ	0.22320000000000001
471	2018-08-16	46256534	66685100200	AMOX TR-K CLV 500-125 MG TA	60	0	0	50	50	13.3900000000000006	36.6099999999999994	0.732199999999999962	N	SANDOZ	0.22320000000000001
472	2018-08-30	46258234	66685100100	AMOX TR-K CLV 875-125 MG TA	3	8.90000000000000036	0	0	8.90000000000000036	0.979999999999999982	7.91999999999999993	0.88988763999999998	N0	SANDOZ	0.325000000000000011
473	2018-08-06	46255163	93227534	AMOX TR-K CLV 875-125MG T	30	19.0799999999999983	0	0	19.0799999999999983	9.99000000000000021	9.08999999999999986	0.476415089999999986	N0	TEVA USA	0.333000000000000018
474	2018-08-22	46257177	93227534	AMOX TR-K CLV 875-125MG T	20	7.29000000000000004	0	1.25	8.53999999999999915	6.66000000000000014	1.87999999999999989	0.220140520000000006	N0	TEVA USA	0.333000000000000018
475	2018-08-24	46257468	93227534	AMOX TR-K CLV 875-125MG T	20	0	0	10	10	6.66000000000000014	3.33999999999999986	0.333999999999999964	N0	TEVA USA	0.333000000000000018
476	2018-08-27	46257676	93227534	AMOX TR-K CLV 875-125MG T	14	9.4399999999999995	0	0	9.4399999999999995	4.66000000000000014	4.78000000000000025	0.50635593000000001	N0	TEVA USA	0.333000000000000018
477	2018-08-07	46255268	378083094	AMOX-CLAV 500-125 MG TABLET	60	32.9099999999999966	0	0	32.9099999999999966	16.9499999999999993	15.9600000000000009	0.484958979999999984	N0	MYLAN	0.282499999999999973
478	2018-08-23	46257350	143985316	AMOX-CLAV 600-42.9 MG/5 ML	125	26.3399999999999999	0	0	26.3399999999999999	8.40000000000000036	17.9400000000000013	0.681093390000000021	N0	WEST-WARD/HIKMA	0.0671999999999999958
479	2018-08-01	46254663	781185220	AMOX-CLAV 875-125 MG TABLET	20	8.64000000000000057	0	3.06000000000000005	11.6999999999999993	7.30999999999999961	4.38999999999999968	0.375213680000000049	N0	SANDOZ	1.68799999999999994
480	2018-08-13	46256102	781185220	AMOX-CLAV 875-125 MG TABLET	14	23.7399999999999984	0	0	23.7399999999999984	23.629999999999999	0.110000000000000001	0.00463353000000000029	N0	SANDOZ	1.68799999999999994
481	2018-08-06	46255142	781185220	AMOX-CLAV 875-125 MG TABLET	20	0	0	6.84999999999999964	6.84999999999999964	33.759999999999998	-26.9100000000000001	-3.92846715000000035	N0	SANDOZ	1.68799999999999994
482	2018-08-23	46257356	781185220	AMOX-CLAV 875-125 MG TABLET	28	0	0	10	10	47.259999999999998	-37.259999999999998	-3.72600000000000042	N0	SANDOZ	1.68799999999999994
483	2018-08-02	46226984	143988801	AMOXICILLIN 125 MG/5 ML SUS	100	2.75	0	0	2.75	1.57000000000000006	1.17999999999999994	0.429090909999999992	N0	WEST-WARD/HIKMA	0.0156999999999999987
484	2018-08-28	46226984	143988801	AMOXICILLIN 125 MG/5 ML SUS	100	2.75	0	0	2.75	1.57000000000000006	1.17999999999999994	0.429090909999999992	N0	WEST-WARD/HIKMA	0.0156999999999999987
485	2018-08-20	46245818	93226801	AMOXICILLIN 250 MG TAB CHEW	60	9.69999999999999929	0	5	14.6999999999999993	17.1900000000000013	-2.49000000000000021	-0.169387759999999998	N0	TEVA USA	0.26369999999999999
486	2018-08-13	46249699	93415579	AMOXICILLIN 250 MG/5 ML SUS	80	1.5	0	1.25	2.75	1.8600000000000001	0.890000000000000013	0.323636360000000012	N0	TEVA USA	0.0166000000000000002
487	2018-08-31	46249699	93415579	AMOXICILLIN 250 MG/5 ML SUS	80	1.5	0	1.25	2.75	1.8600000000000001	0.890000000000000013	0.323636360000000012	N0	TEVA USA	0.0166000000000000002
488	2018-08-14	46226113	93415579	AMOXICILLIN 250 MG/5 ML SUS	80	2.83000000000000007	0	0	2.83000000000000007	1.8600000000000001	0.969999999999999973	0.342756180000000021	N0	TEVA USA	0.0166000000000000002
489	2018-08-30	46226113	93415579	AMOXICILLIN 250 MG/5 ML SUS	80	2.83000000000000007	0	0	2.83000000000000007	1.8600000000000001	0.969999999999999973	0.342756180000000021	N0	TEVA USA	0.0166000000000000002
490	2018-08-08	46255515	143988701	AMOXICILLIN 400 MG/5 ML SUS	200	0	0	15	15	3.2799999999999998	11.7200000000000006	0.781333329999999937	N0	WEST-WARD/HIKMA	0.0164000000000000014
491	2018-08-15	46256316	143993905	AMOXICILLIN 500 MG CAPSULE	30	0	0	2.83999999999999986	2.83999999999999986	1.56000000000000005	1.28000000000000003	0.450704229999999983	N0	WEST-WARD/HIKMA	0.0521000000000000005
492	2018-08-22	46257231	143993905	AMOXICILLIN 500 MG CAPSULE	21	0	0	3.33999999999999986	3.33999999999999986	1.09000000000000008	2.25	0.673652689999999943	N0	WEST-WARD/HIKMA	0.0521000000000000005
493	2018-08-07	46255331	143993905	AMOXICILLIN 500 MG CAPSULE	21	5.41000000000000014	0	0	5.41000000000000014	1.09000000000000008	4.32000000000000028	0.79852126000000001	N0	WEST-WARD/HIKMA	0.0521000000000000005
494	2018-08-03	46224561	143993905	AMOXICILLIN 500 MG CAPSULE	48	0	0	24.0199999999999996	24.0199999999999996	2.5	21.5199999999999996	0.895920069999999957	N	WEST-WARD/HIKMA	0.0521000000000000005
495	2018-08-29	46252152	143993905	AMOXICILLIN 500 MG CAPSULE	12	0	0	3	3	0.630000000000000004	2.37000000000000011	0.790000000000000036	N0	WEST-WARD/HIKMA	0.0521000000000000005
496	2018-08-15	46256380	143993905	AMOXICILLIN 500 MG CAPSULE	12	0	0	2.35999999999999988	2.35999999999999988	0.630000000000000004	1.72999999999999998	0.733050850000000032	N0	WEST-WARD/HIKMA	0.0521000000000000005
497	2018-08-22	46257249	143993905	AMOXICILLIN 500 MG CAPSULE	30	2	0	1.25	3.25	1.56000000000000005	1.68999999999999995	0.520000000000000018	N0	WEST-WARD/HIKMA	0.0521000000000000005
498	2018-08-17	46250711	143993905	AMOXICILLIN 500 MG CAPSULE	90	12.5199999999999996	0	5	17.5199999999999996	4.69000000000000039	12.8300000000000001	0.732305939999999933	N0	WEST-WARD/HIKMA	0.0521000000000000005
499	2018-08-28	46251077	143993905	AMOXICILLIN 500 MG CAPSULE	8	0	0	3	3	0.419999999999999984	2.58000000000000007	0.859999999999999987	N0	WEST-WARD/HIKMA	0.0521000000000000005
500	2018-08-02	46254815	143993901	AMOXICILLIN 500 MG CAPSULE	30	1.62999999999999989	0	4	5.62999999999999989	2.81000000000000005	2.81999999999999984	0.500888100000000058	N0	WEST-WARD/HIKMA	0.0937000000000000055
501	2018-08-14	46256215	143993905	AMOXICILLIN 500 MG CAPSULE	60	1.03000000000000003	0	4	5.03000000000000025	3.12999999999999989	1.89999999999999991	0.377733599999999947	N0	WEST-WARD/HIKMA	0.0521000000000000005
502	2018-08-28	46257831	143993905	AMOXICILLIN 500 MG CAPSULE	8	0	0	0.67000000000000004	0.67000000000000004	0.419999999999999984	0.25	0.373134330000000014	N0	WEST-WARD/HIKMA	0.0521000000000000005
503	2018-08-15	46256315	143993905	AMOXICILLIN 500 MG CAPSULE	42	0	0	6.28000000000000025	6.28000000000000025	2.18999999999999995	4.08999999999999986	0.651273889999999911	N0	WEST-WARD/HIKMA	0.0521000000000000005
504	2018-08-09	46255036	143993901	AMOXICILLIN 500 MG CAPSULE	9	0	0	8.64000000000000057	8.64000000000000057	0.839999999999999969	7.79999999999999982	0.902777779999999974	N0	WEST-WARD/HIKMA	0.0937000000000000055
505	2018-08-09	46255688	143993905	AMOXICILLIN 500 MG CAPSULE	20	2.06000000000000005	0	1.6100000000000001	3.66999999999999993	1.04000000000000004	2.62999999999999989	0.716621249999999987	N0	WEST-WARD/HIKMA	0.0521000000000000005
506	2018-08-28	46257828	93310905	AMOXICILLIN 500MG CAPSULE	4	0	0	1.05000000000000004	1.05000000000000004	0.369999999999999996	0.680000000000000049	0.647619049999999974	N0	TEVA USA	0.0933999999999999969
507	2018-08-02	46254877	93310905	AMOXICILLIN 500MG CAPSULE	20	3.75	0	0	3.75	1.87000000000000011	1.87999999999999989	0.501333330000000021	N0	TEVA USA	0.0933999999999999969
508	2018-08-13	46256125	65862001501	AMOXICILLIN 875 MG TABLET	20	2.18000000000000016	0	0	2.18000000000000016	1.41999999999999993	0.760000000000000009	0.348623850000000013	N0	AUROBINDO PHARM	0.0709000000000000047
509	2018-08-27	42056056	13107007001	AMPHETAMINE SALTS 10 MG TAB	30	0	0	52.6400000000000006	52.6400000000000006	7.66999999999999993	44.9699999999999989	0.854293310000000083	N0	AUROBINDO PHARM	0.255800000000000027
510	2018-08-20	42055869	555097202	AMPHETAMINE SALTS 10MG TA	60	28.7399999999999984	0	1.25	29.9899999999999984	31.5799999999999983	-1.59000000000000008	-0.0530176699999999959	N0	TEVA USA	0.344700000000000006
511	2018-08-21	42055933	555097202	AMPHETAMINE SALTS 10MG TA	30	11.6500000000000004	0	5	16.6499999999999986	15.7899999999999991	0.859999999999999987	0.0516516500000000003	N0	TEVA USA	0.344700000000000006
512	2018-08-09	42055541	555077702	AMPHETAMINE SALTS 15MG TA	30	17.2199999999999989	0	0	17.2199999999999989	14.2100000000000009	3.00999999999999979	0.174796750000000001	N0	TEVA USA	0.47370000000000001
513	2018-08-03	42055517	555077702	AMPHETAMINE SALTS 15MG TA	30	13.4299999999999997	0	0	13.4299999999999997	14.2100000000000009	-0.780000000000000027	-0.0580789300000000008	N0	TEVA USA	0.47370000000000001
514	2018-08-21	42055934	555077702	AMPHETAMINE SALTS 15MG TA	30	11.9100000000000001	0	5	16.9100000000000001	14.2100000000000009	2.70000000000000018	0.159668840000000006	N0	TEVA USA	0.47370000000000001
515	2018-08-18	42055847	555077702	AMPHETAMINE SALTS 15MG TA	30	10.4299999999999997	0	3	13.4299999999999997	14.2100000000000009	-0.780000000000000027	-0.0580789300000000008	N0	TEVA USA	0.47370000000000001
516	2018-08-31	42056173	555077702	AMPHETAMINE SALTS 15MG TA	30	13.4299999999999997	0	0	13.4299999999999997	14.2100000000000009	-0.780000000000000027	-0.0580789300000000008	N0	TEVA USA	0.47370000000000001
517	2018-08-10	42055672	555097302	AMPHETAMINE SALTS 20 MG TAB	30	16.9899999999999984	0	0	16.9899999999999984	15.7899999999999991	1.19999999999999996	0.0706297800000000031	N0	TEVA USA	0.526399999999999979
518	2018-08-25	42056030	555097302	AMPHETAMINE SALTS 20 MG TAB	60	33.1400000000000006	0	0	33.1400000000000006	31.5799999999999983	1.56000000000000005	0.0470730200000000001	N0	TEVA USA	0.526399999999999979
519	2018-08-31	42056190	13107007401	AMPHETAMINE SALTS 30 MG TAB	60	30.1499999999999986	0	0	30.1499999999999986	17.4600000000000009	12.6899999999999995	0.420895519999999967	N0	AUROBINDO PHARM	0.290999999999999981
520	2018-08-08	42055633	555097402	AMPHETAMINE SALTS 30MG TA	60	40.3599999999999994	0	1.25	41.6099999999999994	23.379999999999999	18.2300000000000004	0.438115840000000034	N0	TEVA USA	0.389699999999999991
521	2018-08-17	42055837	555097402	AMPHETAMINE SALTS 30MG TA	30	0	0	22.8599999999999994	22.8599999999999994	11.6899999999999995	11.1699999999999999	0.488626420000000006	N0	TEVA USA	0.389699999999999991
522	2018-08-01	42055446	555097402	AMPHETAMINE SALTS 30MG TA	60	28.8999999999999986	0	1.25	30.1499999999999986	23.379999999999999	6.76999999999999957	0.22454395000000002	N0	TEVA USA	0.389699999999999991
523	2018-08-23	42055977	555097402	AMPHETAMINE SALTS 30MG TA	90	54.25	0	0	54.25	35.0700000000000003	19.1799999999999997	0.35354838999999999	N0	TEVA USA	0.389699999999999991
524	2018-08-02	42055415	13107006801	AMPHETAMINE SALTS 5 MG TABL	60	0	0	56.0399999999999991	56.0399999999999991	15.3499999999999996	40.6899999999999977	0.726088509999999965	N0	AUROBINDO PHARM	0.255800000000000027
525	2018-08-28	46257830	16729003515	ANASTROZOLE 1 MG TABLET	2	0	0	2.52000000000000002	2.52000000000000002	0.170000000000000012	2.35000000000000009	0.932539679999999982	N0	ACCORD HEALTHCA	0.0834000000000000019
526	2018-08-02	46254746	16729003515	ANASTROZOLE 1 MG TABLET	30	8.50999999999999979	0	0	8.50999999999999979	2.5	6.00999999999999979	0.706227970000000038	N0	ACCORD HEALTHCA	0.0834000000000000019
527	2018-08-03	46254979	573022110	ANBESOL LIQUID	12	0	0	6.42999999999999972	6.42999999999999972	0	6.42999999999999972	1	N0	WYETH CONSUMER	0
528	2018-08-27	44030156	51846233	ANDROGEL 1.62% GEL PUMP	150	1236.28999999999996	0	0	1236.28999999999996	1124.03999999999996	112.25	0.0907958499999999974	N0	ABBVIE US LLC	7.49359999999999982
529	2018-08-14	46241391	173086910	ANORO ELLIPTA 62.5-25 MCG I	60	388.120000000000005	0	8.34999999999999964	396.470000000000027	328.069999999999993	68.4000000000000057	0.172522510000000018	N0	GLAXOSMITHKLINE	5.46780000000000044
530	2018-08-30	46257233	173086910	ANORO ELLIPTA 62.5-25 MCG I	180	1120.07999999999993	0	3.70000000000000018	1123.77999999999997	984.200000000000045	139.580000000000013	0.12420581	N0	GLAXOSMITHKLINE	5.46780000000000044
531	2018-08-29	46246605	173086910	ANORO ELLIPTA 62.5-25 MCG I	60	343.379999999999995	0	47	390.379999999999995	328.069999999999993	62.3100000000000023	0.159613709999999992	N0	GLAXOSMITHKLINE	5.46780000000000044
532	2018-08-02	46240988	173086910	ANORO ELLIPTA 62.5-25 MCG I	60	388.100000000000023	0	3.70000000000000018	391.800000000000011	328.069999999999993	63.7299999999999969	0.162659519999999974	N0	GLAXOSMITHKLINE	5.46780000000000044
533	2018-08-03	46254921	173086910	ANORO ELLIPTA 62.5-25 MCG I	60	389.300000000000011	0	3	392.300000000000011	328.069999999999993	64.230000000000004	0.163726740000000009	N0	GLAXOSMITHKLINE	5.46780000000000044
534	2018-08-01	46254612	24385055467	ANTI-DIARRHEAL 2 MG CAPLET	48	0	0	6.08999999999999986	6.08999999999999986	2.99000000000000021	3.10000000000000009	0.509031200000000017	N0	AMERISOURCEBERG	0.0621999999999999983
535	2018-08-28	46235156	88250033	APIDRA 100 UNITS/ML VIAL	20	532.17999999999995	0	0	532.17999999999995	444.519999999999982	87.6599999999999966	0.164718699999999996	Y1	SANOFI-AVENTIS	22.2259999999999991
536	2018-08-30	46258231	555904358	APRI 28 DAY TABLET	28	14.0999999999999996	0	0	14.0999999999999996	8.60999999999999943	5.49000000000000021	0.389361699999999977	N0	TEVA USA	0.307499999999999996
537	2018-08-17	46214305	555904358	APRI 28 DAY TABLET	28	13.1699999999999999	0	0	13.1699999999999999	8.60999999999999943	4.55999999999999961	0.346241460000000056	N0	TEVA USA	0.307499999999999996
538	2018-08-04	46244233	555904358	APRI 28 DAY TABLET	28	15.1099999999999994	0	0	15.1099999999999994	8.60999999999999943	6.5	0.430178689999999975	N0	TEVA USA	0.307499999999999996
539	2018-08-21	46224818	58914021460	AQUADEKS PEDIATRIC LIQUID	60	19.1600000000000001	0	0	19.1600000000000001	21	-1.84000000000000008	-0.096033399999999991	N0	ACTAVIS U.S. BR	0.332600000000000007
540	2018-08-01	46254599	58914021460	AQUADEKS PEDIATRIC LIQUID	60	0	0	27.0799999999999983	27.0799999999999983	21	6.08000000000000007	0.224519940000000001	N0	AXCAN PHARMA US	0.349999999999999978
541	2018-08-22	46257144	72140003263	AQUAPHOR OINTMENT	99	8.78999999999999915	0	0	8.78999999999999915	5.38999999999999968	3.39999999999999991	0.386803189999999963	N0	BEIERSDORF	0.054399999999999997
542	2018-08-06	46251903	13668021830	ARIPIPRAZOLE 10 MG TABLET	30	214.75	0	1.25	216	10.5099999999999998	205.490000000000009	0.951342589999999988	N0	TORRENT PHARMAC	0.3503
543	2018-08-21	46246008	13668021830	ARIPIPRAZOLE 10 MG TABLET	30	10.9499999999999993	0	0	10.9499999999999993	10.5099999999999998	0.440000000000000002	0.0401826500000000073	N0	TORRENT PHARMAC	0.3503
544	2018-08-02	46251712	13668021830	ARIPIPRAZOLE 10 MG TABLET	30	75.5699999999999932	0	96.9000000000000057	172.469999999999999	10.5099999999999998	161.960000000000008	0.939061870000000076	N0	TORRENT PHARMAC	0.3503
545	2018-08-09	46250193	13668021830	ARIPIPRAZOLE 10 MG TABLET	30	14.8800000000000008	0	0	14.8800000000000008	10.5099999999999998	4.37000000000000011	0.293682799999999966	N0	TORRENT PHARMAC	0.3503
546	2018-08-21	46257033	13668021830	ARIPIPRAZOLE 10 MG TABLET	15	26.5700000000000003	0	1.25	27.8200000000000003	5.25	22.5700000000000003	0.811286840000000065	N0	TORRENT PHARMAC	0.3503
547	2018-08-13	46251663	13668021830	ARIPIPRAZOLE 10 MG TABLET	30	97.6800000000000068	0	3.35000000000000009	101.030000000000001	10.5099999999999998	90.519999999999996	0.895971489999999982	N0	TORRENT PHARMAC	0.3503
548	2018-08-17	46256613	13668021830	ARIPIPRAZOLE 10 MG TABLET	30	231.050000000000011	0	1.25	232.300000000000011	10.5099999999999998	221.789999999999992	0.954756779999999972	N0	TORRENT PHARMAC	0.3503
549	2018-08-08	46246967	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	15.0500000000000007	0	0	15.0500000000000007	10.5099999999999998	4.54000000000000004	0.30166113	N0	TORRENT PHARMAC	0.3503
550	2018-08-01	46233306	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	20.6099999999999994	0	0	20.6099999999999994	10.5099999999999998	10.0999999999999996	0.490053369999999988	N0	TORRENT PHARMAC	0.3503
551	2018-08-25	46248134	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	204.870000000000005	0	1.25	206.120000000000005	10.5099999999999998	195.610000000000014	0.949010289999999923	N0	TORRENT PHARMAC	0.3503
552	2018-08-07	46241035	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	15.9299999999999997	0	1.25	17.1799999999999997	10.5099999999999998	6.66999999999999993	0.388242139999999958	N0	TORRENT PHARMAC	0.3503
553	2018-08-27	46253372	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	11.2899999999999991	0	0	11.2899999999999991	10.5099999999999998	0.780000000000000027	0.0690876900000000072	N0	TORRENT PHARMAC	0.3503
554	2018-08-02	46248134	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	204.870000000000005	0	1.25	206.120000000000005	10.5099999999999998	195.610000000000014	0.949010289999999923	N0	TORRENT PHARMAC	0.3503
555	2018-08-02	46254867	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	118.790000000000006	0	97.1899999999999977	215.97999999999999	10.5099999999999998	205.469999999999999	0.951338090000000025	N0	TORRENT PHARMAC	0.3503
556	2018-08-20	46256861	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	51.009999999999998	0	96.9000000000000057	147.909999999999997	10.5099999999999998	137.400000000000006	0.928943280000000038	N0	TORRENT PHARMAC	0.3503
557	2018-08-31	46233306	13668021930	ARIPIPRAZOLE 15 MG TABLET	30	11.2899999999999991	0	0	11.2899999999999991	10.5099999999999998	0.780000000000000027	0.0690876900000000072	N0	TORRENT PHARMAC	0.3503
558	2018-08-13	46240256	13668021930	ARIPIPRAZOLE 15 MG TABLET	90	377.300000000000011	0	3.35000000000000009	380.649999999999977	31.5300000000000011	349.120000000000005	0.917168000000000094	N0	TORRENT PHARMAC	0.3503
559	2018-08-02	46254568	13668021630	ARIPIPRAZOLE 2 MG TABLET	30	73.9099999999999966	0	0	73.9099999999999966	10.5099999999999998	63.3999999999999986	0.857800029999999936	N0	TORRENT PHARMAC	0.3503
560	2018-08-09	46253414	13668021630	ARIPIPRAZOLE 2 MG TABLET	30	216.02000000000001	0	0	216.02000000000001	10.5099999999999998	205.509999999999991	0.951347100000000001	N0	TORRENT PHARMAC	0.3503
561	2018-08-27	46254317	13668021630	ARIPIPRAZOLE 2 MG TABLET	30	101.459999999999994	0	0	101.459999999999994	10.5099999999999998	90.9500000000000028	0.896412380000000009	N0	TORRENT PHARMAC	0.3503
562	2018-08-02	46245021	13668022030	ARIPIPRAZOLE 20 MG TABLET	30	20	0	0	20	15.9600000000000009	4.04000000000000004	0.201999999999999985	N0	TORRENT PHARMAC	0.532000000000000028
563	2018-08-29	46251588	13668022030	ARIPIPRAZOLE 20 MG TABLET	30	290.029999999999973	0	1.25	291.279999999999973	15.9600000000000009	275.319999999999993	0.945207359999999941	N0	TORRENT PHARMAC	0.532000000000000028
564	2018-08-06	46251588	13668022030	ARIPIPRAZOLE 20 MG TABLET	30	290.029999999999973	0	1.25	291.279999999999973	15.9600000000000009	275.319999999999993	0.945207359999999941	N0	TORRENT PHARMAC	0.532000000000000028
565	2018-08-06	46255120	13668022030	ARIPIPRAZOLE 20 MG TABLET	30	189.800000000000011	0	40	229.800000000000011	15.9600000000000009	213.840000000000003	0.930548299999999995	N0	TORRENT PHARMAC	0.532000000000000028
566	2018-08-15	46246766	13668022130	ARIPIPRAZOLE 30 MG TABLET	30	14.6500000000000004	0	0	14.6500000000000004	8.90000000000000036	5.75	0.392491470000000009	N0	TORRENT PHARMAC	0.296599999999999975
567	2018-08-13	46256066	13668022130	ARIPIPRAZOLE 30 MG TABLET	30	337.870000000000005	0	1.19999999999999996	339.069999999999993	15.9600000000000009	323.110000000000014	0.952930070000000073	N0	TORRENT PHARMAC	0.296599999999999975
568	2018-08-14	46256275	13668021730	ARIPIPRAZOLE 5 MG TABLET	30	14.7899999999999991	0	0	14.7899999999999991	6.5	8.28999999999999915	0.56051386000000003	N0	TORRENT PHARMAC	0.216599999999999987
569	2018-08-04	46248991	13668021730	ARIPIPRAZOLE 5 MG TABLET	30	0	0	15	15	6.5	8.5	0.566666669999999928	N0	TORRENT PHARMAC	0.216599999999999987
570	2018-08-07	46255339	13668021730	ARIPIPRAZOLE 5 MG TABLET	30	215.990000000000009	0	0	215.990000000000009	6.5	209.490000000000009	0.969906009999999985	N0	TORRENT PHARMAC	0.216599999999999987
571	2018-08-03	46248091	13668021730	ARIPIPRAZOLE 5 MG TABLET	30	14.7899999999999991	0	0	14.7899999999999991	6.5	8.28999999999999915	0.56051386000000003	N0	TORRENT PHARMAC	0.216599999999999987
572	2018-08-06	46255227	456046101	ARMOUR THYROID 120MG TABL	90	0	0	162.110000000000014	162.110000000000014	114.159999999999997	47.9500000000000028	0.295786810000000011	N0	FOREST/ALLERGAN	1.26839999999999997
573	2018-08-28	48011129	456046101	ARMOUR THYROID 120MG TABL	90	0	0	137.72999999999999	137.72999999999999	114.159999999999997	23.5700000000000003	0.171131920000000021	N0	FOREST/ALLERGAN	1.26839999999999997
574	2018-08-20	46253852	85433301	ASMANEX HFA 100 MCG INHALER	13	189.789999999999992	0	0	189.789999999999992	189.449999999999989	0.340000000000000024	0.00179144999999999999	N0	MERCK SHARP & D	14.5731000000000002
575	2018-08-10	46255768	85433401	ASMANEX HFA 200 MCG INHALER	13	222.300000000000011	0	0	222.300000000000011	211.52000000000001	10.7799999999999994	0.0484930299999999995	N0	MERCK SHARP & D	16.2704999999999984
576	2018-08-17	46256607	41167005840	ASPERCREME w/ LIDOC* PATCH*	6	10.5	0	0	10.5	7.48000000000000043	3.02000000000000002	0.287619049999999987	N0	CHATTEM CONS PR	1.18599999999999994
577	2018-08-14	46256226	41167005840	ASPERCREME w/ LIDOC* PATCH*	15	22.0500000000000007	0	0	22.0500000000000007	18.6900000000000013	3.35999999999999988	0.152380949999999987	N0	CHATTEM CONS PR	1.18599999999999994
578	2018-08-06	46254194	41167005840	ASPERCREME w/ LIDOC* PATCH*	10	14.5999999999999996	0	0	14.5999999999999996	11.0899999999999999	3.50999999999999979	0.240410960000000007	N0	CHATTEM CONS PR	1.18599999999999994
579	2018-08-30	46258217	41167005840	ASPERCREME w/ LIDOC* PATCH*	90	112.319999999999993	0	0	112.319999999999993	112.150000000000006	0.170000000000000012	0.00151352999999999991	N0	CHATTEM CONS PR	1.18599999999999994
580	2018-08-18	46256792	41167005840	ASPERCREME w/ LIDOC* PATCH*	10	16.0399999999999991	0	0	16.0399999999999991	12.4600000000000009	3.58000000000000007	0.223192020000000019	N0	CHATTEM CONS PR	1.18599999999999994
581	2018-08-14	46256230	41167005840	ASPERCREME w/ LIDOC* PATCH*	10	16.0399999999999991	0	0	16.0399999999999991	12.4600000000000009	3.58000000000000007	0.223192020000000019	N0	CHATTEM CONS PR	1.18599999999999994
582	2018-08-11	46255879	41167005840	ASPERCREME w/ LIDOC* PATCH*	5	0	0	9.33000000000000007	9.33000000000000007	5.54000000000000004	3.79000000000000004	0.406216509999999975	N0	CHATTEM CONS PR	1.18599999999999994
583	2018-08-02	46252216	536330510	ASPIRIN 325MG TABLET	15	2.22999999999999998	0	0	2.22999999999999998	0.190000000000000002	2.04000000000000004	0.914798210000000056	N0	RUGBY	0.0129
584	2018-08-12	46255630	536330510	ASPIRIN 325MG TABLET	3	2.04999999999999982	0	0	2.04999999999999982	0.0400000000000000008	2.00999999999999979	0.980487799999999909	N0	RUGBY	0.0129
585	2018-08-09	46255630	536330510	ASPIRIN 325MG TABLET	3	2.04999999999999982	0	0	2.04999999999999982	0.0400000000000000008	2.00999999999999979	0.980487799999999909	N0	RUGBY	0.0129
586	2018-08-21	46252216	536330510	ASPIRIN 325MG TABLET	15	2.22999999999999998	0	0	2.22999999999999998	0.190000000000000002	2.04000000000000004	0.914798210000000056	N0	RUGBY	0.0129
587	2018-08-23	46247774	904628889	ASPIRIN 81 MG CHEWABLE TABL	30	0	1.58000000000000007	1	2.58000000000000007	1.06000000000000005	1.52000000000000002	0.58914728999999999	N0	MAJOR PHARMACEU	0.0354000000000000009
588	2018-08-04	46255049	603002622	ASPIRIN 81 MG TABLET EC	200	0	0	4	4	1.8600000000000001	2.14000000000000012	0.535000000000000031	N0	QUALITEST/PAR P	0.00929999999999999924
589	2018-08-01	46254281	57896091136	ASPIRIN 81MG TABLET CHEW	4	2.08000000000000007	0	0	2.08000000000000007	0.0700000000000000067	2.00999999999999979	0.966346150000000015	N0	GERI-CARE	0.0182000000000000009
590	2018-08-02	46248384	904404073	ASPIRIN CHILD 81 MG CHEW	30	1.90999999999999992	0	0	1.90999999999999992	0.589999999999999969	1.32000000000000006	0.691099479999999988	N0	MAJOR PHARMACEU	0.0195
591	2018-08-03	46255021	904404073	ASPIRIN CHILD 81 MG CHEW	30	1.90999999999999992	0	0	1.90999999999999992	0.589999999999999969	1.32000000000000006	0.691099479999999988	N0	MAJOR PHARMACEU	0.0195
592	2018-08-24	46248713	904404073	ASPIRIN CHILD 81 MG CHEW	30	1.90999999999999992	0	0	1.90999999999999992	0.589999999999999969	1.32000000000000006	0.691099479999999988	N0	MAJOR PHARMACEU	0.0195
593	2018-08-14	46242021	536331310	ASPIRIN E.C.325MG  TAB	30	0	0	2.60999999999999988	2.60999999999999988	0.510000000000000009	2.10000000000000009	0.804597700000000082	N0	RUGBY	0.0168999999999999984
594	2018-08-08	46255539	536331310	ASPIRIN E.C.325MG  TAB	3	2.06999999999999984	0	0	2.06999999999999984	0.0500000000000000028	2.02000000000000002	0.975845410000000024	N0	RUGBY	0.0168999999999999984
595	2018-08-29	46228026	363041414	ASPIRIN EC 81 MG TABLET	30	0	0	2.83999999999999986	2.83999999999999986	0.739999999999999991	2.10000000000000009	0.739436619999999989	N0	WALGREEN CO.	0.0246000000000000003
596	2018-08-02	46233034	363041414	ASPIRIN EC 81 MG TABLET	90	0	0	4.51999999999999957	4.51999999999999957	2.20999999999999996	2.31000000000000005	0.511061949999999987	N0	WALGREEN CO.	0.0246000000000000003
597	2018-08-23	46253621	603002632	ASPIRIN EC 81 MG TABLET	30	0	0	3.64000000000000012	3.64000000000000012	0.160000000000000003	3.47999999999999998	0.956043959999999915	N0	QUALITEST/PAR P	0.00530000000000000002
598	2018-08-29	46216428	603002632	ASPIRIN EC 81 MG TABLET	30	0	0	3.64000000000000012	3.64000000000000012	0.160000000000000003	3.47999999999999998	0.956043959999999915	N0	QUALITEST/PAR P	0.00530000000000000002
599	2018-08-21	46250473	603002632	ASPIRIN EC 81 MG TABLET	15	2.70000000000000018	0	0	2.70000000000000018	0.0800000000000000017	2.62000000000000011	0.970370369999999927	N0	QUALITEST/PAR P	0.00530000000000000002
600	2018-08-28	46225503	603002632	ASPIRIN EC 81 MG TABLET	30	0	0	0.660000000000000031	0.660000000000000031	0.160000000000000003	0.5	0.757575760000000042	N0	QUALITEST/PAR P	0.00530000000000000002
601	2018-08-10	46255717	363041414	ASPIRIN EC 81 MG TABLET	30	0	0	2.83999999999999986	2.83999999999999986	0.739999999999999991	2.10000000000000009	0.739436619999999989	N0	WALGREEN CO.	0.0246000000000000003
602	2018-08-20	46256919	603002632	ASPIRIN EC 81 MG TABLET	90	4.69000000000000039	0	0	4.69000000000000039	0.479999999999999982	4.20999999999999996	0.89765457999999998	N0	QUALITEST/PAR P	0.00530000000000000002
603	2018-08-22	46232056	363041414	ASPIRIN EC 81 MG TABLET	30	1.62999999999999989	0	0	1.62999999999999989	0.739999999999999991	0.890000000000000013	0.546012269999999966	N0	WALGREEN CO.	0.0246000000000000003
604	2018-08-30	46235837	536100441	ASPIRIN EC 81 MG TABLET	30	0	0	0.660000000000000031	0.660000000000000031	0.680000000000000049	-0.0200000000000000004	-0.0303030299999999982	N0	RUGBY	0.0347000000000000017
605	2018-08-22	46242721	363041414	ASPIRIN EC 81 MG TABLET	30	0	0	2.83999999999999986	2.83999999999999986	0.739999999999999991	2.10000000000000009	0.739436619999999989	N0	WALGREEN CO.	0.0246000000000000003
606	2018-08-30	46251137	603002632	ASPIRIN EC 81 MG TABLET	120	0	0	8.5600000000000005	8.5600000000000005	0.640000000000000013	7.91999999999999993	0.925233639999999968	N0	QUALITEST/PAR P	0.00530000000000000002
607	2018-08-20	46253445	603002632	ASPIRIN EC 81 MG TABLET	30	0	5.20999999999999996	1	6.20999999999999996	0.160000000000000003	6.04999999999999982	0.974235099999999909	N0	QUALITEST/PAR P	0.00530000000000000002
608	2018-08-28	46238048	603002632	ASPIRIN EC 81 MG TABLET	30	0	0	2	2	0.160000000000000003	1.84000000000000008	0.92000000000000004	N0	QUALITEST/PAR P	0.00530000000000000002
609	2018-08-01	46251137	603002632	ASPIRIN EC 81 MG TABLET	120	0	0	8.5600000000000005	8.5600000000000005	0.640000000000000013	7.91999999999999993	0.925233639999999968	N0	QUALITEST/PAR P	0.00530000000000000002
610	2018-08-03	46254922	603002632	ASPIRIN EC 81 MG TABLET	90	0	0	6.91999999999999993	6.91999999999999993	0.479999999999999982	6.44000000000000039	0.930635840000000103	N0	QUALITEST/PAR P	0.00530000000000000002
611	2018-08-17	46256732	63868036336	ASPIRIN EC 81MG TABLET	5	2.04000000000000004	0	0	2.04000000000000004	0.0500000000000000028	1.98999999999999999	0.975490199999999974	N0	CDMA	0.00919999999999999984
612	2018-08-07	48011805	68382002401	ATENOLOL (T) 2MG/ML SUSP	360	20.7800000000000011	0	0	20.7800000000000011	6.19000000000000039	14.5899999999999999	0.702117419999999992	N0		0
613	2018-08-14	46254324	781150701	ATENOLOL 100MG TABLET	45	6.51999999999999957	0	0	6.51999999999999957	1.90999999999999992	4.61000000000000032	0.707055210000000045	N0	SANDOZ	0.0425000000000000031
614	2018-08-03	46247535	68382002210	ATENOLOL 25 MG TABLET	120	5.61000000000000032	0	0	5.61000000000000032	4.37000000000000011	1.23999999999999999	0.221033870000000021	N0	ZYDUS PHARMACEU	0.0364000000000000018
615	2018-08-14	46253533	68382002210	ATENOLOL 25 MG TABLET	30	0	0	5.75999999999999979	5.75999999999999979	1.09000000000000008	4.66999999999999993	0.810763890000000043	N0	ZYDUS PHARMACEU	0.0364000000000000018
616	2018-08-30	46238111	68382002210	ATENOLOL 25 MG TABLET	90	0	0	6.51999999999999957	6.51999999999999957	3.2799999999999998	3.24000000000000021	0.496932519999999989	N0	ZYDUS PHARMACEU	0.0364000000000000018
617	2018-08-09	46249270	68382002210	ATENOLOL 25 MG TABLET	30	2.43999999999999995	0	0	2.43999999999999995	1.09000000000000008	1.35000000000000009	0.553278689999999962	N0	ZYDUS PHARMACEU	0.0364000000000000018
618	2018-08-13	46251795	781522010	ATENOLOL 25 MG TABLET	30	0.609999999999999987	3.35000000000000009	0	3.95999999999999996	0.130000000000000004	3.83000000000000007	0.967171720000000068	N0	SANDOZ	0.00419999999999999974
619	2018-08-14	46228358	68382002210	ATENOLOL 25 MG TABLET	30	1.30000000000000004	0	4.79999999999999982	6.09999999999999964	1.09000000000000008	5.00999999999999979	0.821311479999999983	N0	ZYDUS PHARMACEU	0.0364000000000000018
620	2018-08-15	46247711	68382002310	ATENOLOL 50 MG TABLET	30	2.56999999999999984	0	0.849999999999999978	3.41999999999999993	1.96999999999999997	1.44999999999999996	0.423976609999999976	N0	ZYDUS PHARMACEU	0.0476999999999999993
621	2018-08-15	46235667	68382002310	ATENOLOL 50 MG TABLET	30	0	0	4.29999999999999982	4.29999999999999982	1.96999999999999997	2.33000000000000007	0.541860470000000038	N0	ZYDUS PHARMACEU	0.0476999999999999993
622	2018-08-31	46250617	68382002310	ATENOLOL 50 MG TABLET	30	1.1100000000000001	0	0	1.1100000000000001	1.96999999999999997	-0.859999999999999987	-0.774774769999999946	N0	ZYDUS PHARMACEU	0.0476999999999999993
623	2018-08-09	46245066	68382002310	ATENOLOL 50 MG TABLET	90	0	0	27.9600000000000009	27.9600000000000009	6.58000000000000007	21.379999999999999	0.764663809999999944	N0	ZYDUS PHARMACEU	0.0476999999999999993
624	2018-08-09	46248343	68382002310	ATENOLOL 50 MG TABLET	30	0	0	1.97999999999999998	1.97999999999999998	1.96999999999999997	0.0100000000000000002	0.00505051000000000024	N0	ZYDUS PHARMACEU	0.0476999999999999993
625	2018-08-25	46229826	68382002310	ATENOLOL 50 MG TABLET	30	3.41999999999999993	0	0	3.41999999999999993	1.96999999999999997	1.44999999999999996	0.423976609999999976	N0	ZYDUS PHARMACEU	0.0476999999999999993
626	2018-08-04	46243505	781150610	ATENOLOL 50MG TABLET	90	23.7699999999999996	0	4.19000000000000039	27.9600000000000009	1.76000000000000001	26.1999999999999993	0.937052930000000006	N0	SANDOZ	0.0195
627	2018-08-29	46258043	64980037903	ATOMOXETINE HCL 100 MG CAPS	90	463.259999999999991	0	3.35000000000000009	466.610000000000014	253.560000000000002	213.050000000000011	0.456591159999999996	N0	RISING PHARM	2.81729999999999992
628	2018-08-25	46253785	64980037903	ATOMOXETINE HCL 100 MG CAPS	30	70.6800000000000068	31.3099999999999987	1	102.989999999999995	84.519999999999996	18.4699999999999989	0.179337799999999992	N0	RISING PHARM	2.81729999999999992
629	2018-08-20	46252717	64980037503	ATOMOXETINE HCL 25 MG CAPSU	30	63.3500000000000014	0	0	63.3500000000000014	72.2000000000000028	-8.84999999999999964	-0.139700080000000004	N0	RISING PHARM	2.40660000000000007
630	2018-08-23	46257358	64980037703	ATOMOXETINE HCL 60 MG CAPSU	30	167.819999999999993	0	1.25	169.069999999999993	63.6899999999999977	105.379999999999995	0.623292130000000055	N0	RISING PHARM	2.12300000000000022
631	2018-08-29	46258038	64980037803	ATOMOXETINE HCL 80 MG CAPSU	7	35.6199999999999974	0	0.770000000000000018	36.3900000000000006	19.7199999999999989	16.6700000000000017	0.458092880000000036	N0	RISING PHARM	2.81729999999999992
632	2018-08-27	46228764	60505257808	ATORVASTATIN 10 MG TABLET	90	0	0	29.8500000000000014	29.8500000000000014	4.66999999999999993	25.1799999999999997	0.843551089999999948	N0	APOTEX CORP	0.0519000000000000017
633	2018-08-24	46249873	60505257808	ATORVASTATIN 10 MG TABLET	30	7.16000000000000014	0	0	7.16000000000000014	1.56000000000000005	5.59999999999999964	0.782122909999999893	N0	APOTEX CORP	0.0519000000000000017
634	2018-08-10	46255709	60505257808	ATORVASTATIN 10 MG TABLET	30	0	0	12.5700000000000003	12.5700000000000003	1.56000000000000005	11.0099999999999998	0.87589499000000004	N0	APOTEX CORP	0.0519000000000000017
635	2018-08-27	46251420	60505257808	ATORVASTATIN 10 MG TABLET	30	1.90999999999999992	0	0	1.90999999999999992	1.56000000000000005	0.349999999999999978	0.183246070000000011	N0	APOTEX CORP	0.0519000000000000017
636	2018-08-13	46251797	60505257808	ATORVASTATIN 10 MG TABLET	30	0.309999999999999998	3.35000000000000009	0	3.66000000000000014	1.56000000000000005	2.10000000000000009	0.573770490000000022	N0	APOTEX CORP	0.0519000000000000017
637	2018-08-23	46239246	60505257808	ATORVASTATIN 10 MG TABLET	30	6.09999999999999964	0	0	6.09999999999999964	1.56000000000000005	4.54000000000000004	0.744262300000000043	N0	APOTEX CORP	0.0519000000000000017
638	2018-08-17	46256627	60505257808	ATORVASTATIN 10 MG TABLET	30	5.91000000000000014	0	1.25	7.16000000000000014	1.56000000000000005	5.59999999999999964	0.782122909999999893	N0	APOTEX CORP	0.0519000000000000017
639	2018-08-13	46241647	60505257808	ATORVASTATIN 10 MG TABLET	31	1.95999999999999996	0	0	1.95999999999999996	1.6100000000000001	0.349999999999999978	0.178571430000000003	N0	APOTEX CORP	0.0519000000000000017
640	2018-08-22	46219892	60505257808	ATORVASTATIN 10 MG TABLET	90	0	0	17.4800000000000004	17.4800000000000004	4.66999999999999993	12.8100000000000005	0.73283753000000007	N0	APOTEX CORP	0.0519000000000000017
641	2018-08-16	46246623	60505257808	ATORVASTATIN 10 MG TABLET	90	0	0	8.47000000000000064	8.47000000000000064	4.66999999999999993	3.79999999999999982	0.44864227000000001	N0	APOTEX CORP	0.0519000000000000017
642	2018-08-27	46257785	60505257909	ATORVASTATIN 20 MG TABLET	30	2.20000000000000018	0	0	2.20000000000000018	2.16999999999999993	0.0299999999999999989	0.0136363600000000001	N0	APOTEX CORP	0.0723000000000000032
643	2018-08-08	46255387	60505257909	ATORVASTATIN 20 MG TABLET	30	1.8600000000000001	0	3	4.86000000000000032	2.16999999999999993	2.68999999999999995	0.553497940000000077	N0	APOTEX CORP	0.0723000000000000032
644	2018-08-28	46242511	60505257909	ATORVASTATIN 20 MG TABLET	30	3.41999999999999993	2.68000000000000016	0	6.09999999999999964	2.16999999999999993	3.93000000000000016	0.644262300000000065	N0	APOTEX CORP	0.0723000000000000032
645	2018-08-30	46244283	60505257909	ATORVASTATIN 20 MG TABLET	30	6.04999999999999982	0	1.25	7.29999999999999982	2.16999999999999993	5.12999999999999989	0.702739730000000007	N0	APOTEX CORP	0.0723000000000000032
646	2018-08-11	46255842	60505257909	ATORVASTATIN 20 MG TABLET	30	6	0	0	6	2.16999999999999993	3.83000000000000007	0.638333330000000032	N0	APOTEX CORP	0.0723000000000000032
647	2018-08-04	46232473	60505257909	ATORVASTATIN 20 MG TABLET	90	24.9200000000000017	0	4.38999999999999968	29.3099999999999987	6.50999999999999979	22.8000000000000007	0.777891500000000069	N0	APOTEX CORP	0.0723000000000000032
648	2018-08-16	46248677	60505257909	ATORVASTATIN 20 MG TABLET	30	8.83999999999999986	0	1.25	10.0899999999999999	2.16999999999999993	7.91999999999999993	0.784935579999999966	N0	APOTEX CORP	0.0723000000000000032
649	2018-08-29	46258020	60505257909	ATORVASTATIN 20 MG TABLET	90	20.1099999999999994	0	3.54000000000000004	23.6499999999999986	6.50999999999999979	17.1400000000000006	0.724735730000000022	N0	APOTEX CORP	0.0723000000000000032
650	2018-08-09	46251930	60505257909	ATORVASTATIN 20 MG TABLET	30	0	0	7.29999999999999982	7.29999999999999982	2.16999999999999993	5.12999999999999989	0.702739730000000007	N0	APOTEX CORP	0.0723000000000000032
651	2018-08-06	46251799	60505257909	ATORVASTATIN 20 MG TABLET	30	7.58000000000000007	0	0	7.58000000000000007	2.16999999999999993	5.41000000000000014	0.713720320000000075	N0	APOTEX CORP	0.0723000000000000032
652	2018-08-11	46219588	60505257909	ATORVASTATIN 20 MG TABLET	30	3.12000000000000011	0	10	13.1199999999999992	2.16999999999999993	10.9499999999999993	0.834603659999999969	N0	APOTEX CORP	0.0723000000000000032
653	2018-08-09	46255567	60505257909	ATORVASTATIN 20 MG TABLET	30	9.48000000000000043	0	1.25	10.7300000000000004	2.16999999999999993	8.5600000000000005	0.797763280000000075	N0	APOTEX CORP	0.0723000000000000032
654	2018-08-08	46230302	60505257909	ATORVASTATIN 20 MG TABLET	30	5.15000000000000036	0	3	8.15000000000000036	2.16999999999999993	5.98000000000000043	0.733742329999999998	N0	APOTEX CORP	0.0723000000000000032
655	2018-08-18	46241098	60505257909	ATORVASTATIN 20 MG TABLET	30	2.20000000000000018	0	0	2.20000000000000018	2.16999999999999993	0.0299999999999999989	0.0136363600000000001	N0	APOTEX CORP	0.0723000000000000032
656	2018-08-11	46242368	60505257909	ATORVASTATIN 20 MG TABLET	30	10.0899999999999999	0	0	10.0899999999999999	2.16999999999999993	7.91999999999999993	0.784935579999999966	N0	APOTEX CORP	0.0723000000000000032
657	2018-08-10	46245222	60505257909	ATORVASTATIN 20 MG TABLET	90	20	0	25	45	6.50999999999999979	38.490000000000002	0.855333330000000003	N0	APOTEX CORP	0.0723000000000000032
658	2018-08-24	46257540	60505257909	ATORVASTATIN 20 MG TABLET	90	16.1499999999999986	0	7.5	23.6499999999999986	6.50999999999999979	17.1400000000000006	0.724735730000000022	N0	APOTEX CORP	0.0723000000000000032
659	2018-08-24	46257476	62175089241	ATORVASTATIN 40 MG TABLET	30	10.6099999999999994	0	1.25	11.8599999999999994	3.93999999999999995	7.91999999999999993	0.667790890000000026	N0	KREMERS/LANNETT	0.1313
660	2018-08-15	46253076	62175089241	ATORVASTATIN 40 MG TABLET	30	8.92999999999999972	0	1.25	10.1799999999999997	3.93999999999999995	6.24000000000000021	0.612966600000000028	N0	KREMERS/LANNETT	0.1313
661	2018-08-28	46257852	60505258009	ATORVASTATIN 40 MG TABLET	30	2.89000000000000012	0	0	2.89000000000000012	1.87999999999999989	1.01000000000000001	0.349480969999999946	N0	APOTEX CORP	0.0626000000000000029
662	2018-08-10	46230578	60505258009	ATORVASTATIN 40 MG TABLET	15	2.33000000000000007	0	5.01999999999999957	7.34999999999999964	0.939999999999999947	6.41000000000000014	0.872108839999999885	N0	APOTEX CORP	0.0626000000000000029
663	2018-08-20	46244201	60505258009	ATORVASTATIN 40 MG TABLET	30	8.92999999999999972	0	1.25	10.1799999999999997	1.87999999999999989	8.30000000000000071	0.815324169999999904	N0	APOTEX CORP	0.0626000000000000029
664	2018-08-22	46252764	62175089241	ATORVASTATIN 40 MG TABLET	30	0.380000000000000004	0	10	10.3800000000000008	3.93999999999999995	6.44000000000000039	0.620423889999999978	N0	KREMERS/LANNETT	0.1313
665	2018-08-24	46247597	60505258008	ATORVASTATIN 40 MG TABLET	90	9.63000000000000078	0	0	9.63000000000000078	6.70000000000000018	2.93000000000000016	0.304257529999999998	N0	APOTEX CORP	0.0743999999999999939
666	2018-08-13	46229955	62175089241	ATORVASTATIN 40 MG TABLET	30	6.78000000000000025	0	9	15.7799999999999994	3.93999999999999995	11.8399999999999999	0.750316859999999974	N0	KREMERS/LANNETT	0.1313
667	2018-08-27	46244849	60505258009	ATORVASTATIN 40 MG TABLET	90	0	0	20	20	5.62999999999999989	14.3699999999999992	0.718499999999999917	N0	APOTEX CORP	0.0626000000000000029
668	2018-08-28	46251608	60505258009	ATORVASTATIN 40 MG TABLET	30	6.19000000000000039	0	3	9.1899999999999995	1.87999999999999989	7.30999999999999961	0.795429819999999954	N0	APOTEX CORP	0.0626000000000000029
669	2018-08-13	46252546	62175089241	ATORVASTATIN 40 MG TABLET	30	10.0800000000000001	0	0	10.0800000000000001	3.93999999999999995	6.13999999999999968	0.609126979999999985	N0	KREMERS/LANNETT	0.1313
670	2018-08-31	46233920	62175089241	ATORVASTATIN 40 MG TABLET	30	0.380000000000000004	0	10	10.3800000000000008	3.93999999999999995	6.44000000000000039	0.620423889999999978	N0	KREMERS/LANNETT	0.1313
671	2018-08-15	46247653	60505258008	ATORVASTATIN 40 MG TABLET	30	10.1799999999999997	0	0	10.1799999999999997	2.22999999999999998	7.95000000000000018	0.780943029999999982	N0	APOTEX CORP	0.0743999999999999939
672	2018-08-24	46249471	62175089241	ATORVASTATIN 40 MG TABLET	30	10.6099999999999994	0	1.25	11.8599999999999994	3.93999999999999995	7.91999999999999993	0.667790890000000026	N0	KREMERS/LANNETT	0.1313
673	2018-08-07	46255237	62175089241	ATORVASTATIN 40 MG TABLET	30	6.57000000000000028	0	0	6.57000000000000028	3.93999999999999995	2.62999999999999989	0.400304410000000055	N0	KREMERS/LANNETT	0.1313
674	2018-08-28	46257934	62175089241	ATORVASTATIN 40 MG TABLET	3	6.63999999999999968	0	0	6.63999999999999968	0.390000000000000013	6.25	0.941265060000000098	N0	KREMERS/LANNETT	0.1313
675	2018-08-10	46251617	62175089241	ATORVASTATIN 40 MG TABLET	30	9.63000000000000078	0	0	9.63000000000000078	3.93999999999999995	5.69000000000000039	0.590861890000000001	N0	KREMERS/LANNETT	0.1313
676	2018-08-06	46249788	62175089241	ATORVASTATIN 40 MG TABLET	30	3.91000000000000014	0	1.25	5.16000000000000014	3.93999999999999995	1.21999999999999997	0.236434110000000003	N0	KREMERS/LANNETT	0.1313
677	2018-08-08	46255420	55111012390	ATORVASTATIN 40 MG TABLET	90	10.8900000000000006	0	15	25.8900000000000006	8.69999999999999929	17.1900000000000013	0.663962920000000012	N0	DR.REDDY'S LAB	0.0966999999999999943
678	2018-08-27	46246381	62175089246	ATORVASTATIN 40 MG TABLET	90	11.9800000000000004	0	1.25	13.2300000000000004	55.2700000000000031	-42.0399999999999991	-3.17762660999999991	N0	KREMERS URBAN	0.614099999999999979
679	2018-08-20	46223482	60505267108	ATORVASTATIN 80 MG TABLET	90	0	0	29.6799999999999997	29.6799999999999997	7.95999999999999996	21.7199999999999989	0.731805930000000049	N0	APOTEX CORP	0.0884000000000000064
680	2018-08-13	46251656	60505267108	ATORVASTATIN 80 MG TABLET	90	26.3299999999999983	0	3.35000000000000009	29.6799999999999997	7.95999999999999996	21.7199999999999989	0.731805930000000049	N0	APOTEX CORP	0.0884000000000000064
681	2018-08-15	46249021	69097094712	ATORVASTATIN 80 MG TABLET	15	0	0	2.99000000000000021	2.99000000000000021	1.79000000000000004	1.19999999999999996	0.401337789999999972	N0	CIPLA USA, INC.	0.118999999999999995
682	2018-08-22	46239302	60505267108	ATORVASTATIN 80 MG TABLET	90	34.2100000000000009	0	0	34.2100000000000009	7.95999999999999996	26.25	0.767319499999999932	N0	APOTEX CORP	0.0884000000000000064
683	2018-08-21	46252647	69097094712	ATORVASTATIN 80 MG TABLET	30	3	0	0	3	2.64999999999999991	0.349999999999999978	0.11666667	N0	CIPLA USA, INC.	0.118999999999999995
684	2018-08-30	46257298	60505267108	ATORVASTATIN 80 MG TABLET	90	32.9600000000000009	0	1.25	34.2100000000000009	7.95999999999999996	26.25	0.767319499999999932	N0	APOTEX CORP	0.0884000000000000064
685	2018-08-11	46216213	69097094712	ATORVASTATIN 80 MG TABLET	30	0	0	9.58000000000000007	9.58000000000000007	3.56999999999999984	6.00999999999999979	0.627348640000000013	N0	CIPLA USA, INC.	0.118999999999999995
686	2018-08-07	46255363	60505267108	ATORVASTATIN 80 MG TABLET	90	10.5	0	10	20.5	7.95999999999999996	12.5399999999999991	0.611707320000000054	N0	APOTEX CORP	0.0884000000000000064
687	2018-08-02	46254795	60505267108	ATORVASTATIN 80 MG TABLET	90	0	0	45	45	7.95999999999999996	37.0399999999999991	0.823111109999999924	N0	APOTEX CORP	0.0884000000000000064
688	2018-08-27	46256300	60505267108	ATORVASTATIN 80 MG TABLET	30	8.52999999999999936	0	10	18.5300000000000011	2.64999999999999991	15.8800000000000008	0.856988670000000119	N0	APOTEX CORP	0.0884000000000000064
689	2018-08-02	46240987	69097094712	ATORVASTATIN 80 MG TABLET	30	11.7300000000000004	0	1.25	12.9800000000000004	3.56999999999999984	9.41000000000000014	0.724961480000000047	N0	CIPLA USA, INC.	0.118999999999999995
690	2018-08-24	46243673	60505267108	ATORVASTATIN 80 MG TABLET	90	34.2100000000000009	0	0	34.2100000000000009	7.95999999999999996	26.25	0.767319499999999932	N0	APOTEX CORP	0.0884000000000000064
691	2018-08-13	46224448	69097094712	ATORVASTATIN 80 MG TABLET	90	4.29999999999999982	0	21	25.3000000000000007	10.7100000000000009	14.5899999999999999	0.576679839999999944	N0	CIPLA USA, INC.	0.118999999999999995
692	2018-08-20	46256979	17478021505	ATROPINE 1%  DROPS	5	45.2899999999999991	0	0	45.2899999999999991	46.7100000000000009	-1.41999999999999993	-0.0313534999999999994	N0	AKORN INC.	9.34200000000000053
693	2018-08-10	46255794	17478021502	ATROPINE 1% DROPS	2	30.1400000000000006	0	0	30.1400000000000006	26.1999999999999993	3.93999999999999995	0.130723289999999992	N0	AKORN INC.	13.0999999999999996
694	2018-08-24	46257473	17478021502	ATROPINE 1% DROPS	2	30.1400000000000006	0	0	30.1400000000000006	26.1999999999999993	3.93999999999999995	0.130723289999999992	N0	AKORN INC.	13.0999999999999996
695	2018-08-14	46256257	17478021502	ATROPINE 1% DROPS	2	30.1400000000000006	0	0	30.1400000000000006	26.1999999999999993	3.93999999999999995	0.130723289999999992	N0	AKORN INC.	13.0999999999999996
696	2018-08-06	46255280	17478021502	ATROPINE 1% DROPS	2	30.1400000000000006	0	0	30.1400000000000006	26.1999999999999993	3.93999999999999995	0.130723289999999992	N0	AKORN INC.	13.0999999999999996
697	2018-08-24	46257548	17478021502	ATROPINE 1% DROPS	2	30.1400000000000006	0	0	30.1400000000000006	26.1999999999999993	3.93999999999999995	0.130723289999999992	N0	AKORN INC.	13.0999999999999996
698	2018-08-14	46245726	597008717	ATROVENT HFA INHALER	12.9000000000000004	311.220000000000027	43.3800000000000026	0	354.600000000000023	310.759999999999991	43.8400000000000034	0.123632259999999994	N0	BOEHRINGER ING.	24.0899000000000001
699	2018-08-20	46240064	597008717	ATROVENT HFA INHALER	12.9000000000000004	360.920000000000016	0	0	360.920000000000016	310.759999999999991	50.1599999999999966	0.138978170000000012	N0	BOEHRINGER ING.	24.0899000000000001
700	2018-08-31	46257969	378100501	AZATHIOPRINE 50MG TABLET	90	10.2599999999999998	0	15	25.2600000000000016	37.759999999999998	-12.5	-0.494853519999999991	N0	MYLAN	0.419599999999999973
701	2018-08-24	46257486	51525029403	AZELASTINE 0.1% (137 MCG) S	30	66.9500000000000028	0	8	74.9500000000000028	13.5600000000000005	61.3900000000000006	0.819079389999999963	N0	WALLACE PHARMAC	0.452000000000000013
702	2018-08-10	46255744	51525029403	AZELASTINE 0.1% (137 MCG) S	30	16.4899999999999984	0	0	16.4899999999999984	13.5600000000000005	2.93000000000000016	0.177683439999999998	N0	WALLACE PHARMAC	0.452000000000000013
703	2018-08-27	46257805	51525029403	AZELASTINE 0.1% (137 MCG) S	30	0	0	37.5900000000000034	37.5900000000000034	13.5600000000000005	24.0300000000000011	0.639265760000000016	N0	WALLACE PHARMAC	0.452000000000000013
704	2018-08-08	46255404	60505084805	AZELASTINE 0.15% NASAL SPRA	30	26.870000000000001	0	15	41.8699999999999974	27.370000000000001	14.5	0.346310010000000001	N0	APOTEX CORP	0.912399999999999989
705	2018-08-13	46251404	59762313001	AZITHROMYCIN 200 MG/5 ML SU	22.5	17.370000000000001	0	0	17.370000000000001	9.50999999999999979	7.86000000000000032	0.452504320000000015	N0	GREENSTONE LLC.	0.422599999999999976
706	2018-08-20	46247408	59762312001	AZITHROMYCIN 200 MG/5 ML SU	15	7.70000000000000018	0	0	7.70000000000000018	6.95000000000000018	0.75	0.097402599999999992	N0	GREENSTONE LLC.	0.463200000000000001
707	2018-08-09	46247408	59762312001	AZITHROMYCIN 200 MG/5 ML SU	15	7.70000000000000018	0	0	7.70000000000000018	6.95000000000000018	0.75	0.097402599999999992	N0	GREENSTONE LLC.	0.463200000000000001
708	2018-08-03	46251404	59762313001	AZITHROMYCIN 200 MG/5 ML SU	22.5	17.370000000000001	0	0	17.370000000000001	9.50999999999999979	7.86000000000000032	0.452504320000000015	N0	GREENSTONE LLC.	0.422599999999999976
709	2018-08-05	46255086	59762306002	AZITHROMYCIN 250 MG TABLET	3	8.57000000000000028	0	0	8.57000000000000028	2.49000000000000021	6.08000000000000007	0.709451580000000082	N0	GREENSTONE LLC.	0.830600000000000005
710	2018-08-02	46254734	68180016013	AZITHROMYCIN 250 MG TABLET	6	4.45000000000000018	0	0	4.45000000000000018	1.6399999999999999	2.81000000000000005	0.631460670000000057	N0	LUPIN PHARMACEU	0.272699999999999998
711	2018-08-28	46257881	68180016013	AZITHROMYCIN 250 MG TABLET	6	4.76999999999999957	0	0	4.76999999999999957	1.6399999999999999	3.12999999999999989	0.656184490000000009	N0	LUPIN PHARMACEU	0.272699999999999998
712	2018-08-31	46258339	68180016013	AZITHROMYCIN 250 MG TABLET	6	0	0	10	10	1.6399999999999999	8.35999999999999943	0.835999999999999965	N0	LUPIN PHARMACEU	0.272699999999999998
713	2018-08-13	46256103	59762306002	AZITHROMYCIN 250 MG TABLET	4	10.0899999999999999	0	0	10.0899999999999999	3.31999999999999984	6.76999999999999957	0.670961350000000012	N0	GREENSTONE LLC.	0.830600000000000005
714	2018-08-20	46253930	68180016006	AZITHROMYCIN 250 MG TABLET	30	10.2400000000000002	0	0	10.2400000000000002	19.9499999999999993	-9.71000000000000085	-0.948242189999999985	N0	LUPIN PHARMACEU	0.665000000000000036
715	2018-08-03	46254950	68180016013	AZITHROMYCIN 250 MG TABLET	6	0	0	7	7	1.6399999999999999	5.36000000000000032	0.765714289999999909	N0	LUPIN PHARMACEU	0.272699999999999998
716	2018-08-01	46254667	68180016006	AZITHROMYCIN 250 MG TABLET	4	6.98000000000000043	0	3.35000000000000009	10.3300000000000001	2.66000000000000014	7.66999999999999993	0.74249757999999999	N0	LUPIN PHARMACEU	0.665000000000000036
717	2018-08-08	46255450	68180016013	AZITHROMYCIN 250 MG TABLET	6	0	0	3.68999999999999995	3.68999999999999995	1.6399999999999999	2.04999999999999982	0.555555560000000059	N0	LUPIN PHARMACEU	0.272699999999999998
718	2018-08-14	46256279	68180016006	AZITHROMYCIN 250 MG TABLET	4	5.16000000000000014	0	0	5.16000000000000014	2.66000000000000014	2.5	0.48449612000000003	N0	LUPIN PHARMACEU	0.665000000000000036
719	2018-08-29	46257984	68180016013	AZITHROMYCIN 250 MG TABLET	6	0	0	3.89999999999999991	3.89999999999999991	1.6399999999999999	2.25999999999999979	0.579487179999999991	N0	LUPIN PHARMACEU	0.272699999999999998
720	2018-08-17	46256702	59762306001	AZITHROMYCIN 250MG PAK	6	5.63999999999999968	0	0	5.63999999999999968	2	3.64000000000000012	0.645390069999999927	N0	GREENSTONE LLC.	0.333799999999999986
721	2018-08-23	46257234	68180016106	AZITHROMYCIN 500 MG TABLET	2	0	0	3.18999999999999995	3.18999999999999995	2.79999999999999982	0.390000000000000013	0.122257049999999992	N0	LUPIN PHARMACEU	1.4003000000000001
722	2018-08-02	46243394	65027510	AZOPT 1% EYE DROPS	10	181.72999999999999	0	97.8400000000000034	279.569999999999993	271.810000000000002	7.75999999999999979	0.0277569100000000027	N0	ALCON/NOVARTIS	26.8554999999999993
723	2018-08-11	46249935	65027510	AZOPT 1% EYE DROPS	10	120.959999999999994	0	141	261.95999999999998	268.560000000000002	-6.59999999999999964	-0.0251946899999999988	N0	ALCON/NOVARTIS	26.8554999999999993
724	2018-08-29	46250517	6551071950	B-12 1000MCG METHYLCOB QD-T	60	0	0	8.73000000000000043	8.73000000000000043	5.45999999999999996	3.27000000000000002	0.374570449999999999	N0		0.0909999999999999976
725	2018-08-03	46238856	6551001380	B-12 1000MCG TABLET	90	0	0	8.44999999999999929	8.44999999999999929	4.70999999999999996	3.74000000000000021	0.442603549999999957	N0	NATURE'S BOUNTY	0.0522999999999999993
726	2018-08-30	46258154	6551001380	B-12 1000MCG TABLET	30	0	0	4.15000000000000036	4.15000000000000036	1.57000000000000006	2.58000000000000007	0.621686749999999955	N0	NATURE'S BOUNTY	0.0522999999999999993
727	2018-08-09	46255705	74312013700	B-12 500MCG TABLET	150	0	0	11.5199999999999996	11.5199999999999996	7.41000000000000014	4.11000000000000032	0.356770830000000039	N0		0.0493999999999999995
728	2018-08-19	46256819	74312003595	B-12 500MCG TABLET SL	5	2.20000000000000018	0	0	2.20000000000000018	0.190000000000000002	2.00999999999999979	0.913636360000000036	N0	NATURE'S BOUNTY	0.0374999999999999986
729	2018-08-16	46239201	63948056231	B-12 ACTIVE SL TAB1000MCG	120	0	0	26	26	24	2	0.0769230799999999909	N0		0.200000000000000011
730	2018-08-29	46257981	8290309581	B-D 3ML SYRINGE 25GX1	10	0	0	1.84000000000000008	1.84000000000000008	1.12000000000000011	0.719999999999999973	0.391304349999999967	N0	BD HOSPITAL DIV	0.111600000000000005
731	2018-08-09	46244981	66298009377	B12 LIQUID 1,000MCG/ML -PUR	30	0	0	16	16	14	2	0.125	N	PURE	0.466700000000000004
732	2018-08-01	46254697	66298009377	B12 LIQUID 1,000MCG/ML -PUR	60	0	0	34.2000000000000028	34.2000000000000028	28	6.20000000000000018	0.181286549999999991	N0	PURE	0.466700000000000004
733	2018-08-09	46255588	66298009377	B12 LIQUID 1,000MCG/ML -PUR	30	0	0	17.1000000000000014	17.1000000000000014	14	3.10000000000000009	0.181286549999999991	N	PURE	0.466700000000000004
734	2018-08-06	46255172	45802006001	BACITRACIN  OINT	14.1999999999999993	5.05999999999999961	0	0	5.05999999999999961	0	5.05999999999999961	1	N0	PERRIGO CO.	0
735	2018-08-27	46257712	45802006001	BACITRACIN  OINT	14	5.05999999999999961	0	0	5.05999999999999961	0	5.05999999999999961	1	N0	PERRIGO CO.	0
736	2018-08-20	46255172	45802006001	BACITRACIN  OINT	14.1999999999999993	5.05999999999999961	0	0	5.05999999999999961	0	5.05999999999999961	1	N0	PERRIGO CO.	0
737	2018-08-22	48012442	38779038805	BACLOFEN (GT)5MG/ML SUSP	135	8.67999999999999972	0	0	8.67999999999999972	1.39999999999999991	7.28000000000000025	0.838709680000000013	N0		0
738	2018-08-14	46256260	527133010	BACLOFEN 10 MG TABLET	60	0	0	14.9299999999999997	14.9299999999999997	4.00999999999999979	10.9199999999999999	0.731413260000000065	N0	LANNETT CO. INC	0.0667999999999999983
739	2018-08-03	46247536	527133010	BACLOFEN 10 MG TABLET	90	9.57000000000000028	0	0	9.57000000000000028	8.63000000000000078	0.939999999999999947	0.0982236199999999976	N0	LANNETT CO. INC	0.0667999999999999983
740	2018-08-03	46250972	527133010	BACLOFEN 10 MG TABLET	120	10.3300000000000001	0	0	10.3300000000000001	8.01999999999999957	2.31000000000000005	0.223620519999999989	N0	LANNETT CO. INC	0.0667999999999999983
741	2018-08-29	46258126	527133010	BACLOFEN 10 MG TABLET	90	22.1400000000000006	0	0	22.1400000000000006	6.00999999999999979	16.129999999999999	0.728545620000000005	N0	LANNETT CO. INC	0.0667999999999999983
742	2018-08-03	46243795	527133010	BACLOFEN 10 MG TABLET	101	8.91999999999999993	0	0	8.91999999999999993	6.75	2.16999999999999993	0.24327354000000001	N0	LANNETT CO. INC	0.0667999999999999983
743	2018-08-28	46254551	527133010	BACLOFEN 10 MG TABLET	60	10	0	0	10	4.00999999999999979	5.99000000000000021	0.598999999999999977	N0	LANNETT CO. INC	0.0667999999999999983
744	2018-08-22	46257174	527133010	BACLOFEN 10 MG TABLET	23	15.4100000000000001	0	0	15.4100000000000001	1.54000000000000004	13.8699999999999992	0.900064890000000006	N0	LANNETT CO. INC	0.0667999999999999983
745	2018-08-21	46257017	172409660	BACLOFEN 10MG TABLET	6	6.98000000000000043	0	0	6.98000000000000043	0.469999999999999973	6.50999999999999979	0.932664759999999982	N0	TEVA USA	0.0777000000000000052
746	2018-08-08	46255455	172409660	BACLOFEN 10MG TABLET	15	11.4399999999999995	0	0	11.4399999999999995	2.06999999999999984	9.36999999999999922	0.819055939999999927	N0	TEVA USA	0.0777000000000000052
747	2018-08-06	46244508	172409660	BACLOFEN 10MG TABLET	45	4.08999999999999986	0	0	4.08999999999999986	3.5	0.589999999999999969	0.144254280000000012	N0	TEVA USA	0.0777000000000000052
748	2018-08-16	46247484	172409660	BACLOFEN 10MG TABLET	45	26.3200000000000003	0	0	26.3200000000000003	3.5	22.8200000000000003	0.867021280000000005	N0	TEVA USA	0.0777000000000000052
749	2018-08-22	46257017	172409660	BACLOFEN 10MG TABLET	30	18.879999999999999	0	0	18.879999999999999	2.33000000000000007	16.5500000000000007	0.876588979999999962	N0	TEVA USA	0.0777000000000000052
750	2018-08-23	46257362	172409660	BACLOFEN 10MG TABLET	270	21.8099999999999987	0	1	22.8099999999999987	20.9800000000000004	1.83000000000000007	0.0802279700000000096	N0	TEVA USA	0.0777000000000000052
751	2018-08-15	48011459	38779038805	BACLOFEN 10MG/ML SUSP	45	20.3200000000000003	0	0	20.3200000000000003	1.97999999999999998	18.3399999999999999	0.902559059999999969	N0		0
752	2018-08-29	48011671	51927180000	BACLOFEN 10MG/ML SUSP.	100	38.7100000000000009	0	0	38.7100000000000009	4.20999999999999996	34.5	0.891242569999999956	N0		0
756	2018-08-06	46255275	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
757	2018-08-07	46255496	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
758	2018-08-22	46257323	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
759	2018-08-31	46258499	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
760	2018-08-07	46255504	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
761	2018-08-14	46256321	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
762	2018-08-27	46257870	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
763	2018-08-03	46255040	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
764	2018-08-13	46256171	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
765	2018-08-25	46257722	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
766	2018-08-08	46255596	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
767	2018-08-02	46254884	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
768	2018-08-21	46257166	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
769	2018-08-07	46255503	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
770	2018-08-17	46256891	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
771	2018-08-20	46257061	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
772	2018-08-16	46256659	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
773	2018-08-12	46256041	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
774	2018-08-08	46255595	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
775	2018-08-23	46257515	409196612	BACTERIOSTATIC SALINE VIAL	20	6.95999999999999996	0	0	6.95999999999999996	2.45999999999999996	4.5	0.646551719999999941	N0	HOSPIRA/PFIZER	0.122999999999999998
776	2018-08-22	46257320	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
777	2018-08-17	46256890	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
778	2018-08-11	46256008	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
779	2018-08-03	46255047	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
780	2018-08-13	46256187	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
781	2018-08-21	46257165	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
782	2018-08-01	46254809	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
783	2018-08-28	46257437	409196612	BACTERIOSTATIC SALINE VIAL	10	0	0	6.12999999999999989	6.12999999999999989	1.22999999999999998	4.90000000000000036	0.79934747000000006	N	HOSPIRA/PFIZER	0.122999999999999998
784	2018-08-24	46257590	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
785	2018-08-13	46256169	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
786	2018-08-25	46257720	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
787	2018-08-10	46256014	409196612	BACTERIOSTATIC SALINE VIAL	10	3.74000000000000021	0	0	3.74000000000000021	1.22999999999999998	2.50999999999999979	0.671122989999999975	N0	HOSPIRA/PFIZER	0.122999999999999998
788	2018-08-18	46256877	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
789	2018-08-30	46258298	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
790	2018-08-07	46255501	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
791	2018-08-15	46256514	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
792	2018-08-25	46257719	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
793	2018-08-12	46256054	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
794	2018-08-23	46257502	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
795	2018-08-19	46256914	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
796	2018-08-08	46255593	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
797	2018-08-30	46258307	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
798	2018-08-29	46258157	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
799	2018-08-17	46256900	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
800	2018-08-13	46256184	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
801	2018-08-01	46254807	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
802	2018-08-24	46257598	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
803	2018-08-04	46255103	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
804	2018-08-24	46257588	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
805	2018-08-19	46256913	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
806	2018-08-08	46255590	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
807	2018-08-06	46255281	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
808	2018-08-21	46257172	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
809	2018-08-05	46255159	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
810	2018-08-22	46257329	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
811	2018-08-14	46256327	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
812	2018-08-29	46258167	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
813	2018-08-13	46256182	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
814	2018-08-16	46256665	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
815	2018-08-01	46254805	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
816	2018-08-04	46255101	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
817	2018-08-30	46258305	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
818	2018-08-29	46258155	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
819	2018-08-14	46256341	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
820	2018-08-28	46258030	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
821	2018-08-18	46256886	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
822	2018-08-02	46254909	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
823	2018-08-27	46257865	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
824	2018-08-22	46257328	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
825	2018-08-27	46257874	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
826	2018-08-03	46255043	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
827	2018-08-28	46258029	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
828	2018-08-16	46256663	409196612	BACTERIOSTATIC SALINE VIAL	1	4.67999999999999972	0	0	4.67999999999999972	0.119999999999999996	4.55999999999999961	0.974358969999999935	N0	HOSPIRA/PFIZER	0.122999999999999998
829	2018-08-06	46255279	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
830	2018-08-01	46254803	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
831	2018-08-15	46256510	409196612	BACTERIOSTATIC SALINE VIAL	20	6.95999999999999996	0	0	6.95999999999999996	2.45999999999999996	4.5	0.646551719999999941	N0	HOSPIRA/PFIZER	0.122999999999999998
832	2018-08-31	46258504	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
833	2018-08-15	46256523	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
834	2018-08-25	46257640	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
835	2018-08-10	46256006	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
836	2018-08-01	46254802	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
837	2018-08-22	46257313	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
838	2018-08-21	46257170	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
839	2018-08-22	46257326	409196612	BACTERIOSTATIC SALINE VIAL	1	4.67999999999999972	0	0	4.67999999999999972	0.119999999999999996	4.55999999999999961	0.974358969999999935	N0	HOSPIRA/PFIZER	0.122999999999999998
840	2018-08-15	46256522	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
841	2018-08-24	46257594	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
842	2018-08-11	46256015	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
843	2018-08-26	46257726	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
844	2018-08-16	46256650	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
845	2018-08-19	46256908	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
846	2018-08-06	46255278	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
847	2018-08-24	46257585	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
848	2018-08-26	46257725	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
849	2018-08-12	46256027	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
850	2018-08-23	46257506	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
851	2018-08-21	46257169	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
852	2018-08-05	46255154	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
853	2018-08-14	46256323	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
854	2018-08-15	46256519	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
855	2018-08-26	46257724	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
856	2018-08-17	46256907	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
857	2018-08-28	46258027	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
858	2018-08-10	46256004	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
859	2018-08-24	46257583	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
860	2018-08-16	46256648	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
861	2018-08-20	46257062	409196612	BACTERIOSTATIC SALINE VIAL	10	5.48000000000000043	0	0	5.48000000000000043	1.22999999999999998	4.25	0.775547449999999916	N0	HOSPIRA/PFIZER	0.122999999999999998
862	2018-08-22	46257210	8137005566	BAND AID WATERPROOF TOUGH S	10	0	0	4.16999999999999993	4.16999999999999993	1.90999999999999992	2.25999999999999979	0.541966429999999999	N0		0.191000000000000003
863	2018-08-13	46255982	8137004670	BAND-AID CLEAR ADH BANDAG	30	0	0	3.9700000000000002	3.9700000000000002	1.59000000000000008	2.37999999999999989	0.599496220000000024	N0	J&J CONS PROD	0.0529999999999999985
864	2018-08-02	46243382	62856058352	BANZEL 400 MG TABLET	150	3247.55000000000018	0	0	3247.55000000000018	2687.84999999999991	559.700000000000045	0.172345310000000002	N0	EISAI INC.	17.9190000000000005
865	2018-08-18	46236577	2771559	BASAGLAR 100 UNIT/ML KWIKPE	15	323.439999999999998	0	0	323.439999999999998	313.680000000000007	9.75999999999999979	0.030175610000000002	N0	ELI LILLY & CO.	20.911999999999999
866	2018-08-04	46255059	2771559	BASAGLAR 100 UNIT/ML KWIKPE	9	193.680000000000007	0	0	193.680000000000007	188.210000000000008	5.46999999999999975	0.0282424600000000005	N0	ELI LILLY & CO.	20.911999999999999
867	2018-08-28	46257837	2771559	BASAGLAR 100 UNIT/ML KWIKPE	3	64.9699999999999989	0	0	64.9699999999999989	62.740000000000002	2.22999999999999998	0.0343235299999999979	N0	ELI LILLY & CO.	20.911999999999999
868	2018-08-02	46254766	2771559	BASAGLAR 100 UNIT/ML KWIKPE	15	326.649999999999977	0	3.70000000000000018	330.350000000000023	313.680000000000007	16.6700000000000017	0.0504616300000000004	N0	ELI LILLY & CO.	20.911999999999999
869	2018-08-28	46230046	2771559	BASAGLAR 100 UNIT/ML KWIKPE	6	129.590000000000003	0	0	129.590000000000003	125.469999999999999	4.12000000000000011	0.0317925800000000008	N0	ELI LILLY & CO.	20.911999999999999
870	2018-08-13	46235161	2771559	BASAGLAR 100 UNIT/ML KWIKPE	21	450.589999999999975	0	0	450.589999999999975	439.149999999999977	11.4399999999999995	0.0253889299999999971	N0	ELI LILLY & CO.	20.911999999999999
871	2018-08-08	46252287	2771559	BASAGLAR 100 UNIT/ML KWIKPE	30	646.539999999999964	0	0	646.539999999999964	627.360000000000014	19.1799999999999997	0.0296656000000000003	N0	ELI LILLY & CO.	20.911999999999999
872	2018-08-29	46232566	12843013105	BAYER CHILDREN'S ASPIRIN	36	0	0	4.48000000000000043	4.48000000000000043	1.97999999999999998	2.5	0.558035709999999963	N0	BAYER INC.	0.0550000000000000003
873	2018-08-31	46258360	11701004514	BAZA ANTIFUNGAL 2%	142	10.2200000000000006	0	0	10.2200000000000006	10.1500000000000004	0.0700000000000000067	0.00684931999999999989	N0	COLOPLAST/SWEEN	0.0714999999999999941
874	2018-08-15	46231847	11701004514	BAZA ANTIFUNGAL 2%	142	10.2200000000000006	0	0	10.2200000000000006	10.1500000000000004	0.0700000000000000067	0.00684931999999999989	N0	COLOPLAST/SWEEN	0.0714999999999999941
875	2018-08-10	46255827	8290309572	BD 3 ML SYRINGE WITH NEEDLE	50	0	0	10.6600000000000001	10.6600000000000001	6.58000000000000007	4.08000000000000007	0.382739209999999996	N0	BD MEDICAL SURG	0.131500000000000006
876	2018-08-20	46253678	8290328418	BD INSULIN SYR 1 ML 31GX5/1	60	16.4699999999999989	0	0	16.4699999999999989	12.6500000000000004	3.81999999999999984	0.231936849999999972	y1	BD DIABETES	0.210900000000000004
877	2018-08-07	46215583	8290328418	BD INSULIN SYR 1 ML 31GX5/1	30	8.5600000000000005	0	0	8.5600000000000005	6.33000000000000007	2.22999999999999998	0.260514020000000013	N0	BD DIABETES	0.210900000000000004
878	2018-08-22	46236083	8290236712	BD LACTINEX	15	14.8699999999999992	0	0	14.8699999999999992	13.5	1.37000000000000011	0.0921318100000000084	Y1	BD NON-MEDICAL	0.900000000000000022
879	2018-08-22	46232684	8290236712	BD LACTINEX	15	14.8699999999999992	0	0	14.8699999999999992	13.5	1.37000000000000011	0.0921318100000000084	Y1	BD NON-MEDICAL	0.900000000000000022
880	2018-08-15	46236083	8290236712	BD LACTINEX	15	14.8699999999999992	0	0	14.8699999999999992	13.5	1.37000000000000011	0.0921318100000000084	Y1	BD NON-MEDICAL	0.900000000000000022
881	2018-08-21	46254961	8290320119	BD PEN NEEDLE MINI 31GX3/16	100	41.3699999999999974	0	0	41.3699999999999974	30.9899999999999984	10.3800000000000008	0.250906450000000003	N0	BD DIABETES	0.309900000000000009
882	2018-08-03	46216662	8290320119	BD PEN NEEDLE MINI 31GX3/16	100	41.3699999999999974	0	0	41.3699999999999974	30.9899999999999984	10.3800000000000008	0.250906450000000003	N0	BD DIABETES	0.309900000000000009
883	2018-08-28	46231730	8290320119	BD PEN NEEDLE MINI 31GX3/16	30	12.3900000000000006	0	0	12.3900000000000006	9.30000000000000071	3.08999999999999986	0.249394670000000013	N0	BD DIABETES	0.309900000000000009
884	2018-08-16	46256592	8290320119	BD PEN NEEDLE MINI 31GX3/16	400	116.709999999999994	0	38.9099999999999966	155.620000000000005	123.959999999999994	31.6600000000000001	0.203444290000000028	N0	BD DIABETES	0.309900000000000009
885	2018-08-15	46243171	8290320122	BD PEN NEEDLE NANO 32GX5/32	400	170.909999999999997	0	0	170.909999999999997	154.47999999999999	16.4299999999999997	0.0961324699999999976	Y1	BD DIABETES	0.386199999999999988
886	2018-08-17	46247636	8290320122	BD PEN NEEDLE NANO 32GX5/32	150	64.5799999999999983	0	0	64.5799999999999983	57.9299999999999997	6.65000000000000036	0.102973060000000005	N0	BD DIABETES	0.386199999999999988
887	2018-08-08	46230972	8290320122	BD PEN NEEDLE NANO 32GX5/32	34	14.9600000000000009	0	0	14.9600000000000009	13.1300000000000008	1.83000000000000007	0.12232620000000001	N0	BD DIABETES	0.386199999999999988
888	2018-08-07	46255292	8290320122	BD PEN NEEDLE NANO 32GX5/32	120	52.2700000000000031	0	0	52.2700000000000031	46.3400000000000034	5.92999999999999972	0.113449399999999992	N0	BD DIABETES	0.386199999999999988
889	2018-08-09	46255650	8290320122	BD PEN NEEDLE NANO 32GX5/32	100	43.3299999999999983	0	0	43.3299999999999983	38.6199999999999974	4.70999999999999996	0.108700669999999999	N0	BD DIABETES	0.386199999999999988
890	2018-08-04	46255057	8290320122	BD PEN NEEDLE NANO 32GX5/32	200	86.4399999999999977	0	0	86.4399999999999977	77.2399999999999949	9.19999999999999929	0.106432209999999999	Y1	BD DIABETES	0.386199999999999988
891	2018-08-10	46255815	8290320122	BD PEN NEEDLE NANO 32GX5/32	100	43.3299999999999983	0	0	43.3299999999999983	38.6199999999999974	4.70999999999999996	0.108700669999999999	N0	BD DIABETES	0.386199999999999988
892	2018-08-08	46255514	8290320122	BD PEN NEEDLE NANO 32GX5/32	100	43.2199999999999989	0	0	43.2199999999999989	38.6199999999999974	4.59999999999999964	0.106432209999999999	N0	BD DIABETES	0.386199999999999988
893	2018-08-20	46251468	8290320122	BD PEN NEEDLE NANO 32GX5/32	30	12.8599999999999994	0	1.25	14.1099999999999994	11.5899999999999999	2.52000000000000002	0.178596739999999976	N0	BD DIABETES	0.386199999999999988
894	2018-08-03	46254986	8290328203	BD PEN NEEDLE ORIG 29GX1/2	180	78.4500000000000028	0	0	78.4500000000000028	32.7100000000000009	45.740000000000002	0.583046530000000063	N0	BD DIABETES	0.1817
895	2018-08-28	46257947	8290328438	BD UF INS SYR 0.3 ML 31GX5/	100	32	0	0	32	23.5300000000000011	8.47000000000000064	0.26468750000000002	N0	BD DIABETES	0.235300000000000009
896	2018-08-02	46226123	8290328466	BD UF INS SYR 0.5 ML 30GX1/	150	40.4200000000000017	0	0	40.4200000000000017	37.0399999999999991	3.37999999999999989	0.0836219700000000038	N0	BD DIABETES	0.246900000000000008
897	2018-08-01	46254717	8290328468	BD UF INS SYR 0.5 ML 31GX5/	150	40.4200000000000017	0	0	40.4200000000000017	36.6199999999999974	3.79999999999999982	0.0940128600000000036	N0	BD DIABETES	0.244100000000000011
898	2018-08-08	46235974	8290328411	BD UF INS SYR 1 ML 30GX1/2	120	28.7300000000000004	0	3.70000000000000018	32.4299999999999997	29.629999999999999	2.79999999999999982	0.0863398100000000029	N0	BD DIABETES	0.246900000000000008
899	2018-08-13	46251949	8290320119	BD UF MINI PEN 31GX3 NEEDLE	30	12.9600000000000009	0	0	12.9600000000000009	9.30000000000000071	3.66000000000000014	0.282407410000000025	N0	BD DIABETES	0.309900000000000009
900	2018-08-08	46226377	8290320119	BD UF MINI PEN 31GX3 NEEDLE	100	30.879999999999999	0	8.34999999999999964	39.2299999999999969	30.9899999999999984	8.24000000000000021	0.21004333	N0	BD DIABETES	0.309900000000000009
901	2018-08-28	46251949	8290320119	BD UF MINI PEN 31GX3 NEEDLE	30	12.9600000000000009	0	0	12.9600000000000009	9.30000000000000071	3.66000000000000014	0.282407410000000025	N0	BD DIABETES	0.309900000000000009
902	2018-08-28	46257963	8290320119	BD UF MINI PEN 31GX3 NEEDLE	100	40.3800000000000026	0	0	40.3800000000000026	30.9899999999999984	9.39000000000000057	0.232540860000000016	N0	BD DIABETES	0.309900000000000009
903	2018-08-22	46257123	8290320119	BD UF MINI PEN NEEDLE 31GX3	60	0	0	25.0399999999999991	25.0399999999999991	18.5899999999999999	6.45000000000000018	0.257587860000000002	N0	BD DIABETES	0.309900000000000009
904	2018-08-29	46258047	8290320119	BD UF MINI PEN NEEDLE 31GX3	100	39.2100000000000009	0	1.25	40.4600000000000009	30.9899999999999984	9.47000000000000064	0.234058330000000009	N0	BD DIABETES	0.309900000000000009
905	2018-08-03	46244296	8290320109	BD UF SHORT PEN #3201 31GX8	90	20.629999999999999	0	16.1999999999999993	36.8299999999999983	30.629999999999999	6.20000000000000018	0.168341030000000003	N0	BD DIABETES	0.340299999999999991
906	2018-08-20	46229727	8290320109	BD UF SHORT PEN #3201 31GX8	100	0	0	40.9600000000000009	40.9600000000000009	34.0300000000000011	6.92999999999999972	0.169189450000000019	N0	BD DIABETES	0.340299999999999991
907	2018-08-06	46230139	8290320109	BD UF SHORT PEN #3201 31GX8	30	13.0199999999999996	0	0	13.0199999999999996	10.2100000000000009	2.81000000000000005	0.215821809999999975	N0	BD DIABETES	0.340299999999999991
908	2018-08-15	46256371	8290320109	BD UF SHORT PEN #3201 31GX8	100	35.5600000000000023	0	6.26999999999999957	41.8299999999999983	34.0300000000000011	7.79999999999999982	0.186469040000000003	N0	BD DIABETES	0.340299999999999991
909	2018-08-22	46245353	8290320109	BD UF SHORT PEN #3201 31GX8	100	39.8900000000000006	0	0	39.8900000000000006	34.0300000000000011	5.86000000000000032	0.146903989999999984	N0	BD DIABETES	0.340299999999999991
910	2018-08-01	42055457	574704012	BELLADONNA & OPIUM SUPP60MG	4	117.739999999999995	0	0	117.739999999999995	99.0100000000000051	18.7300000000000004	0.159079329999999991	N0	PERRIGO CO.	24.7515999999999998
911	2018-08-30	42056143	574704512	BELLADONNA-OPIUM 16.2-30 SU	12	0	0	274	274	244.300000000000011	29.6999999999999993	0.108394160000000003	N0	PERRIGO CO.	20.3582999999999998
912	2018-08-06	44031205	6033530	BELSOMRA 20 MG TABLET	30	246.650000000000006	0	70	316.649999999999977	281.310000000000002	35.3400000000000034	0.111605869999999996	N0	MERCK SHARP & D	9.37700000000000067
913	2018-08-15	46256318	43547033610	BENAZEPRIL HCL 10 MG TABLET	30	3.64000000000000012	0	5	8.64000000000000057	1.15999999999999992	7.48000000000000043	0.865740740000000009	N0	SOLCO HEALTHCAR	0.0384999999999999995
914	2018-08-02	46254731	43547033710	BENAZEPRIL HCL 20 MG TABLET	90	0	0	28.2399999999999984	28.2399999999999984	3.14999999999999991	25.0899999999999999	0.888456089999999921	N0	SOLCO HEALTHCAR	0.0350000000000000033
915	2018-08-20	46243903	65597010390	BENICAR 20MG TABLET	30	0	147.080000000000013	62.1000000000000014	209.180000000000007	207.310000000000002	1.87000000000000011	0.00893967000000000026	Y1	DAIICHI SANKYO,	6.91019999999999968
916	2018-08-02	46254774	67877057305	BENZONATATE 100 MG CAPSULE	30	18.879999999999999	0	0	18.879999999999999	3.29999999999999982	15.5800000000000001	0.825211860000000019	N0	ASCEND LABORATO	0.110000000000000001
917	2018-08-13	46254576	67877057305	BENZONATATE 100 MG CAPSULE	45	26.3200000000000003	0	0	26.3200000000000003	4.95000000000000018	21.370000000000001	0.811930090000000049	N0	ASCEND LABORATO	0.110000000000000001
918	2018-08-27	46257799	67877057305	BENZONATATE 100 MG CAPSULE	90	21.3500000000000014	0	0	21.3500000000000014	9.90000000000000036	11.4499999999999993	0.536299769999999953	N0	ASCEND LABORATO	0.110000000000000001
919	2018-08-27	46254576	67877057305	BENZONATATE 100 MG CAPSULE	45	26.3200000000000003	0	0	26.3200000000000003	4.95000000000000018	21.370000000000001	0.811930090000000049	N0	ASCEND LABORATO	0.110000000000000001
920	2018-08-02	46254850	67877057305	BENZONATATE 100 MG CAPSULE	20	13.9199999999999999	0	0	13.9199999999999999	2.20000000000000018	11.7200000000000006	0.84195401999999997	N0	ASCEND LABORATO	0.110000000000000001
921	2018-08-16	46254366	67877057305	BENZONATATE 100 MG CAPSULE	45	26.3200000000000003	0	0	26.3200000000000003	4.95000000000000018	21.370000000000001	0.811930090000000049	N0	ASCEND LABORATO	0.110000000000000001
922	2018-08-24	46255838	67877057305	BENZONATATE 100 MG CAPSULE	45	26.3200000000000003	0	0	26.3200000000000003	4.95000000000000018	21.370000000000001	0.811930090000000049	N0	ASCEND LABORATO	0.110000000000000001
923	2018-08-10	46255838	67877057305	BENZONATATE 100 MG CAPSULE	45	26.3200000000000003	0	0	26.3200000000000003	4.95000000000000018	21.370000000000001	0.811930090000000049	N0	ASCEND LABORATO	0.110000000000000001
924	2018-08-27	46256633	67877057305	BENZONATATE 100 MG CAPSULE	45	26.3200000000000003	0	0	26.3200000000000003	4.95000000000000018	21.370000000000001	0.811930090000000049	N0	ASCEND LABORATO	0.110000000000000001
925	2018-08-06	46255197	65162053650	BENZONATATE 100 MG CAPSULE	30	7.78000000000000025	0	0	7.78000000000000025	4.48000000000000043	3.29999999999999982	0.424164519999999989	N0	AMNEAL PHARMACE	0.149400000000000005
926	2018-08-17	46256633	67877057305	BENZONATATE 100 MG CAPSULE	45	26.3200000000000003	0	0	26.3200000000000003	4.95000000000000018	21.370000000000001	0.811930090000000049	N0	ASCEND LABORATO	0.110000000000000001
927	2018-08-07	46253723	65162053650	BENZONATATE 100 MG CAPSULE	45	26.3200000000000003	0	0	26.3200000000000003	6.71999999999999975	19.6000000000000014	0.744680850000000061	N0	AMNEAL PHARMACE	0.149400000000000005
928	2018-08-22	46254864	65162053710	BENZONATATE 200 MG CAPSULE	30	22.9899999999999984	0	0	22.9899999999999984	10.2799999999999994	12.7100000000000009	0.552849060000000003	N0	AMNEAL PHARMACE	0.342500000000000027
929	2018-08-22	46237923	65162053710	BENZONATATE 200 MG CAPSULE	100	0	25.1999999999999993	6	31.1999999999999993	34.25	-3.04999999999999982	-0.0977564100000000019	N0	AMNEAL PHARMACE	0.342500000000000027
930	2018-08-27	46257804	67877057501	BENZONATATE 200 MG CAPSULE	45	14.0700000000000003	0	0	14.0700000000000003	8.44999999999999929	5.62000000000000011	0.399431409999999987	N0	ASCEND LABORATO	0.187700000000000006
931	2018-08-29	46257986	65162053710	BENZONATATE 200 MG CAPSULE	40	0	0	10.3900000000000006	10.3900000000000006	13.6999999999999993	-3.31000000000000005	-0.318575550000000041	N0	AMNEAL PHARMACE	0.342500000000000027
932	2018-08-02	46254864	65162053710	BENZONATATE 200 MG CAPSULE	30	22.9899999999999984	0	0	22.9899999999999984	10.2799999999999994	12.7100000000000009	0.552849060000000003	N0	AMNEAL PHARMACE	0.342500000000000027
933	2018-08-14	46252027	536409256	BENZOYL PEROXIDE 10% GEL	42.5	0	0	8.99000000000000021	8.99000000000000021	3.83000000000000007	5.16000000000000014	0.573971080000000078	N0	RUGBY	0.0899999999999999967
934	2018-08-06	46252147	76385010310	BENZTROPINE MES 0.5 MG TAB	60	10.8900000000000006	0	0	10.8900000000000006	4.30999999999999961	6.58000000000000007	0.604224060000000063	N0	BAYSHORE PHARMA	0.0718000000000000027
935	2018-08-03	46254974	76385010310	BENZTROPINE MES 0.5 MG TAB	30	5.94000000000000039	0	0	5.94000000000000039	2.14999999999999991	3.79000000000000004	0.638047139999999957	N0	BAYSHORE PHARMA	0.0718000000000000027
936	2018-08-24	46255822	76385010310	BENZTROPINE MES 0.5 MG TAB	28	5.61000000000000032	0	0	5.61000000000000032	2.00999999999999979	3.60000000000000009	0.641711229999999966	N0	BAYSHORE PHARMA	0.0718000000000000027
937	2018-08-10	46255822	76385010310	BENZTROPINE MES 0.5 MG TAB	28	5.61000000000000032	0	0	5.61000000000000032	2.00999999999999979	3.60000000000000009	0.641711229999999966	N0	BAYSHORE PHARMA	0.0718000000000000027
938	2018-08-02	46245020	76385010410	BENZTROPINE MES 1 MG TABLET	60	12.2699999999999996	0	0	12.2699999999999996	5.09999999999999964	7.16999999999999993	0.584352080000000051	N0	BAYSHORE PHARMA	0.0850000000000000061
939	2018-08-29	46252797	76385010410	BENZTROPINE MES 1 MG TABLET	90	17.8999999999999986	0	0	17.8999999999999986	7.65000000000000036	10.25	0.572625700000000015	N0	BAYSHORE PHARMA	0.0850000000000000061
940	2018-08-27	46256873	76385010410	BENZTROPINE MES 1 MG TABLET	60	8.65000000000000036	0	0	8.65000000000000036	5.09999999999999964	3.54999999999999982	0.410404619999999998	N0	BAYSHORE PHARMA	0.0850000000000000061
941	2018-08-27	46256397	76385010410	BENZTROPINE MES 1 MG TABLET	31	5.95999999999999996	0	1.25	7.20999999999999996	2.64000000000000012	4.57000000000000028	0.633841890000000019	N0	BAYSHORE PHARMA	0.0850000000000000061
942	2018-08-21	46248148	76385010410	BENZTROPINE MES 1 MG TABLET	90	8.35999999999999943	0	1.25	9.60999999999999943	7.65000000000000036	1.95999999999999996	0.203954209999999997	N0	BAYSHORE PHARMA	0.0850000000000000061
943	2018-08-15	46237979	603243521	BENZTROPINE MES 2 MG TABLET	60	11.5999999999999996	0	1.25	12.8499999999999996	8.65000000000000036	4.20000000000000018	0.326848249999999951	N0	QUALITEST/PAR P	0.144100000000000006
944	2018-08-04	46249410	603243521	BENZTROPINE MES 2 MG TABLET	30	6.91000000000000014	0	0	6.91000000000000014	4.32000000000000028	2.58999999999999986	0.374819100000000016	N0	QUALITEST/PAR P	0.144100000000000006
945	2018-08-30	46258169	168005515	BETAMETH DIP .05% CREAM	15	29.6099999999999994	0	1.25	30.8599999999999994	27.5300000000000011	3.33000000000000007	0.107906680000000005	N0	FOUGERA	1.83529999999999993
946	2018-08-28	46229658	168005615	BETAMETHASONE DP 0.05% OI	15	40.4200000000000017	0	1	41.4200000000000017	48.4200000000000017	-7	-0.169000480000000008	N0	FOUGERA/SANDOZ	3.2280000000000002
947	2018-08-11	48012254	395009116	BETHANECHOL (T)1MG/ML SUS	195	36.1700000000000017	0	0	36.1700000000000017	3.60000000000000009	32.5700000000000003	0.900469999999999993	N0		0
948	2018-08-14	48011821	395009116	BETHANECHOL 1MG/ML SUSP	300	0	19.5399999999999991	0	19.5399999999999991	25.0500000000000007	-5.50999999999999979	-0.281985670000000022	N0		0
949	2018-08-25	48012226	65162057310	BETHANECHOL 1MG/ML SUSP	90	32.8299999999999983	0	0	32.8299999999999983	2.66000000000000014	30.1700000000000017	0.918976550000000003	N0		0
950	2018-08-14	46253403	65162057110	BETHANECHOL 5 MG TABLET	60	5.33000000000000007	10.7400000000000002	0	16.0700000000000003	11.8000000000000007	4.26999999999999957	0.265712510000000013	N0	AMNEAL PHARMACE	0.196599999999999997
951	2018-08-24	44032504	395806619	BI-E1MG/P100MG/T1MG CR	30	0	0	38.3500000000000014	38.3500000000000014	3.75999999999999979	34.5900000000000034	0.901955669999999987	N0		0
952	2018-08-10	44031885	395806619	BI-EST.5/P25/T1MG/GM CR	30	0	0	32.8999999999999986	32.8999999999999986	2.39999999999999991	30.5	0.927051669999999994	N0		0
953	2018-08-28	44032543	395806619	BI.5PROG30T.5PREG5 TR	30	0	0	60.7999999999999972	60.7999999999999972	2.24000000000000021	58.5600000000000023	0.963157889999999961	N0		0
954	2018-08-16	46249391	93022001	BICALUTAMIDE 50 MG TABLET	30	477.300000000000011	0	0	477.300000000000011	10.4299999999999997	466.870000000000005	0.978147919999999949	N0	TEVA USA	0.347700000000000009
955	2018-08-07	46249391	93022001	BICALUTAMIDE 50 MG TABLET	30	477.300000000000011	0	0	477.300000000000011	10.4299999999999997	466.870000000000005	0.978147919999999949	N0	TEVA USA	0.347700000000000009
956	2018-08-17	46256619	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
957	2018-08-06	46255243	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
958	2018-08-16	46255920	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
959	2018-08-12	46255893	713010901	BISAC-EVAC 10MG SUPPOSITO	12	4.01999999999999957	0	0	4.01999999999999957	1.25	2.77000000000000002	0.68905473000000006	N0	G & W LABS.	0.104099999999999998
960	2018-08-31	46258290	713010905	BISAC-EVAC 10MG SUPPOSITO	1	2.16000000000000014	0	0	2.16000000000000014	0.100000000000000006	2.06000000000000005	0.953703699999999932	N0	G & W LABS.	0.0989000000000000018
961	2018-08-09	46255672	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
962	2018-08-02	46254799	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
963	2018-08-22	46242107	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
964	2018-08-15	46256309	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
965	2018-08-02	46254835	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
966	2018-08-24	46257547	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
967	2018-08-12	46255920	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
968	2018-08-31	46258323	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
969	2018-08-11	46255867	713010901	BISAC-EVAC 10MG SUPPOSITO	5	2.83999999999999986	0	0	2.83999999999999986	0.520000000000000018	2.31999999999999984	0.816901409999999939	N0	G & W LABS.	0.104099999999999998
970	2018-08-09	46255633	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
971	2018-08-26	46257638	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
972	2018-08-17	46256647	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
973	2018-08-09	46255698	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
974	2018-08-13	46256081	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
975	2018-08-10	46254835	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
976	2018-08-14	46256256	713010901	BISAC-EVAC 10MG SUPPOSITO	5	2.83999999999999986	0	0	2.83999999999999986	0.520000000000000018	2.31999999999999984	0.816901409999999939	N0	G & W LABS.	0.104099999999999998
977	2018-08-23	46257425	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
978	2018-08-06	46254458	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
979	2018-08-21	46252610	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
980	2018-08-27	46257734	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
981	2018-08-30	46258183	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
982	2018-08-15	46256312	713010901	BISAC-EVAC 10MG SUPPOSITO	6	0	0	3.18999999999999995	3.18999999999999995	0.619999999999999996	2.56999999999999984	0.805642629999999915	N	G & W LABS.	0.104099999999999998
983	2018-08-14	46256205	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
984	2018-08-05	46254799	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
985	2018-08-07	46255255	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
986	2018-08-02	46254782	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
987	2018-08-17	46256758	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
988	2018-08-13	46256087	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
989	2018-08-01	46254417	713010901	BISAC-EVAC 10MG SUPPOSITO	2	2.33999999999999986	0	0	2.33999999999999986	0.209999999999999992	2.12999999999999989	0.910256409999999905	N0	G & W LABS.	0.104099999999999998
990	2018-08-29	46234456	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
991	2018-08-13	46255959	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
992	2018-08-01	46242107	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
993	2018-08-30	46258272	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
994	2018-08-07	46255298	713010901	BISAC-EVAC 10MG SUPPOSITO	30	0	6.94000000000000039	1	7.94000000000000039	3.12000000000000011	4.82000000000000028	0.607052900000000006	N0	G & W LABS.	0.104099999999999998
995	2018-08-21	46257071	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
996	2018-08-24	46257545	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
997	2018-08-08	46255522	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
998	2018-08-22	46257256	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
999	2018-08-09	46255609	713010901	BISAC-EVAC 10MG SUPPOSITO	2	2.33999999999999986	0	0	2.33999999999999986	0.209999999999999992	2.12999999999999989	0.910256409999999905	N0	G & W LABS.	0.104099999999999998
1000	2018-08-01	46254458	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
1001	2018-08-03	46254238	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
1002	2018-08-04	46255028	713010901	BISAC-EVAC 10MG SUPPOSITO	5	2.83999999999999986	0	0	2.83999999999999986	0.520000000000000018	2.31999999999999984	0.816901409999999939	N0	G & W LABS.	0.104099999999999998
1003	2018-08-27	46257673	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
1004	2018-08-06	46255145	713010901	BISAC-EVAC 10MG SUPPOSITO	3	2.5	0	0	2.5	0.309999999999999998	2.18999999999999995	0.87599999999999989	N0	G & W LABS.	0.104099999999999998
1005	2018-08-22	46234456	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
1006	2018-08-17	46250620	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
1007	2018-08-01	46254671	713010901	BISAC-EVAC 10MG SUPPOSITO	1	2.16999999999999993	0	0	2.16999999999999993	0.100000000000000006	2.06999999999999984	0.953917050000000044	N0	G & W LABS.	0.104099999999999998
1008	2018-08-12	46255909	713010901	BISAC-EVAC 10MG SUPPOSITO	7	3.18000000000000016	0	0	3.18000000000000016	0.57999999999999996	2.60000000000000009	0.817610059999999916	N0	G & W LABS.	0.104099999999999998
1009	2018-08-21	46257100	713010901	BISAC-EVAC 10MG SUPPOSITO	6	3.00999999999999979	0	0	3.00999999999999979	0.619999999999999996	2.39000000000000012	0.79401993000000004	N0	G & W LABS.	0.104099999999999998
1010	2018-08-21	46257167	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1011	2018-08-12	46256056	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1012	2018-08-30	46258301	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1013	2018-08-15	46256516	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1014	2018-08-17	46256903	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1015	2018-08-03	46255048	536135501	BISACODYL 10MG SUPPOSITOR	1	2.10999999999999988	0	0	2.10999999999999988	0.0800000000000000017	2.0299999999999998	0.962085309999999971	N0	RUGBY	0.0779999999999999999
1016	2018-08-09	46255769	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1017	2018-08-10	46256017	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1018	2018-08-20	46257060	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1019	2018-08-28	46258023	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1020	2018-08-08	46255582	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1021	2018-08-23	46257504	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1022	2018-08-06	46255272	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1023	2018-08-17	46256889	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1024	2018-08-15	46256515	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1025	2018-08-22	46257333	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1026	2018-08-14	46256332	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1027	2018-08-20	46257059	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1028	2018-08-03	46255046	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1029	2018-08-12	46256055	536135512	BISACODYL 10MG SUPPOSITOR	10	4.23000000000000043	0	0	4.23000000000000043	2.33000000000000007	1.89999999999999991	0.449172579999999988	N0	RUGBY	0.232899999999999996
1030	2018-08-06	46255270	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1031	2018-08-17	46256836	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1032	2018-08-16	46256657	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1033	2018-08-23	46257512	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1034	2018-08-06	46255284	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1035	2018-08-22	46257331	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1036	2018-08-15	46256528	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1037	2018-08-25	46257718	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1038	2018-08-10	46256009	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1039	2018-08-06	46255283	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1040	2018-08-02	46254912	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1041	2018-08-13	46256167	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1042	2018-08-14	46256329	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1043	2018-08-17	46256899	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1044	2018-08-20	46257057	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1045	2018-08-28	46258022	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1046	2018-08-16	46256666	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1047	2018-08-25	46257717	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1048	2018-08-14	46256328	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1049	2018-08-17	46256898	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1050	2018-08-23	46257500	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1051	2018-08-15	46256513	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1052	2018-08-26	46257729	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1053	2018-08-16	46256653	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1054	2018-08-25	46257715	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1055	2018-08-12	46256050	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1056	2018-08-16	46256652	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1057	2018-08-12	46256030	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1058	2018-08-23	46257509	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1059	2018-08-07	46255489	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1060	2018-08-26	46257728	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1061	2018-08-12	46256048	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1062	2018-08-19	46256910	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1063	2018-08-12	46256063	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1064	2018-08-04	46255099	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1065	2018-08-20	46257055	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1066	2018-08-29	46258165	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1067	2018-08-13	46256178	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1068	2018-08-02	46254888	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1069	2018-08-12	46256060	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1070	2018-08-24	46257575	536135501	BISACODYL 10MG SUPPOSITOR	12	1.92999999999999994	0	0	1.92999999999999994	0.939999999999999947	0.989999999999999991	0.512953370000000075	N0	RUGBY	0.0779999999999999999
1071	2018-08-31	46258502	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1072	2018-08-15	46256521	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1073	2018-08-27	46257873	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1074	2018-08-24	46257593	536135501	BISACODYL 10MG SUPPOSITOR	1	0.160000000000000003	0	0	0.160000000000000003	0.0800000000000000017	0.0800000000000000017	0.5	N0	RUGBY	0.0779999999999999999
1075	2018-08-11	46256013	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1076	2018-08-14	46256339	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1077	2018-08-08	46255585	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1078	2018-08-12	46256059	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1079	2018-08-29	46258164	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1080	2018-08-13	46256173	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1081	2018-08-09	46255770	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1082	2018-08-20	46236838	536135501	BISACODYL 10MG SUPPOSITOR	16	2.77000000000000002	0	0	2.77000000000000002	1.25	1.52000000000000002	0.548736459999999981	N0	RUGBY	0.0779999999999999999
1083	2018-08-12	46256058	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1084	2018-08-18	46256882	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1085	2018-08-02	46254886	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1086	2018-08-30	46258302	536135512	BISACODYL 10MG SUPPOSITOR	1	2.29999999999999982	0	0	2.29999999999999982	0.23000000000000001	2.06999999999999984	0.900000000000000022	N0	RUGBY	0.232899999999999996
1087	2018-08-27	46257862	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1088	2018-08-31	46258500	536135512	BISACODYL 10MG SUPPOSITOR	1	2.2200000000000002	0	0	2.2200000000000002	0.23000000000000001	1.98999999999999999	0.896396399999999982	N0	RUGBY	0.232899999999999996
1089	2018-08-29	46257991	574000411	BISACODYL 5 MG TABLET EC	15	3.45000000000000018	0	0	3.45000000000000018	1.37999999999999989	2.06999999999999984	0.599999999999999978	N0	PADDOCK LABS.	0.0917000000000000037
1090	2018-08-31	46240283	574000411	BISACODYL 5 MG TABLET EC	15	3.45000000000000018	0	0	3.45000000000000018	1.37999999999999989	2.06999999999999984	0.599999999999999978	N0	PADDOCK LABS.	0.0917000000000000037
1091	2018-08-29	46258087	536338110	BISACODYL 5MG TABLET EC	15	2.20000000000000018	0	0	2.20000000000000018	0.110000000000000001	2.08999999999999986	0.949999999999999956	N0	RUGBY	0.00759999999999999998
1092	2018-08-11	46255855	536338110	BISACODYL 5MG TABLET EC	24	2.31999999999999984	0	0	2.31999999999999984	0.179999999999999993	2.14000000000000012	0.9224137899999999	N0	RUGBY	0.00759999999999999998
1093	2018-08-05	46254905	536338110	BISACODYL 5MG TABLET EC	3	2.04000000000000004	0	0	2.04000000000000004	0.0200000000000000004	2.02000000000000002	0.990196080000000034	N0	RUGBY	0.00759999999999999998
1094	2018-08-29	46257993	536338110	BISACODYL 5MG TABLET EC	30	0	1.39999999999999991	1	2.39999999999999991	0.23000000000000001	2.16999999999999993	0.904166670000000061	N0	RUGBY	0.00759999999999999998
1095	2018-08-04	46255029	536338110	BISACODYL 5MG TABLET EC	5	2.06999999999999984	0	0	2.06999999999999984	0.0400000000000000008	2.0299999999999998	0.980676329999999985	N0	RUGBY	0.00759999999999999998
1096	2018-08-03	46254905	536338110	BISACODYL 5MG TABLET EC	3	2.04000000000000004	0	0	2.04000000000000004	0.0200000000000000004	2.02000000000000002	0.990196080000000034	N0	RUGBY	0.00759999999999999998
1097	2018-08-29	46258099	904792780	BISACODYL EC 5 MG TABLET	4	1.06000000000000005	0	0	1.06000000000000005	0.0299999999999999989	1.03000000000000003	0.971698109999999948	N0	MAJOR PHARMACEU	0.0189000000000000001
1098	2018-08-23	46237335	904792780	BISACODYL EC 5 MG TABLET	30	0	0	2.2200000000000002	2.2200000000000002	0.190000000000000002	2.0299999999999998	0.914414410000000011	N0	MAJOR PHARMACEU	0.0189000000000000001
1099	2018-08-30	46241561	29300012701	BISOPROLOL FUMARATE 10 MG T	15	19.5599999999999987	0	0	19.5599999999999987	3.79999999999999982	15.7599999999999998	0.805725970000000014	N0	UNICHEM PHARMAC	0.253500000000000003
1100	2018-08-23	46241561	29300012701	BISOPROLOL FUMARATE 10 MG T	15	19.5599999999999987	0	0	19.5599999999999987	3.79999999999999982	15.7599999999999998	0.805725970000000014	N0	UNICHEM PHARMAC	0.253500000000000003
1101	2018-08-20	46256969	29300012701	BISOPROLOL FUMARATE 10 MG T	30	12.6400000000000006	0	1.25	13.8900000000000006	7.61000000000000032	6.28000000000000025	0.452123830000000004	N0	UNICHEM PHARMAC	0.253500000000000003
1102	2018-08-15	46255943	29300012613	BISOPROLOL FUMARATE 5 MG TA	6	10.2200000000000006	0	0	10.2200000000000006	1.51000000000000001	8.71000000000000085	0.852250489999999972	N0	UNICHEM PHARMAC	0.25159999999999999
1103	2018-08-13	46255943	29300012613	BISOPROLOL FUMARATE 5 MG TA	6	10.2200000000000006	0	0	10.2200000000000006	1.51000000000000001	8.71000000000000085	0.852250489999999972	N0	UNICHEM PHARMAC	0.25159999999999999
1104	2018-08-14	46253263	29300012613	BISOPROLOL FUMARATE 5 MG TA	30	0	0	22.8299999999999983	22.8299999999999983	7.54999999999999982	15.2799999999999994	0.669294790000000028	N0	UNICHEM PHARMAC	0.25159999999999999
1105	2018-08-25	46247606	40985021740	BLACK COHASH & SOY ISOFLAVO	60	0	0	7.88999999999999968	7.88999999999999968	4.92999999999999972	2.95999999999999996	0.375158429999999987	N0		0.0821999999999999953
1106	2018-08-23	48012449	38779110502	BORIC ACID 600MG VAG CAPS	30	26.6900000000000013	0	0	26.6900000000000013	6.41999999999999993	20.2699999999999996	0.759460469999999943	N0		0
1107	2018-08-07	48012368	409613922	BORIC4%ETHANOL20%ST H2O	60	0	0	18.1999999999999993	18.1999999999999993	2.49000000000000021	15.7100000000000009	0.863186809999999971	N0		0
1108	2018-08-21	48010917	409613922	BORIC4%ETHANOL20%ST H2O	60	18.1999999999999993	0	0	18.1999999999999993	2.49000000000000021	15.7100000000000009	0.863186809999999971	N0		0
1109	2018-08-07	48012370	409613922	BORIC4%ETHANOL20%ST H2O	120	0	0	18.1999999999999993	18.1999999999999993	4.96999999999999975	13.2300000000000004	0.726923079999999944	N0		0
1111	2018-08-24	46257570	173085910	BREO ELLIPTA 100-25 MCG INH	60	332.389999999999986	0	3.70000000000000018	336.089999999999975	277.110000000000014	58.9799999999999969	0.175488709999999992	N0	GLAXOSMITHKLINE	4.61850000000000005
1112	2018-08-08	46228953	173085910	BREO ELLIPTA 100-25 MCG INH	60	308.860000000000014	0	40	348.860000000000014	277.110000000000014	71.75	0.205669900000000017	N0	GLAXOSMITHKLINE	4.61850000000000005
1113	2018-08-16	46254976	173085910	BREO ELLIPTA 100-25 MCG INH	180	963.779999999999973	0	0	963.779999999999973	831.330000000000041	132.449999999999989	0.137427629999999995	N0	GLAXOSMITHKLINE	4.61850000000000005
1114	2018-08-17	46256715	173085910	BREO ELLIPTA 100-25 MCG INH	60	351.860000000000014	0	0	351.860000000000014	277.110000000000014	74.75	0.212442450000000005	N0	GLAXOSMITHKLINE	4.61850000000000005
1115	2018-08-27	46248232	173085910	BREO ELLIPTA 100-25 MCG INH	60	280.759999999999991	0	45	325.759999999999991	277.110000000000014	48.6499999999999986	0.149343069999999994	N0	GLAXOSMITHKLINE	4.61850000000000005
1116	2018-08-04	46251415	173085910	BREO ELLIPTA 100-25 MCG INH	60	236.879999999999995	0	127.549999999999997	364.430000000000007	277.110000000000014	87.3199999999999932	0.239607059999999983	N0	GLAXOSMITHKLINE	4.61850000000000005
1117	2018-08-20	46256837	173088210	BREO ELLIPTA 200-25 MCG INH	60	307.980000000000018	0	30	337.980000000000018	277.110000000000014	60.8699999999999974	0.180099410000000015	N0	GLAXOSMITHKLINE	5.34579999999999966
1118	2018-08-15	46256360	173088210	BREO ELLIPTA 200-25 MCG INH	60	308.860000000000014	0	40	348.860000000000014	277.110000000000014	71.75	0.205669900000000017	N0	GLAXOSMITHKLINE	5.34579999999999966
1119	2018-08-29	46238894	173088210	BREO ELLIPTA 200-25 MCG INH	60	333.579999999999984	0	3	336.579999999999984	277.110000000000014	59.4699999999999989	0.176689049999999986	N0	GLAXOSMITHKLINE	5.34579999999999966
1120	2018-08-11	46225710	173088210	BREO ELLIPTA 200-25 MCG INH	60	217.689999999999998	0	117.209999999999994	334.899999999999977	277.110000000000014	57.7899999999999991	0.172558970000000006	N0	GLAXOSMITHKLINE	5.34579999999999966
1121	2018-08-24	46257479	186077760	BRILINTA 90 MG TABLET	60	373.430000000000007	0	3.70000000000000018	377.129999999999995	319.730000000000018	57.3999999999999986	0.152202160000000003	N0	ASTRAZENECA	5.3288000000000002
1122	2018-08-06	46248239	17478071510	BRIMONIDINE 0.2% EYE DROP	5	0	0	27.120000000000001	27.120000000000001	3.52000000000000002	23.6000000000000014	0.870206490000000055	N0	AKORN INC.	0.703999999999999959
1123	2018-08-13	46255625	54766090753	BRISDELLE 7.5 MG CAPSULE	30	125	0	70.4300000000000068	195.430000000000007	183.599999999999994	11.8300000000000001	0.0605331799999999989	N9	SEBELA PHARMACE	6.11990000000000034
1124	2018-08-02	46225025	63402091164	BROVANA 15 MCG/2 ML SOLUTIO	120	955.769999999999982	0	0	955.769999999999982	539.950000000000045	415.819999999999993	0.435062830000000011	N0	SUNOVION PHARMA	4.49960000000000004
1125	2018-08-22	46250947	63402091130	BROVANA 15 MCG/2 ML SOLUTIO	120	582.720000000000027	0	0	582.720000000000027	545.67999999999995	37.0399999999999991	0.063563980000000006	N0	SUNOVION PHARMA	4.5472999999999999
1126	2018-08-30	46225025	63402091164	BROVANA 15 MCG/2 ML SOLUTIO	120	955.769999999999982	0	0	955.769999999999982	539.950000000000045	415.819999999999993	0.435062830000000011	N0	SUNOVION PHARMA	4.49960000000000004
1127	2018-08-26	46257631	63402091130	BROVANA 15 MCG/2 ML SOLUTIO	10	86.519999999999996	0	0	86.519999999999996	45.4699999999999989	41.0499999999999972	0.474456770000000028	N0	SUNOVION PHARMA	4.5472999999999999
1128	2018-08-06	46244821	63402091130	BROVANA 15 MCG/2 ML SOLUTIO	120	582.720000000000027	0	0	582.720000000000027	545.67999999999995	37.0399999999999991	0.063563980000000006	N0	SUNOVION PHARMA	4.5472999999999999
1129	2018-08-20	46256947	115168774	BUDESONIDE 0.25 MG/2 ML SUS	60	209.75	0	0	209.75	282.300000000000011	-72.5499999999999972	-0.345887959999999994	N0	IMPAX GENERICS	0.87390000000000001
1130	2018-08-02	46251413	69097031987	BUDESONIDE 0.5 MG/2 ML SUSP	120	216.400000000000006	0	0	216.400000000000006	68.9300000000000068	147.469999999999999	0.681469500000000061	N0	CIPLA USA, INC.	0.574400000000000022
1131	2018-08-06	46248017	115168974	BUDESONIDE 0.5 MG/2 ML SUSP	120	380.939999999999998	89.2399999999999949	6	476.180000000000007	89.7999999999999972	386.379999999999995	0.81141585000000005	N0	IMPAX GENERICS	0.748299999999999965
1132	2018-08-22	46252085	69097031987	BUDESONIDE 0.5 MG/2 ML SUSP	60	157	0	0	157	34.4600000000000009	122.540000000000006	0.780509549999999996	N0	CIPLA USA, INC.	0.574400000000000022
1133	2018-08-30	46251318	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	30	145.259999999999991	0	0	145.259999999999991	24.4800000000000004	120.780000000000001	0.831474599999999953	N0	TEVA USA	0.816100000000000048
1134	2018-08-30	46254084	115168974	BUDESONIDE 0.5 MG/2 ML SUSP	60	286.509999999999991	0	0	286.509999999999991	44.8999999999999986	241.610000000000014	0.84328644999999991	N0	IMPAX GENERICS	0.748299999999999965
1135	2018-08-17	46256645	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	120	137.52000000000001	0	0	137.52000000000001	97.9300000000000068	39.5900000000000034	0.287885400000000014	N0	TEVA USA	0.816100000000000048
1136	2018-08-27	46257697	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	120	159.710000000000008	31.9299999999999997	8	199.639999999999986	97.9300000000000068	101.709999999999994	0.509467039999999982	N0	TEVA USA	0.816100000000000048
1137	2018-08-29	46257990	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	60	286.509999999999991	0	0	286.509999999999991	48.9699999999999989	237.539999999999992	0.829081010000000007	N0	TEVA USA	0.816100000000000048
1138	2018-08-10	46255735	69097031987	BUDESONIDE 0.5 MG/2 ML SUSP	120	136.52000000000001	0	1	137.52000000000001	68.9300000000000068	68.5900000000000034	0.498763819999999969	N0	CIPLA USA, INC.	0.574400000000000022
1139	2018-08-10	46252085	69097031987	BUDESONIDE 0.5 MG/2 ML SUSP	60	157	0	0	157	34.4600000000000009	122.540000000000006	0.780509549999999996	N0	CIPLA USA, INC.	0.574400000000000022
1140	2018-08-10	46255742	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	120	154.52000000000001	0	0	154.52000000000001	97.9300000000000068	56.5900000000000034	0.366230910000000021	N0	TEVA USA	0.816100000000000048
1141	2018-08-30	46251413	115168974	BUDESONIDE 0.5 MG/2 ML SUSP	120	264.519999999999982	0	0	264.519999999999982	89.7999999999999972	174.719999999999999	0.660517159999999937	N0	IMPAX GENERICS	0.748299999999999965
1142	2018-08-04	46255068	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	60	286.509999999999991	0	0	286.509999999999991	82.269999999999996	204.240000000000009	0.712854700000000063	N0	TEVA USA	0.816100000000000048
1143	2018-08-26	46257632	115168974	BUDESONIDE 0.5 MG/2 ML SUSP	10	51.0900000000000034	0	0	51.0900000000000034	7.48000000000000043	43.6099999999999994	0.853591700000000064	N0	IMPAX GENERICS	0.748299999999999965
1144	2018-08-14	46255068	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	60	286.509999999999991	0	0	286.509999999999991	48.9699999999999989	237.539999999999992	0.829081010000000007	N0	TEVA USA	0.816100000000000048
1145	2018-08-22	46250946	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	120	240.590000000000003	0	0	240.590000000000003	97.9300000000000068	142.659999999999997	0.592958980000000024	N0	TEVA USA	0.816100000000000048
1146	2018-08-06	46244684	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	120	240.590000000000003	0	0	240.590000000000003	164.539999999999992	76.0499999999999972	0.316097929999999971	N0	TEVA USA	0.816100000000000048
1147	2018-08-13	46254084	93681673	BUDESONIDE 0.5 MG/2 ML SUSP	60	286.509999999999991	0	0	286.509999999999991	48.9699999999999989	237.539999999999992	0.829081010000000007	N0	TEVA USA	0.816100000000000048
1148	2018-08-29	48011503	38779019803	BUDESONIDE 6MG/ML SUSP	45	0	0	87.75	87.75	19.2100000000000009	68.5400000000000063	0.78108261999999995	N0		0
1149	2018-08-24	46254111	49884050101	BUDESONIDE EC 3 MG CAPSULE	42	136.800000000000011	0	0	136.800000000000011	323.189999999999998	-186.389999999999986	-1.36250000000000004	N0	PAR PHARM.	7.69510000000000005
1150	2018-08-09	46255606	55390050005	BUMETANIDE 0.25 MG/ML VIAL	32	18.2800000000000011	0	0	18.2800000000000011	14.0700000000000003	4.20999999999999996	0.230306349999999993	N0	BEDFORD LABS	0.43969999999999998
1151	2018-08-24	46257471	641600810	BUMETANIDE 0.25MG/ML VIAL	24	19.3000000000000007	0	0	19.3000000000000007	10.5600000000000005	8.74000000000000021	0.452849740000000001	N0	WEST-WARD,INC.	0.439800000000000024
1152	2018-08-30	46258144	409141204	BUMETANIDE 0.25MG/ML VIAL	24	16.8500000000000014	0	0	16.8500000000000014	5.07000000000000028	11.7799999999999994	0.699109789999999953	N0	HOSPIRA/PFIZER	0.21110000000000001
1153	2018-08-01	46254690	42799011901	BUMETANIDE 0.5 MG TABLET	60	39.8800000000000026	0	0	39.8800000000000026	26.6000000000000014	13.2799999999999994	0.332998999999999989	N0	EDENBRIDGE PHAR	0.320299999999999974
1154	2018-08-12	46255890	42799011901	BUMETANIDE 0.5 MG TABLET	9	9.38000000000000078	0	0	9.38000000000000078	3.25999999999999979	6.12000000000000011	0.652452030000000072	N0	EDENBRIDGE PHAR	0.320299999999999974
1155	2018-08-07	46255359	42799011901	BUMETANIDE 0.5 MG TABLET	60	32.2000000000000028	0	0	32.2000000000000028	21.7300000000000004	10.4700000000000006	0.325155280000000046	N0	EDENBRIDGE PHAR	0.320299999999999974
1156	2018-08-21	46236817	42799011901	BUMETANIDE 0.5 MG TABLET	60	9.09999999999999964	0	5	14.0999999999999996	21.7300000000000004	-7.62999999999999989	-0.541134749999999998	N0	EDENBRIDGE PHAR	0.320299999999999974
1157	2018-08-08	46254051	42799011901	BUMETANIDE 0.5 MG TABLET	3	5.79000000000000004	0	0	5.79000000000000004	1.09000000000000008	4.70000000000000018	0.811744390000000093	N0	EDENBRIDGE PHAR	0.320299999999999974
1158	2018-08-04	46254051	42799011901	BUMETANIDE 0.5 MG TABLET	9	9.38000000000000078	0	0	9.38000000000000078	3.99000000000000021	5.38999999999999968	0.574626870000000012	N0	EDENBRIDGE PHAR	0.320299999999999974
1159	2018-08-31	46252952	42799011901	BUMETANIDE 0.5 MG TABLET	30	7.23000000000000043	0	0	7.23000000000000043	10.8699999999999992	-3.64000000000000012	-0.503457810000000006	N0	EDENBRIDGE PHAR	0.320299999999999974
1160	2018-08-28	46254407	42799011901	BUMETANIDE 0.5 MG TABLET	15	12.9700000000000006	0	0	12.9700000000000006	5.42999999999999972	7.54000000000000004	0.581341559999999924	N0	EDENBRIDGE PHAR	0.320299999999999974
1161	2018-08-01	46254051	42799011901	BUMETANIDE 0.5 MG TABLET	9	9.38000000000000078	0	0	9.38000000000000078	3.99000000000000021	5.38999999999999968	0.574626870000000012	N0	EDENBRIDGE PHAR	0.320299999999999974
1162	2018-08-14	46252952	42799011901	BUMETANIDE 0.5 MG TABLET	30	7.23000000000000043	0	0	7.23000000000000043	10.8699999999999992	-3.64000000000000012	-0.503457810000000006	N0	EDENBRIDGE PHAR	0.320299999999999974
1163	2018-08-31	46258294	42799012001	BUMETANIDE 1 MG TABLET	30	23.5	0	0	23.5	13.6999999999999993	9.80000000000000071	0.417021279999999994	N0	EDENBRIDGE PHAR	0.456600000000000006
1164	2018-08-27	46257796	42799012001	BUMETANIDE 1 MG TABLET	15	8.97000000000000064	0	0	8.97000000000000064	6.84999999999999964	2.12000000000000011	0.236343369999999997	N0	EDENBRIDGE PHAR	0.456600000000000006
1165	2018-08-24	46257474	42799012001	BUMETANIDE 1 MG TABLET	3	5.95000000000000018	0	0	5.95000000000000018	1.37000000000000011	4.58000000000000007	0.76974790000000004	N0	EDENBRIDGE PHAR	0.456600000000000006
1166	2018-08-23	46257378	42799012001	BUMETANIDE 1 MG TABLET	6	7.90000000000000036	0	0	7.90000000000000036	2.74000000000000021	5.16000000000000014	0.653164560000000005	N0	EDENBRIDGE PHAR	0.456600000000000006
1167	2018-08-08	46252392	42799012001	BUMETANIDE 1 MG TABLET	30	23.5	0	0	23.5	13.6999999999999993	9.80000000000000071	0.417021279999999994	N0	EDENBRIDGE PHAR	0.456600000000000006
1168	2018-08-20	46254201	42799012001	BUMETANIDE 1 MG TABLET	90	62.5	0	0	62.5	41.0900000000000034	21.4100000000000001	0.342559999999999976	N0	EDENBRIDGE PHAR	0.456600000000000006
1169	2018-08-27	46257763	42799012001	BUMETANIDE 1 MG TABLET	6	7.90000000000000036	0	0	7.90000000000000036	2.74000000000000021	5.16000000000000014	0.653164560000000005	N0	EDENBRIDGE PHAR	0.456600000000000006
1170	2018-08-10	46255801	42799012001	BUMETANIDE 1 MG TABLET	5	7.25	0	0	7.25	2.2799999999999998	4.96999999999999975	0.685517239999999917	N0	EDENBRIDGE PHAR	0.456600000000000006
1171	2018-08-26	46257474	42799012001	BUMETANIDE 1 MG TABLET	3	5.95000000000000018	0	0	5.95000000000000018	1.37000000000000011	4.58000000000000007	0.76974790000000004	N0	EDENBRIDGE PHAR	0.456600000000000006
1172	2018-08-30	46258201	42799012001	BUMETANIDE 1 MG TABLET	15	13.75	0	0	13.75	6.84999999999999964	6.90000000000000036	0.501818180000000003	N0	EDENBRIDGE PHAR	0.456600000000000006
1173	2018-08-14	46238234	42799012001	BUMETANIDE 1 MG TABLET	60	43	0	0	43	27.3999999999999986	15.5999999999999996	0.362790699999999966	N0	EDENBRIDGE PHAR	0.456600000000000006
1174	2018-08-07	46254368	42799012001	BUMETANIDE 1 MG TABLET	30	23.5	0	0	23.5	13.6999999999999993	9.80000000000000071	0.417021279999999994	N0	EDENBRIDGE PHAR	0.456600000000000006
1175	2018-08-28	46257763	42799012001	BUMETANIDE 1 MG TABLET	10	10.5	0	0	10.5	4.57000000000000028	5.92999999999999972	0.564761900000000039	N0	EDENBRIDGE PHAR	0.456600000000000006
1176	2018-08-29	46257474	42799012001	BUMETANIDE 1 MG TABLET	3	5.95000000000000018	0	0	5.95000000000000018	1.37000000000000011	4.58000000000000007	0.76974790000000004	N0	EDENBRIDGE PHAR	0.456600000000000006
1177	2018-08-09	46252835	42799012001	BUMETANIDE 1 MG TABLET	23	18.9499999999999993	0	0	18.9499999999999993	10.5	8.44999999999999929	0.445910289999999987	N0	EDENBRIDGE PHAR	0.456600000000000006
1178	2018-08-12	46256037	185012905	BUMETANIDE 1MG TABLET	1	4.65000000000000036	0	0	4.65000000000000036	0.619999999999999996	4.03000000000000025	0.866666670000000083	N0	SANDOZ	0.62170000000000003
1179	2018-08-08	46255523	55390050005	BUMETANIDE 1MG/4ML	32	18.2800000000000011	0	0	18.2800000000000011	14.0700000000000003	4.20999999999999996	0.230306349999999993	N0	BEDFORD LABS	0.43969999999999998
1180	2018-08-23	46257303	42799012101	BUMETANIDE 2 MG TABLET	3	7.90000000000000036	0	0	7.90000000000000036	2.50999999999999979	5.38999999999999968	0.682278479999999909	N0	EDENBRIDGE PHAR	0.837600000000000011
1181	2018-08-17	46235931	42799012101	BUMETANIDE 2 MG TABLET	15	23.5	0	0	23.5	12.5600000000000005	10.9399999999999995	0.465531909999999993	N0	EDENBRIDGE PHAR	0.837600000000000011
1182	2018-08-06	46255014	42799012101	BUMETANIDE 2 MG TABLET	3	7.90000000000000036	0	0	7.90000000000000036	2.50999999999999979	5.38999999999999968	0.682278479999999909	N0	EDENBRIDGE PHAR	0.837600000000000011
1183	2018-08-22	46257262	42799012101	BUMETANIDE 2 MG TABLET	15	0	0	31.7899999999999991	31.7899999999999991	12.5600000000000005	19.2300000000000004	0.604907199999999978	N	EDENBRIDGE PHAR	0.837600000000000011
1184	2018-08-03	46255014	42799012101	BUMETANIDE 2 MG TABLET	3	7.90000000000000036	0	0	7.90000000000000036	2.50999999999999979	5.38999999999999968	0.682278479999999909	N0	EDENBRIDGE PHAR	0.837600000000000011
1185	2018-08-06	46252468	42799012101	BUMETANIDE 2 MG TABLET	15	23.5	0	0	23.5	12.5600000000000005	10.9399999999999995	0.465531909999999993	N0	EDENBRIDGE PHAR	0.837600000000000011
1186	2018-08-29	46252392	42799012101	BUMETANIDE 2 MG TABLET	15	23.5	0	0	23.5	12.5600000000000005	10.9399999999999995	0.465531909999999993	N0	EDENBRIDGE PHAR	0.837600000000000011
1187	2018-08-02	46235931	42799012101	BUMETANIDE 2 MG TABLET	15	23.5	0	0	23.5	12.5600000000000005	10.9399999999999995	0.465531909999999993	N0	EDENBRIDGE PHAR	0.837600000000000011
1188	2018-08-31	46258184	42799012101	BUMETANIDE 2 MG TABLET	15	0	0	31.7899999999999991	31.7899999999999991	12.5600000000000005	19.2300000000000004	0.604907199999999978	N	EDENBRIDGE PHAR	0.837600000000000011
1189	2018-08-21	46256828	185013001	BUMETANIDE 2MG TABLET	5	10.5	0	0	10.5	2.72999999999999998	7.76999999999999957	0.739999999999999991	N0	SANDOZ	0.545399999999999996
1190	2018-08-19	46256828	185013001	BUMETANIDE 2MG TABLET	3	7.90000000000000036	0	0	7.90000000000000036	1.6399999999999999	6.25999999999999979	0.792405059999999994	N0	SANDOZ	0.545399999999999996
1191	2018-08-07	46255380	185013001	BUMETANIDE 2MG TABLET	12	19.6000000000000014	0	0	19.6000000000000014	6.54000000000000004	13.0600000000000005	0.666326530000000083	N0	SANDOZ	0.545399999999999996
1192	2018-08-03	44032222	43598058230	BUPRENORP-NALOX 8-2 MG SL F	15	96	0	0	96	83.25	12.75	0.1328125	N0	DR.REDDY'S LAB	5.55030000000000001
1193	2018-08-07	44032271	43598058230	BUPRENORP-NALOX 8-2 MG SL F	11	78.1500000000000057	0	0.28999999999999998	78.4399999999999977	61.0499999999999972	17.3900000000000006	0.221698110000000004	N0	DR.REDDY'S LAB	5.55030000000000001
1194	2018-08-14	44032362	43598058230	BUPRENORP-NALOX 8-2 MG SL F	11	78.1500000000000057	0	0.28999999999999998	78.4399999999999977	61.0499999999999972	17.3900000000000006	0.221698110000000004	N0	DR.REDDY'S LAB	5.55030000000000001
1195	2018-08-08	44032269	43598058230	BUPRENORP-NALOX 8-2 MG SL F	90	570.970000000000027	0	0	570.970000000000027	499.529999999999973	71.4399999999999977	0.125120409999999987	N0	DR.REDDY'S LAB	5.55030000000000001
1196	2018-08-02	44032204	43598058230	BUPRENORP-NALOX 8-2 MG SL F	7	43.3400000000000034	0	3.25999999999999979	46.6000000000000014	38.8500000000000014	7.75	0.166309010000000007	N0	DR.REDDY'S LAB	5.55030000000000001
1197	2018-08-07	44032272	228315573	BUPRENORPHIN-NALOXON 8-2 MG	42	127.090000000000003	0	0	127.090000000000003	119.769999999999996	7.32000000000000028	0.057596979999999999	N0	ACTAVIS PHARMA,	2.85159999999999991
1198	2018-08-21	44032443	228315573	BUPRENORPHIN-NALOXON 8-2 MG	11	51.4500000000000028	0	0.28999999999999998	51.740000000000002	31.370000000000001	20.370000000000001	0.39369926999999999	N0	ACTAVIS PHARMA,	2.85159999999999991
1816	2018-08-20	46246001	93440401	CLOZAPINE 50 MG TABLET	28	10.25	0	0	10.25	19.2600000000000016	-9.00999999999999979	-0.879024389999999989	N0	TEVA	0.687699999999999978
1199	2018-08-20	44031695	54017613	BUPRENORPHINE 2 MG TABLET S	60	0	0	75.5900000000000034	75.5900000000000034	57.3200000000000003	18.2699999999999996	0.241698639999999992	N0	WEST-WARD, INC.	0.955300000000000038
1200	2018-08-27	44032532	93360340	BUPRENORPHINE 20 MCG/HR PAT	4	324.990000000000009	0	174.97999999999999	499.970000000000027	453.740000000000009	46.2299999999999969	0.0924655500000000075	N0	TEVA USA	113.435000000000002
1201	2018-08-14	44032337	54017713	BUPRENORPHINE 8 MG TABLET S	2	0	0	5	5	2.7200000000000002	2.2799999999999998	0.456000000000000016	N	ROXANE/WEST-WAR	1.35860000000000003
1202	2018-08-11	44032337	54017713	BUPRENORPHINE 8 MG TABLET S	6	0	0	15	15	8.15000000000000036	6.84999999999999964	0.456666669999999941	N	ROXANE/WEST-WAR	1.35860000000000003
1203	2018-08-16	46253291	60505015701	BUPROPION HCL 100 MG TABLET	28	14.6099999999999994	0	0	14.6099999999999994	11.1600000000000001	3.45000000000000018	0.236139629999999989	N0	APOTEX CORP	0.39860000000000001
1204	2018-08-30	46258069	781106401	BUPROPION HCL 100 MG TABLET	60	15.5700000000000003	0	0	15.5700000000000003	18.8999999999999986	-3.33000000000000007	-0.213872830000000014	N0	SANDOZ	0.315000000000000002
1205	2018-08-14	46256156	60505015801	BUPROPION HCL 75 MG TABLET	7	8.72000000000000064	0	0	8.72000000000000064	1.12000000000000011	7.59999999999999964	0.87155963000000003	N0	APOTEX CORP	0.109600000000000003
1206	2018-08-29	46258116	591354060	BUPROPION HCL SR 100 MG TAB	30	6.74000000000000021	0	0	6.74000000000000021	3.64999999999999991	3.08999999999999986	0.458456970000000019	N0	ACTAVIS PHARMA/	0.121800000000000005
1207	2018-08-21	46253672	591354060	BUPROPION HCL SR 100 MG TAB	60	12.4900000000000002	0	0	12.4900000000000002	7.30999999999999961	5.17999999999999972	0.414731790000000045	N0	ACTAVIS PHARMA/	0.121800000000000005
1208	2018-08-13	46253081	591354060	BUPROPION HCL SR 100 MG TAB	60	12.4900000000000002	0	0	12.4900000000000002	7.30999999999999961	5.17999999999999972	0.414731790000000045	N0	ACTAVIS PHARMA/	0.121800000000000005
1209	2018-08-02	46254875	591354060	BUPROPION HCL SR 100 MG TAB	60	8.73000000000000043	0	0	8.73000000000000043	7.30999999999999961	1.41999999999999993	0.16265750000000001	N0	ACTAVIS PHARMA/	0.121800000000000005
1210	2018-08-11	46254829	591354060	BUPROPION HCL SR 100MG TA	30	6.86000000000000032	0	1.25	8.10999999999999943	3.64999999999999991	4.45999999999999996	0.54993835000000002	N0	ACTAVIS PHARMA/	0.121800000000000005
1211	2018-08-31	46252492	69097087812	BUPROPION HCL SR 150 MG TAB	60	5.33999999999999986	0	1	6.33999999999999986	5.61000000000000032	0.729999999999999982	0.115141960000000002	N0	CIPLA USA, INC.	0.0934999999999999998
1212	2018-08-08	46255459	69097087812	BUPROPION HCL SR 150 MG TAB	30	2.33999999999999986	0	1	3.33999999999999986	2.81000000000000005	0.530000000000000027	0.158682630000000019	N0	CIPLA USA, INC.	0.0934999999999999998
1213	2018-08-31	46246987	69097087812	BUPROPION HCL SR 150 MG TAB	60	30.3200000000000003	0	0	30.3200000000000003	5.61000000000000032	24.7100000000000009	0.814973610000000015	N0	CIPLA USA, INC.	0.0934999999999999998
1214	2018-08-30	46244347	43547028950	BUPROPION HCL SR 150 MG TAB	30	20.6799999999999997	0	3.35000000000000009	24.0300000000000011	2.93000000000000016	21.1000000000000014	0.878069080000000057	N0	SOLCO HEALTHCAR	0.0575000000000000025
1215	2018-08-14	46250312	69097087812	BUPROPION HCL SR 150 MG TAB	60	9.71000000000000085	0	0	9.71000000000000085	5.61000000000000032	4.09999999999999964	0.422245109999999979	N0	CIPLA USA, INC.	0.0934999999999999998
1216	2018-08-01	46244347	69097087812	BUPROPION HCL SR 150 MG TAB	30	20.6799999999999997	0	3.35000000000000009	24.0300000000000011	2.81000000000000005	21.2199999999999989	0.883062840000000016	N0	CIPLA USA, INC.	0.0934999999999999998
1217	2018-08-23	46257341	69097087812	BUPROPION HCL SR 150 MG TAB	30	2.33999999999999986	0	1	3.33999999999999986	2.81000000000000005	0.530000000000000027	0.158682630000000019	N0	CIPLA USA, INC.	0.0934999999999999998
1218	2018-08-24	46239474	43547028950	BUPROPION HCL SR 150 MG TAB	30	0	0	9.13000000000000078	9.13000000000000078	2.93000000000000016	6.20000000000000018	0.679079959999999927	N0	SOLCO HEALTHCAR	0.0575000000000000025
1219	2018-08-13	46254472	43547028950	BUPROPION HCL SR 150 MG TAB	90	14.0600000000000005	0	0	14.0600000000000005	8.78999999999999915	5.26999999999999957	0.37482219	N0	SOLCO HEALTHCAR	0.0575000000000000025
1220	2018-08-01	46233304	69097087812	BUPROPION HCL SR 150 MG TAB	60	6.84999999999999964	0	1	7.84999999999999964	5.61000000000000032	2.24000000000000021	0.285350319999999991	N0	CIPLA USA, INC.	0.0934999999999999998
1221	2018-08-29	46238769	69097087812	BUPROPION HCL SR 150 MG TAB	60	9.71000000000000085	0	0	9.71000000000000085	5.61000000000000032	4.09999999999999964	0.422245109999999979	N0	CIPLA USA, INC.	0.0934999999999999998
1222	2018-08-03	46252884	43547028950	BUPROPION HCL SR 150 MG TAB	60	5.33999999999999986	0	1	6.33999999999999986	5.86000000000000032	0.479999999999999982	0.0757097800000000043	N0	SOLCO HEALTHCAR	0.0575000000000000025
1223	2018-08-28	46257936	69097087812	BUPROPION HCL SR 150 MG TAB	6	10.2400000000000002	0	0	10.2400000000000002	0.560000000000000053	9.67999999999999972	0.9453125	N0	CIPLA USA, INC.	0.0934999999999999998
1224	2018-08-22	46257120	69097087812	BUPROPION HCL SR 150 MG TAB	60	0	0	9.5	9.5	5.61000000000000032	3.89000000000000012	0.409473679999999951	N0	CIPLA USA, INC.	0.0934999999999999998
1225	2018-08-29	46258000	69097087812	BUPROPION HCL SR 150 MG TAB	60	0	0	10	10	5.61000000000000032	4.38999999999999968	0.439000000000000001	N0	CIPLA USA, INC.	0.0934999999999999998
1226	2018-08-31	46233304	69097087812	BUPROPION HCL SR 150 MG TAB	60	5.33999999999999986	0	1	6.33999999999999986	5.61000000000000032	0.729999999999999982	0.115141960000000002	N0	CIPLA USA, INC.	0.0934999999999999998
1227	2018-08-09	46241793	591354260	BUPROPION HCL SR 200 MG TAB	60	38.8200000000000003	0	0	38.8200000000000003	12.0099999999999998	26.8099999999999987	0.690623389999999948	N0	ACTAVIS PHARMA/	0.2001
1228	2018-08-29	46258102	591354260	BUPROPION HCL SR 200 MG TAB	60	15.4000000000000004	0	0	15.4000000000000004	12.0099999999999998	3.39000000000000012	0.220129869999999978	N0	ACTAVIS PHARMA/	0.2001
1229	2018-08-11	46254828	591354260	BUPROPION HCL SR 200 MG TAB	30	29.2800000000000011	0	1.25	30.5300000000000011	6	24.5300000000000011	0.803471990000000025	N0	ACTAVIS PHARMA/	0.2001
1230	2018-08-29	46254993	115681110	BUPROPION HCL XL 150 MG TAB	30	15.9900000000000002	0	0	15.9900000000000002	6.88999999999999968	9.09999999999999964	0.569105689999999997	N0	GLOBAL PHARM	0.112000000000000002
1231	2018-08-23	46248684	115681110	BUPROPION HCL XL 150 MG TAB	30	6.49000000000000021	0	0	6.49000000000000021	6.88999999999999968	-0.400000000000000022	-0.0616332799999999986	N0	GLOBAL PHARM	0.112000000000000002
1232	2018-08-06	46239820	115681110	BUPROPION HCL XL 150 MG TAB	60	73.2099999999999937	0	3.35000000000000009	76.5600000000000023	15.3599999999999994	61.2000000000000028	0.799373039999999979	N0	GLOBAL PHARM	0.112000000000000002
1233	2018-08-18	46249742	115681110	BUPROPION HCL XL 150 MG TAB	30	26.3399999999999999	0	20.6999999999999993	47.0399999999999991	6.88999999999999968	40.1499999999999986	0.853528909999999974	N0	GLOBAL PHARM	0.112000000000000002
1234	2018-08-03	46254993	115681110	BUPROPION HCL XL 150 MG TAB	30	20.9600000000000009	0	0	20.9600000000000009	7.67999999999999972	13.2799999999999994	0.633587789999999984	N0	GLOBAL PHARM	0.112000000000000002
1235	2018-08-24	46257465	115681110	BUPROPION HCL XL 150 MG TAB	30	12.0700000000000003	0	0	12.0700000000000003	6.88999999999999968	5.17999999999999972	0.429163210000000017	N0	GLOBAL PHARM	0.112000000000000002
1236	2018-08-30	46218707	115681110	BUPROPION HCL XL 150 MG TAB	30	15.0899999999999999	0	0	15.0899999999999999	6.88999999999999968	8.19999999999999929	0.54340622999999999	N0	GLOBAL PHARM	0.112000000000000002
1237	2018-08-03	46246549	115681110	BUPROPION HCL XL 150 MG TAB	30	5.49000000000000021	0	1	6.49000000000000021	6.88999999999999968	-0.400000000000000022	-0.0616332799999999986	N0	GLOBAL PHARM	0.112000000000000002
1238	2018-08-06	46251721	115681110	BUPROPION HCL XL 150 MG TAB	15	24.25	0	0	24.25	3.83999999999999986	20.4100000000000001	0.84164947999999995	N0	GLOBAL PHARM	0.112000000000000002
1239	2018-08-31	46258331	115681110	BUPROPION HCL XL 150 MG TAB	90	0	0	97.6700000000000017	97.6700000000000017	20.6600000000000001	77.0100000000000051	0.788471380000000055	N0	GLOBAL PHARM	0.112000000000000002
1240	2018-08-27	46256392	68180031902	BUPROPION HCL XL 150 MG TAB	90	17.4899999999999984	0	13.7300000000000004	31.2199999999999989	32.1599999999999966	-0.939999999999999947	-0.0301088999999999975	N0	LUPIN PHARMACEU	0.357300000000000006
1241	2018-08-24	46255821	115681110	BUPROPION HCL XL 150 MG TAB	14	6.16000000000000014	0	0	6.16000000000000014	3.20999999999999996	2.95000000000000018	0.478896099999999991	N0	GLOBAL PHARM	0.112000000000000002
1242	2018-08-23	46256201	115681110	BUPROPION HCL XL 150 MG TAB	30	12.0700000000000003	0	0	12.0700000000000003	6.88999999999999968	5.17999999999999972	0.429163210000000017	N0	GLOBAL PHARM	0.112000000000000002
1243	2018-08-21	46243176	115681110	BUPROPION HCL XL 150 MG TAB	30	0	0	15.9900000000000002	15.9900000000000002	6.88999999999999968	9.09999999999999964	0.569105689999999997	N0	GLOBAL PHARM	0.112000000000000002
1244	2018-08-17	46256153	115681110	BUPROPION HCL XL 150 MG TAB	30	0	0	12	12	6.88999999999999968	5.11000000000000032	0.42583333000000001	N0	GLOBAL PHARM	0.112000000000000002
1245	2018-08-09	46244905	115681110	BUPROPION HCL XL 150 MG TAB	30	0	0	35.1700000000000017	35.1700000000000017	6.88999999999999968	28.2800000000000011	0.804094399999999987	N0	GLOBAL PHARM	0.112000000000000002
1246	2018-08-10	46255821	115681110	BUPROPION HCL XL 150 MG TAB	14	6.16000000000000014	0	0	6.16000000000000014	3.20999999999999996	2.95000000000000018	0.478896099999999991	N0	GLOBAL PHARM	0.112000000000000002
1247	2018-08-31	46258311	115681110	BUPROPION HCL XL 150 MG TAB	15	10.1300000000000008	0	0.619999999999999996	10.75	3.43999999999999995	7.30999999999999961	0.680000000000000049	N0	GLOBAL PHARM	0.112000000000000002
1248	2018-08-28	46246549	115681110	BUPROPION HCL XL 150 MG TAB	30	5.49000000000000021	0	1	6.49000000000000021	6.88999999999999968	-0.400000000000000022	-0.0616332799999999986	N0	GLOBAL PHARM	0.112000000000000002
1249	2018-08-17	46256712	42806034905	BUPROPION HCL XL 300 MG TAB	30	9.91000000000000014	0	1	10.9100000000000001	8.41999999999999993	2.49000000000000021	0.228230980000000028	N0	EPIC PHARMA LLC	0.280500000000000027
1250	2018-08-31	46258310	42806034905	BUPROPION HCL XL 300 MG TAB	15	9.75	0	0.619999999999999996	10.3699999999999992	4.20999999999999996	6.16000000000000014	0.594021220000000016	N0	EPIC PHARMA LLC	0.280500000000000027
1251	2018-08-28	46249228	42806034905	BUPROPION HCL XL 300 MG TAB	30	0	0	15.3300000000000001	15.3300000000000001	11.5	3.83000000000000007	0.249836920000000018	N0	EPIC PHARMA LLC	0.280500000000000027
1252	2018-08-18	46253375	42806034905	BUPROPION HCL XL 300 MG TAB	30	10.9100000000000001	0	0	10.9100000000000001	8.41999999999999993	2.49000000000000021	0.228230980000000028	N0	EPIC PHARMA LLC	0.280500000000000027
1253	2018-08-21	46257028	45963014230	BUPROPION HCL XL 300 MG TAB	30	22.7699999999999996	0	0	22.7699999999999996	11.5	11.2699999999999996	0.494949489999999992	N0	ACTAVIS PHARMA/	0.383299999999999974
1254	2018-08-23	46257342	378116591	BUSPIRONE 15MG TAB	90	2.35999999999999988	0	1	3.35999999999999988	6.83999999999999986	-3.47999999999999998	-1.03571429000000004	N0	MYLAN	0.0488000000000000031
1255	2018-08-23	46257397	115169101	BUSPIRONE HCL 10 MG TABLET	60	0	0	5.20000000000000018	5.20000000000000018	3.16000000000000014	2.04000000000000004	0.392307690000000042	N0	IMPAX GENERICS	0.0526000000000000009
1256	2018-08-22	46257280	115169101	BUSPIRONE HCL 10 MG TABLET	3	7.42999999999999972	0	0	7.42999999999999972	0.160000000000000003	7.26999999999999957	0.978465680000000004	N0	IMPAX GENERICS	0.0526000000000000009
1257	2018-08-09	46252294	115169101	BUSPIRONE HCL 10 MG TABLET	60	7.71999999999999975	0	0	7.71999999999999975	3.16000000000000014	4.55999999999999961	0.590673580000000031	N0	IMPAX GENERICS	0.0526000000000000009
1258	2018-08-31	46258262	68382018105	BUSPIRONE HCL 10 MG TABLET	60	3.66999999999999993	0	1.25	4.91999999999999993	2.5	2.41999999999999993	0.491869919999999961	N0	ZYDUS PHARMACEU	0.0415999999999999981
1259	2018-08-28	46257956	115169101	BUSPIRONE HCL 10 MG TABLET	15	21.1499999999999986	0	0	21.1499999999999986	0.790000000000000036	20.3599999999999994	0.962647750000000024	N0	IMPAX GENERICS	0.0526000000000000009
1260	2018-08-03	46254896	68382018105	BUSPIRONE HCL 10 MG TABLET	60	3.68999999999999995	0	1.22999999999999998	4.91999999999999993	2.5	2.41999999999999993	0.491869919999999961	N0	ZYDUS PHARMACEU	0.0415999999999999981
1261	2018-08-14	46244906	68382018105	BUSPIRONE HCL 10 MG TABLET	60	0	0	11.0199999999999996	11.0199999999999996	2.5	8.51999999999999957	0.773139750000000014	N0	ZYDUS PHARMACEU	0.0415999999999999981
1262	2018-08-28	46253964	68382018105	BUSPIRONE HCL 10 MG TABLET	60	3.66999999999999993	0	1.25	4.91999999999999993	2.5	2.41999999999999993	0.491869919999999961	N0	ZYDUS PHARMACEU	0.0415999999999999981
1263	2018-08-28	46257890	68382018105	BUSPIRONE HCL 10 MG TABLET	60	7.71999999999999975	0	0	7.71999999999999975	2.5	5.21999999999999975	0.676165800000000039	N0	ZYDUS PHARMACEU	0.0415999999999999981
1264	2018-08-01	46251389	115169202	BUSPIRONE HCL 15 MG TABLET	60	8.3100000000000005	0	1.25	9.5600000000000005	4.19000000000000039	5.37000000000000011	0.561715480000000045	N0	IMPAX GENERICS	0.0699000000000000038
1265	2018-08-13	46250612	115169202	BUSPIRONE HCL 15 MG TABLET	75	5.55999999999999961	0	1.25	6.80999999999999961	5.24000000000000021	1.57000000000000006	0.230543319999999996	N0	IMPAX GENERICS	0.0699000000000000038
1266	2018-08-03	46253975	115169202	BUSPIRONE HCL 15 MG TABLET	30	0.349999999999999978	0	1	1.35000000000000009	2.10000000000000009	-0.75	-0.555555560000000059	N0	IMPAX GENERICS	0.0699000000000000038
1267	2018-08-09	46255652	115169202	BUSPIRONE HCL 15 MG TABLET	90	0	0	13.1400000000000006	13.1400000000000006	6.29000000000000004	6.84999999999999964	0.521308980000000033	N0	IMPAX GENERICS	0.0699000000000000038
1268	2018-08-28	46253975	115169202	BUSPIRONE HCL 15 MG TABLET	30	0.349999999999999978	0	1	1.35000000000000009	2.10000000000000009	-0.75	-0.555555560000000059	N0	IMPAX GENERICS	0.0699000000000000038
1269	2018-08-28	46251466	115169202	BUSPIRONE HCL 15 MG TABLET	60	5.37000000000000011	0	1.25	6.62000000000000011	4.19000000000000039	2.43000000000000016	0.367069489999999998	N0	IMPAX GENERICS	0.0699000000000000038
1270	2018-08-07	46255248	115169202	BUSPIRONE HCL 15 MG TABLET	60	7.12000000000000011	0	1.25	8.36999999999999922	4.19000000000000039	4.17999999999999972	0.499402630000000014	N0	IMPAX GENERICS	0.0699000000000000038
1271	2018-08-21	46253903	378116580	BUSPIRONE HCL 15MG	60	1.35000000000000009	0	1	2.35000000000000009	4.19000000000000039	-1.84000000000000008	-0.782978720000000017	N0	MYLAN	0.0671000000000000069
1272	2018-08-03	46251455	378116580	BUSPIRONE HCL 15MG	90	11.8800000000000008	0	0	11.8800000000000008	6.04000000000000004	5.83999999999999986	0.491582489999999983	N0	MYLAN	0.0671000000000000069
1273	2018-08-15	46256422	93520006	BUSPIRONE HCL 30 MG TABLET	30	96.6899999999999977	0	0	96.6899999999999977	12.4800000000000004	84.2099999999999937	0.870927710000000022	N0	TEVA USA	0.231300000000000006
1274	2018-08-27	46256422	93520006	BUSPIRONE HCL 30 MG TABLET	30	96.6899999999999977	0	0	96.6899999999999977	12.4800000000000004	84.2099999999999937	0.870927710000000022	N0	TEVA USA	0.231300000000000006
1275	2018-08-09	46255554	93520006	BUSPIRONE HCL 30 MG TABLET	90	67.019999999999996	0	1.25	68.269999999999996	37.4399999999999977	30.8299999999999983	0.451589280000000037	N0	TEVA USA	0.231300000000000006
1276	2018-08-28	46257957	68382018005	BUSPIRONE HCL 5 MG TABLET	45	32.6099999999999994	0	0	32.6099999999999994	1.10000000000000009	31.5100000000000016	0.966268020000000005	N0	ZYDUS PHARMACEU	0.0244000000000000015
1277	2018-08-13	46252507	115169001	BUSPIRONE HCL 5 MG TABLET	30	23.6700000000000017	0	0	23.6700000000000017	0.839999999999999969	22.8299999999999983	0.964512040000000015	N0	IMPAX GENERICS	0.0281
1278	2018-08-27	46252507	115169001	BUSPIRONE HCL 5 MG TABLET	30	23.6700000000000017	0	0	23.6700000000000017	0.839999999999999969	22.8299999999999983	0.964512040000000015	N0	IMPAX GENERICS	0.0281
1279	2018-08-05	46255079	115169001	BUSPIRONE HCL 5 MG TABLET	9	9.90000000000000036	0	0	9.90000000000000036	0.25	9.65000000000000036	0.974747469999999949	N0	IMPAX GENERICS	0.0281
1280	2018-08-23	46243213	68382018005	BUSPIRONE HCL 5 MG TABLET	90	2.10000000000000009	0	0.530000000000000027	2.62999999999999989	2.20000000000000018	0.429999999999999993	0.163498100000000007	N0	ZYDUS PHARMACEU	0.0244000000000000015
1281	2018-08-17	46238922	16729020101	BUSPIRONE HCL 7.5 MG TABLET	90	84.9399999999999977	0	0	84.9399999999999977	47.2000000000000028	37.740000000000002	0.444313629999999959	N0	ACCORD HEALTHCA	0.325500000000000012
1282	2018-08-23	46254022	16729020101	BUSPIRONE HCL 7.5 MG TABLET	90	62.2299999999999969	0	0	62.2299999999999969	47.2000000000000028	15.0299999999999994	0.24152338000000001	N0	ACCORD HEALTHCA	0.325500000000000012
1283	2018-08-30	44032550	527155201	BUTALBITAL COMPOUND CAPSU	60	76.8900000000000006	0	0	76.8900000000000006	48.9299999999999997	27.9600000000000009	0.363636359999999992	N0	LANNETT CO. INC	0.815500000000000003
1284	2018-08-21	44032434	527155201	BUTALBITAL COMPOUND CAPSU	60	76.8900000000000006	0	0	76.8900000000000006	48.9299999999999997	27.9600000000000009	0.363636359999999992	N0	LANNETT CO. INC	0.815500000000000003
1285	2018-08-03	44032232	527155201	BUTALBITAL COMPOUND CAPSU	60	76.8900000000000006	0	0	76.8900000000000006	48.9299999999999997	27.9600000000000009	0.363636359999999992	N0	LANNETT CO. INC	0.815500000000000003
1286	2018-08-13	44032351	527155201	BUTALBITAL COMPOUND CAPSU	60	76.8900000000000006	0	0	76.8900000000000006	48.9299999999999997	27.9600000000000009	0.363636359999999992	N0	LANNETT CO. INC	0.815500000000000003
1287	2018-08-02	44031511	527155201	BUTALBITAL COMPOUND CAPSU	60	46.1899999999999977	0	15	61.1899999999999977	48.9299999999999997	12.2599999999999998	0.200359540000000003	N0	LANNETT CO. INC	0.815500000000000003
1288	2018-08-18	44032407	591336901	BUTALBITAL-APAP-CAFFEINE TA	80	62.4600000000000009	0	2.20000000000000018	64.6599999999999966	41.5600000000000023	23.1000000000000014	0.35725332999999998	N0	ACTAVIS PHARMA/	0.519499999999999962
1289	2018-08-18	46256781	51672210002	BUTENAFINE HCL 1% CREAM	30	0	0	14.0399999999999991	14.0399999999999991	7.28000000000000025	6.75999999999999979	0.481481480000000017	N0	TARO PHARM USA	0.242499999999999993
1290	2018-08-10	46248510	310653004	BYDUREON 2 MG PEN INJECT	4	669.990000000000009	26.2600000000000016	9	705.25	585.730000000000018	119.519999999999996	0.169471820000000023	N0	ASTRAZENECA	146.432500000000005
1291	2018-08-01	46254578	456141030	BYSTOLIC 10 MG TABLET	90	65.1899999999999977	255	45	365.189999999999998	354.629999999999995	10.5600000000000005	0.0289164499999999998	N0	FOREST/ALLERGAN	3.94030000000000014
1292	2018-08-29	46247387	456141030	BYSTOLIC 10 MG TABLET	90	260.829999999999984	0	140.439999999999998	401.269999999999982	354.629999999999995	46.6400000000000006	0.116230969999999989	N0	FOREST/ALLERGAN	3.94030000000000014
1293	2018-08-05	46255083	456142030	BYSTOLIC 20 MG TABLET	6	30.6799999999999997	0	0	30.6799999999999997	23.6400000000000006	7.04000000000000004	0.229465450000000015	N0	FOREST/ALLERGAN	3.94030000000000014
1294	2018-08-27	48011596	51927120200	CAFFEINE CIT 20MG/ML SOL	55	0	21.370000000000001	0	21.370000000000001	2	19.370000000000001	0.90641086000000004	N0		0
1295	2018-08-14	46256244	64380072306	CALCITRIOL 0.25 MCG CAPSULE	15	19.4200000000000017	0	0	19.4200000000000017	4.04000000000000004	15.3800000000000008	0.791967039999999955	N0	STRIDES PHARMA	0.269600000000000006
1296	2018-08-19	46256821	64380072306	CALCITRIOL 0.25 MCG CAPSULE	5	9.14000000000000057	0	0	9.14000000000000057	1.35000000000000009	7.79000000000000004	0.852297590000000049	N0	STRIDES PHARMA	0.269600000000000006
1297	2018-08-02	46233948	64380072306	CALCITRIOL 0.25 MCG CAPSULE	36	0	0	49.7899999999999991	49.7899999999999991	9.71000000000000085	40.0799999999999983	0.804980919999999989	N0	STRIDES PHARMA	0.269600000000000006
1298	2018-08-09	46255693	64380072306	CALCITRIOL 0.25 MCG CAPSULE	10	14.2899999999999991	0	0	14.2899999999999991	2.70000000000000018	11.5899999999999999	0.811056679999999974	N0	STRIDES PHARMA	0.269600000000000006
1299	2018-08-20	46247709	64380072306	CALCITRIOL 0.25 MCG CAPSULE	8	4.20000000000000018	0	0	4.20000000000000018	2.16000000000000014	2.04000000000000004	0.485714289999999993	N0	STRIDES PHARMA	0.269600000000000006
1300	2018-08-21	46257008	63304023901	CALCITRIOL 0.25 MCG CAPSULE	8	3.08999999999999986	0	1.15999999999999992	4.25	2.16000000000000014	2.08999999999999986	0.491764709999999994	N0	RANBAXY/SUN PHA	0.270100000000000007
1301	2018-08-06	46241797	904546092	CALCIUM + D 500/200	180	0	0	5.66999999999999993	5.66999999999999993	3.08000000000000007	2.58999999999999986	0.456790120000000022	N0	MAJOR PHARMACEU	0.0171000000000000006
1302	2018-08-20	46256973	904546092	CALCIUM + D 500/200	60	0	0	3.2200000000000002	3.2200000000000002	1.03000000000000003	2.18999999999999995	0.680124220000000057	N0	MAJOR PHARMACEU	0.0171000000000000006
1303	2018-08-06	46217646	6551007090	CALCIUM 500 W/D 400MG	60	0	0	5.5	5.5	3.5	2	0.363636359999999992	N0	NATURE'S BOUNTY	0.0582999999999999977
1304	2018-08-02	46254737	46122007887	CALCIUM 600-VIT D3 800 TABL	300	0	0	9.99000000000000021	9.99000000000000021	8.78999999999999915	1.19999999999999996	0.120120119999999997	N0	AMERISOURCEBERG	0.0292999999999999997
1305	2018-08-29	46248486	68180013415	CALCIUM ACETATE 667 MG CAPS	180	88.75	0	0	88.75	184.77000000000001	-96.019999999999996	-1.08191548999999987	N0	LUPIN PHARMACEU	1.02649999999999997
1306	2018-08-16	46256539	63868005315	CALCIUM ANTACID   500MG	5	0	0	15	15	0.0599999999999999978	14.9399999999999995	0.995999999999999996	N0	CHAIN DRUG	0.0120999999999999996
1307	2018-08-29	46258045	6386800572	CALCIUM ANTACID 1,000 MG TA	72	0	0	3.99000000000000021	3.99000000000000021	1.75	2.24000000000000021	0.561403509999999994	N	CHAIN DRUG	0.0242999999999999987
1308	2018-08-04	46251981	54311763	CALCIUM CARB   1250MG/5ML*	165	3.99000000000000021	0	0	3.99000000000000021	2.33999999999999986	1.64999999999999991	0.413533829999999991	N0	ROXANE/WEST-WAR	0.0142000000000000008
1309	2018-08-13	46250405	54311763	CALCIUM CARB   1250MG/5ML*	150	3.12999999999999989	0	0	3.12999999999999989	2.12999999999999989	1	0.319488820000000007	N0	ROXANE/WEST-WAR	0.0142000000000000008
1310	2018-08-17	46242610	54311763	CALCIUM CARB   1250MG/5ML*	75	0	1.75	0	1.75	1.07000000000000006	0.680000000000000049	0.388571429999999995	N0	ROXANE/WEST-WAR	0.0142000000000000008
1311	2018-08-13	46249958	54311763	CALCIUM CARB   1250MG/5ML*	60	1.45999999999999996	0	0	1.45999999999999996	0.849999999999999978	0.609999999999999987	0.417808220000000008	N0	ROXANE/WEST-WAR	0.0142000000000000008
1312	2018-08-14	46223004	54311763	CALCIUM CARB   1250MG/5ML*	300	5.91999999999999993	0	0	5.91999999999999993	4.25999999999999979	1.65999999999999992	0.280405410000000022	N0	ROXANE/WEST-WAR	0.0142000000000000008
1313	2018-08-25	46244507	54311763	CALCIUM CARB   1250MG/5ML*	75	0	2.35999999999999988	0	2.35999999999999988	1.07000000000000006	1.29000000000000004	0.546610170000000006	N0	ROXANE/WEST-WAR	0.0142000000000000008
1314	2018-08-13	46249427	904323352	CALCIUM W/VIT D3  600/400 T	60	0	0	3.43000000000000016	3.43000000000000016	1.18999999999999995	2.24000000000000021	0.653061219999999998	N0	MAJOR PHARMACEU	0.019900000000000001
1315	2018-08-22	46235929	904323352	CALCIUM W/VIT D3  600/400 T	60	0	0	3.43000000000000016	3.43000000000000016	1.18999999999999995	2.24000000000000021	0.653061219999999998	N0	MAJOR PHARMACEU	0.019900000000000001
1316	2018-08-01	46240647	904546080	CALCIUM500 OYS SHL/ D200 TA	60	0	0	2.83000000000000007	2.83000000000000007	0.569999999999999951	2.25999999999999979	0.798586569999999885	N0	MAJOR PHARMACEU	0.00949999999999999976
1317	2018-08-30	46240647	904546080	CALCIUM500 OYS SHL/ D200 TA	60	0	0	2.83000000000000007	2.83000000000000007	0.569999999999999951	2.25999999999999979	0.798586569999999885	N0	MAJOR PHARMACEU	0.00949999999999999976
1318	2018-08-08	46233562	904546080	CALCIUM500 OYS SHL/ D200 TA	60	2.33999999999999986	0	0	2.33999999999999986	0.569999999999999951	1.77000000000000002	0.756410260000000001	N0	MAJOR PHARMACEU	0.00949999999999999976
1319	2018-08-20	46221107	799000104	CALMOSEPTINE OINTMENT	113	0	0	9.5	9.5	4.26999999999999957	5.23000000000000043	0.550526320000000013	N0	CALMOSEPTINE	0.0378000000000000003
1320	2018-08-08	46245233	51862010206	CAMILA 0.35 MG TABLET	28	2.37000000000000011	0	0	2.37000000000000011	10.2300000000000004	-7.86000000000000032	-3.31645570000000012	N0	MAYNE PHARMA IN	0.365200000000000025
1321	2018-08-06	46240832	93313482	CAMRESE 0.15-0.03-0.01 MG T	91	127.439999999999998	0	0	127.439999999999998	54.3800000000000026	73.0600000000000023	0.57328939000000001	N0	TEVA USA	0.59760000000000002
1322	2018-08-14	46256150	0	CANDY BAR	2	0	0	2	2	1	1	0.5	N0		0.499900000000000011
1323	2018-08-30	46256150	0	CANDY BAR	2	0	0	2	2	1	1	0.5	N0		0.499900000000000011
1324	2018-08-21	46252158	0	CANDY BAR	3	0	0	3	3	1.5	1.5	0.5	N0		0.499900000000000011
1325	2018-08-01	46248625	0	CANDY BAR	2	0	0	2	2	1	1	0.5	N0		0.499900000000000011
1326	2018-08-07	46252158	0	CANDY BAR	3	0	0	3	3	1.5	1.5	0.5	N0		0.499900000000000011
1327	2018-08-09	46219829	0	CANDY BAR	2	0	0	2	2	1	1	0.5	N0		0.499900000000000011
1328	2018-08-09	46253336	143117101	CAPTOPRIL 12.5MG TABLET	15	23.8599999999999994	0	0	23.8599999999999994	8.39000000000000057	15.4700000000000006	0.648365469999999999	N0	WEST-WARD/HIKMA	0.559100000000000041
1329	2018-08-27	46253336	143117101	CAPTOPRIL 12.5MG TABLET	15	23.8599999999999994	0	0	23.8599999999999994	8.39000000000000057	15.4700000000000006	0.648365469999999999	N0	WEST-WARD/HIKMA	0.559100000000000041
1330	2018-08-14	48011979	51927148300	CAPTOPRIL(T)(GT) 1MG/ML S	480	36.3200000000000003	0	0	36.3200000000000003	20.8500000000000014	15.4700000000000006	0.425936120000000029	N0		0
1331	2018-08-24	44032461	62991102702	CARBAM/DIAZ 250MG-5MG SUP	9	43.9099999999999966	0	0	43.9099999999999966	2.27000000000000002	41.6400000000000006	0.9483033500000001	N0		0
1332	2018-08-22	44032461	62991102702	CARBAM/DIAZ 250MG-5MG SUP	6	29.2699999999999996	0	0	29.2699999999999996	1.51000000000000001	27.7600000000000016	0.948411339999999936	N0		0
1333	2018-08-20	46253443	60432012916	CARBAMAZEPINE 100 MG/5 ML S	300	32.1899999999999977	0	0	32.1899999999999977	23.5500000000000007	8.64000000000000057	0.268406340000000021	N0	MORTON GROVE PH	0.0785000000000000003
1334	2018-08-01	46253443	60432012916	CARBAMAZEPINE 100 MG/5 ML S	300	32.1899999999999977	0	0	32.1899999999999977	23.5500000000000007	8.64000000000000057	0.268406340000000021	N0	MORTON GROVE PH	0.0785000000000000003
1335	2018-08-10	46253443	60432012916	CARBAMAZEPINE 100 MG/5 ML S	300	32.1899999999999977	0	0	32.1899999999999977	23.5500000000000007	8.64000000000000057	0.268406340000000021	N0	MORTON GROVE PH	0.0785000000000000003
1336	2018-08-30	46253443	60432012916	CARBAMAZEPINE 100 MG/5 ML S	300	32.1899999999999977	0	0	32.1899999999999977	23.5500000000000007	8.64000000000000057	0.268406340000000021	N0	MORTON GROVE PH	0.0785000000000000003
1337	2018-08-13	46254444	51672404101	CARBAMAZEPINE 100MG TAB CHE	60	37.5799999999999983	0	0	37.5799999999999983	18.0300000000000011	19.5500000000000007	0.520223519999999939	N0	TARO PHARM USA	0.300499999999999989
1338	2018-08-01	46250645	13668026810	CARBAMAZEPINE 200 MG TABLET	60	46.9299999999999997	0	0	46.9299999999999997	19.9699999999999989	26.9600000000000009	0.574472619999999989	N0	TORRENT PHARMAC	0.332899999999999974
1339	2018-08-22	46256704	60505018300	CARBAMAZEPINE 200 MG TABLET	60	43.1799999999999997	0	1.19999999999999996	44.3800000000000026	84.4399999999999977	-40.0600000000000023	-0.902658859999999952	N0	APOTEX CORP	0.243999999999999995
1340	2018-08-08	46248246	13668026810	CARBAMAZEPINE 200 MG TABLET	90	132.780000000000001	0	0	132.780000000000001	29.9600000000000009	102.819999999999993	0.774363610000000091	N0	TORRENT PHARMAC	0.332899999999999974
1341	2018-08-29	46247403	93010901	CARBAMAZEPINE 200MG TABLE	102	38.3999999999999986	0	0	38.3999999999999986	56.6400000000000006	-18.2399999999999984	-0.474999999999999978	N0	TEVA USA	0.555300000000000016
1342	2018-08-22	48012447	62991102702	CARBAMAZEPINE 250MG SUPP	12	0	0	33.9099999999999966	33.9099999999999966	2.20999999999999996	31.6999999999999993	0.934827480000000044	N		0
1343	2018-08-28	48012447	62991102702	CARBAMAZEPINE 250MG SUPP	12	0	0	33.9099999999999966	33.9099999999999966	2.20999999999999996	31.6999999999999993	0.934827480000000044	N		0
1344	2018-08-07	48012367	62991102702	CARBAMAZEPINE 250MG SUPP	6	14.4900000000000002	0	0	14.4900000000000002	1.1100000000000001	13.3800000000000008	0.923395450000000007	N0		0
1345	2018-08-06	48012364	62991102702	CARBAMAZEPINE 250MG SUPP	6	14.4900000000000002	0	0	14.4900000000000002	1.1100000000000001	13.3800000000000008	0.923395450000000007	N0		0
1346	2018-08-08	48012378	62991102702	CARBAMAZEPINE 250MG SUPP	30	65.7199999999999989	0	0	65.7199999999999989	5.53000000000000025	60.1899999999999977	0.915855140000000012	N0		0
1347	2018-08-10	48012393	62991102702	CARBAMAZEPINE 250MG SUPP	30	65.7199999999999989	0	0	65.7199999999999989	5.53000000000000025	60.1899999999999977	0.915855140000000012	N0		0
1348	2018-08-18	48012429	62991102702	CARBAMAZEPINE 250MG SUPP	20	45.1400000000000006	0	0	45.1400000000000006	3.68999999999999995	41.4500000000000028	0.918254320000000068	N0		0
1349	2018-08-20	48012429	62991102702	CARBAMAZEPINE 250MG SUPP	20	45.1400000000000006	0	0	45.1400000000000006	3.68999999999999995	41.4500000000000028	0.918254320000000068	N0		0
1350	2018-08-07	48012366	62991102702	CARBAMAZEPINE 500MG SUPP	1	6.78000000000000025	0	0	6.78000000000000025	0.320000000000000007	6.45999999999999996	0.95280236000000007	N0		0
1351	2018-08-06	48012363	62991102702	CARBAMAZEPINE 500MG SUPP	1	6.78000000000000025	0	0	6.78000000000000025	0.320000000000000007	6.45999999999999996	0.95280236000000007	N0		0
1352	2018-08-22	48012443	62991102702	CARBAMAZEPINE 500MG SUPP	1	6.78000000000000025	0	0	6.78000000000000025	0.320000000000000007	6.45999999999999996	0.95280236000000007	N0		0
1353	2018-08-24	46246814	62756051818	CARBIDOPA-LEVODOPA 25-100 T	240	0	0	53.1499999999999986	53.1499999999999986	16.3000000000000007	36.8500000000000014	0.693320789999999909	N0	SUN PHARMACEUTI	0.067900000000000002
1354	2018-08-23	46257365	62756051813	CARBIDOPA-LEVODOPA 25-100 T	180	13.3800000000000008	0	0	13.3800000000000008	12.1300000000000008	1.25	0.0934230199999999955	N0	SUN PHARMACEUTI	0.0674000000000000016
1355	2018-08-17	46256600	62756051818	CARBIDOPA-LEVODOPA 25-100 T	150	2.79999999999999982	0	15	17.8000000000000007	10.1899999999999995	7.61000000000000032	0.427528089999999972	N0	SUN PHARMACEUTI	0.067900000000000002
1356	2018-08-27	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1357	2018-08-30	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1358	2018-08-18	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1359	2018-08-24	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1360	2018-08-06	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1361	2018-08-12	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1362	2018-08-01	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	22.8999999999999986	0	0	22.8999999999999986	2.54999999999999982	20.3500000000000014	0.88864628999999995	N0	ACTAVIS PHARMA/	0.127500000000000002
1363	2018-08-09	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1364	2018-08-15	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1365	2018-08-21	46254144	228254010	CARBIDOPA/LEVO 25/250 TAB	12	15.3399999999999999	0	0	15.3399999999999999	1.53000000000000003	13.8100000000000005	0.900260759999999993	N0	ACTAVIS PHARMA/	0.127500000000000002
1366	2018-08-15	46242055	378008801	CARBIDOPA/LEVO ER 25/100	30	0	0	12.7300000000000004	12.7300000000000004	8.23000000000000043	4.5	0.353495679999999979	N0	MYLAN	0.274299999999999988
1367	2018-08-15	44031455	603258232	CARISOPRODOL 350MG	90	0	0	31.5300000000000011	31.5300000000000011	8.14000000000000057	23.3900000000000006	0.741833170000000042	N0	QUALITEST	0.0903999999999999942
1368	2018-08-10	44032320	591551310	CARISOPRODOL 350MG	120	0	0	32.0900000000000034	32.0900000000000034	8.10999999999999943	23.9800000000000004	0.747273289999999979	N0	ACTAVIS PHARMA/	0.0675999999999999934
1369	2018-08-17	44031158	591551310	CARISOPRODOL 350MG	120	45	0	0	45	8.10999999999999943	36.8900000000000006	0.819777780000000011	N0	ACTAVIS PHARMA/	0.0675999999999999934
1370	2018-08-21	44032440	591551310	CARISOPRODOL 350MG	120	0	0	60	60	8.10999999999999943	51.8900000000000006	0.864833330000000067	N0	ACTAVIS PHARMA/	0.0675999999999999934
1371	2018-08-11	46224576	62037059790	CARTIA XT 120MG	60	5.51999999999999957	0	45	50.5200000000000031	16	34.5200000000000031	0.683293750000000033	N0	ACTAVIS PHARMA/	0.266699999999999993
1372	2018-08-30	46247389	68462016401	CARVEDILOL 12.5 MG TABLET	30	20.0799999999999983	0	0	20.0799999999999983	0.599999999999999978	19.4800000000000004	0.970119519999999902	N0	GLENMARK PHARMA	0.0200999999999999998
1373	2018-08-11	46231843	68462016405	CARVEDILOL 12.5 MG TABLET	30	0.209999999999999992	0	1.25	1.45999999999999996	0.550000000000000044	0.910000000000000031	0.62328766999999996	N0	GLENMARK PHARMA	0.0182000000000000009
1374	2018-08-06	46251072	68382009405	CARVEDILOL 12.5 MG TABLET	30	20.0799999999999983	0	0	20.0799999999999983	0.800000000000000044	19.2800000000000011	0.960159360000000017	N0	ZYDUS PHARMACEU	0.0265999999999999986
1375	2018-08-31	46258289	68462016405	CARVEDILOL 12.5 MG TABLET	3	5.61000000000000032	0	0	5.61000000000000032	0.0500000000000000028	5.55999999999999961	0.991087339999999983	N0	GLENMARK PHARMA	0.0182000000000000009
1376	2018-08-24	46257263	68382009405	CARVEDILOL 12.5 MG TABLET	4	6.13999999999999968	0	0	6.13999999999999968	0.110000000000000001	6.03000000000000025	0.982084689999999982	N0	ZYDUS PHARMACEU	0.0265999999999999986
1377	2018-08-04	46254947	68462016405	CARVEDILOL 12.5 MG TABLET	6	4.86000000000000032	0	0	4.86000000000000032	0.110000000000000001	4.75	0.977366260000000042	N0	GLENMARK PHARMA	0.0182000000000000009
1378	2018-08-04	46245041	68462016405	CARVEDILOL 12.5 MG TABLET	60	0	0	4.41999999999999993	4.41999999999999993	1.09000000000000008	3.33000000000000007	0.753393669999999904	N0	GLENMARK PHARMA	0.0182000000000000009
1379	2018-08-28	46257946	68462016405	CARVEDILOL 12.5 MG TABLET	180	7.29000000000000004	0	1.25	8.53999999999999915	3.2799999999999998	5.25999999999999979	0.615925060000000024	N0	GLENMARK PHARMA	0.0182000000000000009
1380	2018-08-11	46255538	68462016405	CARVEDILOL 12.5 MG TABLET	6	7	0	0	7	0.110000000000000001	6.88999999999999968	0.984285710000000091	N0	GLENMARK PHARMA	0.0182000000000000009
1381	2018-08-02	46254848	68462016405	CARVEDILOL 12.5 MG TABLET	6	4.86000000000000032	0	0	4.86000000000000032	0.110000000000000001	4.75	0.977366260000000042	N0	GLENMARK PHARMA	0.0182000000000000009
1382	2018-08-22	46257263	68382009405	CARVEDILOL 12.5 MG TABLET	4	6.13999999999999968	0	0	6.13999999999999968	0.110000000000000001	6.03000000000000025	0.982084689999999982	N0	ZYDUS PHARMACEU	0.0265999999999999986
1383	2018-08-22	46257226	68462016401	CARVEDILOL 12.5 MG TABLET	30	20.0799999999999983	0	0	20.0799999999999983	0.800000000000000044	19.2800000000000011	0.960159360000000017	N0	GLENMARK PHARMA	0.0200999999999999998
1384	2018-08-27	46253052	68462016405	CARVEDILOL 12.5 MG TABLET	30	16.9800000000000004	0	0	16.9800000000000004	0.550000000000000044	16.4299999999999997	0.967608950000000023	N0	GLENMARK PHARMA	0.0182000000000000009
1385	2018-08-15	46242485	68382009405	CARVEDILOL 12.5 MG TABLET	60	3.81999999999999984	0	3.35000000000000009	7.16999999999999993	1.60000000000000009	5.57000000000000028	0.776847979999999994	N0	ZYDUS PHARMACEU	0.0265999999999999986
1386	2018-08-02	46247389	68462016401	CARVEDILOL 12.5 MG TABLET	30	20.0799999999999983	0	0	20.0799999999999983	0.599999999999999978	19.4800000000000004	0.970119519999999902	N0	GLENMARK PHARMA	0.0200999999999999998
1387	2018-08-08	46254462	68462016405	CARVEDILOL 12.5 MG TABLET	90	2.75	0	1.25	4	1.6399999999999999	2.35999999999999988	0.589999999999999969	N0	GLENMARK PHARMA	0.0182000000000000009
1388	2018-08-16	46247389	68462016401	CARVEDILOL 12.5 MG TABLET	30	20.0799999999999983	0	0	20.0799999999999983	0.599999999999999978	19.4800000000000004	0.970119519999999902	N0	GLENMARK PHARMA	0.0200999999999999998
1389	2018-08-12	46255910	68382009405	CARVEDILOL 12.5 MG TABLET	14	11.5	0	0	11.5	0.369999999999999996	11.1300000000000008	0.967826089999999972	N0	ZYDUS PHARMACEU	0.0265999999999999986
1390	2018-08-24	46251072	68382009405	CARVEDILOL 12.5 MG TABLET	30	20.0799999999999983	0	0	20.0799999999999983	0.800000000000000044	19.2800000000000011	0.960159360000000017	N0	ZYDUS PHARMACEU	0.0265999999999999986
1391	2018-08-08	46255538	68462016405	CARVEDILOL 12.5 MG TABLET	6	7	0	0	7	0.110000000000000001	6.88999999999999968	0.984285710000000091	N0	GLENMARK PHARMA	0.0182000000000000009
1392	2018-08-14	46247626	68462016505	CARVEDILOL 25 MG TABLET	60	0.0700000000000000067	0	10	10.0700000000000003	1.26000000000000001	8.8100000000000005	0.87487587	N0	GLENMARK PHARMA	0.0210000000000000013
1393	2018-08-17	46256733	68382009501	CARVEDILOL 25 MG TABLET	10	14.5600000000000005	0	0	14.5600000000000005	0.270000000000000018	14.2899999999999991	0.981456040000000085	N0	ZYDUS PHARMACEU	0.0270999999999999991
1394	2018-08-06	46242906	68382009501	CARVEDILOL 25 MG TABLET	180	16.1600000000000001	0	0	16.1600000000000001	4.87999999999999989	11.2799999999999994	0.698019799999999968	N0	ZYDUS PHARMACEU	0.0270999999999999991
1395	2018-08-29	46250839	68001015203	CARVEDILOL 25 MG TABLET	60	5.91999999999999993	0	1.25	7.16999999999999993	2.06000000000000005	5.11000000000000032	0.712691769999999947	N0	BLUEPOINT LABOR	0.0344
1396	2018-08-25	46245875	68462016505	CARVEDILOL 25 MG TABLET	60	3.70000000000000018	0	1.25	4.95000000000000018	1.26000000000000001	3.68999999999999995	0.745454550000000049	N0	GLENMARK PHARMA	0.0210000000000000013
1397	2018-08-13	46246986	68462016505	CARVEDILOL 25 MG TABLET	60	6.04999999999999982	0	0	6.04999999999999982	1.26000000000000001	4.79000000000000004	0.791735539999999904	N0	GLENMARK PHARMA	0.0210000000000000013
1398	2018-08-07	46255343	68462016205	CARVEDILOL 3.125 MG TABLET	30	13.2699999999999996	0	0	13.2699999999999996	0.550000000000000044	12.7200000000000006	0.958553129999999975	N0	GLENMARK PHARMA	0.0182000000000000009
1399	2018-08-31	46233921	68462016205	CARVEDILOL 3.125 MG TABLET	60	0.0200000000000000004	0	10	10.0199999999999996	1.09000000000000008	8.92999999999999972	0.891217560000000075	N0	GLENMARK PHARMA	0.0182000000000000009
1400	2018-08-27	46254361	68462016205	CARVEDILOL 3.125 MG TABLET	30	13.2699999999999996	0	0	13.2699999999999996	0.550000000000000044	12.7200000000000006	0.958553129999999975	N0	GLENMARK PHARMA	0.0182000000000000009
1401	2018-08-10	46255802	68462016205	CARVEDILOL 3.125 MG TABLET	10	7.08999999999999986	0	0	7.08999999999999986	0.179999999999999993	6.91000000000000014	0.974612130000000021	N0	GLENMARK PHARMA	0.0182000000000000009
1402	2018-08-12	46255903	68462016205	CARVEDILOL 3.125 MG TABLET	6	5.84999999999999964	0	0	5.84999999999999964	0.110000000000000001	5.74000000000000021	0.981196579999999985	N0	GLENMARK PHARMA	0.0182000000000000009
1403	2018-08-28	46238432	68462016205	CARVEDILOL 3.125 MG TABLET	30	13.2699999999999996	0	0	13.2699999999999996	0.550000000000000044	12.7200000000000006	0.958553129999999975	N0	GLENMARK PHARMA	0.0182000000000000009
1404	2018-08-21	46254361	68462016205	CARVEDILOL 3.125 MG TABLET	30	13.2699999999999996	0	0	13.2699999999999996	0.550000000000000044	12.7200000000000006	0.958553129999999975	N0	GLENMARK PHARMA	0.0182000000000000009
1405	2018-08-14	46238432	68462016205	CARVEDILOL 3.125 MG TABLET	30	13.2699999999999996	0	0	13.2699999999999996	0.550000000000000044	12.7200000000000006	0.958553129999999975	N0	GLENMARK PHARMA	0.0182000000000000009
1406	2018-08-29	46255343	68462016205	CARVEDILOL 3.125 MG TABLET	30	13.2699999999999996	0	0	13.2699999999999996	0.550000000000000044	12.7200000000000006	0.958553129999999975	N0	GLENMARK PHARMA	0.0182000000000000009
1407	2018-08-29	46258119	68462016205	CARVEDILOL 3.125 MG TABLET	6	5.84999999999999964	0	0	5.84999999999999964	0.110000000000000001	5.74000000000000021	0.981196579999999985	N0	GLENMARK PHARMA	0.0182000000000000009
1408	2018-08-23	46257381	68382009305	CARVEDILOL 6.25 MG TABLET	60	3.97999999999999998	0	0	3.97999999999999998	1.45999999999999996	2.52000000000000002	0.633165830000000041	N0	ZYDUS PHARMACEU	0.0244000000000000015
1409	2018-08-07	46249814	13107014305	CARVEDILOL 6.25 MG TABLET	30	13.4800000000000004	0	0	13.4800000000000004	0.609999999999999987	12.8699999999999992	0.954747769999999996	N0	AUROBINDO PHARM	0.0201999999999999992
1410	2018-08-13	46250185	68382009305	CARVEDILOL 6.25 MG TABLET	15	8.74000000000000021	0	0	8.74000000000000021	0.369999999999999996	8.36999999999999922	0.957665899999999959	N0	ZYDUS PHARMACEU	0.0244000000000000015
1411	2018-08-22	46257278	68382009305	CARVEDILOL 6.25 MG TABLET	180	6.41000000000000014	0	0	6.41000000000000014	4.38999999999999968	2.02000000000000002	0.31513260999999998	N0	ZYDUS PHARMACEU	0.0244000000000000015
1412	2018-08-13	46256091	65862014305	CARVEDILOL 6.25 MG TABLET	60	0	0	10	10	1.07000000000000006	8.92999999999999972	0.893000000000000016	N0	AUROBINDO PHARM	0.0177999999999999999
1413	2018-08-03	46254418	65862014305	CARVEDILOL 6.25 MG TABLET	4	5.25999999999999979	0	0	5.25999999999999979	0.0700000000000000067	5.19000000000000039	0.986692020000000003	N0	AUROBINDO PHARM	0.0177999999999999999
1414	2018-08-13	46246040	65862014305	CARVEDILOL 6.25 MG TABLET	30	13.4800000000000004	0	0	13.4800000000000004	0.530000000000000027	12.9499999999999993	0.960682489999999945	N0	AUROBINDO PHARM	0.0177999999999999999
1415	2018-08-15	46249814	13107014305	CARVEDILOL 6.25 MG TABLET	30	13.4800000000000004	0	0	13.4800000000000004	0.609999999999999987	12.8699999999999992	0.954747769999999996	N0	AUROBINDO PHARM	0.0201999999999999992
1416	2018-08-27	46246040	65862014305	CARVEDILOL 6.25 MG TABLET	30	13.4800000000000004	0	0	13.4800000000000004	0.530000000000000027	12.9499999999999993	0.960682489999999945	N0	AUROBINDO PHARM	0.0177999999999999999
1417	2018-08-22	46257257	65862014305	CARVEDILOL 6.25 MG TABLET	15	8.74000000000000021	0	0	8.74000000000000021	0.270000000000000018	8.47000000000000064	0.969107549999999929	N0	AUROBINDO PHARM	0.0177999999999999999
1418	2018-08-25	46245966	65862014305	CARVEDILOL 6.25 MG TABLET	60	0	0	4	4	1.07000000000000006	2.93000000000000016	0.73250000000000004	N0	AUROBINDO PHARM	0.0177999999999999999
1419	2018-08-05	46254418	65862014305	CARVEDILOL 6.25 MG TABLET	4	5.25999999999999979	0	0	5.25999999999999979	0.0700000000000000067	5.19000000000000039	0.986692020000000003	N0	AUROBINDO PHARM	0.0177999999999999999
1420	2018-08-22	46257119	65862021960	CEFDINIR 250 MG/5 ML SUSP	60	25.0799999999999983	0	1.25	26.3299999999999983	8.33999999999999986	17.9899999999999984	0.683251039999999921	N0	AUROBINDO PHARM	0.1187
1421	2018-08-24	46257581	65862021960	CEFDINIR 250 MG/5 ML SUSP	60	0	60.1700000000000017	0	60.1700000000000017	8.33999999999999986	51.8299999999999983	0.861392720000000001	N0	AUROBINDO PHARM	0.1187
1422	2018-08-14	46256250	65862017760	CEFDINIR 300 MG CAPSULE	20	2.75	0	20	22.75	16.4200000000000017	6.33000000000000007	0.278241760000000005	N0	AUROBINDO PHARM	0.491800000000000015
1423	2018-08-23	46257367	781217660	CEFDINIR 300 MG CAPSULE	7	0	0	4.29999999999999982	4.29999999999999982	5.75	-1.44999999999999996	-0.337209300000000045	N0	SANDOZ	0.820799999999999974
1424	2018-08-24	46257485	781543820	CEFPODOXIME 100 MG TABLET	8	49.8200000000000003	0	0	49.8200000000000003	36.9200000000000017	12.9000000000000004	0.258932159999999967	N0	SANDOZ	4.61549999999999994
1425	2018-08-06	46255238	65862009620	CEFPODOXIME 200 MG TABLET	14	43.6599999999999966	26.2899999999999991	8	77.9500000000000028	54.8500000000000014	23.1000000000000014	0.296343809999999985	N0	AUROBINDO PHARM	3.91749999999999998
1426	2018-08-03	46254962	65862009620	CEFPODOXIME 200 MG TABLET	14	89.2000000000000028	0	0	89.2000000000000028	54.8500000000000014	34.3500000000000014	0.385089689999999985	N0	AUROBINDO PHARM	3.91749999999999998
1427	2018-08-31	46258324	409733201	CEFTRIAXONE 1 GM VIAL	2	7.05999999999999961	0	0	7.05999999999999961	2.85000000000000009	4.20999999999999996	0.596317280000000061	N0	HOSPIRA	1.42599999999999993
1428	2018-08-01	46254619	409733201	CEFTRIAXONE 1 GM VIAL	1	5.53000000000000025	0	0	5.53000000000000025	1.42999999999999994	4.09999999999999964	0.741410489999999922	N0	HOSPIRA	1.42599999999999993
1429	2018-08-02	46254794	409733201	CEFTRIAXONE 1 GM VIAL	4	10.1199999999999992	0	0	10.1199999999999992	5.70000000000000018	4.41999999999999993	0.436758889999999955	N0	HOSPIRA	1.42599999999999993
1430	2018-08-12	46255922	60505075204	CEFTRIAXONE 1 GM VIAL	3	51.3100000000000023	0	0	51.3100000000000023	5.12000000000000011	46.1899999999999977	0.900214379999999981	N0	APOTEX CORP	1.70700000000000007
1431	2018-08-23	46257235	60505075004	CEFTRIAXONE 250 MG VIAL	1	0	0	5.79999999999999982	5.79999999999999982	1.26000000000000001	4.54000000000000004	0.782758620000000072	N0	APOTEX CORP	1.26000000000000001
1432	2018-08-09	46255604	60505615104	CEFTRIAXONE 250 MG VIAL	1	2.31000000000000005	0	10	12.3100000000000005	1.12000000000000011	11.1899999999999995	0.909017060000000043	N0	APOTEX CORP	1.1160000000000001
1433	2018-08-08	46255393	68180030220	CEFUROXIME AXETIL 250 MG TA	20	4.5	0	3.72999999999999998	8.23000000000000043	16.9299999999999997	-8.69999999999999929	-1.05710813999999997	N0	LUPIN PHARMACEU	0.846400000000000041
1434	2018-08-21	46257084	65862069920	CEFUROXIME AXETIL 250 MG TA	20	0	0	30.4899999999999984	30.4899999999999984	6.57000000000000028	23.9200000000000017	0.784519509999999975	N0	AUROBINDO PHARM	0.328500000000000014
1435	2018-08-10	46255733	68180030220	CEFUROXIME AXETIL 250 MG TA	28	10.3800000000000008	0	1	11.3800000000000008	23.6999999999999993	-12.3200000000000003	-1.08260105000000006	N0	LUPIN PHARMACEU	0.846400000000000041
1436	2018-08-08	46255477	65862070060	CEFUROXIME AXETIL 500 MG TA	14	8.38000000000000078	0	6.58999999999999986	14.9700000000000006	7.88999999999999968	7.08000000000000007	0.472945890000000035	N0	AUROBINDO PHARM	0.563899999999999957
1437	2018-08-13	46220312	64679092201	CEFUROXIME AXETIL 500MG	24	0	0	40	40	14.9900000000000002	25.0100000000000016	0.625249999999999972	N	WOCKHARDT USA L	0.624500000000000055
1438	2018-08-06	46250431	62332014231	CELECOXIB 200 MG CAPSULE	60	156.090000000000003	0	1.25	157.340000000000003	45	112.340000000000003	0.713995170000000012	N0	ALEMBIC PHARMAC	0.75
1439	2018-08-06	46255212	62332014231	CELECOXIB 200 MG CAPSULE	30	34.8500000000000014	0	3.35000000000000009	38.2000000000000028	22.5	15.6999999999999993	0.410994760000000015	N0	ALEMBIC PHARMAC	0.75
1440	2018-08-08	46255466	591398401	CELECOXIB 200 MG CAPSULE	90	186.870000000000005	0	152.889999999999986	339.759999999999991	65.2999999999999972	274.45999999999998	0.807805510000000004	N0	ACTAVIS PHARMA,	0.725600000000000023
1441	2018-08-13	46244175	65862090901	CELECOXIB 200 MG CAPSULE	60	18.0199999999999996	0	0	18.0199999999999996	14.6600000000000001	3.35999999999999988	0.186459490000000006	N0	AUROBINDO PHARM	0.244400000000000006
1442	2018-08-04	46238273	33342015715	CELECOXIB 200 MG CAPSULE	30	54.4699999999999989	0	20	74.4699999999999989	2.18000000000000016	72.2900000000000063	0.970726470000000008	N0	MACLEODS PHARMA	0.0725999999999999979
1443	2018-08-01	46254710	68180012101	CEPHALEXIN 250 MG CAPSULE	10	0	0	1.29000000000000004	1.29000000000000004	0.599999999999999978	0.689999999999999947	0.534883719999999951	N0	LUPIN PHARMACEU	0.0601000000000000006
1444	2018-08-29	46257971	68180012101	CEPHALEXIN 250 MG CAPSULE	90	12.0999999999999996	0	0	12.0999999999999996	5.41000000000000014	6.69000000000000039	0.552892559999999977	N0	LUPIN PHARMACEU	0.0601000000000000006
1445	2018-08-02	46254741	68180012101	CEPHALEXIN 250 MG CAPSULE	6	6.90000000000000036	0	0	6.90000000000000036	0.359999999999999987	6.54000000000000004	0.947826089999999954	N0	LUPIN PHARMACEU	0.0601000000000000006
1446	2018-08-30	46258202	68180012101	CEPHALEXIN 250 MG CAPSULE	14	10.7799999999999994	0	0	10.7799999999999994	0.839999999999999969	9.9399999999999995	0.922077919999999995	N0	LUPIN PHARMACEU	0.0601000000000000006
1447	2018-08-02	46249374	68180012101	CEPHALEXIN 250 MG CAPSULE	28	1.97999999999999998	0	4	5.98000000000000043	1.42999999999999994	4.54999999999999982	0.760869569999999995	N0	LUPIN PHARMACEU	0.0509999999999999967
1448	2018-08-01	46254705	68180012101	CEPHALEXIN 250 MG CAPSULE	9	8.35999999999999943	0	0	8.35999999999999943	0.540000000000000036	7.82000000000000028	0.935406700000000035	N0	LUPIN PHARMACEU	0.0601000000000000006
1449	2018-08-13	46256116	68180012101	CEPHALEXIN 250 MG CAPSULE	30	2.56000000000000005	1.43999999999999995	1	5	1.80000000000000004	3.20000000000000018	0.640000000000000013	N0	LUPIN PHARMACEU	0.0601000000000000006
1450	2018-08-24	46257481	68180044102	CEPHALEXIN 250 MG/5 ML SUSP	200	38.7899999999999991	0	0	38.7899999999999991	45.1000000000000014	-6.30999999999999961	-0.162670789999999982	N0	LUPIN PHARMACEU	0.0681999999999999967
1451	2018-08-02	46254771	68180044101	CEPHALEXIN 250 MG/5 ML SUSP	300	0	0	45	45	26.6700000000000017	18.3299999999999983	0.407333329999999993	N0	LUPIN PHARMACEU	0.0889000000000000068
1452	2018-08-25	46257620	68180044102	CEPHALEXIN 250 MG/5 ML SUSP	204	0	0	58.8900000000000006	58.8900000000000006	46	12.8900000000000006	0.218882660000000007	N0	LUPIN PHARMACEU	0.0681999999999999967
1453	2018-08-16	46256545	68180012202	CEPHALEXIN 500 MG CAPSULE	21	0	0	2.2799999999999998	2.2799999999999998	1.34000000000000008	0.939999999999999947	0.4122807	N0	LUPIN PHARMACEU	0.0638999999999999985
1454	2018-08-14	46256195	68180012202	CEPHALEXIN 500 MG CAPSULE	14	0.489999999999999991	0	1.25	1.73999999999999999	0.890000000000000013	0.849999999999999978	0.488505750000000016	N0	LUPIN PHARMACEU	0.0638999999999999985
1455	2018-08-24	46257469	68180012202	CEPHALEXIN 500 MG CAPSULE	14	18.5899999999999999	0	0	18.5899999999999999	0.890000000000000013	17.6999999999999993	0.952124799999999993	N0	LUPIN PHARMACEU	0.0638999999999999985
1456	2018-08-15	46256297	68180012202	CEPHALEXIN 500 MG CAPSULE	8	3.4700000000000002	0	0	3.4700000000000002	0.510000000000000009	2.95999999999999996	0.853025939999999983	N0	LUPIN PHARMACEU	0.0638999999999999985
1457	2018-08-14	46256222	68180012202	CEPHALEXIN 500 MG CAPSULE	21	25.879999999999999	0	0	25.879999999999999	1.34000000000000008	24.5399999999999991	0.948222569999999987	N0	LUPIN PHARMACEU	0.0638999999999999985
1458	2018-08-31	46258270	68180012202	CEPHALEXIN 500 MG CAPSULE	14	18.5899999999999999	0	0	18.5899999999999999	0.890000000000000013	17.6999999999999993	0.952124799999999993	N0	LUPIN PHARMACEU	0.0638999999999999985
1459	2018-08-03	46238472	68180012202	CEPHALEXIN 500 MG CAPSULE	44	0	0	15	15	2.81000000000000005	12.1899999999999995	0.812666670000000035	N	LUPIN PHARMACEU	0.0638999999999999985
1460	2018-08-13	46256075	68180012202	CEPHALEXIN 500 MG CAPSULE	20	2.08000000000000007	0	0	2.08000000000000007	1.28000000000000003	0.800000000000000044	0.384615379999999951	N0	LUPIN PHARMACEU	0.0638999999999999985
1461	2018-08-26	46257626	68180012202	CEPHALEXIN 500 MG CAPSULE	30	2.31000000000000005	0	0	2.31000000000000005	1.91999999999999993	0.390000000000000013	0.168831169999999975	N0	LUPIN PHARMACEU	0.0638999999999999985
1462	2018-08-30	46258148	68180012202	CEPHALEXIN 500 MG CAPSULE	30	8.03999999999999915	0	0	8.03999999999999915	1.91999999999999993	6.12000000000000011	0.761194030000000077	N0	LUPIN PHARMACEU	0.0638999999999999985
1463	2018-08-14	46256266	68180012202	CEPHALEXIN 500 MG CAPSULE	30	0	0	3.22999999999999998	3.22999999999999998	1.91999999999999993	1.31000000000000005	0.405572760000000032	N0	LUPIN PHARMACEU	0.0638999999999999985
1464	2018-08-16	46256537	68180012202	CEPHALEXIN 500 MG CAPSULE	15	0	0	4.20000000000000018	4.20000000000000018	0.959999999999999964	3.24000000000000021	0.771428570000000091	N0	LUPIN PHARMACEU	0.0638999999999999985
1465	2018-08-21	46257086	68180012202	CEPHALEXIN 500 MG CAPSULE	21	2.89999999999999991	0	0.719999999999999973	3.62000000000000011	1.34000000000000008	2.2799999999999998	0.629834249999999929	N0	LUPIN PHARMACEU	0.0638999999999999985
1466	2018-08-10	46255738	68180012202	CEPHALEXIN 500 MG CAPSULE	6	0	0	1.18999999999999995	1.18999999999999995	0.380000000000000004	0.810000000000000053	0.680672270000000079	N0	LUPIN PHARMACEU	0.0638999999999999985
1467	2018-08-06	46255181	68180012202	CEPHALEXIN 500 MG CAPSULE	21	25.879999999999999	0	0	25.879999999999999	1.34000000000000008	24.5399999999999991	0.948222569999999987	N0	LUPIN PHARMACEU	0.0638999999999999985
1468	2018-08-08	46255508	68180012202	CEPHALEXIN 500 MG CAPSULE	20	24.8399999999999999	0	0	24.8399999999999999	1.28000000000000003	23.5599999999999987	0.94847020999999998	N0	LUPIN PHARMACEU	0.0638999999999999985
1469	2018-08-02	46254837	68180012202	CEPHALEXIN 500 MG CAPSULE	20	0	0	2.20000000000000018	2.20000000000000018	1.28000000000000003	0.92000000000000004	0.418181819999999982	N0	LUPIN PHARMACEU	0.0638999999999999985
1470	2018-08-23	46257418	68180012202	CEPHALEXIN 500 MG CAPSULE	28	3.75	0	1.25	5	1.79000000000000004	3.20999999999999996	0.642000000000000015	N0	LUPIN PHARMACEU	0.0638999999999999985
1471	2018-08-01	46254706	68180012202	CEPHALEXIN 500 MG CAPSULE	21	0	0	10	10	1.34000000000000008	8.66000000000000014	0.865999999999999992	N	LUPIN PHARMACEU	0.0638999999999999985
1472	2018-08-03	46254906	68180012202	CEPHALEXIN 500 MG CAPSULE	14	0	1.52000000000000002	1	2.52000000000000002	0.890000000000000013	1.62999999999999989	0.646825399999999995	N0	LUPIN PHARMACEU	0.0638999999999999985
1473	2018-08-08	46255452	68180012202	CEPHALEXIN 500 MG CAPSULE	30	0	0	2.93000000000000016	2.93000000000000016	1.91999999999999993	1.01000000000000001	0.344709900000000014	N0	LUPIN PHARMACEU	0.0638999999999999985
1474	2018-08-15	46256391	68180012202	CEPHALEXIN 500 MG CAPSULE	30	3.43999999999999995	0	1	4.44000000000000039	1.91999999999999993	2.52000000000000002	0.567567570000000021	N0	LUPIN PHARMACEU	0.0638999999999999985
1475	2018-08-06	46251184	644002216	CERAVE MOISTURIZING CREAM	453	0	0	16.8900000000000006	16.8900000000000006	10.6500000000000004	6.24000000000000021	0.369449379999999994	N0	VALEANT	0.0235000000000000001
1476	2018-08-08	46255434	536344238	CEROVITE ADVANCED FORM TA	30	1.20999999999999996	0	0	1.20999999999999996	0.880000000000000004	0.330000000000000016	0.272727270000000022	N0	RUGBY	0.0292999999999999997
1477	2018-08-30	46240187	536344308	CEROVITE JR TABLET CHEW	30	0	0	3.45000000000000018	3.45000000000000018	1.21999999999999997	2.22999999999999998	0.646376810000000024	N	RUGBY	0.0405000000000000013
1478	2018-08-13	46251657	904548652	CERTAVITE SR-ANTIOXIDANT TA	30	0	0	3.41999999999999993	3.41999999999999993	1.18999999999999995	2.22999999999999998	0.652046780000000048	N0	MAJOR PHARMACEU	0.0395000000000000004
1479	2018-08-24	46252787	45802097426	CETIRIZINE HCL 1 MG/ML SYRU	60	0	0	7.15000000000000036	7.15000000000000036	1.93999999999999995	5.20999999999999996	0.72867132999999995	N0	PERRIGO CO.	0.0321999999999999995
1480	2018-08-14	46242866	45802097426	CETIRIZINE HCL 1 MG/ML SYRU	118	0	6.75	0	6.75	3.81000000000000005	2.93999999999999995	0.435555560000000008	N0	PERRIGO CO.	0.0321999999999999995
1481	2018-08-15	46256294	45802097426	CETIRIZINE HCL 1 MG/ML SYRU	300	0	11.8200000000000003	0	11.8200000000000003	9.6899999999999995	2.12999999999999989	0.180203050000000004	N0	PERRIGO CO.	0.0321999999999999995
1482	2018-08-27	46241357	45802097426	CETIRIZINE HCL 1 MG/ML SYRU	150	0	2.99000000000000021	0	2.99000000000000021	4.84999999999999964	-1.8600000000000001	-0.622073580000000015	N0	PERRIGO CO.	0.0321999999999999995
1483	2018-08-17	46252816	45802097426	CETIRIZINE HCL 1 MG/ML SYRU	300	11.8200000000000003	0	0	11.8200000000000003	9.6899999999999995	2.12999999999999989	0.180203050000000004	N0	PERRIGO CO.	0.0321999999999999995
1484	2018-08-20	46244159	51991083716	CETIRIZINE HCL 1 MG/ML SYRU	75	3.58999999999999986	0	0	3.58999999999999986	1.40999999999999992	2.18000000000000016	0.607242339999999992	N0	BRECKENRIDGE	0.0188000000000000007
1485	2018-08-28	46219586	45802097426	CETIRIZINE HCL 1 MG/ML SYRU	300	5.62999999999999989	0	0	5.62999999999999989	9.6899999999999995	-4.05999999999999961	-0.721136769999999983	N0	PERRIGO CO.	0.0321999999999999995
1486	2018-08-03	46243793	45802097426	CETIRIZINE HCL 1 MG/ML SYRU	300	11.8200000000000003	0	0	11.8200000000000003	9.72000000000000064	2.10000000000000009	0.177664970000000005	N0	PERRIGO CO.	0.0323999999999999982
1487	2018-08-03	46254940	51991083716	CETIRIZINE HCL 1 MG/ML SYRU	75	10.6899999999999995	0	0	10.6899999999999995	1.40999999999999992	9.27999999999999936	0.86810102999999994	N0	BRECKENRIDGE	0.0188000000000000007
1488	2018-08-03	46254938	51991083716	CETIRIZINE HCL 1 MG/ML SYRU	75	10.6899999999999995	0	0	10.6899999999999995	1.40999999999999992	9.27999999999999936	0.86810102999999994	N0	BRECKENRIDGE	0.0188000000000000007
1489	2018-08-20	46249257	536408807	CETIRIZINE HCL 10 MG TABLET	30	0	0	4.49000000000000021	4.49000000000000021	4.13999999999999968	0.349999999999999978	0.0779509999999999925	N0	RUGBY	0.153299999999999992
1490	2018-08-30	46258176	45802091987	CETIRIZINE HCL 10 MG TABLET	30	0	0	7.29999999999999982	7.29999999999999982	1.30000000000000004	6	0.821917810000000082	N0	PERRIGO CO.	0.0434000000000000011
1491	2018-08-29	46247402	45802091987	CETIRIZINE HCL 10 MG TABLET	30	5.80999999999999961	0	0	5.80999999999999961	1.30000000000000004	4.50999999999999979	0.776247850000000073	N0	PERRIGO CO.	0.0434000000000000011
1492	2018-08-03	46240837	45802091987	CETIRIZINE HCL 10 MG TABLET	30	5.80999999999999961	0	0	5.80999999999999961	1.48999999999999999	4.32000000000000028	0.743545610000000079	N0	PERRIGO CO.	0.0434000000000000011
1493	2018-08-03	46245780	45802091987	CETIRIZINE HCL 10 MG TABLET	30	0.359999999999999987	0	1	1.3600000000000001	1.30000000000000004	0.0599999999999999978	0.0441176500000000013	N0	PERRIGO CO.	0.0434000000000000011
1494	2018-08-01	46253377	45802091987	CETIRIZINE HCL 10 MG TABLET	3	2.22999999999999998	0	0	2.22999999999999998	0.149999999999999994	2.08000000000000007	0.932735430000000032	N0	PERRIGO CO.	0.0434000000000000011
1495	2018-08-03	46240838	45802091987	CETIRIZINE HCL 10 MG TABLET	90	11.6799999999999997	0	0	11.6799999999999997	4.48000000000000043	7.20000000000000018	0.616438359999999963	N0	PERRIGO CO.	0.0434000000000000011
1496	2018-08-09	46222124	45802091987	CETIRIZINE HCL 10 MG TABLET	30	5.80999999999999961	0	0	5.80999999999999961	1.30000000000000004	4.50999999999999979	0.776247850000000073	N0	PERRIGO CO.	0.0434000000000000011
1497	2018-08-29	46250119	45802091987	CETIRIZINE HCL 10 MG TABLET	30	5.80999999999999961	0	0	5.80999999999999961	1.30000000000000004	4.50999999999999979	0.776247850000000073	N0	PERRIGO CO.	0.0434000000000000011
1498	2018-08-06	46255118	45802091987	CETIRIZINE HCL 10 MG TABLET	30	0	0	7.29999999999999982	7.29999999999999982	1.48999999999999999	5.80999999999999961	0.795890409999999937	N0	PERRIGO CO.	0.0434000000000000011
1499	2018-08-08	46252300	45802091987	CETIRIZINE HCL 10 MG TABLET	30	5.80999999999999961	0	0	5.80999999999999961	1.30000000000000004	4.50999999999999979	0.776247850000000073	N0	PERRIGO CO.	0.0434000000000000011
1500	2018-08-08	46238898	45802091987	CETIRIZINE HCL 10 MG TABLET	30	5.80999999999999961	0	0	5.80999999999999961	1.30000000000000004	4.50999999999999979	0.776247850000000073	N0	PERRIGO CO.	0.0434000000000000011
1501	2018-08-23	46257347	45802091987	CETIRIZINE HCL 10 MG TABLET	50	0	0	10.8399999999999999	10.8399999999999999	2.16999999999999993	8.66999999999999993	0.799815500000000013	N0	PERRIGO CO.	0.0434000000000000011
1502	2018-08-07	46233655	45802091987	CETIRIZINE HCL 10 MG TABLET	30	5.80999999999999961	0	0	5.80999999999999961	1.30000000000000004	4.50999999999999979	0.776247850000000073	N0	PERRIGO CO.	0.0434000000000000011
1503	2018-08-09	46255662	45802091987	CETIRIZINE HCL 10 MG TABLET	30	5.80999999999999961	0	0	5.80999999999999961	1.30000000000000004	4.50999999999999979	0.776247850000000073	N0	PERRIGO CO.	0.0434000000000000011
1504	2018-08-04	46253377	45802091987	CETIRIZINE HCL 10 MG TABLET	3	2.22999999999999998	0	0	2.22999999999999998	0.149999999999999994	2.08000000000000007	0.932735430000000032	N0	PERRIGO CO.	0.0434000000000000011
1505	2018-08-06	46255204	69046956	CHANTIX 1 MG TABLET	56	393.889999999999986	0	0	393.889999999999986	362.970000000000027	30.9200000000000017	0.0784990700000000041	N0	PFIZER US PHARM	6.48160000000000025
1506	2018-08-13	46256113	69046956	CHANTIX 1 MG TABLET	56	402.490000000000009	0	7.79000000000000004	410.279999999999973	362.970000000000027	47.3100000000000023	0.115311489999999989	N0	PFIZER US PHARM	6.48160000000000025
1507	2018-08-16	46256591	69046956	CHANTIX 1 MG TABLET	56	392.829999999999984	0	3	395.829999999999984	362.970000000000027	32.8599999999999994	0.0830154399999999959	Y1	PFIZER US PHARM	6.48160000000000025
1508	2018-08-23	46257441	69046956	CHANTIX 1 MG TABLET	56	392.829999999999984	0	3	395.829999999999984	362.970000000000027	32.8599999999999994	0.0830154399999999959	N0	PFIZER US PHARM	6.48160000000000025
1509	2018-08-01	46240371	69046956	CHANTIX 1 MG TABLET	56	392.899999999999977	0	0	392.899999999999977	362.970000000000027	29.9299999999999997	0.0761771400000000043	N0	PFIZER US PHARM	6.48160000000000025
1510	2018-08-04	46250524	51293060701	CHLORDIAZEPOXIDE-CLIDINIUM	120	130.129999999999995	0	15	145.129999999999995	46.509999999999998	98.6200000000000045	0.679528700000000097	N0	ECI PHARMACEUTI	0.3876
1511	2018-08-09	46255641	116200116	CHLORHEXIDINE 0.12% RINSE	473	8.63000000000000078	0	0	8.63000000000000078	2.31999999999999984	6.30999999999999961	0.73117034000000003	N0	XTTRIUM LABS.	0.00489999999999999984
1512	2018-08-11	46255864	116200116	CHLORHEXIDINE 0.12% RINSE	473	0.25	0	5	5.25	2.31999999999999984	2.93000000000000016	0.558095239999999992	N0	XTTRIUM LABS.	0.00489999999999999984
1513	2018-08-27	46223174	116200116	CHLORHEXIDINE 0.12% RINSE	473	10.8499999999999996	0	0	10.8499999999999996	2.31999999999999984	8.52999999999999936	0.786175120000000005	N0	XTTRIUM LABS.	0.00489999999999999984
1514	2018-08-13	46256068	116200116	CHLORHEXIDINE 0.12% RINSE	473	4.70999999999999996	0	1	5.70999999999999996	2.31999999999999984	3.39000000000000012	0.593695269999999997	N0	XTTRIUM LABS.	0.00489999999999999984
1515	2018-08-13	46256073	116200116	CHLORHEXIDINE 0.12% RINSE	473	1.53000000000000003	0	1	2.5299999999999998	2.31999999999999984	0.209999999999999992	0.083003949999999993	N0	XTTRIUM LABS.	0.00489999999999999984
1516	2018-08-21	46257090	116200116	CHLORHEXIDINE 0.12% RINSE	473	1.53000000000000003	0	1	2.5299999999999998	2.31999999999999984	0.209999999999999992	0.083003949999999993	N0	XTTRIUM LABS.	0.00489999999999999984
1517	2018-08-20	48012300	51927119900	CHLOROTHIAZIDE 50MG/ML SU	300	42.1400000000000006	0	0	42.1400000000000006	30.7300000000000004	11.4100000000000001	0.270764119999999997	N0		0
1518	2018-08-27	48012143	51927180000	CHLOROTHIAZIDE 50MG/ML SU	144	37.3400000000000034	0	0	37.3400000000000034	10.3100000000000005	27.0300000000000011	0.723888589999999943	N0		0
1519	2018-08-23	48011837	38779061301	CHLOROTHIAZIDE 50MG/ML SU	120	40	0	0	40	13.7300000000000004	26.2699999999999996	0.656749999999999945	N0		0
1520	2018-08-02	46253634	781591601	CHLORPROMAZINE 100MG	15	163.430000000000007	0	0	163.430000000000007	106.439999999999998	56.990000000000002	0.348711989999999972	N0	SANDOZ	7.09600000000000009
1521	2018-08-26	48012465	38779042305	CHLORPROMAZINE 100MG SUPP	15	27.7699999999999996	0	0	27.7699999999999996	1.91999999999999993	25.8500000000000014	0.930860639999999906	N0		0
1522	2018-08-13	48012399	38779042305	CHLORPROMAZINE 200MG SUPP	12	35.8699999999999974	0	0	35.8699999999999974	2.5299999999999998	33.3400000000000034	0.929467519999999991	N0		0
1523	2018-08-21	46257162	781591401	CHLORPROMAZINE 25 MG TABLET	2	12.2400000000000002	0	0	12.2400000000000002	8.99000000000000021	3.25	0.265522880000000017	N0	SANDOZ	4.49610000000000021
1524	2018-08-17	46256897	781591401	CHLORPROMAZINE 25 MG TABLET	4	20.4899999999999984	0	0	20.4899999999999984	17.9800000000000004	2.50999999999999979	0.122498780000000002	N0	SANDOZ	4.49610000000000021
1525	2018-08-16	46256658	832030100	CHLORPROMAZINE 25MG	2	12.2400000000000002	0	0	12.2400000000000002	4.50999999999999979	7.73000000000000043	0.631535950000000068	N0	UPSHER SMITH LA	3.37530000000000019
1526	2018-08-14	46256220	832030100	CHLORPROMAZINE 25MG	20	86.4399999999999977	0	0	86.4399999999999977	67.5100000000000051	18.9299999999999997	0.218995839999999997	N0	UPSHER SMITH LA	3.37530000000000019
1527	2018-08-20	46256854	832030100	CHLORPROMAZINE 25MG	30	127.659999999999997	0	0	127.659999999999997	67.6500000000000057	60.009999999999998	0.470076770000000033	N0	UPSHER SMITH LA	3.37530000000000019
1528	2018-08-21	48012438	38779042304	CHLORPROMAZINE 25MG SUPP	6	13.3499999999999996	0	0	13.3499999999999996	7.36000000000000032	5.99000000000000021	0.448689139999999986	N0	MEDISCA INC.	1.22700000000000009
1529	2018-08-22	48012438	38779042304	CHLORPROMAZINE 25MG SUPP	15	26.5	0	0	26.5	18.4100000000000001	8.08999999999999986	0.305283020000000016	N0	MEDISCA INC.	1.22700000000000009
1530	2018-08-25	48012463	38779042305	CHLORPROMAZINE 50MG SUPP	15	19.9400000000000013	0	0	19.9400000000000013	1.32000000000000006	18.620000000000001	0.933801400000000004	N0		0
1531	2018-08-16	46256491	781591501	CHLORPROMAZINE 50MG TABLE	20	106.439999999999998	0	0	106.439999999999998	96.2000000000000028	10.2400000000000002	0.0962044299999999936	N0	SANDOZ	4.80989999999999984
1532	2018-08-29	46250727	781591501	CHLORPROMAZINE 50MG TABLE	90	453.160000000000025	0	0	453.160000000000025	432.889999999999986	20.2699999999999996	0.0447303400000000004	N0	SANDOZ	4.80989999999999984
1533	2018-08-21	46256491	781591501	CHLORPROMAZINE 50MG TABLE	20	106.439999999999998	0	0	106.439999999999998	96.2000000000000028	10.2400000000000002	0.0962044299999999936	N0	SANDOZ	4.80989999999999984
1534	2018-08-16	46255950	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1535	2018-08-17	46255950	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1536	2018-08-18	46256254	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1537	2018-08-14	46256042	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1538	2018-08-18	46256880	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1539	2018-08-23	46257516	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1540	2018-08-29	46258172	641139835	CHLORPROMAZINE 50MG/2ML A	12	179.72999999999999	0	0	179.72999999999999	121.680000000000007	58.0499999999999972	0.322984480000000018	N0	WEST-WARD/HIKMA	10.1400000000000006
1541	2018-08-18	46256879	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1542	2018-08-14	46255947	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1543	2018-08-18	46256878	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1544	2018-08-13	46256185	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1545	2018-08-18	46255950	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1546	2018-08-01	46253897	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1547	2018-08-13	46256042	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1548	2018-08-05	46255113	641139835	CHLORPROMAZINE 50MG/2ML A	30	150.439999999999998	0	0	150.439999999999998	101.400000000000006	49.0399999999999991	0.325977130000000004	N0	WEST-WARD/HIKMA	10.1400000000000006
1549	2018-08-05	46255161	641139835	CHLORPROMAZINE 50MG/2ML A	6	91.8599999999999994	0	0	91.8599999999999994	60.8400000000000034	31.0199999999999996	0.337687790000000043	N0	WEST-WARD/HIKMA	10.1400000000000006
1550	2018-08-13	46256168	641139835	CHLORPROMAZINE 50MG/2ML A	14	209.009999999999991	0	0	209.009999999999991	141.960000000000008	67.0499999999999972	0.320798050000000001	N0	WEST-WARD/HIKMA	10.1400000000000006
1551	2018-08-29	46257973	641139835	CHLORPROMAZINE 50MG/2ML A	40	589.75	0	0	589.75	405.600000000000023	184.150000000000006	0.312250949999999972	N0	WEST-WARD/HIKMA	10.1400000000000006
1552	2018-08-07	46255500	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1553	2018-08-16	46256655	641139835	CHLORPROMAZINE 50MG/2ML A	8	121.150000000000006	0	0	121.150000000000006	81.1200000000000045	40.0300000000000011	0.330416839999999989	N0	WEST-WARD/HIKMA	10.1400000000000006
1554	2018-08-25	46257716	641139835	CHLORPROMAZINE 50MG/2ML A	6	91.8599999999999994	0	0	91.8599999999999994	60.8400000000000034	31.0199999999999996	0.337687790000000043	N0	WEST-WARD/HIKMA	10.1400000000000006
1555	2018-08-04	46255102	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1556	2018-08-17	46256835	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1557	2018-08-16	46256593	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1558	2018-08-15	46255950	641139835	CHLORPROMAZINE 50MG/2ML A	20	296.879999999999995	0	0	296.879999999999995	202.800000000000011	94.0799999999999983	0.316895719999999992	N0	WEST-WARD/HIKMA	10.1400000000000006
1559	2018-08-07	46255113	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1560	2018-08-15	46256512	641139835	CHLORPROMAZINE 50MG/2ML A	4	62.5700000000000003	0	0	62.5700000000000003	40.5600000000000023	22.0100000000000016	0.351766020000000013	N0	WEST-WARD/HIKMA	10.1400000000000006
1561	2018-08-13	46256165	641139835	CHLORPROMAZINE 50MG/2ML A	6	91.8599999999999994	0	0	91.8599999999999994	60.8400000000000034	31.0199999999999996	0.337687790000000043	N0	WEST-WARD/HIKMA	10.1400000000000006
1562	2018-08-06	46255113	641139835	CHLORPROMAZINE 50MG/2ML A	10	150.439999999999998	0	0	150.439999999999998	101.400000000000006	49.0399999999999991	0.325977130000000004	N0	WEST-WARD/HIKMA	10.1400000000000006
1563	2018-08-14	46256325	641139835	CHLORPROMAZINE 50MG/2ML A	4	62.5700000000000003	0	0	62.5700000000000003	40.5600000000000023	22.0100000000000016	0.351766020000000013	N0	WEST-WARD/HIKMA	10.1400000000000006
1564	2018-08-12	46256047	641139835	CHLORPROMAZINE 50MG/2ML A	4	62.5700000000000003	0	0	62.5700000000000003	40.5600000000000023	22.0100000000000016	0.351766020000000013	N0	WEST-WARD/HIKMA	10.1400000000000006
1565	2018-08-16	46255947	641139835	CHLORPROMAZINE 50MG/2ML A	12	179.72999999999999	0	0	179.72999999999999	121.680000000000007	58.0499999999999972	0.322984480000000018	N0	WEST-WARD/HIKMA	10.1400000000000006
1566	2018-08-12	46256028	641139835	CHLORPROMAZINE 50MG/2ML A	10	150.439999999999998	0	0	150.439999999999998	101.400000000000006	49.0399999999999991	0.325977130000000004	N0	WEST-WARD/HIKMA	10.1400000000000006
1567	2018-08-19	46256593	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1568	2018-08-15	46256042	641139835	CHLORPROMAZINE 50MG/2ML A	40	589.75	0	0	589.75	405.600000000000023	184.150000000000006	0.312250949999999972	N0	WEST-WARD/HIKMA	10.1400000000000006
1569	2018-08-16	46256661	641139835	CHLORPROMAZINE 50MG/2ML A	4	62.5700000000000003	0	0	62.5700000000000003	40.5600000000000023	22.0100000000000016	0.351766020000000013	N0	WEST-WARD/HIKMA	10.1400000000000006
1570	2018-08-23	46257452	641139835	CHLORPROMAZINE 50MG/2ML A	20	296.879999999999995	0	0	296.879999999999995	202.800000000000011	94.0799999999999983	0.316895719999999992	N0	WEST-WARD/HIKMA	10.1400000000000006
1571	2018-08-19	46256905	641139835	CHLORPROMAZINE 50MG/2ML A	2	33.2899999999999991	0	0	33.2899999999999991	20.2800000000000011	13.0099999999999998	0.390808049999999962	N0	WEST-WARD/HIKMA	10.1400000000000006
1572	2018-08-24	46257592	641139835	CHLORPROMAZINE 50MG/2ML A	4	62.5700000000000003	0	0	62.5700000000000003	40.5600000000000023	22.0100000000000016	0.351766020000000013	N0	WEST-WARD/HIKMA	10.1400000000000006
1573	2018-08-18	46256798	641139835	CHLORPROMAZINE 50MG/2ML A	20	296.879999999999995	0	0	296.879999999999995	202.800000000000011	94.0799999999999983	0.316895719999999992	N0	WEST-WARD/HIKMA	10.1400000000000006
1574	2018-08-14	46256322	641139835	CHLORPROMAZINE 50MG/2ML A	8	121.150000000000006	0	0	121.150000000000006	81.1200000000000045	40.0300000000000011	0.330416839999999989	N0	WEST-WARD/HIKMA	10.1400000000000006
1575	2018-08-12	46255950	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1576	2018-08-13	46256254	641139835	CHLORPROMAZINE 50MG/2ML A	30	443.310000000000002	0	0	443.310000000000002	304.199999999999989	139.110000000000014	0.313798469999999996	N0	WEST-WARD/HIKMA	10.1400000000000006
1577	2018-08-04	46255074	641139735	CHLORPROMAZINE25MG/1ML AMP	6	157.360000000000014	0	0	157.360000000000014	115.780000000000001	41.5799999999999983	0.264234880000000005	N0	WEST-WARD,INC.	19.2959999999999994
1578	2018-08-20	46241036	64980030301	CHLORTHALIDONE 25 MG TABLET	30	24.3000000000000007	0	1.25	25.5500000000000007	31.8599999999999994	-6.30999999999999961	-0.246966729999999995	N0	RISING PHARM	0.269500000000000017
1579	2018-08-27	46242335	57664064988	CHLORTHALIDONE 50 MG TABLET	30	22.1999999999999993	0	0	22.1999999999999993	44.6499999999999986	-22.4499999999999993	-1.01126125999999994	N0	SUN PHARMACEUTI	0.558000000000000052
1580	2018-08-02	46254769	49884046667	CHOLESTYRAMINE LIGHT POWDER	210	30.5500000000000007	0	10	40.5499999999999972	55.1499999999999986	-14.5999999999999996	-0.360049319999999951	N0	PAR PHARM.	0.2626
1581	2018-08-22	46257243	42195055014	CIPROFLOXACIN 0.2% OTIC SOL	14	47.2199999999999989	0	11	58.2199999999999989	89.6099999999999994	-31.3900000000000006	-0.539161800000000024	N0	XSPIRE PHARMA	6.40069999999999961
1582	2018-08-31	46258332	16571041110	CIPROFLOXACIN HCL 250 MG TA	14	19.5300000000000011	0	0	19.5300000000000011	1.37000000000000011	18.1600000000000001	0.929851509999999992	N0	RISING PHARM	0.0975000000000000033
1583	2018-08-09	46255668	16571041110	CIPROFLOXACIN HCL 250 MG TA	10	15.0899999999999999	0	0	15.0899999999999999	0.979999999999999982	14.1099999999999994	0.935056329999999991	N0	RISING PHARM	0.0975000000000000033
1584	2018-08-15	46256429	16571041110	CIPROFLOXACIN HCL 250 MG TA	14	0	0	5	5	1.37000000000000011	3.62999999999999989	0.725999999999999979	N0	RISING PHARM	0.0975000000000000033
1585	2018-08-15	46256365	16571041110	CIPROFLOXACIN HCL 250 MG TA	14	19.5300000000000011	0	0	19.5300000000000011	1.37000000000000011	18.1600000000000001	0.929851509999999992	N0	RISING PHARM	0.0975000000000000033
1586	2018-08-06	46255180	16571041110	CIPROFLOXACIN HCL 250 MG TA	14	3.7799999999999998	0	1.25	5.03000000000000025	1.37000000000000011	3.66000000000000014	0.727634189999999959	N0	RISING PHARM	0.0975000000000000033
1587	2018-08-15	46256357	16571041110	CIPROFLOXACIN HCL 250 MG TA	14	19.5300000000000011	0	0	19.5300000000000011	1.37000000000000011	18.1600000000000001	0.929851509999999992	N0	RISING PHARM	0.0975000000000000033
1588	2018-08-13	46256094	16571041110	CIPROFLOXACIN HCL 250 MG TA	12	3.75	0	1.25	5	1.16999999999999993	3.83000000000000007	0.765999999999999903	N0	RISING PHARM	0.0975000000000000033
1589	2018-08-29	46246724	16571041110	CIPROFLOXACIN HCL 250 MG TA	15	20.6400000000000006	0	0	20.6400000000000006	1.45999999999999996	19.1799999999999997	0.929263569999999928	N0	RISING PHARM	0.0975000000000000033
1590	2018-08-19	46256815	16571041110	CIPROFLOXACIN HCL 250 MG TA	6	10.6500000000000004	0	0	10.6500000000000004	0.589999999999999969	10.0600000000000005	0.944600939999999945	N0	RISING PHARM	0.0975000000000000033
1591	2018-08-15	46246724	16571041110	CIPROFLOXACIN HCL 250 MG TA	15	20.6400000000000006	0	0	20.6400000000000006	1.45999999999999996	19.1799999999999997	0.929263569999999928	N0	RISING PHARM	0.0975000000000000033
1592	2018-08-04	46255064	16571041210	CIPROFLOXACIN HCL 500 MG TA	14	3.47999999999999998	0	3.35000000000000009	6.83000000000000007	1.3899999999999999	5.44000000000000039	0.796486089999999924	N0	RISING PHARM	0.0995000000000000051
1593	2018-08-18	46256773	16571041250	CIPROFLOXACIN HCL 500 MG TA	20	5.07000000000000028	0	0	5.07000000000000028	1.89999999999999991	3.16999999999999993	0.625246550000000068	N0	PACK PHARMACEUT	0.0950000000000000011
1594	2018-08-11	46255845	16571041210	CIPROFLOXACIN HCL 500 MG TA	20	16.9800000000000004	0	0	16.9800000000000004	1.98999999999999999	14.9900000000000002	0.882803300000000069	N0	RISING PHARM	0.0995000000000000051
1595	2018-08-08	46255486	16571041210	CIPROFLOXACIN HCL 500 MG TA	10	10.7400000000000002	0	0	10.7400000000000002	1	9.74000000000000021	0.906890130000000072	N0	RISING PHARM	0.0995000000000000051
1596	2018-08-07	46255383	16571041210	CIPROFLOXACIN HCL 500 MG TA	3	0	0	1.16999999999999993	1.16999999999999993	0.299999999999999989	0.869999999999999996	0.743589739999999999	N0	RISING PHARM	0.0995000000000000051
1597	2018-08-14	46256280	65862007701	CIPROFLOXACIN HCL 500 MG TA	14	0	0	5	5	1.39999999999999991	3.60000000000000009	0.719999999999999973	N0	AUROBINDO PHARM	0.0999000000000000027
1598	2018-08-13	46256084	16571041210	CIPROFLOXACIN HCL 500 MG TA	20	0	0	4	4	1.98999999999999999	2.00999999999999979	0.502499999999999947	N0	RISING PHARM	0.0995000000000000051
1599	2018-08-13	46255976	16571041250	CIPROFLOXACIN HCL 500 MG TA	20	0	0.359999999999999987	8	8.35999999999999943	1.89999999999999991	6.45999999999999996	0.772727270000000077	N0	PACK PHARMACEUT	0.0950000000000000011
1600	2018-08-14	46256161	16571041210	CIPROFLOXACIN HCL 500 MG TA	42	19	0	6	25	4.17999999999999972	20.8200000000000003	0.832799999999999985	N0	RISING PHARM	0.0995000000000000051
1601	2018-08-12	46255894	65862000501	CITALOPRAM HBR 10 MG TABLET	7	7.37999999999999989	0	0	7.37999999999999989	0.130000000000000004	7.25	0.982384820000000047	N0	AUROBINDO PHARM	0.0182000000000000009
1602	2018-08-23	46256271	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1603	2018-08-29	46256271	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1604	2018-08-15	46255941	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1605	2018-08-16	46256569	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1606	2018-08-21	46254365	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1607	2018-08-06	46254365	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1608	2018-08-27	46241009	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1609	2018-08-26	46256271	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1610	2018-08-09	46241009	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1611	2018-08-01	46254670	65862000501	CITALOPRAM HBR 10 MG TABLET	1	4.48000000000000043	0	0	4.48000000000000043	0.0200000000000000004	4.45999999999999996	0.995535710000000074	N0	AUROBINDO PHARM	0.0182000000000000009
1612	2018-08-22	46248224	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1613	2018-08-19	46256271	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1614	2018-08-20	46249946	65862000501	CITALOPRAM HBR 10 MG TABLET	30	0	0	6	6	0.550000000000000044	5.45000000000000018	0.908333329999999939	N0	AUROBINDO PHARM	0.0182000000000000009
1615	2018-08-12	46255253	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1616	2018-08-15	46249194	65862000501	CITALOPRAM HBR 10 MG TABLET	45	25.7399999999999984	0	0	25.7399999999999984	0.819999999999999951	24.9200000000000017	0.968142969999999936	N0	AUROBINDO PHARM	0.0182000000000000009
1617	2018-08-16	46256271	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1618	2018-08-13	46255941	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1619	2018-08-21	46254694	65862000501	CITALOPRAM HBR 10 MG TABLET	30	0	0	1	1	0.550000000000000044	0.450000000000000011	0.450000000000000011	N0	AUROBINDO PHARM	0.0182000000000000009
1620	2018-08-14	46256271	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1621	2018-08-09	46255253	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1622	2018-08-23	46249194	65862000501	CITALOPRAM HBR 10 MG TABLET	45	25.7399999999999984	0	0	25.7399999999999984	0.819999999999999951	24.9200000000000017	0.968142969999999936	N0	AUROBINDO PHARM	0.0182000000000000009
1623	2018-08-17	46256623	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1624	2018-08-20	46256927	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1625	2018-08-28	46238167	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1626	2018-08-09	46250192	65862000501	CITALOPRAM HBR 10 MG TABLET	30	5	0	0	5	0.550000000000000044	4.45000000000000018	0.890000000000000013	N0	AUROBINDO PHARM	0.0182000000000000009
1627	2018-08-07	46255253	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1628	2018-08-09	46255306	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1629	2018-08-17	46255941	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1630	2018-08-02	46254425	65862000501	CITALOPRAM HBR 10 MG TABLET	2	4.96999999999999975	0	0	4.96999999999999975	0.0400000000000000008	4.92999999999999972	0.991951710000000042	N0	AUROBINDO PHARM	0.0182000000000000009
1631	2018-08-06	46255224	65862000501	CITALOPRAM HBR 10 MG TABLET	15	11.25	0	0	11.25	0.270000000000000018	10.9800000000000004	0.975999999999999979	N0	AUROBINDO PHARM	0.0182000000000000009
1632	2018-08-20	46256987	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1633	2018-08-07	46255306	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1634	2018-08-14	46255253	65862000501	CITALOPRAM HBR 10 MG TABLET	5	6.41999999999999993	0	0	6.41999999999999993	0.0899999999999999967	6.33000000000000007	0.98598131	N0	AUROBINDO PHARM	0.0182000000000000009
1635	2018-08-23	46256987	65862000501	CITALOPRAM HBR 10 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0500000000000000028	5.40000000000000036	0.990825690000000092	N0	AUROBINDO PHARM	0.0182000000000000009
1636	2018-08-03	46246021	31722056424	CITALOPRAM HBR 10 MG/5 ML S	450	0	87.3799999999999955	0	87.3799999999999955	104.629999999999995	-17.25	-0.197413599999999995	N0	CAMBER PHARMACE	0.232500000000000012
1637	2018-08-31	46246021	31722056424	CITALOPRAM HBR 10 MG/5 ML S	450	0	87.3799999999999955	0	87.3799999999999955	104.629999999999995	-17.25	-0.197413599999999995	N0	CAMBER PHARMACE	0.232500000000000012
1638	2018-08-10	46235964	59746054405	CITALOPRAM HBR 20 MG TABLET	30	0.0500000000000000028	0	10	10.0500000000000007	0.520000000000000018	9.52999999999999936	0.94825871000000006	N0	JUBILANT CADIST	0.0173999999999999988
1639	2018-08-16	46256531	65862000605	CITALOPRAM HBR 20 MG TABLET	15	0	2.41999999999999993	0	2.41999999999999993	0.28999999999999998	2.12999999999999989	0.8801652900000001	N0	AUROBINDO PHARM	0.0195
1640	2018-08-21	46247534	65862000605	CITALOPRAM HBR 20 MG TABLET	45	0	0	4	4	0.880000000000000004	3.12000000000000011	0.780000000000000027	N0	AUROBINDO PHARM	0.0195
1641	2018-08-10	46249551	65862000605	CITALOPRAM HBR 20 MG TABLET	15	11.25	0	0	11.25	0.28999999999999998	10.9600000000000009	0.974222220000000028	N0	AUROBINDO PHARM	0.0195
1642	2018-08-12	46255885	65862000605	CITALOPRAM HBR 20 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0599999999999999978	5.38999999999999968	0.988990830000000098	N0	AUROBINDO PHARM	0.0195
1643	2018-08-15	46255885	65862000605	CITALOPRAM HBR 20 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0599999999999999978	5.38999999999999968	0.988990830000000098	N0	AUROBINDO PHARM	0.0195
1644	2018-08-30	46258241	65862000605	CITALOPRAM HBR 20 MG TABLET	30	2.00999999999999979	0	0	2.00999999999999979	0.589999999999999969	1.41999999999999993	0.706467659999999942	N0	AUROBINDO PHARM	0.0195
1645	2018-08-06	46253944	65862000605	CITALOPRAM HBR 20 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0599999999999999978	5.38999999999999968	0.988990830000000098	N0	AUROBINDO PHARM	0.0195
1646	2018-08-01	46250647	65862000605	CITALOPRAM HBR 20 MG TABLET	30	4.24000000000000021	0	0	4.24000000000000021	0.589999999999999969	3.64999999999999991	0.860849060000000055	N0	AUROBINDO PHARM	0.0195
1647	2018-08-08	46254846	65862000605	CITALOPRAM HBR 20 MG TABLET	15	11.25	0	0	11.25	0.28999999999999998	10.9600000000000009	0.974222220000000028	N0	AUROBINDO PHARM	0.0195
1648	2018-08-14	46253494	59746054405	CITALOPRAM HBR 20 MG TABLET	15	0	0	11.2899999999999991	11.2899999999999991	0.28999999999999998	11	0.974313549999999973	N0	JUBILANT CADIST	0.0173999999999999988
1649	2018-08-20	46256936	65862000605	CITALOPRAM HBR 20 MG TABLET	30	1.83000000000000007	0	1	2.83000000000000007	0.589999999999999969	2.24000000000000021	0.791519430000000024	N0	AUROBINDO PHARM	0.0195
1650	2018-08-09	46253944	65862000605	CITALOPRAM HBR 20 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0599999999999999978	5.38999999999999968	0.988990830000000098	N0	AUROBINDO PHARM	0.0195
1651	2018-08-21	46254675	59746054405	CITALOPRAM HBR 20 MG TABLET	30	4.96999999999999975	0	0	4.96999999999999975	0.589999999999999969	4.37999999999999989	0.881287729999999936	N0	JUBILANT CADIST	0.0173999999999999988
1652	2018-08-02	46253261	65862000605	CITALOPRAM HBR 20 MG TABLET	15	11.25	0	0	11.25	0.28999999999999998	10.9600000000000009	0.974222220000000028	N0	AUROBINDO PHARM	0.0195
1653	2018-08-30	46240984	59746054405	CITALOPRAM HBR 20 MG TABLET	30	0.28999999999999998	0	1.25	1.54000000000000004	0.520000000000000018	1.02000000000000002	0.66233766000000005	N0	JUBILANT CADIST	0.0173999999999999988
1654	2018-08-25	46249190	65862000605	CITALOPRAM HBR 20 MG TABLET	30	0	0	3.06000000000000005	3.06000000000000005	0.589999999999999969	2.4700000000000002	0.807189539999999983	N0	AUROBINDO PHARM	0.0195
1655	2018-08-03	46253944	65862000605	CITALOPRAM HBR 20 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0599999999999999978	5.38999999999999968	0.988990830000000098	N0	AUROBINDO PHARM	0.0195
1656	2018-08-10	46255798	59746054405	CITALOPRAM HBR 20 MG TABLET	15	11.25	0	0	11.25	0.260000000000000009	10.9900000000000002	0.97688889000000001	N0	JUBILANT CADIST	0.0173999999999999988
1657	2018-08-03	46254972	59746054405	CITALOPRAM HBR 20 MG TABLET	30	4.96999999999999975	0	0	4.96999999999999975	0.520000000000000018	4.45000000000000018	0.895372229999999991	N0	JUBILANT CADIST	0.0173999999999999988
1658	2018-08-22	46256038	65862000605	CITALOPRAM HBR 20 MG TABLET	30	4.96999999999999975	0	0	4.96999999999999975	0.589999999999999969	4.37999999999999989	0.881287729999999936	N0	AUROBINDO PHARM	0.0195
1659	2018-08-14	46256155	59746054405	CITALOPRAM HBR 20 MG TABLET	3	5.45000000000000018	0	0	5.45000000000000018	0.0599999999999999978	5.38999999999999968	0.988990830000000098	N0	JUBILANT CADIST	0.0173999999999999988
1660	2018-08-02	46240984	59746054405	CITALOPRAM HBR 20 MG TABLET	30	1.15999999999999992	0	0.380000000000000004	1.54000000000000004	0.520000000000000018	1.02000000000000002	0.66233766000000005	N0	JUBILANT CADIST	0.0173999999999999988
1661	2018-08-30	46258244	65862000605	CITALOPRAM HBR 20 MG TABLET	15	11.25	0	0	11.25	0.28999999999999998	10.9600000000000009	0.974222220000000028	N0	AUROBINDO PHARM	0.0195
1662	2018-08-17	46256154	65862000605	CITALOPRAM HBR 20 MG TABLET	30	0	0	11.3000000000000007	11.3000000000000007	0.589999999999999969	10.7100000000000009	0.947787610000000003	N0	AUROBINDO PHARM	0.0195
1663	2018-08-23	46242937	59746054405	CITALOPRAM HBR 20 MG TABLET	30	0	0	2.54999999999999982	2.54999999999999982	0.520000000000000018	2.0299999999999998	0.796078430000000004	N0	JUBILANT CADIST	0.0173999999999999988
1664	2018-08-24	46249551	65862000605	CITALOPRAM HBR 20 MG TABLET	15	11.25	0	0	11.25	0.28999999999999998	10.9600000000000009	0.974222220000000028	N0	AUROBINDO PHARM	0.0195
1665	2018-08-29	46253494	59746054405	CITALOPRAM HBR 20 MG TABLET	15	0	0	38.3100000000000023	38.3100000000000023	0.260000000000000009	38.0499999999999972	0.993213259999999987	N0	JUBILANT CADIST	0.0173999999999999988
1666	2018-08-23	46257409	65862000605	CITALOPRAM HBR 20 MG TABLET	90	0	0	7.24000000000000021	7.24000000000000021	1.76000000000000001	5.48000000000000043	0.756906079999999926	N0	AUROBINDO PHARM	0.0195
1667	2018-08-03	46241721	59746054405	CITALOPRAM HBR 20 MG TABLET	30	2.00999999999999979	0	0	2.00999999999999979	0.520000000000000018	1.48999999999999999	0.741293529999999978	N0	JUBILANT CADIST	0.0173999999999999988
1668	2018-08-23	46257322	65862000705	CITALOPRAM HBR 40 MG TABLET	30	2.89000000000000012	0	1	3.89000000000000012	0.869999999999999996	3.02000000000000002	0.776349610000000023	N0	AUROBINDO PHARM	0.0288999999999999986
1669	2018-08-16	46251950	65862000705	CITALOPRAM HBR 40 MG TABLET	30	5	0	0	5	0.869999999999999996	4.12999999999999989	0.825999999999999956	N0	AUROBINDO PHARM	0.0288999999999999986
1670	2018-08-28	46236791	65862000705	CITALOPRAM HBR 40 MG TABLET	30	0.660000000000000031	0	1.25	1.90999999999999992	0.869999999999999996	1.04000000000000004	0.544502620000000048	N0	AUROBINDO PHARM	0.0288999999999999986
1671	2018-08-09	46250191	65862000705	CITALOPRAM HBR 40 MG TABLET	30	5.54000000000000004	0	0	5.54000000000000004	0.869999999999999996	4.66999999999999993	0.842960290000000056	N0	AUROBINDO PHARM	0.0288999999999999986
1672	2018-08-17	46245893	65862000705	CITALOPRAM HBR 40 MG TABLET	30	0.660000000000000031	0	1.25	1.90999999999999992	0.869999999999999996	1.04000000000000004	0.544502620000000048	N0	AUROBINDO PHARM	0.0288999999999999986
1673	2018-08-20	46222006	65862000705	CITALOPRAM HBR 40 MG TABLET	30	1.43999999999999995	0	0.469999999999999973	1.90999999999999992	0.869999999999999996	1.04000000000000004	0.544502620000000048	N0	AUROBINDO PHARM	0.0288999999999999986
1674	2018-08-19	46256818	178080001	CITRACAL + D TABS PETITES	10	2.54000000000000004	0	0	2.54000000000000004	0.530000000000000027	2.00999999999999979	0.791338580000000014	Y1	BAYER INC.	0.0524999999999999981
1675	2018-08-29	46258007	781602346	CLARITHROMYCIN 250 MG/5 ML	100	122.060000000000002	0	0	122.060000000000002	119.560000000000002	2.5	0.0204817299999999966	N0	SANDOZ	1.1956
1676	2018-08-28	46257887	41100008043	CLARITIN-D 24 HOUR TAB SA	10	0	0	18.1900000000000013	18.1900000000000013	10.7200000000000006	7.46999999999999975	0.410665199999999952	N0	MSD CONSUMER CA	1.07230000000000003
1677	2018-08-03	46254918	591570801	CLINDAMYCIN 150MG CAPSULE	48	0	0	63.9799999999999969	63.9799999999999969	3.60999999999999988	60.3699999999999974	0.943576120000000018	N	ACTAVIS PHARMA/	0.0753000000000000058
1678	2018-08-18	46256780	591570801	CLINDAMYCIN 150MG CAPSULE	40	3.16999999999999993	0	5	8.16999999999999993	3.00999999999999979	5.16000000000000014	0.631578950000000083	N0	ACTAVIS PHARMA/	0.0753000000000000058
1679	2018-08-15	46256454	574012901	CLINDAMYCIN 75 MG/5 ML SOLN	500	100.75	0	0	100.75	58.6499999999999986	42.1000000000000014	0.417866000000000015	N0	PADDOCK LABS.	0.117300000000000001
1680	2018-08-22	46257012	574012901	CLINDAMYCIN 75 MG/5 ML SOLN	600	191.620000000000005	0	0	191.620000000000005	70.3799999999999955	121.239999999999995	0.632710569999999972	N0	PADDOCK LABS.	0.117300000000000001
1681	2018-08-15	46256359	63304069201	CLINDAMYCIN HCL 150 MG CAPS	28	0	0	5.20000000000000018	5.20000000000000018	3.33999999999999986	1.8600000000000001	0.357692309999999958	N0	RANBAXY/SUN PHA	0.117999999999999994
1682	2018-08-06	46248856	63304069201	CLINDAMYCIN HCL 150 MG CAPS	8	0.819999999999999951	0	1	1.82000000000000006	0.939999999999999947	0.880000000000000004	0.483516479999999971	N0	RANBAXY/SUN PHA	0.117999999999999994
1683	2018-08-21	46257007	59762332801	CLINDAMYCIN HCL 150MG CAP	20	24.25	0	0	24.25	2.39000000000000012	21.8599999999999994	0.901443299999999947	N0	GREENSTONE LLC.	0.119300000000000003
1684	2018-08-11	46255863	59762332801	CLINDAMYCIN HCL 150MG CAP	28	0.75	0	5	5.75	3.33999999999999986	2.41000000000000014	0.419130429999999998	N0	GREENSTONE LLC.	0.119300000000000003
1685	2018-08-28	46257816	591293201	CLINDAMYCIN HCL 300 MG CAPS	40	4.83999999999999986	0	14	18.8399999999999999	7.17999999999999972	11.6600000000000001	0.618895970000000073	N0	ACTAVIS PHARMA/	0.17960000000000001
1686	2018-08-18	46256783	591293201	CLINDAMYCIN HCL 300 MG CAPS	40	9.97000000000000064	0	1.25	11.2200000000000006	7.17999999999999972	4.04000000000000004	0.360071299999999983	N0	ACTAVIS PHARMA/	0.17960000000000001
1687	2018-08-11	46255876	591293201	CLINDAMYCIN HCL 300 MG CAPS	30	11.2300000000000004	0	5	16.2300000000000004	5.38999999999999968	10.8399999999999999	0.667898949999999991	N0	ACTAVIS PHARMA/	0.17960000000000001
1688	2018-08-23	46257414	63304069301	CLINDAMYCIN HCL 300MG CAP	20	2.29999999999999982	0	3	5.29999999999999982	3.00999999999999979	2.29000000000000004	0.432075469999999962	N0	RANBAXY/SUN PHA	0.150700000000000001
1689	2018-08-28	46257966	63304069301	CLINDAMYCIN HCL 300MG CAP	21	8.75	0	0	8.75	3.16000000000000014	5.58999999999999986	0.638857140000000046	N0	RANBAXY/SUN PHA	0.150700000000000001
1690	2018-08-13	46256097	65862059601	CLINDAMYCIN PEDIATR 75 MG/5	200	61.509999999999998	0	0	61.509999999999998	31.5	30.0100000000000016	0.48788815000000002	N0	AUROBINDO PHARM	0.157500000000000001
1691	2018-08-10	46248896	45802056202	CLINDAMYCIN PH 1% SOLUTION	120	0	0	124.920000000000002	124.920000000000002	22.6400000000000006	102.280000000000001	0.818764009999999987	N0	PERRIGO CO.	0.188700000000000007
1692	2018-08-22	46248896	45802056202	CLINDAMYCIN PH 1% SOLUTION	60	2.75999999999999979	0	15	17.7600000000000016	11.3200000000000003	6.44000000000000039	0.362612610000000002	N0	PERRIGO CO.	0.188700000000000007
1693	2018-08-22	46248896	45802056202	CLINDAMYCIN PH 1% SOLUTION	60	0	0	58.6899999999999977	58.6899999999999977	11.3200000000000003	47.3699999999999974	0.807122169999999972	N0	PERRIGO CO.	0.188700000000000007
1694	2018-08-13	46244934	59762374401	CLINDAMYCIN PHOSP 1% LOTION	60	47.490000000000002	0	12	59.490000000000002	41.2899999999999991	18.1999999999999993	0.305933769999999994	N0	GREENSTONE LLC.	0.688200000000000034
1695	2018-08-23	46240374	378868854	CLINDAMYCIN-BENZOYL PEROX G	50	8.98000000000000043	0	213.419999999999987	222.400000000000006	268.769999999999982	-46.3699999999999974	-0.208498200000000022	N0	MYLAN	5.37539999999999996
1696	2018-08-14	46256264	51672125802	CLOBETASOL 0.05% CREAM	30	32.4500000000000028	0	10	42.4500000000000028	28.870000000000001	13.5800000000000001	0.319905769999999978	N0	TARO PHARM USA	0.962300000000000044
1697	2018-08-08	46255461	51672125802	CLOBETASOL 0.05% CREAM	30	31.4499999999999993	0	0	31.4499999999999993	28.870000000000001	2.58000000000000007	0.0820349799999999935	N0	TARO PHARM USA	0.962300000000000044
1698	2018-08-15	46256451	51672125903	CLOBETASOL 0.05% OINTMENT	60	67.25	0	10	77.25	19.5799999999999983	57.6700000000000017	0.746537220000000001	N0	TARO PHARM USA	0.326400000000000023
1699	2018-08-23	46257412	472040250	CLOBETASOL 0.05% SOLUTION	50	44.9799999999999969	0	15	59.9799999999999969	44.759999999999998	15.2200000000000006	0.253751249999999984	N0	ACTAVIS PHARMA/	0.895199999999999996
1700	2018-08-16	46252714	574210302	CLOBETASOL 0.05% TOPICAL LO	118	434.420000000000016	0	0	434.420000000000016	226.75	207.669999999999987	0.478039690000000017	N0	PERRIGO CO.	1.92159999999999997
1701	2018-08-13	46249158	406880801	CLOMIPRAMINE 75 MG CAPSULE	60	213.699999999999989	0	7	220.699999999999989	185.5	35.2000000000000028	0.159492519999999999	N0	MALLINCKRODT PH	3.09169999999999989
1702	2018-08-20	44030446	93083401	CLONAZEPAM 0.1MG/ML SUSP	150	11.6199999999999992	0	0	11.6199999999999992	2.41999999999999993	9.19999999999999929	0.791738380000000075	N0		0
1703	2018-08-14	44031596	395009016	CLONAZEPAM 0.1MG/ML SUSP	210	13.4700000000000006	0	0	13.4700000000000006	3.33999999999999986	10.1300000000000008	0.752041569999999937	N0		0
1704	2018-08-22	44030998	555009596	CLONAZEPAM 0.25 MG DIS TABL	60	55.9600000000000009	0	0	55.9600000000000009	34.9600000000000009	21	0.37526805000000002	N0	TEVA USA	0.582600000000000007
1705	2018-08-31	44032374	16729013616	CLONAZEPAM 0.5 MG TABLET	30	0	0	0.719999999999999973	0.719999999999999973	0.440000000000000002	0.280000000000000027	0.388888889999999987	N0	ACCORD HEALTHCA	0.0146999999999999995
1706	2018-08-27	44031850	378191010	CLONAZEPAM 0.5 MG TABLET	90	0.839999999999999969	0	3.29999999999999982	4.13999999999999968	1.40999999999999992	2.72999999999999998	0.65942029000000002	N0	MYLAN	0.0156999999999999987
1707	2018-08-24	44032493	93083205	CLONAZEPAM 0.5 MG TABLET	30	4.45999999999999996	0	0	4.45999999999999996	0.380000000000000004	4.08000000000000007	0.914798210000000056	N0	TEVA USA	0.0128000000000000006
1708	2018-08-08	44032295	228300350	CLONAZEPAM 0.5MG TABLET	60	0	0	3.02000000000000002	3.02000000000000002	0.739999999999999991	2.2799999999999998	0.754966890000000057	N0	ACTAVIS PHARMA/	0.0123000000000000002
1709	2018-08-14	44032060	93083201	CLONAZEPAM 0.5MG TABLET	15	8.00999999999999979	0	0	8.00999999999999979	0.209999999999999992	7.79999999999999982	0.97378277000000002	N0	TEVA USA	0.0140000000000000003
1710	2018-08-02	44032202	228300350	CLONAZEPAM 0.5MG TABLET	60	3.97999999999999998	0	1.25	5.23000000000000043	1.81000000000000005	3.41999999999999993	0.653919690000000053	N0	ACTAVIS PHARMA/	0.0123000000000000002
1711	2018-08-29	44032060	228300350	CLONAZEPAM 0.5MG TABLET	15	8.00999999999999979	0	0	8.00999999999999979	0.179999999999999993	7.83000000000000007	0.977528089999999961	N0	ACTAVIS PHARMA/	0.0123000000000000002
1712	2018-08-07	44030763	228300350	CLONAZEPAM 0.5MG TABLET	15	0	0.800000000000000044	0	0.800000000000000044	0.179999999999999993	0.619999999999999996	0.775000000000000022	N0	ACTAVIS PHARMA/	0.0123000000000000002
1713	2018-08-11	44032197	228300350	CLONAZEPAM 0.5MG TABLET	90	17.0700000000000003	0	1.25	18.3200000000000003	1.1100000000000001	17.2100000000000009	0.939410479999999937	N0	ACTAVIS PHARMA/	0.0123000000000000002
1714	2018-08-31	44030627	228300350	CLONAZEPAM 0.5MG TABLET	15	0	0	0.800000000000000044	0.800000000000000044	0.179999999999999993	0.619999999999999996	0.775000000000000022	N0	ACTAVIS PHARMA/	0.0123000000000000002
1715	2018-08-01	44030627	93083201	CLONAZEPAM 0.5MG TABLET	15	0	0	0.800000000000000044	0.800000000000000044	0.209999999999999992	0.589999999999999969	0.737500000000000044	N0	TEVA USA	0.0140000000000000003
1716	2018-08-27	44032405	228300350	CLONAZEPAM 0.5MG TABLET	30	12.0099999999999998	0	0	12.0099999999999998	0.369999999999999996	11.6400000000000006	0.969192339999999986	N0	ACTAVIS PHARMA/	0.0123000000000000002
1717	2018-08-27	44032521	93083201	CLONAZEPAM 0.5MG TABLET	60	12.2799999999999994	0	0	12.2799999999999994	0.839999999999999969	11.4399999999999995	0.931596089999999988	N0	TEVA USA	0.0140000000000000003
1718	2018-08-15	44032374	228300350	CLONAZEPAM 0.5MG TABLET	30	0	0	0.719999999999999973	0.719999999999999973	0.440000000000000002	0.280000000000000027	0.388888889999999987	N0	ACTAVIS PHARMA/	0.0123000000000000002
1719	2018-08-02	44032203	228300350	CLONAZEPAM 0.5MG TABLET	45	16.0199999999999996	0	0	16.0199999999999996	1.3600000000000001	14.6600000000000001	0.915106119999999912	N0	ACTAVIS PHARMA/	0.0123000000000000002
1720	2018-08-27	44032382	93083201	CLONAZEPAM 0.5MG TABLET	30	12.0099999999999998	0	0	12.0099999999999998	0.419999999999999984	11.5899999999999999	0.965029140000000063	N0	TEVA USA	0.0140000000000000003
1721	2018-08-10	44031753	228300350	CLONAZEPAM 0.5MG TABLET	30	0	0	1.09000000000000008	1.09000000000000008	0.369999999999999996	0.719999999999999973	0.660550460000000061	N0	ACTAVIS PHARMA/	0.0123000000000000002
1722	2018-08-22	44032240	228300350	CLONAZEPAM 0.5MG TABLET	30	12.0099999999999998	0	0	12.0099999999999998	0.369999999999999996	11.6400000000000006	0.969192339999999986	N0	ACTAVIS PHARMA/	0.0123000000000000002
1723	2018-08-03	44032215	228300350	CLONAZEPAM 0.5MG TABLET	15	0	0	1.89999999999999991	1.89999999999999991	0.450000000000000011	1.44999999999999996	0.763157890000000005	N0	ACTAVIS PHARMA/	0.0123000000000000002
1724	2018-08-15	44032382	93083201	CLONAZEPAM 0.5MG TABLET	30	12.0099999999999998	0	0	12.0099999999999998	0.419999999999999984	11.5899999999999999	0.965029140000000063	N0	TEVA USA	0.0140000000000000003
1725	2018-08-17	44032405	228300350	CLONAZEPAM 0.5MG TABLET	30	12.0099999999999998	0	0	12.0099999999999998	0.369999999999999996	11.6400000000000006	0.969192339999999986	N0	ACTAVIS PHARMA/	0.0123000000000000002
1726	2018-08-03	44031982	228300350	CLONAZEPAM 0.5MG TABLET	60	0.75	0	0	0.75	0.739999999999999991	0.0100000000000000002	0.0133333300000000009	N0	ACTAVIS PHARMA/	0.0123000000000000002
1727	2018-08-06	44031763	228300350	CLONAZEPAM 0.5MG TABLET	30	2.33000000000000007	0	0	2.33000000000000007	0.369999999999999996	1.95999999999999996	0.841201719999999931	N0	ACTAVIS PHARMA/	0.0123000000000000002
1728	2018-08-29	44031976	228300350	CLONAZEPAM 0.5MG TABLET	60	7.91999999999999993	0	0	7.91999999999999993	0.739999999999999991	7.17999999999999972	0.90656565999999994	N0	ACTAVIS PHARMA/	0.0123000000000000002
1729	2018-08-08	44032287	93083201	CLONAZEPAM 0.5MG TABLET	26	4	0	0	4	0.359999999999999987	3.64000000000000012	0.910000000000000031	N0	TEVA USA	0.0140000000000000003
1730	2018-08-05	44032240	93083201	CLONAZEPAM 0.5MG TABLET	30	12.0099999999999998	0	0	12.0099999999999998	0.419999999999999984	11.5899999999999999	0.965029140000000063	N0	TEVA USA	0.0140000000000000003
1731	2018-08-21	44032432	228300350	CLONAZEPAM 0.5MG TABLET	120	0	0	3.41000000000000014	3.41000000000000014	1.47999999999999998	1.92999999999999994	0.565982399999999997	N0	ACTAVIS PHARMA/	0.0123000000000000002
1732	2018-08-31	44032240	228300350	CLONAZEPAM 0.5MG TABLET	30	12.0099999999999998	0	0	12.0099999999999998	0.369999999999999996	11.6400000000000006	0.969192339999999986	N0	ACTAVIS PHARMA/	0.0123000000000000002
1733	2018-08-07	44032265	93083301	CLONAZEPAM 1MG TABLET	30	2.45000000000000018	0	1.66999999999999993	4.12000000000000011	0.589999999999999969	3.5299999999999998	0.85679612000000005	N0	TEVA USA	0.0198000000000000016
1734	2018-08-05	44032238	93083301	CLONAZEPAM 1MG TABLET	60	0	0	6.57000000000000028	6.57000000000000028	1.18999999999999995	5.37999999999999989	0.818873669999999998	N0	TEVA USA	0.0198000000000000016
1735	2018-08-01	44032132	93083301	CLONAZEPAM 1MG TABLET	10	11.2699999999999996	0	0	11.2699999999999996	0.200000000000000011	11.0700000000000003	0.982253769999999915	N0	TEVA USA	0.0198000000000000016
1736	2018-08-27	44032522	93083301	CLONAZEPAM 1MG TABLET	180	14.2799999999999994	0	0	14.2799999999999994	3.56000000000000005	10.7200000000000006	0.750700279999999887	N0	TEVA USA	0.0198000000000000016
1737	2018-08-21	44032054	93083301	CLONAZEPAM 1MG TABLET	30	0	0	3.39999999999999991	3.39999999999999991	0.589999999999999969	2.81000000000000005	0.826470590000000005	N0	TEVA USA	0.0198000000000000016
1738	2018-08-02	44030325	93083301	CLONAZEPAM 1MG TABLET	60	5.32000000000000028	0	1.25	6.57000000000000028	1.18999999999999995	5.37999999999999989	0.818873669999999998	N0	TEVA USA	0.0198000000000000016
1739	2018-08-28	44031778	93083301	CLONAZEPAM 1MG TABLET	60	5.92999999999999972	0	0.82999999999999996	6.75999999999999979	1.18999999999999995	5.57000000000000028	0.823964499999999989	N0	TEVA USA	0.0198000000000000016
1740	2018-08-07	44032262	93083301	CLONAZEPAM 1MG TABLET	60	6.45000000000000018	0	0	6.45000000000000018	1.18999999999999995	5.25999999999999979	0.815503879999999959	N0	TEVA USA	0.0198000000000000016
1741	2018-08-30	44032364	93083301	CLONAZEPAM 1MG TABLET	60	6.57000000000000028	0	0	6.57000000000000028	1.18999999999999995	5.37999999999999989	0.818873669999999998	N0	TEVA USA	0.0198000000000000016
1742	2018-08-06	44031603	93083301	CLONAZEPAM 1MG TABLET	90	3.39000000000000012	0	0	3.39000000000000012	1.78000000000000003	1.6100000000000001	0.474926249999999994	N0	TEVA USA	0.0198000000000000016
1743	2018-08-13	44032347	93083301	CLONAZEPAM 1MG TABLET	90	8.1899999999999995	0	0	8.1899999999999995	1.78000000000000003	6.41000000000000014	0.782661779999999974	N0	TEVA USA	0.0198000000000000016
1744	2018-08-09	44031227	93083301	CLONAZEPAM 1MG TABLET	75	1.65999999999999992	0	1.25	2.91000000000000014	1.48999999999999999	1.41999999999999993	0.487972510000000026	N0	TEVA USA	0.0198000000000000016
1745	2018-08-18	44031785	555009896	CLONAZEPAM 2 MG ODT	30	44.3200000000000003	0	0	44.3200000000000003	24.120000000000001	20.1999999999999993	0.45577616999999998	N0	TEVA USA	0.804100000000000037
1746	2018-08-03	44032227	93083405	CLONAZEPAM 2 MG TABLET	60	6.29000000000000004	0	0	6.29000000000000004	1.6100000000000001	4.67999999999999972	0.744038160000000115	N0	TEVA USA	0.0268000000000000009
1747	2018-08-08	48012032	228212910	CLONIDINE (A)0.1MG/ML SUS	45	0	21.0199999999999996	0	21.0199999999999996	0.75	20.2699999999999996	0.964319700000000113	N0		0
1748	2018-08-14	48012408	228212910	CLONIDINE (A)0.1MG/ML SUS	30	8.41999999999999993	0	0	8.41999999999999993	0.5	7.91999999999999993	0.940617579999999953	N0		0
1749	2018-08-07	48012241	51927180000	CLONIDINE 0.1MG/ML SUSP	15	7.79000000000000004	0	0	7.79000000000000004	0.409999999999999976	7.37999999999999989	0.947368419999999989	N0		0
1750	2018-08-15	48012193	51927180000	CLONIDINE 0.1MG/ML SUSP.	90	0	11.7400000000000002	0	11.7400000000000002	2.47999999999999998	9.25999999999999979	0.788756390000000085	N0		0
1751	2018-08-20	46252722	378019901	CLONIDINE 0.3MG	15	2.60000000000000009	0	0	2.60000000000000009	0.719999999999999973	1.87999999999999989	0.723076920000000012	N0	MYLAN	0.0481999999999999998
1752	2018-08-29	46256143	228212710	CLONIDINE HCL 0.1 MG TABLET	60	2.43000000000000016	0	0	2.43000000000000016	1.23999999999999999	1.18999999999999995	0.489711930000000017	N0	ACTAVIS PHARMA/	0.0206000000000000003
1753	2018-08-16	46256520	228212710	CLONIDINE HCL 0.1 MG TABLET	15	0	1.58000000000000007	0	1.58000000000000007	0.309999999999999998	1.27000000000000002	0.803797469999999903	N0	ACTAVIS PHARMA/	0.0206000000000000003
1754	2018-08-17	46256746	228212750	CLONIDINE HCL 0.1MG TABLE	21	0.660000000000000031	0	0	0.660000000000000031	0.429999999999999993	0.23000000000000001	0.348484849999999957	N0	ACTAVIS PHARMA/	0.0205000000000000009
1755	2018-08-20	46252718	378015201	CLONIDINE HCL 0.1MG TABLE	60	6.49000000000000021	0	0	6.49000000000000021	1.72999999999999998	4.75999999999999979	0.733436059999999945	N0	MYLAN	0.0287999999999999992
1756	2018-08-25	46249634	228212750	CLONIDINE HCL 0.1MG TABLE	60	1.23999999999999999	0	0	1.23999999999999999	1.22999999999999998	0.0100000000000000002	0.00806451999999999863	N0	ACTAVIS PHARMA/	0.0205000000000000009
1757	2018-08-15	46237981	378015210	CLONIDINE HCL 0.1MG TABLE	60	1.85000000000000009	0	0.619999999999999996	2.4700000000000002	2.10999999999999988	0.359999999999999987	0.145748989999999995	N0	MYLAN	0.0352000000000000021
1758	2018-08-20	46256964	378015201	CLONIDINE HCL 0.1MG TABLE	60	6.49000000000000021	0	0	6.49000000000000021	1.72999999999999998	4.75999999999999979	0.733436059999999945	N0	MYLAN	0.0287999999999999992
1759	2018-08-30	46255678	228212750	CLONIDINE HCL 0.1MG TABLE	30	1.82000000000000006	0	0	1.82000000000000006	0.619999999999999996	1.19999999999999996	0.659340660000000023	N0	ACTAVIS PHARMA/	0.0205000000000000009
1760	2018-08-18	46253786	68001023800	CLONIDINE HCL 0.2 MG TABLET	30	2.0299999999999998	0	0.780000000000000027	2.81000000000000005	1.02000000000000002	1.79000000000000004	0.63701067999999994	N0	BLUEPOINT LABOR	0.3755
1761	2018-08-01	46247493	378018610	CLONIDINE HCL 0.2MG TABLE	30	4.37999999999999989	0	0	4.37999999999999989	1.02000000000000002	3.35999999999999988	0.767123290000000013	N0	MYLAN	0.0338999999999999996
1762	2018-08-17	46256735	378018610	CLONIDINE HCL 0.2MG TABLE	15	7.94000000000000039	0	0	7.94000000000000039	0.510000000000000009	7.42999999999999972	0.935768260000000018	N0	MYLAN	0.0338999999999999996
1763	2018-08-28	46253671	378018610	CLONIDINE HCL 0.2MG TABLE	30	4.37999999999999989	0	0	4.37999999999999989	1.02000000000000002	3.35999999999999988	0.767123290000000013	N0	MYLAN	0.0338999999999999996
1764	2018-08-13	48011501	38779056101	CLONIDINE SR 0.15MG	30	-4.41999999999999993	0	66.2900000000000063	61.8699999999999974	6.67999999999999972	55.1899999999999977	0.892031680000000105	N0		0
1765	2018-08-29	46254359	51927180000	CLONIDINE(GT)0.1MG/MLSUS	90	33.1499999999999986	0	0	33.1499999999999986	1.58000000000000007	31.5700000000000003	0.952337859999999981	N0		0
1766	2018-08-06	48011513	409613922	CLONIDINE-GT 0.1MG/ML SUS	30	32.8999999999999986	0	0	32.8999999999999986	0.890000000000000013	32.009999999999998	0.972948329999999917	N0		0
1767	2018-08-17	48012145	55111019605	CLOPIDOGREL 5MG/ML SUSP	225	37.0499999999999972	0	0	37.0499999999999972	4.25	32.7999999999999972	0.885290149999999998	N0		0
1768	2018-08-20	46256968	55111019605	CLOPIDOGREL 75 MG TABLET	30	5.16000000000000014	0	1.25	6.41000000000000014	2.06000000000000005	4.34999999999999964	0.678627149999999957	N0	DR.REDDY'S LAB	0.0688
1769	2018-08-29	46258034	55111019690	CLOPIDOGREL 75 MG TABLET	90	0	0	29.6900000000000013	29.6900000000000013	6.58000000000000007	23.1099999999999994	0.778376559999999995	N0	DR.REDDY'S LAB	0.0730999999999999983
1770	2018-08-28	46257853	65862035705	CLOPIDOGREL 75 MG TABLET	30	1	0	1	2	1.52000000000000002	0.479999999999999982	0.239999999999999991	N0	AUROBINDO PHARM	0.0505000000000000032
1771	2018-08-30	46253422	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1772	2018-08-04	46234731	65862035705	CLOPIDOGREL 75 MG TABLET	90	16.379999999999999	0	0	16.379999999999999	4.54999999999999982	11.8300000000000001	0.722222220000000026	N0	AUROBINDO PHARM	0.0505000000000000032
1773	2018-08-12	46255906	55111019690	CLOPIDOGREL 75 MG TABLET	3	6.87999999999999989	0	0	6.87999999999999989	0.220000000000000001	6.66000000000000014	0.968023259999999941	N0	DR.REDDY'S LAB	0.0730999999999999983
1774	2018-08-14	46256287	55111019690	CLOPIDOGREL 75 MG TABLET	3	6.87999999999999989	0	0	6.87999999999999989	0.220000000000000001	6.66000000000000014	0.968023259999999941	N0	DR.REDDY'S LAB	0.0730999999999999983
1775	2018-08-09	46214679	55111019690	CLOPIDOGREL 75 MG TABLET	90	0	0	10.2599999999999998	10.2599999999999998	6.58000000000000007	3.68000000000000016	0.358674460000000028	N0	DR.REDDY'S LAB	0.0730999999999999983
1776	2018-08-21	46257010	55111019690	CLOPIDOGREL 75 MG TABLET	90	0	0	29.6900000000000013	29.6900000000000013	6.58000000000000007	23.1099999999999994	0.778376559999999995	N0	DR.REDDY'S LAB	0.0730999999999999983
1777	2018-08-31	46233922	55111019690	CLOPIDOGREL 75 MG TABLET	30	0.560000000000000053	0	10	10.5600000000000005	2.18999999999999995	8.36999999999999922	0.792613640000000008	N0	DR.REDDY'S LAB	0.0730999999999999983
1778	2018-08-03	46245177	55111019690	CLOPIDOGREL 75 MG TABLET	30	2.83000000000000007	0	3	5.83000000000000007	2.18999999999999995	3.64000000000000012	0.624356780000000056	N0	DR.REDDY'S LAB	0.0730999999999999983
1779	2018-08-22	46257275	55111019690	CLOPIDOGREL 75 MG TABLET	90	16.1999999999999993	0	0	16.1999999999999993	6.58000000000000007	9.61999999999999922	0.593827160000000021	N0	DR.REDDY'S LAB	0.0730999999999999983
1780	2018-08-23	46257388	55111019690	CLOPIDOGREL 75 MG TABLET	30	5.83000000000000007	0	0	5.83000000000000007	2.18999999999999995	3.64000000000000012	0.624356780000000056	N0	DR.REDDY'S LAB	0.0730999999999999983
1781	2018-08-09	46253188	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1782	2018-08-20	46256848	55111019690	CLOPIDOGREL 75 MG TABLET	90	10.1799999999999997	0	1.25	11.4299999999999997	6.58000000000000007	4.84999999999999964	0.424321959999999998	N0	DR.REDDY'S LAB	0.0730999999999999983
1783	2018-08-07	46236424	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1784	2018-08-26	46256287	55111019690	CLOPIDOGREL 75 MG TABLET	3	6.87999999999999989	0	0	6.87999999999999989	0.220000000000000001	6.66000000000000014	0.968023259999999941	N0	DR.REDDY'S LAB	0.0730999999999999983
1785	2018-08-02	46248213	55111019690	CLOPIDOGREL 75 MG TABLET	90	16.379999999999999	0	0	16.379999999999999	6.58000000000000007	9.80000000000000071	0.598290600000000006	N0	DR.REDDY'S LAB	0.0730999999999999983
1786	2018-08-23	46256287	55111019690	CLOPIDOGREL 75 MG TABLET	3	6.87999999999999989	0	0	6.87999999999999989	0.220000000000000001	6.66000000000000014	0.968023259999999941	N0	DR.REDDY'S LAB	0.0730999999999999983
1787	2018-08-13	46224475	55111019690	CLOPIDOGREL 75 MG TABLET	90	0	0	12.6999999999999993	12.6999999999999993	6.58000000000000007	6.12000000000000011	0.481889759999999945	N0	DR.REDDY'S LAB	0.0730999999999999983
1788	2018-08-20	46256287	55111019690	CLOPIDOGREL 75 MG TABLET	3	6.87999999999999989	0	0	6.87999999999999989	0.220000000000000001	6.66000000000000014	0.968023259999999941	N0	DR.REDDY'S LAB	0.0730999999999999983
1789	2018-08-23	46253188	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1790	2018-08-16	46253422	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1791	2018-08-02	46253422	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1792	2018-08-13	46252703	55111019690	CLOPIDOGREL 75 MG TABLET	30	1.87000000000000011	0	0.469999999999999973	2.33999999999999986	2.18999999999999995	0.149999999999999994	0.0641025600000000029	N0	DR.REDDY'S LAB	0.0730999999999999983
1793	2018-08-08	46254776	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1794	2018-08-02	46254776	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1795	2018-08-29	46256287	55111019690	CLOPIDOGREL 75 MG TABLET	3	6.87999999999999989	0	0	6.87999999999999989	0.220000000000000001	6.66000000000000014	0.968023259999999941	N0	DR.REDDY'S LAB	0.0730999999999999983
1796	2018-08-17	46256287	55111019690	CLOPIDOGREL 75 MG TABLET	3	6.87999999999999989	0	0	6.87999999999999989	0.220000000000000001	6.66000000000000014	0.968023259999999941	N0	DR.REDDY'S LAB	0.0730999999999999983
1797	2018-08-13	46247684	55111019690	CLOPIDOGREL 75 MG TABLET	15	18.4200000000000017	0	0	18.4200000000000017	1.10000000000000009	17.3200000000000003	0.940282299999999904	N0	DR.REDDY'S LAB	0.0730999999999999983
1798	2018-08-21	44032446	378003005	CLORAZEPATE 3.75MG TABLET	90	3.62999999999999989	0	47	50.6300000000000026	80.5999999999999943	-29.9699999999999989	-0.591941539999999988	N0	MYLAN	0.895499999999999963
1799	2018-08-23	44032100	378003005	CLORAZEPATE 3.75MG TABLET	135	72.3199999999999932	0	1	73.3199999999999932	120.890000000000001	-47.5700000000000003	-0.648799779999999937	N0	MYLAN	0.895499999999999963
1800	2018-08-21	44032445	378004001	CLORAZEPATE 7.5MG TABLET	90	83.5300000000000011	0	65.6299999999999955	149.159999999999997	113.450000000000003	35.7100000000000009	0.239407349999999991	N0	MYLAN	1.26049999999999995
1801	2018-08-17	46256641	51672127501	CLOTRIMAZOLE 1% CREAM	15	9.5	0	7.45000000000000018	16.9499999999999993	6.16000000000000014	10.7899999999999991	0.636578170000000054	N0	TARO PHARM USA	0.410600000000000021
1802	2018-08-11	46255865	51672127502	CLOTRIMAZOLE 1% CREAM	30	14.3399999999999999	0	0	14.3399999999999999	11.2599999999999998	3.08000000000000007	0.214783819999999986	N0	TARO PHARM USA	0.0695999999999999952
1803	2018-08-01	46254192	51672127502	CLOTRIMAZOLE 1% CREAM	30	33.6199999999999974	0	0	33.6199999999999974	11.2599999999999998	22.3599999999999994	0.665080310000000008	N0	TARO PHARM USA	0.0695999999999999952
1804	2018-08-06	46255230	51672127502	CLOTRIMAZOLE 1% CREAM	30	7.49000000000000021	0	1	8.49000000000000021	2.08999999999999986	6.40000000000000036	0.753828029999999982	N0	TARO PHARM USA	0.0695999999999999952
1805	2018-08-29	46257992	51672127502	CLOTRIMAZOLE 1% CREAM	30	14.3399999999999999	0	0	14.3399999999999999	2.08999999999999986	12.25	0.854253839999999931	N0	TARO PHARM USA	0.0695999999999999952
1806	2018-08-30	46258224	63868059528	CLOTRIMAZOLE 1% CREAM	360	0	0	75	75	33.8400000000000034	41.1599999999999966	0.548800000000000066	N		0.0940000000000000002
1807	2018-08-24	46257573	51672127502	CLOTRIMAZOLE 1% CREAM	30	8.49000000000000021	0	0	8.49000000000000021	2.08999999999999986	6.40000000000000036	0.753828029999999982	N0	TARO PHARM USA	0.0695999999999999952
1808	2018-08-03	46255018	51672127506	CLOTRIMAZOLE 1% CREAM	45	26.0599999999999987	0	0	26.0599999999999987	16.1400000000000006	9.91999999999999993	0.380660019999999988	N0	TARO PHARM USA	0.224500000000000005
1809	2018-08-01	46254631	574010714	CLOTRIMAZOLE 10 MG TROCHE	70	90.8700000000000045	0	0	90.8700000000000045	20.7199999999999989	70.1500000000000057	0.771981950000000028	N0	PADDOCK LABS.	0.295999999999999985
1810	2018-08-21	46257106	574010770	CLOTRIMAZOLE 10 MG TROCHE	50	12.6500000000000004	0	10	22.6499999999999986	14.8000000000000007	7.84999999999999964	0.346578370000000024	N0	PERRIGO CO.	0.295999999999999985
1811	2018-08-09	46255602	68462029855	CLOTRIMAZOLE-BETAMETHASONE	45	64.2199999999999989	0	0	64.2199999999999989	7.67999999999999972	56.5399999999999991	0.880411090000000063	N0	GLENMARK PHARMA	0.170600000000000002
1812	2018-08-13	46255992	68462029855	CLOTRIMAZOLE-BETAMETHASONE	45	63.6899999999999977	0	1	64.6899999999999977	7.67999999999999972	57.009999999999998	0.881279950000000034	N0	GLENMARK PHARMA	0.170600000000000002
1813	2018-08-09	46255628	51672404801	CLOTRIMAZOLE/BETAMETH CRE	15	0	0	36.4500000000000028	36.4500000000000028	14.5999999999999996	21.8500000000000014	0.59945130000000002	N0	TARO PHARM USA	0.973300000000000054
1814	2018-08-20	46246000	378086001	CLOZAPINE 100 MG TABLET	168	85.9599999999999937	0	0	85.9599999999999937	574.32000000000005	-488.360000000000014	-5.68124709000000028	N0	MYLAN	3.41860000000000008
1815	2018-08-02	46247205	378086005	CLOZAPINE 100MG TABLET	30	0	0	20.25	20.25	16.3999999999999986	3.85000000000000009	0.190123460000000022	N0	MYLAN	0.546699999999999964
1817	2018-08-24	46257491	143301801	COLCHICINE 0.6 MG CAPSULE	30	0	0	188.060000000000002	188.060000000000002	118.799999999999997	69.2600000000000051	0.368286719999999956	N0	WEST-WARD,INC.	3.95999999999999996
1818	2018-08-01	46254709	66993016502	COLCHICINE 0.6 MG TABLET	30	158.120000000000005	1.29000000000000004	6	165.409999999999997	146.969999999999999	18.4400000000000013	0.111480560000000006	N0	PRASCO LABS	4.89900000000000002
1819	2018-08-16	46256487	59762045001	COLESTIPOL HCL 1 GM TABLET	60	27.6000000000000014	0	10	37.6000000000000014	29.629999999999999	7.96999999999999975	0.21196808999999997	N0	GREENSTONE LLC.	0.493900000000000006
1820	2018-08-20	46256838	597002402	COMBIVENT RESPIMAT INH SPRA	4	339.129999999999995	0	30	369.129999999999995	314.589999999999975	54.5399999999999991	0.147752820000000007	Y1	BOEHRINGER ING.	78.6474999999999937
1821	2018-08-07	46254318	597002402	COMBIVENT RESPIMAT INH SPRA	4	383.95999999999998	0	0	383.95999999999998	314.589999999999975	69.3700000000000045	0.180669859999999988	N0	BOEHRINGER ING.	78.6474999999999937
1822	2018-08-29	46244767	597002402	COMBIVENT RESPIMAT INH SPRA	4	383.95999999999998	0	0	383.95999999999998	314.589999999999975	69.3700000000000045	0.180669859999999988	N0	BOEHRINGER ING.	78.6474999999999937
1823	2018-08-06	46240778	597002402	COMBIVENT RESPIMAT INH SPRA	4	359.199999999999989	0	8.34999999999999964	367.550000000000011	314.589999999999975	52.9600000000000009	0.144089240000000007	N0	BOEHRINGER ING.	78.6474999999999937
1824	2018-08-20	46244767	597002402	COMBIVENT RESPIMAT INH SPRA	4	383.95999999999998	0	0	383.95999999999998	314.589999999999975	69.3700000000000045	0.180669859999999988	N0	BOEHRINGER ING.	78.6474999999999937
1825	2018-08-22	46252348	597002402	COMBIVENT RESPIMAT INH SPRA	4	323.819999999999993	48.1400000000000006	9	380.95999999999998	314.589999999999975	66.3700000000000045	0.174217769999999994	N0	BOEHRINGER ING.	78.6474999999999937
1826	2018-08-03	46250425	597002402	COMBIVENT RESPIMAT INH SPRA	4	394.339999999999975	0	3.70000000000000018	398.04000000000002	314.589999999999975	83.4500000000000028	0.209652299999999986	N0	BOEHRINGER ING.	78.6474999999999937
1827	2018-08-10	46244767	597002402	COMBIVENT RESPIMAT INH SPRA	4	383.95999999999998	0	0	383.95999999999998	314.589999999999975	69.3700000000000045	0.180669859999999988	N0	BOEHRINGER ING.	78.6474999999999937
1828	2018-08-02	46254865	597002402	COMBIVENT RESPIMAT INH SPRA	4	383.95999999999998	0	0	383.95999999999998	314.589999999999975	69.3700000000000045	0.180669859999999988	N0	BOEHRINGER ING.	78.6474999999999937
1829	2018-08-09	46227394	597002402	COMBIVENT RESPIMAT INH SPRA	4	383.95999999999998	0	0	383.95999999999998	314.589999999999975	69.3700000000000045	0.180669859999999988	N0	BOEHRINGER ING.	78.6474999999999937
1830	2018-08-14	46256175	574722612	COMPRO 25MG SUPPOSITORY	8	69.8900000000000006	0	0	69.8900000000000006	53.8500000000000014	16.0399999999999991	0.229503510000000022	N0	PERRIGO CO.	6.73160000000000025
1831	2018-08-10	46255837	574722612	COMPRO 25MG SUPPOSITORY	12	102.840000000000003	0	0	102.840000000000003	80.7800000000000011	22.0599999999999987	0.21450797000000002	N0	PERRIGO CO.	6.73160000000000025
1832	2018-08-03	42055301	50458058601	CONCERTA 36 MG TABLET ER	30	327.769999999999982	0	5	332.769999999999982	314.050000000000011	18.7199999999999989	0.0562550699999999973	Y9	JANSSEN PHARM.	10.4682999999999993
1833	2018-08-06	42055569	50458058601	CONCERTA 36 MG TABLET ER	30	317.970000000000027	29.0199999999999996	0	346.990000000000009	314.050000000000011	32.9399999999999977	0.0949306899999999981	N9	JANSSEN PHARM.	10.4682999999999993
1834	2018-08-03	46254981	7430000391	CORTIZONE-10 CREAM	56	0	0	9.66999999999999993	9.66999999999999993	6.71999999999999975	2.95000000000000018	0.305067219999999972	N0		0.119999999999999996
1835	2018-08-27	46250150	56016970	COUMADIN 1MG TABLET	12	23.9299999999999997	0	0	23.9299999999999997	22.370000000000001	1.56000000000000005	0.0651901400000000075	Y1	BMS	1.86420000000000008
1836	2018-08-27	46249369	56017070	COUMADIN 2MG TABLET	90	186.530000000000001	0	0	186.530000000000001	175.039999999999992	11.4900000000000002	0.0615986700000000012	Y1	BMS	1.94490000000000007
1837	2018-08-08	46222196	40985213247	CRANBERRY EXTRACT 200 MG	90	0	0	8	8	6	2	0.25	N0		0.0666999999999999954
1838	2018-08-21	46257087	32121207	CREON DR 12,000 UNITS CAPSU	450	1304.96000000000004	0	0	1304.96000000000004	1220.40000000000009	84.5600000000000023	0.0647989200000000098	N0	ABBVIE US LLC	2.71200000000000019
1839	2018-08-21	46257085	32121201	CREON DR 12,000 UNITS CAPSU	540	1565.93000000000006	0	0	1565.93000000000006	1464.48000000000002	101.450000000000003	0.0647857800000000011	N0	ABBVIE US LLC	2.71200000000000019
1840	2018-08-24	46246611	32122401	CREON DR 24,000 UNITS CAPSU	135	803.32000000000005	0	0	803.32000000000005	725.730000000000018	77.5900000000000034	0.0965866699999999995	N0	ABBVIE US LLC	5.37579999999999991
1841	2018-08-07	46243987	32122401	CREON DR 24,000 UNITS CAPSU	180	1069.75999999999999	0	0	1069.75999999999999	967.639999999999986	102.120000000000005	0.0954606600000000027	N0	ABBVIE US LLC	5.37579999999999991
1842	2018-08-22	46243987	32122401	CREON DR 24,000 UNITS CAPSU	180	1069.75999999999999	0	0	1069.75999999999999	967.639999999999986	102.120000000000005	0.0954606600000000027	N0	ABBVIE US LLC	5.37579999999999991
1843	2018-08-24	46257580	49100036374	CULTURELLE HLTH & WELLNESS	28	18.4200000000000017	0	0	18.4200000000000017	13.5299999999999994	4.88999999999999968	0.265472309999999989	Y1	I-HEALTH, INC	0.483300000000000007
1844	2018-08-06	46248488	49100040008	CULTURELLE KIDS PACKET	30	0	0	20	20	5.03000000000000025	14.9700000000000006	0.748499999999999943	N0	I-HEALTH, INC	0.167800000000000005
1845	2018-08-21	46235223	49100040008	CULTURELLE KIDS PACKET	30	16.879999999999999	0	0	16.879999999999999	5.03000000000000025	11.8499999999999996	0.702014219999999911	N0	I-HEALTH, INC	0.167800000000000005
1846	2018-08-15	46248827	259050116	CUVPOSA 1 MG/5 ML SOLUTION	240	262.519999999999982	0	0	262.519999999999982	12.0700000000000003	250.449999999999989	0.954022549999999914	N0	MERZ	0.0502999999999999975
1847	2018-08-13	46245703	259050116	CUVPOSA 1 MG/5 ML SOLUTION	360	376.720000000000027	0	0	376.720000000000027	18.1099999999999994	358.610000000000014	0.951927159999999994	N0	MERZ	0.0502999999999999975
1848	2018-08-02	46252643	259050116	CUVPOSA 1 MG/5 ML SOLUTION	90	94.4399999999999977	0	0	94.4399999999999977	4.53000000000000025	89.9099999999999966	0.952033039999999997	N0	MERZ	0.0502999999999999975
1849	2018-08-29	46248827	259050116	CUVPOSA 1 MG/5 ML SOLUTION	240	262.519999999999982	0	0	262.519999999999982	12.0700000000000003	250.449999999999989	0.954022549999999914	N0	MERZ	0.0502999999999999975
1850	2018-08-04	46222605	63323004401	CYANOCOBALAMIN 1,000 MCG/ML	1	0	0	12.2599999999999998	12.2599999999999998	7.80999999999999961	4.45000000000000018	0.362968999999999986	N0	APP/FRESENIUS K	2.41520000000000001
1851	2018-08-21	46225042	63323004401	CYANOCOBALAMIN 1,000 MCG/ML	3	-5	0	21.2899999999999991	16.2899999999999991	7.25	9.03999999999999915	0.554941679999999993	N0	APP/FRESENIUS K	2.41520000000000001
1852	2018-08-02	46254745	63323004401	CYANOCOBALAMIN 1,000 MCG/ML	11	44.9299999999999997	0	10	54.9299999999999997	26.5700000000000003	28.3599999999999994	0.516293459999999982	N0	APP/FRESENIUS K	2.41520000000000001
1853	2018-08-01	46239687	603752149	CYCLAFEM 1-35-28 TABLET	28	14.8399999999999999	0	1.25	16.0899999999999999	11.4199999999999999	4.66999999999999993	0.290242390000000017	N0	QUALITEST/PAR P	0.38919999999999999
1854	2018-08-14	46253781	603752149	CYCLAFEM 1-35-28 TABLET	28	10.5500000000000007	0	0	10.5500000000000007	10.9000000000000004	-0.349999999999999978	-0.033175360000000001	N0	QUALITEST/PAR P	0.38919999999999999
1855	2018-08-31	46253781	603752149	CYCLAFEM 1-35-28 TABLET	28	10.5500000000000007	0	0	10.5500000000000007	10.9000000000000004	-0.349999999999999978	-0.033175360000000001	N0	QUALITEST/PAR P	0.38919999999999999
1856	2018-08-29	46239687	603752149	CYCLAFEM 1-35-28 TABLET	28	14.8399999999999999	0	1.25	16.0899999999999999	10.9000000000000004	5.19000000000000039	0.322560599999999975	N0	QUALITEST/PAR P	0.38919999999999999
1857	2018-08-09	46255607	43547040010	CYCLOBENZAPRINE 10 MG TABLE	90	10.4000000000000004	0	0	10.4000000000000004	1.31000000000000005	9.08999999999999986	0.874038459999999962	N0	SOLCO HEALTHCAR	0.0145000000000000007
1858	2018-08-06	46237880	591565805	CYCLOBENZAPRINE 10 MG TABLE	30	13.4900000000000002	0	3.35000000000000009	16.8399999999999999	0.739999999999999991	16.1000000000000014	0.956057009999999985	N0	ACTAVIS PHARMA/	0.0225999999999999986
1859	2018-08-03	46237035	378075110	CYCLOBENZAPRINE 10 MG TABLE	30	0	0	3.99000000000000021	3.99000000000000021	0.489999999999999991	3.5	0.8771929799999999	N0	MYLAN	0.0161999999999999991
1860	2018-08-27	46257696	591565805	CYCLOBENZAPRINE 10 MG TABLE	20	0	0	0.969999999999999973	0.969999999999999973	0.450000000000000011	0.520000000000000018	0.536082469999999978	N0	ACTAVIS PHARMA/	0.0225999999999999986
1861	2018-08-06	46228363	43547040010	CYCLOBENZAPRINE 10 MG TABLE	30	4.12999999999999989	0	0	4.12999999999999989	34.7299999999999969	-30.6000000000000014	-7.40920097000000055	N0	SOLCO HEALTHCAR	0.0145000000000000007
1862	2018-08-21	46256991	378075110	CYCLOBENZAPRINE 10 MG TABLE	60	0	0	2.0299999999999998	2.0299999999999998	0.969999999999999973	1.06000000000000005	0.522167489999999956	N0	MYLAN	0.0161999999999999991
1863	2018-08-25	46219473	378075110	CYCLOBENZAPRINE 10 MG TABLE	80	0	0	2.45999999999999996	2.45999999999999996	1.30000000000000004	1.15999999999999992	0.471544719999999973	N0	MYLAN	0.0161999999999999991
1864	2018-08-20	46245900	378075110	CYCLOBENZAPRINE 10 MG TABLE	90	2.22999999999999998	0	0.739999999999999991	2.9700000000000002	1.45999999999999996	1.51000000000000001	0.508417510000000017	N0	MYLAN	0.0161999999999999991
1865	2018-08-13	46256074	378075110	CYCLOBENZAPRINE 10 MG TABLE	30	0.849999999999999978	0	0.209999999999999992	1.06000000000000005	0.489999999999999991	0.569999999999999951	0.537735850000000015	N0	MYLAN	0.0161999999999999991
1866	2018-08-25	46247873	378075110	CYCLOBENZAPRINE 10 MG TABLE	90	2.22999999999999998	0	0.739999999999999991	2.9700000000000002	1.45999999999999996	1.51000000000000001	0.508417510000000017	N0	MYLAN	0.0161999999999999991
1867	2018-08-29	46257994	378075110	CYCLOBENZAPRINE 10 MG TABLE	30	3.79000000000000004	0	0	3.79000000000000004	0.489999999999999991	3.29999999999999982	0.870712400000000053	N0	MYLAN	0.0161999999999999991
1868	2018-08-23	46257401	378075110	CYCLOBENZAPRINE 10 MG TABLE	20	0.709999999999999964	0	5	5.70999999999999996	0.320000000000000007	5.38999999999999968	0.943957970000000035	N0	MYLAN	0.0161999999999999991
1869	2018-08-16	46256577	378075110	CYCLOBENZAPRINE 10 MG TABLE	90	0.75	0	1	1.75	1.45999999999999996	0.28999999999999998	0.165714289999999986	N0	MYLAN	0.0161999999999999991
1870	2018-08-27	46257667	591565805	CYCLOBENZAPRINE 10 MG TABLE	30	0	0	1.26000000000000001	1.26000000000000001	0.680000000000000049	0.57999999999999996	0.460317459999999956	N0	ACTAVIS PHARMA/	0.0225999999999999986
1871	2018-08-03	46245125	591565805	CYCLOBENZAPRINE 10 MG TABLE	60	0.28999999999999998	0	1	1.29000000000000004	1.3600000000000001	-0.0700000000000000067	-0.054263570000000004	N0	ACTAVIS PHARMA/	0.0225999999999999986
1872	2018-08-13	46236447	591565801	CYCLOBENZAPRINE 10MG TABL	30	0	0	8	8	2.14000000000000012	5.86000000000000032	0.73250000000000004	N0	ACTAVIS PHARMA/	0.0236999999999999988
1873	2018-08-23	46253933	591565801	CYCLOBENZAPRINE 10MG TABL	90	2.22999999999999998	0	0.739999999999999991	2.9700000000000002	6.42999999999999972	-3.45999999999999996	-1.16498316000000002	N0	ACTAVIS PHARMA/	0.0236999999999999988
1874	2018-08-20	46252322	591565801	CYCLOBENZAPRINE 10MG TABL	42	3.70999999999999996	0	0.57999999999999996	4.29000000000000004	3	1.29000000000000004	0.300699300000000003	N0	ACTAVIS PHARMA/	0.0236999999999999988
1875	2018-08-01	46254637	10702000601	CYCLOBENZAPRINE 5 MG TABLET	30	0	0	6.45999999999999996	6.45999999999999996	0.939999999999999947	5.51999999999999957	0.85448915999999997	N0	KVK-TECH, INC.	0.0294999999999999984
1876	2018-08-28	46250874	69097084507	CYCLOBENZAPRINE 5 MG TABLET	30	10.6199999999999992	0	1.25	11.8699999999999992	0.680000000000000049	11.1899999999999995	0.942712719999999949	N0	CIPLA USA, INC.	0.0224999999999999992
1877	2018-08-11	46255872	69097084507	CYCLOBENZAPRINE 5 MG TABLET	30	0	0	6.45999999999999996	6.45999999999999996	0.680000000000000049	5.78000000000000025	0.894736840000000089	N0	CIPLA USA, INC.	0.0224999999999999992
1878	2018-08-27	46257743	603307821	CYCLOBENZAPRINE 5 MG TABLET	30	0	0	1.79000000000000004	1.79000000000000004	1.03000000000000003	0.760000000000000009	0.424581009999999981	N0	QUALITEST/PAR P	0.0344
1879	2018-08-28	46244285	10702000601	CYCLOBENZAPRINE 5 MG TABLET	30	3.75	0	1.25	5	0.890000000000000013	4.11000000000000032	0.822000000000000064	N0	KVK-TECH, INC.	0.0294999999999999984
1880	2018-08-13	46256108	10702000650	CYCLOBENZAPRINE 5 MG TABLET	60	0	0	10	10	1.65999999999999992	8.33999999999999986	0.834000000000000075	N0	KVK-TECH, INC.	0.0276999999999999989
1881	2018-08-27	46257650	603307821	CYCLOBENZAPRINE 5 MG TABLET	21	0	0	2.89000000000000012	2.89000000000000012	0.719999999999999973	2.16999999999999993	0.750865050000000034	N0	QUALITEST/PAR P	0.0344
1882	2018-08-29	46258136	10702000601	CYCLOBENZAPRINE 5 MG TABLET	15	1.81000000000000005	0	0.190000000000000002	2	0.440000000000000002	1.56000000000000005	0.780000000000000027	N0	KVK-TECH, INC.	0.0294999999999999984
1883	2018-08-14	46223972	39328004416	CYPROHEPTADINE 2 MG/5 ML SY	300	0	24.6000000000000014	0	24.6000000000000014	17.8200000000000003	6.78000000000000025	0.275609759999999981	N0	PATRIN PHARMA	0.0594000000000000014
1884	2018-08-27	46257751	64980050448	CYPROHEPTADINE 2 MG/5 ML SY	225	18.6999999999999993	0	0	18.6999999999999993	16.2199999999999989	2.47999999999999998	0.132620319999999986	N0	RISING PHARM	0.0720999999999999974
1885	2018-08-25	46243106	51991083801	CYPROHEPTADINE 4 MG TABLET	30	3.62999999999999989	0	0	3.62999999999999989	10.5800000000000001	-6.95000000000000018	-1.91460055000000007	N0	BRECKENRIDGE	0.143199999999999994
1886	2018-08-04	46243383	60258000501	CYTRA-K CRYSTALS PACKET	60	74.9500000000000028	0	0	74.9500000000000028	30.1400000000000006	44.8100000000000023	0.597865239999999964	N0	CYPRESS PHARM.	0.502399999999999958
1887	2018-08-23	48012451	54879000308	D-DIP S-ALMGS S-GL-N-L AA	70	0	0	40.0300000000000011	40.0300000000000011	5.12999999999999989	34.8999999999999986	0.871846120000000058	N0		0
1888	2018-08-04	46253359	53191036212	D3-50 50,000 UNIT CHOLECALC	4	0	0	5	5	3.02000000000000002	1.97999999999999998	0.396000000000000019	N0	BIO-TECH	0.753800000000000026
1889	2018-08-13	46234929	53191036212	D3-50 50,000 UNIT CHOLECALC	3	0	0	4.25	4.25	2.25999999999999979	1.98999999999999999	0.468235290000000026	N0	BIO-TECH	0.753800000000000026
1890	2018-08-23	46252447	310009530	DALIRESP 500 MCG TABLET	15	182.830000000000013	0	0	182.830000000000013	146.509999999999991	36.3200000000000003	0.198654490000000017	N0	ASTRAZENECA	9.76699999999999946
1891	2018-08-27	46257767	310009530	DALIRESP 500 MCG TABLET	3	39.7700000000000031	0	0	39.7700000000000031	29.3000000000000007	10.4700000000000006	0.263263770000000008	N0	ASTRAZENECA	9.76699999999999946
1892	2018-08-03	46240461	591437530	DARIFENACIN ER 7.5 MG TABLE	30	126.489999999999995	0	3.70000000000000018	130.189999999999998	96.8700000000000045	33.3200000000000003	0.255933640000000018	N0	ACTAVIS PHARMA,	3.22889999999999988
1893	2018-08-22	46257206	904386575	DEEP SEA 0.65% NOSE SPRAY	45	2.93999999999999995	0	0	2.93999999999999995	0.680000000000000049	2.25999999999999979	0.76870748000000011	N0	MAJOR PHARMACEU	0.0149999999999999994
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 36, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, false);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 9, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 22, true);


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Name: main_reference_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('main_reference_id_seq', 3513, true);


--
-- Name: main_refund_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('main_refund_id_seq', 1893, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: main_reference main_reference_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY main_reference
    ADD CONSTRAINT main_reference_pkey PRIMARY KEY (id);


--
-- Name: main_refund main_refund_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY main_refund
    ADD CONSTRAINT main_refund_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_site_domain_a2e37b91_like ON django_site USING btree (domain varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

