from main.views import *
from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from rxrefund.views import *
from django.http import HttpResponseRedirect
from django.views.defaults import page_not_found
from django.conf.urls import handler404 , handler500


# Genel URL
app_name = "main"

urlpatterns = [
    url(r'^$', rxrefund_mainpage, name="mainpage"),
    url(r'^dashboard/$', rxrefund_dashboard, name="dashboard"),
    url(r'^results/$', click_dashboard, name="results"),
    url(r'^uploadlogs/$', rxrefund_uploadlogs, name="uploadlogs"),
    url(r'^uploadreferences/$', rxrefund_uploadreferences, name="uploadreferences"),
    url(r'^showexceptions/$', rxrefund_showexceptions, name="showexceptions"),
    url(r'^savingstracking/$', rxrefund_savingstracking, name="savingstracking"),
    url(r'^potentialtracking/$', rxrefund_potentialstracking, name="potentialstracking"),


]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
