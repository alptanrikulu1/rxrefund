from django import template

register = template.Library()

@register.simple_tag
def update_variable(value):
    """Allows to update existing variable in template"""
    return value

@register.filter(name='subtract')
def subtract(value, arg):
    sub = value - arg
    sun = round(sub, 2)
    return sun