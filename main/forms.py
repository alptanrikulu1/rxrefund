from django import forms
from main.models import Potential,Refund


class rx_query_form(forms.ModelForm):
    dateFilled = forms.DateField(widget=forms.SelectDateWidget(years=range(2019,2018,1)))
    class Meta:
        model = Refund
        fields = [
            'dateFilled',


        ]


class Potentialc(forms.ModelForm):
    class Meta:
        model = Potential
        fields = ['dateFilled', 'generic_drug_code','Acq_unit_cost','Acquistion_cost']
