from django.db import models

# Create your models here.


class Refund(models.Model):
    dateFilled = models.DateField(blank=True, null=True, verbose_name="DOB")
    Rxnumber = models.CharField(max_length=50)
    NDC = models.BigIntegerField(default=0)
    dispended_drug_description = models.TextField(verbose_name="dispended drug description", null=True)
    Qty_dispended = models.FloatField(default=0)
    Primary_amt = models.FloatField(default=0)
    Second_amt = models.FloatField(default=0)
    Patient_copay = models.FloatField(default=0)
    Total_sales = models.FloatField(default=0)
    Acquistion_cost = models.FloatField(default=0)
    Profit = models.FloatField(default=0)
    Margin = models.FloatField(default=0)
    Dawcode = models.CharField(default=0, max_length=10)
    Drug_man = models.CharField(max_length=50)
    Acq_unit_cost = models.FloatField(default=0)
    Al_unit_cost = models.FloatField(default=0, null=True)


class Reference(models.Model):
    generic_drug_code = models.IntegerField(default=0)
    item_iden = models.BigIntegerField(default=0)
    item_description = models.TextField(verbose_name="item description", null=True)
    item_status =  models.CharField(max_length=10)
    item_generic = models.CharField(max_length=10)
    item_scheduled = models.CharField(blank=True, null=True, verbose_name="DOB",max_length=50)
    items_package_size = models.IntegerField(default=0)


class Potential(models.Model):
    dateFilled = models.DateField(blank=True, null=True, verbose_name="DOB")
    generic_drug_code = models.IntegerField(default=0)
    Acq_unit_cost = models.FloatField(default=0)
    Acquistion_cost = models.FloatField(default=0)
    margin = models.FloatField(default=0)