from django.http import HttpResponse
from django.shortcuts import get_object_or_404, get_list_or_404
from django.shortcuts import render
from django.db.models import Sum,F,FloatField
from django.db.models.functions import Cast
from .models import *
from main.forms import *
from itertools import chain

import datetime


def click_dashboard(request):

    info = Refund.objects.filter(NDC="59148001113")
    print (info)
    description = get_list_or_404(Refund, NDC="59148001113")
    profit_sum = Refund.objects.filter(NDC="59148001113").aggregate(Sum('Profit'))
    total_sales = Refund.objects.filter(NDC="59148001113").aggregate(Sum('Total_sales'))
    total_cost = Refund.objects.filter(NDC="59148001113").aggregate(Sum('Acquistion_cost'))

    return render(request, 'results.html', {"profit_sum": profit_sum,
                                            "total_sales": total_sales,
                                            "total_cost": total_cost,
                                            "info": info,
                                            "description": description
                                            })

def rxrefund_dashboard(request):

    now = datetime.datetime.now()
    now_month = now.month
    now_year = now.year

    month1 = now_month
    year1 = now_year

    month2 = now_month - 1
    year2 = now_year
    if month2 < 1:
        month2 += 12
        year2 -=1

    month3 = now_month -3
    year3 = now_year
    if month3 < 1:
        month3 += 12
        year3 -=1

    month4 = now_month - 4
    year4 = now_year
    if month4 < 1:
        month4 += 12
        year4 -=1

    month5 = now_month - 5
    year5 = now_year
    if month5 < 1:
        month5 += 12
        year5 -=1

    month6 = now_month - 6
    year6 = now_year
    if month6 < 1:
        month6 += 12
        year6 -=1




    infoDemo = Refund.objects.values('Acquistion_cost', 'Profit', 'Total_sales').filter(dateFilled__year=2018,
                                                                                    dateFilled__month=8).aggregate(
        total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'))

    info1 = Refund.objects.values('Acquistion_cost', 'Profit', 'Total_sales').filter(dateFilled__year=year1,
                                                                                    dateFilled__month=month1).aggregate(
        total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'))

    info2 = Refund.objects.values('Acquistion_cost', 'Profit', 'Total_sales').filter(dateFilled__year=year2,
                                                                                    dateFilled__month=month2).aggregate(
        total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'))

    info3 = Refund.objects.values('Acquistion_cost', 'Profit', 'Total_sales').filter(dateFilled__year=year3,
                                                                                    dateFilled__month=month3).aggregate(
        total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'))

    info4 = Refund.objects.values('Acquistion_cost', 'Profit', 'Total_sales').filter(dateFilled__year=year4,
                                                                                    dateFilled__month=month4).aggregate(
        total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'))

    info5 = Refund.objects.values('Acquistion_cost', 'Profit', 'Total_sales').filter(dateFilled__year=year5,
                                                                                    dateFilled__month=month5).aggregate(
        total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'))

    info6 = Refund.objects.values('Acquistion_cost', 'Profit', 'Total_sales').filter(dateFilled__year=year6,
                                                                                    dateFilled__month=month6).aggregate(
        total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'))

    context = {
        "infoDemo": infoDemo,
        "info1": info1,
        "info2": info2,
        "info3": info3,
        "info4": info4,
        "info5": info5,
        "info6": info6,

        "now_month": now_month,
        "now_year": now_year,

        "month1" : month1,
        "month2" : month2,
        "month3" : month3,
        "month4" : month4,
        "month5" : month5,
        "month6" : month6,

        "year1" : year1,
        "year2" : year2,
        "year3" : year3,
        "year4" : year4,
        "year5" : year5,
        "year6" : year6
    }

    return render(request, 'dashboard.html', context)


def rxrefund_mainpage(request):

    if request.POST.get('submit') == 'save':
        y = request.POST.get('year')
        m = request.POST.get('month')
        a = request.POST.get('option')

        a = int(a)

        # print (a,type(a))
        # d = request.POST.get('day')
        # dob = str(y)+"-"+str(m)+"-"+str(d)
        info = Refund.objects.none()

        if (a == 1):  ####### for max revenue
            # print("hop")
            info = Refund.objects.values('dateFilled','NDC', 'dispended_drug_description', 'Qty_dispended', 'Drug_man', 'Profit',
                                             'Margin','Total_sales').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended')).order_by('-Margin').order_by('Qty_dispended')[:25]
        elif (a == 2):  ####### for max revenue
            # print("hop2")
            info = Refund.objects.values('dateFilled','NDC', 'dispended_drug_description', 'Qty_dispended', 'Drug_man', 'Profit',
                                             'Margin','Total_sales').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended')).order_by('-Margin').order_by('-Qty_dispended')[:25]

        elif (a == 3):  ####### for max revenue
            # print("hop3")
            info = Refund.objects.values('dateFilled','NDC', 'dispended_drug_description', 'Qty_dispended', 'Drug_man', 'Profit',
                                             'Margin','Total_sales').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended')).order_by('-Margin').order_by('profit_sum')[:25]
        elif (a == 4):  ####### for max revenue
            # print("hop4")
            info = Refund.objects.values('dateFilled','NDC', 'dispended_drug_description', 'Qty_dispended', 'Drug_man', 'Profit',
                                             'Margin','Total_sales').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended')).order_by('-Margin').order_by('-profit_sum')[:25]

        des = Reference.objects.values('generic_drug_code', 'item_iden').annotate(score=Sum('item_iden'))
        des2 = Reference.objects.all()


        context = {
            "total_sales": info,
            "des": des,
            "des2": des2,
            "des":des,
            "des2":des2,
            "a":a,
        }

    else:
        info = Refund.objects.none()
        context = {
            "total_sales": info,

        }
    if request.POST.get('sub') == 'OK':

        u = request.POST.get('unitcost')

        print(request.POST.get('unitcost'), request.POST.get('alcost'), request.POST.get('date'), request.POST.get('gdc'))
        a = request.POST.get('alcost')
        d = request.POST.get('date')

        g = request.POST.get('gdc')
        ne = u.replace("$", "")
        na = a.replace("$", "")
        m=float(ne)-float(na)
        if request.POST.get('unitcost') or request.POST.get('alcost') or request.POST.get('date') or request.POST.get('gdc') :
            data = Potential(dateFilled=d,generic_drug_code=g,Acq_unit_cost=ne,Acquistion_cost=na,margin=m)
            data.save()

    return render(request, 'mainpage.html', context)

def rxrefund_potentialstracking(request):
    if request.POST.get('submit') == 'save':
        y = request.POST.get('year')
        m = request.POST.get('month')
        info = Potential.objects.values('dateFilled', 'generic_drug_code','Acq_unit_cost','Acquistion_cost','margin').filter(dateFilled__year=y, dateFilled__month=m)[:25]

        context = {
            "total_sales": info,
        }
    else:
        info = Potential.objects.none()
        context = {
            "total_sales": info,

        }

    return render(request, 'potentialtracking.html',context)

def rxrefund_uploadlogs(request):
    return render(request, 'uploadlogs.html')


def rxrefund_uploadreferences(request):
    return render(request, 'uploadreferences.html')


def rxrefund_showexceptions(request):
    return render(request, 'showexceptions.html')


def rxrefund_savingstracking(request):

    if request.POST.get('submit') == 'save':
        y = request.POST.get('year')
        m = request.POST.get('month')
        a = request.POST.get('option')

        a = int(a)
        m2 = int(m)
        y2 = int(y)

        #info = Refund.objects.none()
        #des2 = Refund.objects.none()

        if (a == 1):
            info = Refund.objects.values('NDC', 'dispended_drug_description', 'Qty_dispended', 'Drug_man', 'Profit',
                                             'Margin','Total_sales').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended')).order_by('-Margin').order_by('Qty_dispended')[:25]

            if(m2 == 1):
                y2 = y2-1
                m2 = 12
                m = str(m2)
                y = str(y2)
                des2 = Refund.objects.values('NDC', 'Profit','Total_sales','Acquistion_cost').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended'))
            else:
                m2 = m2 - 1
                m = str(m2)
                y = str(y2)
                des2 = Refund.objects.values('NDC', 'Profit', 'Total_sales', 'Acquistion_cost').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'), unit_cost=F('total_cost') / F('Qty_dispended'))
                # .order_by().values('NDC').distinct()
                # print(len(des2.order_by().values('NDC').distinct()))



        elif (a == 2):
            info = Refund.objects.values('NDC', 'dispended_drug_description', 'Qty_dispended', 'Drug_man', 'Profit',
                                             'Margin','Total_sales').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended')).order_by('-Margin').order_by('-Qty_dispended')[:25]
            if(m2 == 1):
                y2 = y2-1
                m2 = 12
                m = str(m2)
                y = str(y2)
                des2 = Refund.objects.values('NDC', 'Profit','Total_sales','Acquistion_cost').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended'))
            else:
                m2 = m2 - 1
                m = str(m2)
                y = str(y2)
                des2 = Refund.objects.values('NDC', 'Profit','Total_sales','Acquistion_cost').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended'))

        elif (a == 3):
            info = Refund.objects.values('NDC', 'dispended_drug_description', 'Qty_dispended', 'Drug_man', 'Profit',
                                             'Margin','Total_sales').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended')).order_by('-Margin').order_by('profit_sum')[:25]
            if(m2 == 1):
                y2 = y2-1
                m2 = 12
                m = str(m2)
                y = str(y2)
                des2 = Refund.objects.values('NDC', 'Profit','Total_sales','Acquistion_cost').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended'))
            else:
                m2 = m2 - 1
                m = str(m2)
                y = str(y2)
                des2 = Refund.objects.values('NDC', 'Profit','Total_sales','Acquistion_cost').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended'))
        elif (a == 4):
            info = Refund.objects.values('NDC', 'dispended_drug_description', 'Qty_dispended', 'Drug_man', 'Profit',
                                             'Margin','Total_sales').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    total_sales=Sum('Total_sales'), profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended')).order_by('-Margin').order_by('-profit_sum')[:25]
            if(m2 == 1):
                y2 = y2-1
                m2 = 12
                m = str(m2)
                y = str(y2)
                des2 = Refund.objects.values('NDC', 'Profit','Total_sales','Acquistion_cost').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended'))
            else:
                m2 = m2 - 1
                m = str(m2)
                y = str(y2)
                des2 = Refund.objects.values('NDC', 'Profit','Total_sales','Acquistion_cost').filter(dateFilled__year=y, dateFilled__month=m).annotate(
                    profit_sum=Sum('Profit'), total_cost=Sum('Acquistion_cost'),
                    unit_cost=F('total_cost') / F('Qty_dispended'))

        des = Reference.objects.values('generic_drug_code', 'item_iden').annotate(score=Sum('item_iden'))
        des3 = Reference.objects.all()
        context = {
            "total_sales": info,
            "des": des,
            "des2": des2,
            "des3": des3
        }

    else:
        info = Refund.objects.none()
        context = {
            "total_sales": info,
        }

    return render(request, 'savingstracking.html', context)



