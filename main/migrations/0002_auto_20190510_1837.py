# Generated by Django 2.1.5 on 2019-05-10 15:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Potential',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('generic_drug_code', models.IntegerField(default=0)),
                ('Acq_unit_cost', models.FloatField(default=0)),
                ('Acquistion_cost', models.FloatField(default=0)),
                ('margin', models.FloatField(default=0)),
            ],
        ),
        migrations.AddField(
            model_name='refund',
            name='Al_unit_cost',
            field=models.FloatField(default=0, null=True),
        ),
    ]
