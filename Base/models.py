from django.db import models
import sys,time,os

# Create your models here.
class BaseModel (models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Oluşturulma Tarihi")
    modified_at = models.DateTimeField(auto_now=True, verbose_name="Son Güncellenme tarihi")
    is_deleted = models.BooleanField(verbose_name="Silindi", default=False)

    class Meta:
        abstract = True