from django.apps import AppConfig


class RestoredataConfig(AppConfig):
    name = 'RestoreData'
