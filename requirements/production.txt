Django==2.0.7
mysqlclient
django-simple-history
gevent==1.2.2
django-filter
django-widget-tweaks
django-material==1.1.0
gunicorn
Pillow
django-dynamic-formsets
boto3
django-storages
django-formset-js-improved
iyzipay
django-ipware
django-registration-redux

django_compressor

python-dateutil
sorl-thumbnail
