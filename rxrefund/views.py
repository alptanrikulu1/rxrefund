from django.conf.urls import url,include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static
from rxrefund.views import *
from django.http import HttpResponseRedirect
from django.views.defaults import page_not_found
from django.conf.urls import handler404 , handler500
# from registration.backends.default.urls import urlpatterns as other_app_urls
from RestoreData.urls import *

urlpatterns = [
    url(r'^management/administration', admin.site.urls),
    url(r'^x/', include('RestoreData.urls')),
    url(r'^',include('main.urls')),

]